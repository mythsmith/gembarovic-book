Project:
Thermal Diffusivity Measurements of Anisotropical Materials Using the Flash Method

Phase 2 description:
Application of the software to existing (already available) experimental data and comparing calculated thermal diffusivity values with existing results.

Duration: 2 months

Cost estimate: $7,000.00

Comments:
In Phase 1 of the project we simulated temperature responses in the flash method experiment and identified sources and amplitudes of possible errors due to anisotropy of tested material. 

It was shown that using data reduction techniques based on an assumption that the specimen material is isotropic, can (in certain situations) lead to relative errors which are of order of magnitude larger than usually claimed uncertainty (+/-3%) of the flash method.

The most important parameters affecting the error in thermal diffusivity determination in the flash method are specimen dimensions, the degree of material anisotropy, and the heat transfer coefficient for specimen lateral surfaces. 

It was also noted that cross-sectional spatial non-uformity of laser beam (the heat source) also leads to an additional error if only one dimensional models are used. All comercially available instruments have a certain build-in degree of non-uniformity in their laser beams and in the specimen holder design. Tested specimen surface is therefore never uniformly heated, and this problem is sometimes compounded with nonuformity of the specimen surface absorption ability.   

Preliminary anysis of data obtained last year on carbon-carbon composite sample at NASA Glenn RC showed that the thermal diffusivity results calculated using more sofisticated curve fitting procedure (called Levenberg-Marquardt nonlinear fitting algorithm) were from 5% to 30% lower than the results obtained by the standard Clark and Taylor procedure, which were originally reported. 

Using more realistic two-dimensional analytical models which take into account anisotropy of the material as well as other parameters (e.i. non-uniform heating, different heat losses from sample surfaces) can lead to more precise and reproducible thermal properies results. It can also improve experimental design, which very important for increasing overall sensitivity and robustness of the laser flash experiment. 

The laser flash method is still the only choice for reliable testing materials in extreme temperatures and conditions. Future progress of the method depends on increasing quality and reliability of data analysis. Existing software has to be upgraded to more realistic models and more advanced curve fitting procedures. 
    

 

