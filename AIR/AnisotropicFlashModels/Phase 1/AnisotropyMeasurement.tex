\documentclass[10pt]{article}
\usepackage{graphicx}% Include figure files
\usepackage{epstopdf}% for eps format figures
%%
% multiline equations
\usepackage{amsmath}
% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb, amsfonts}
\usepackage{relsize}
\usepackage{mathrsfs}
\usepackage{wasysym}
\usepackage{authblk}
\usepackage{booktabs}
\usepackage{refcount}

\newcommand*{\rttensortwo}[1]{\bar{\bar{#1}}}
\begin{document}

\begin{titlepage}
   \vspace*{\stretch{1.0}}
   \begin{center}
      \Large\textbf{Thermal Diffusivity Measurements of Anisotropic Materials Using the Flash Method}\\     
      \vspace*{\stretch{0.2}} 
      \Large\textbf{Experiment}\\
      \vspace*{\stretch{0.2}} 
      \Large\textbf{A Report to NASA Langley Research Center}\\
         \vspace*{\stretch{0.5}}
      \large\textit{Jozef Gembarovic}\\
      
      
   
   	  \vspace*{\stretch{2.0}}
   	  \large {\today}\\
   	  \vspace*{\stretch{0.1}}
      \large{Advanced Insulation Research LLC\\
      130 Tweedsmere Drive\\
      Townsend, DE 19734}
    \end{center}
\end{titlepage}

\tableofcontents
\clearpage
\label{listoffigures}
\listoffigures
\newpage

\section{Introduction}

In the flash method, the thermal diffusivity is tested in the specimen's "through-thickness" (axial) direction. Anisotropic materials, like carbon-carbon composites, are usually tested the way that two separate specimens are machined out of the material; one for material's axial and the other one for "in-plane" (radial) direction. The "in-plane" sample consists of side-by-side stacked strips of the material, so the axial and radial diffusivity components are reversed. These two specimens are then tested separately and experimental response curves are analyzed using traditional techniques based on isotropic models. 

In what follows we are proposing a method which will use a more realistic anisotropic model, described in detail in our previous report for Phase 1, and where only one specimen (instead of two) will be tested twice. First with a laser beam uniformly irradiating the specimen front surface, and then with a shaded central region of the specimen, creating thus a nonuniform ('doughnut' shape) heat source.

Response curves from the uniformly irradiated specimen will be used to calculate the the axial component of thermal diffusivity, $a_z$, using any of traditional techniques based on one or two dimensional isotropic models. 

Experimental curves from the doughnut shape heat source will be used to calculate the degree of the diffusivity anisotropy ratio $\zeta= a_r/a_z$, using our two dimensional anisotropic model. The radial diffusivity component $a_r$ will then be calculated as $a_r=\zeta a_z$.

The specimen will machined easily from the material as the one for the through-thickness direction in today used methodology for testing the anisotropic materials. 

\section{Testing Anisotropic Specimens}

Thermal diffusivity of anisotropic specimens, where the axial and radial thermal diffusivity components are aligned with the axial and radial axis of a cylindrical specimen, can be tested in two steps:

\begin{description}
	\item[1.] \textit{Test a thin specimen with a uniform heat source}. 
	
	It is clear from Figures \ref{fig:item1} and \ref{fig:item1_3D} that responses for a uniformly heated specimen are the same regardless of the thermal diffusivity anisotropy, especially for small heat loss anisotropy ratios, $h_r/h_z \geq 20$. Time detail view of the response curves from Figure \ref{fig:item1} is in Figure \ref{fig:item1_detail}. Responses for a different degree of $a_r/a_z \geq 1$  and  $h_r/h_z<20$ are practically indistinguishable.
	
	Axial thermal diffusivity $a_z$ can be estimated from earlier parts of the experimentally obtained response curve using any of simple techniques based on one dimensional heat flow (e.g. Clark and Taylor formula, described in our previous report), or by fitting the curve with the two dimensional anisotropic formula, where $a_r = a_z$, $h_r/h_z=1$, and actual experimental heat source and observation area radii (e.g. $R/L=6$, $r_0=R_0=0$, $r_1=0.99$, $R_1=0.2$).  In case of a high heat loss ratio, the uncertainty of $a_z$ evaluation for anisotropic specimen is slightly higher, but still within a reasonable limit.

	\item[2.] \textit{Test the same specimen with a non-uniform, "doughnut" shape heat source.} 
	Proposed "doughnut" shape heat source can be relatively easily realized by shading the central area of the specimen with a round piece of sheet metal, not touching the specimen surface.
	
	 As we can see from Figures \ref{fig:item2} and \ref{fig:item2_3D}, the dimensionless halftime $time_{1/2}$, calculated as ratio $a_zt_{1/2}/L^2$, is for isotropic material with heat loss ratio 1 equal to 1.8. For anisotropic case, when ratio $a_r/a_z = 1.5$, the dimensional halftime is only $1.2$. The halftime is decreasing with increasing thermal diffusivity anisotropy and can be used to estimate the degree of thermal diffusivity anisotropy. 
	
	More precisely, the radial thermal diffusivity $a_r$ can be estimated by fitting the response curve using analytical formula for a anisotropic specimen, with already known $a_z$ and changed heat source and observed area parameters. 
\end{description}

\section{Conclusion}

Temperature response curves, generated for a cylindrical specimen using two different heat source shapes can be used to measure both axial and radial thermal diffusivity components. 

Analysis will be based on a more realistic model, which will take into account
anisotropic nature of the specimen material.

Proposed methodology will shorten time necessary for specimen preparation, because only one specimen will be used in the experiment. 
\newpage
\begin{figure}[h]
 \centering
  \includegraphics[width=\linewidth]{\detokenize{../../../myFlashMethodTheory/Literature/Anisotropy/item1.eps}}%
  \caption{Normalized response curves for a different degree of anisotropy of both the heat transfer coefficient and the thermal diffusivity. Specimen was heated by a uniform heat source.}
  \label{fig:item1}%
\end{figure}
%generated using shortdoughnut1.R (5/27/2017)
\begin{figure}[h]
  \centering
  \includegraphics[width=\linewidth]{\detokenize{../../../myFlashMethodTheory/Literature/Anisotropy/item1_3D.png}}%
  \caption{3D View of normalized response curves from Figure \ref{fig:item1}.}
  \label{fig:item1_3D}%
\end{figure}
%generated using shortdoughnut1.R (5/27/2017)
\begin{figure}
  \centering
  \includegraphics[scale=0.7]{\detokenize{../../../myFlashMethodTheory/Literature/Anisotropy/hrha1graphR099.png}}%
  \caption{Time detail view of superimposed response curves from Figure \ref{fig:item1}.}
  \label{fig:item1_detail}%
\end{figure}
%generated using s_dough_smalltimes2.R (5/29/2017)

\begin{figure}[h]
 \centering
  \includegraphics[width=\linewidth]{\detokenize{../../../myFlashMethodTheory/Literature/Anisotropy/item2.eps}}%
  \caption{Normalized response curves for a different degree of anisotropy of both the heat transfer coefficient and the thermal diffusivity. Specimen was heated by a "doughnut" shape heat source.}
  \label{fig:item2}%
\end{figure}
%generated using shortdoughnut2.R (5/27/2017)
\begin{figure}[]
  \centering
  \includegraphics[width=\linewidth]{\detokenize{../../../myFlashMethodTheory/Literature/Anisotropy/item2_3D.png}}%
  \caption{3D View of normalized response curves from Figure \ref{fig:item2} .}
  \label{fig:item2_3D}%
\end{figure}
%generated using shortdoughnut2.R (5/27/2017)


%\newpage
%\bibliographystyle{ieeetr}
%%\bibliographystyle{}
%\bibliography{\detokenize{../../../myFlashMethodTheory/mybook}}

\end{document}