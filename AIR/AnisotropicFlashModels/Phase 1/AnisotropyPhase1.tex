\documentclass[10pt]{article}
\usepackage{graphicx}% Include figure files
\usepackage{epstopdf}% for eps format figures
%%
% multiline equations
\usepackage{amsmath}
% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb, amsfonts}
\usepackage{relsize}
\usepackage{mathrsfs}
\usepackage{wasysym}
\usepackage{authblk}
\usepackage{booktabs}

\newcommand*{\rttensortwo}[1]{\bar{\bar{#1}}}
\begin{document}

\begin{titlepage}
   \vspace*{\stretch{1.0}}
   \begin{center}
      \Large\textbf{Thermal Diffusivity Measurements of Anisotropic Materials Using the Flash Method}\\     
      \vspace*{\stretch{0.2}} 
      \Large\textbf{Phase 1}\\
      \vspace*{\stretch{0.2}} 
      \Large\textbf{A Report to NASA Langley Research Center}\\
         \vspace*{\stretch{0.5}}
      \large\textit{Jozef Gembarovic}\\
      
      
   
   	  \vspace*{\stretch{2.0}}
   	  \large {April 2017}\\
   	  \vspace*{\stretch{0.1}}
      \large{Advanced Insulation Research LLC\\
      130 Tweedsmere Drive\\
      Townsend, DE 19734}
    \end{center}
\end{titlepage}

\tableofcontents
\newpage

\section{Introduction}

Thermal diffusivity in a composite specimen with anisotropical thermal properties is a 2nd rank tensor, with components aligned with the tensor of the thermal conductivity of the specimen material. The flash method \cite{parker1961flash} was originally proposed for the thermal diffusivity measurement of homogeneous and isotropic materials. 

For an isotropic material, the vector of heat flux $\vec{q}$ has the same but opposite direction as the temperature gradient vector $\nabla T$ and their relationship is described in the Fourier's Law

\begin{equation}
   \vec{q}=-k\nabla T,
   \end{equation}   
where $k$ is the thermal conductivity, which can be expressed as a single number (scalar) with proper dimensions. 

For anisotropic materials, (e.g. carbon-carbon composites, layered structures, textiles, graphenes, etc.), heat is not flowing equally in all directions. Heat flows more easily in one or more preferred directions than the other. In this case, the heat flux vector and the temperature gradient are not necessarily parallel and the relationship between these two vectors is more complicated. In general, every one component of the heat flux vector depends on all three components of the temperature gradient vector. In Cartesian coordinates it is expressed as

\begin{equation}
\begin{split}
q_x=-k_{11}\nabla_x T - k_{12}\nabla_y T - k_{13}\nabla_z T\\
q_y=-k_{21}\nabla_x T - k_{22}\nabla_y T - k_{23}\nabla_z T\\
q_z=-k_{31}\nabla_x T - k_{32}\nabla_y T - k_{33}\nabla_z T,
\end{split}
\end{equation}

This set of equations can be written down in a matrix notation as
\begin{equation}
\left( 
 	\begin {array}{c}
  		q_x\\q_y\\q_z
	 \end{array} 
 \right)
=-\left( 
 	\begin {array}{ccc}
  		k_{11} & k_{12} & k_{13} \\
  		k_{21} & k_{22} & k_{23} \\
  		k_{31} & k_{32} & k_{33}
	 \end{array} 
 \right)
 \left( 
 	\begin {array}{c}
  		\nabla_x\\ \nabla_y\\ \nabla_z
	 \end{array} 
 \right).
\end{equation}
In a short notation
\begin{equation}
\vec{q}=-\rttensortwo{k}\nabla T,
\end{equation}
where $\vec{q}$ is the heat flux vector 
\begin{equation}
\vec{q} = \left( 
 	\begin {array}{c}
  		q_x\\q_y\\q_z
	 \end{array} 
 \right),
 \end{equation} 
$\rttensortwo{k}$ is $3 \times 3$ matrix (also called the thermal conductivity tensor of the second-rank)
\begin{equation}
 \rttensortwo{k}=
 \left( 
 	\begin {array}{ccc}
  		k_{11} & k_{12} & k_{13} \\
  		k_{21} & k_{22} & k_{23} \\
  		k_{31} & k_{32} & k_{33}
	 \end{array} 
 \right),
 \end{equation} 
and  $\nabla T$ is the temperature gradient vector
\begin{equation}
\nabla T = \left(
\begin {array}{c}
\frac{\partial T}{\partial x}\\ \frac{\partial T}{\partial y}\\\frac{\partial T}{\partial z}
\end{array}
 \right).
\end{equation} 

The thermal conductivity tensor is symmetric, i. e. $k_{ij}=k_{ji}$ (Onsager's principle) and if the main coordinate system axis are aligned with its principal (diagonal) components, then it can be simply written as
\begin{equation}
 \rttensortwo{k}=
 \left( 
 	\begin {array}{ccc}
  		k_x & 0 & 0 \\
  		0 & k_y & 0 \\
  		0 & 0 & k_z
	 \end{array} 
 \right).
 \end{equation} 
    
The thermal diffusivity in an anisotropic specimen $\rttensortwo{a}$ is also a second-rank tensor, with components aligned with the tensor of the thermal conductivity

\begin{equation}
 \rttensortwo{a}=\frac{1}{\rho c_p}
 \left( 
 	\begin {array}{ccc}
  		k_x & 0 & 0 \\
  		0 & k_y & 0 \\
  		0 & 0 & k_z
	 \end{array}
 \right)=
 \left( 
 	\begin {array}{ccc}
  		a_x & 0 & 0 \\
  		0 & a_y & 0 \\
  		0 & 0 & a_z
	 \end{array}
 \right).
 \end{equation}    

An extension of the flash diffusivity method to measure simultaneously both the radial and the axial thermal diffusivity components was proposed in papers \cite{donaldson1975thermal}\cite{chu1980thermal}. In this technique, (called the radial flash method), the energy source of radius $R$ irradiating the front surface of the sample, is considerably smaller than the sample's surface. The resulting temperature rises on the rear surface are measured at two locations (1) directly opposite the center of the energy pulse and (2) at a distance $r<R$. The method was tested with both an isotropic material (POCO AXM-5Q Graphite) and an anisotropic material (reactor grade graphite) at high temperatures. The results showed that thermal diffusivity in the radial direction could be measured without accounting for the very large radiation heat losses. 

The radial flash method was never implemented in mass produced flash instruments, because they are using only one non-contact optical sensor for temperature response measurements of the rear side of a specimen. Attempts to use an infrared camera to create transient temperature maps of whole rear surface of a flash specimen, were also reported (see e.g. \cite{graham1999multidimensional}), but due to its inherent mathematical and experimental complexity, they are used only for non-destructive testing, and not for the thermal diffusivity determination.

\newpage 
\section {Analytical Models}
\subsection{Anisotropic Cylinder}
Heat conduction equation for a cylindrical specimen with an axial symmetry and the thermal conductivity in the radial direction $k_r$ different from the thermal conductivity in the axial (through-thickness) direction $k_z$, becomes
\begin{equation}
\label{eq:CylindricalHCEQAnis}
k_r\bigg(\frac{\partial^2 T}{\partial r^2}+\frac{1}{r}\frac{\partial T}{\partial r}\bigg)+ k_z\frac{\partial^2 T}{\partial z^2} = c_p\rho\frac{\partial T}{\partial t},
\end{equation}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.4]{anisCylinder.eps}
  \caption{Cylindrical Specimen.}%
  \label{fig:CylinderSampleAnis}%
\end{figure}

This equation is solved for boundary conditions of the third kind
\begin{equation}
\label{eq:CylHCEboundaryAnis}
\begin{split}
\frac{\partial T(z,r,t)}{\partial z} + H_0 T =0,\quad \text{for} \quad z=0,\\
\frac{\partial T(z,r,t)}{\partial z} - H_L T =0,\quad  \text{for}\quad z=L,\\
\frac{\partial T}{\partial r} + H_r T =0,\quad  \text{for} \quad r=R,\\
T(z,r,t) < \infty \quad \text{for} \quad r=0,
\end{split}
\end{equation}
where 
\begin{equation}
 \quad H_0=\frac{h_0 L}{k_z}, \quad H_L=\frac{h_L L}{k_z},H_r=\frac{h_rR}{k_r},
\end{equation}
and for an instantaneous heat source distribution at $t=0$:
\begin{equation}
\label{eq:2DHCinitAnis}
g(z, r)=\delta(z)[\text{U}(r)-\text{U}(r-R)],
\end{equation}  
where $\text{U}(r)$ is the Heaviside unit step function, defined as
\begin{equation}
\label{eq:Heaviside Step}
 \text{U}(r) = \begin{cases}  0 &\mbox{if } 0<r \\ 
1 & \mbox{if }  r \geq 0. \end{cases}
\end{equation}

The solution of equation (\ref{eq:CylindricalHCEQAnis}), with conditions (\ref{eq:CylHCEboundaryAnis}) and (\ref{eq:2DHCinitAnis}), can be expressed as a product of radial and axial components
\begin{equation}
T(r,z,t)=\Phi(r,t)\Psi(z,t).
\end{equation}

For the axial component $\Psi(z, t)$, for $(t > 0)$, and $(0 < z < L)$
\begin{equation}
\label{eq:CylindricalHCE A Anis}
 \frac{\partial^2 \Psi}{\partial z^2} = \frac{1}{a_z}\frac{\partial \Psi}{\partial t},
\end{equation}
with boundary conditions
\begin{equation}
\label{eq:CylAboundary1Anis}
-\frac{\partial \Psi}{\partial z} + H_0 \Psi = 0,\quad \text{at} \quad z=0,
\end{equation}
\begin{equation}
\label{eq:CylAboundary2Anis}
\frac{\partial \Psi}{\partial z} + H_L \Psi = 0,\quad \text{at} \quad z=L, 
\end{equation}
and the initial condition
\begin{equation}
\label{eq:CylAinitialAnis}
\Psi=\delta(z), \quad \text{at} \quad  t=0. 
\end{equation}

For the radial component $\Phi(r, t)$, for $(t > 0)$, and $(0 < r < R)$
\begin{equation}
\label{eq:CylindricalHCE R Anis}
\frac{\partial^2 \Phi}{\partial r^2}+\frac{1}{r}\frac{\partial \Phi}{\partial r}= \frac{1}{a_r}\frac{\partial \Phi}{\partial t},
\end{equation}
with boundary conditions
\begin{equation}
\label{eq:CylRboundary0Anis}
\Phi < \infty,\quad \text{at} \quad r=0.
\end{equation}
\begin{equation}
\label{eq:CylRboundaryAnis}
-\frac{\partial \Phi}{\partial r} + H_R \Phi = 0,\quad \text{at} \quad r=R,
\end{equation}
and the initial condition
\begin{equation}
\label{eq:CylAinitialAnis}
\Phi=\text{U}(r)-\text{U}(r-R), \quad \text{at} \quad  t=0. 
\end{equation}

Resulting temperature response for $t>0$ is   
\begin{equation}
\label{eq:AxialRadialComponentsAnis}
T(r,z,t) = \frac{Q}{\rho c_p L}\sum_{m=0}^\infty \sum_{n=0}^\infty \Phi_m\Psi_n\Gamma_{mn}(t), 
\end{equation}
where 
\begin{equation}
\label{eq:PhiAnis}
\Phi_m = \frac{ 2\beta_m^2(\beta_m^2+H_L^2)\big(\cos{\beta_mz/L}+\frac{H_0}{\beta_m}\sin{\beta_mz/L}\big)}{(\beta_m^2+H_0^2)(\beta_m^2+H_L^2)+(H_0+H_L)(\beta_m^2+H_0H_L)},
\end{equation}
\begin{equation}
\label{eq:PsiAnis}
\Psi_n=\frac{2\gamma_n J_1(\gamma_nr/R)}{(\gamma_n^2+H_r^2)J_0(\gamma_n) \pi R^2},
\end{equation}  
\begin{equation}
\label{eq:GammaAnis}
\Gamma_{mn}(t)=\exp{\bigg[-\bigg(a_z\frac{\beta_m^2}{L^2} + a_r\frac{\gamma_n^2}{R^2}\bigg)t\bigg]}
\end{equation}
and $\beta$ and $\gamma$ are positive roots of equations
\begin{equation}
\label{eq:AxialRadialTranscendentalEquations}
\begin{split}
(\beta^2-H_0H_L)\sin{\beta} - (H_0+H_L)\cos{\beta}=0,\\
\gamma J_1(\gamma) - H_rJ_0(\gamma)=0.
\end{split}
\end{equation}

If the specimen material is \textit{isotropic}, then the thermal conductivities are $k_z=k_r=k$, and also the thermal diffusivities are $a_z = a_r = a$. The solution given in formulas (\ref{eq:AxialRadialComponentsAnis}), (\ref{eq:PhiAnis}), and (\ref{eq:PsiAnis}) remain the same, but the Biot numbers are now defined as 

\begin{equation}
 \quad H_0=\frac{h_0 L}{k}, \quad H_L=\frac{h_L L}{k},H_r=\frac{h_rR}{k},
\end{equation} 
and function $\Gamma_{mn}(t)$ in Equation (\ref{eq:GammaAnis}) is changed to
\begin{equation}
\Gamma_{mn}(t)=\exp{\bigg[-\bigg(\frac{\beta_m^2}{L^2} + \frac{\gamma_n^2}{R^2}\bigg)at\bigg]}.
\end{equation}

\subsection {Anisotropic Rectangular Parallelepiped}

Heat conduction equation for a three dimensional rectangular parallelepiped specimen of an anisotropic material (see Figure \ref{fig:RectangularSample}), initially at temperature $ T_0(x,y,z)$, with boundary conditions of the third kind, is formulated as
\begin{equation}
\label{eq:RectangularHCEAnis}
\begin{split}
k_x\frac{\partial^2 T}{\partial x^2}+k_y\frac{\partial^2 T}{\partial y^2}+k_z\frac{\partial^2 T}{\partial z^2} = \rho c_p\frac{\partial T}{\partial t}, \\
x\in (0,L), y\in (0,b), z \in (0,c), t>0,
\end{split}
\end{equation}
\begin{figure}[h]
  \centering
  \includegraphics[scale=0.6]{parallelepiped2.eps}
  \caption{Rectangular Specimen.}%
  \label{fig:RectangularSample}%
\end{figure}

with boundary conditions
\begin{equation}
\label{eq:BoundConditionsRectHCEAnis}
\begin{split}
-\frac{\partial  T(0,y,z,t)}{\partial x} + H_1 T(0,y,z,t) = 0,\\
\frac{\partial  T(L,y,z,t)}{\partial x} + H_2 T(L,y,z,t) = 0,\\
-\frac{\partial  T(x,0,z,t)}{\partial y} + H_3 T(x,0,z,t) = 0,\\
\frac{\partial  T(x,b,z,t)}{\partial x} + H_4 T(x,b,z,t) = 0,\\
-\frac{\partial  T(x,y,0,t)}{\partial z} + H_5 T(x,y,0,t) = 0,\\
\frac{\partial  T(x,y,c,t)}{\partial z} + H_6 T(x,y,c,t) = 0,
\end{split}
\end{equation}
and the initial condition for an instantaneous uniform heat pulse at $x=0$ 
\begin{equation}
\label{eq:Rect_Initial_deltax}
T(x,y,z,0)= T_0(x,y,z)=\delta(x).
\end{equation}

Biot numbers $H_i$ are defined for all six sample surfaces as 
\begin{equation}
\begin{split}
 \quad H_1=\frac{h_1 L}{k_x}, \quad H_2=\frac{h_2 L}{k_x},\\
 \quad H_3=\frac{h_3 b}{k_y}, \quad H_4=\frac{h_4 b}{k_y},\\
 \quad H_5=\frac{h_5 c}{k_z}, \quad H_6=\frac{h_6 c}{k_z}.
 \end{split}
\end{equation} 
The solution of Equation (\ref{eq:RectangularHCEAnis}) with conditions (\ref{eq:BoundConditionsRectHCEAnis} -- \ref{eq:Rect_Initial_deltax}) is
 \begin{equation}
 \label{eq:parallelepiped_Temp_Rise_Anis}
  T(x,y,z,t)=
   \sum_{m=1}^\infty \sum_{n=1}^\infty\sum_{k=1}^\infty \Gamma_{nmk}(t)X_m( x) Y_n(y) Z_k(z),
 \end{equation}
 where
 \begin{equation}
 \Gamma_{nmk}(t) = \exp\bigg\{-\bigg[a_x\bigg(\frac{\beta_m}{L}\bigg)^2 + a_y\bigg(\frac{\nu_n}{b}\bigg)^2 + a_z\bigg(\frac{\omega_k}{c}\bigg)^2\bigg]t\bigg\}
 \end{equation}
\begin{equation}
\label{eq:parallelepipedX}
  X_m(x) = \frac{2\beta_m[\beta_m \cos(\beta_m x) +H_1\sin(\beta_m x)]}{(\beta_n^2+H_1^2)\bigg(L+\frac{H_2}{\beta_m^2+H_2^2}\bigg)+H_1},
\end{equation}
\begin{equation}
\label{eq:parallelepipedY}
  Y_n(y) = \frac{2\nu_n[\sin(\nu_n b) +\frac{H_3}{\nu_n}(1-\cos(\nu_n b))][\cos(\nu_n y) +H_3\sin(\nu_n y)]}{(\nu_n^2+H_3^2)\bigg(b+\frac{H_4}{\nu_n^2+H_4^2}\bigg)+H_3},
\end{equation}
\begin{equation}
\label{eq:parallelepipedZ}
  Z_k(z) = \frac{2\omega_k[\sin(\omega_k c) +\frac{H_5}{\omega_k}(1-\cos(\omega_k c))][\cos(\omega_k z) +H_5\sin(\omega_k z)]}{(\omega_k^2+H_5^2)\bigg(c+\frac{H_6}{\omega_k^2+H_6^2}\bigg)+H_5},
\end{equation}
and $\beta_m$, $\nu_n$, and $\omega_k$, are positive roots of transcendental equations
  \begin{equation} 
  \label{eq:beta_m} 
  	\tan(\beta_m) = \frac{\beta_m(H_1+H_2)}{\beta_m^2-H_1H_2},
  \end{equation} 
 
  \begin{equation} 
  \label{eq:nu_n} 
  	\tan(\nu_n) = \frac{\nu_n(H_3+H_4)}{\nu_n^2-H_3H_4},
  \end{equation} 
  
  \begin{equation} 
  \label{eq:omega_k} 
  	\tan(\omega_k) = \frac{\omega_k(H_5+H_6)}{\omega_k^2-H_5H_6}.
  \end{equation}  
  
If temperature rise after the pulse is measured in the center of the rear side of the specimen (in $x=L$, $y=b/2$, $z=c/2$), then the temperature is
 \begin{equation}
 \label{eq:Parallelepiped}
  T(L, b/2, c/2, t)=\sum_{m=1}^\infty \sum_{n=1}^\infty\sum_{k=1}^\infty \Gamma_{nmk}(t) X_m(L) Y_n(b/2)Z_k(c/2).
\end{equation}

If the specimen material is \textit{isotropic}, then the thermal conductivities are $k_x=k_y=k_z=k$, and also the thermal diffusivities are $a_x = a_y=a_z = a$. The solution given in formulas (\ref{eq:parallelepiped_Temp_Rise_Anis} --\ref{eq:parallelepipedZ}) remains the same, but the Biot numbers are defined as
\begin{equation}
\begin{split}
 \quad H_1=\frac{h_1 L}{k}, \quad H_2=\frac{h_2 L}{k},\\
 \quad H_3=\frac{h_3 b}{k}, \quad H_4=\frac{h_4 b}{k},\\
 \quad H_5=\frac{h_5 c}{k}, \quad H_6=\frac{h_6 c}{k}.
 \end{split}
\end{equation} 
and function $\Gamma_{nmk}$ is
 \begin{equation}
 \Gamma_{nmk}(t) = \exp\bigg\{-\bigg[\bigg(\frac{\beta_m}{L}\bigg)^2 + \bigg(\frac{\nu_n}{b}\bigg)^2 + \bigg(\frac{\omega_k}{c}\bigg)^2\bigg]at\bigg\}.
 \end{equation}

\newpage
\section{Data Reduction in the Flash Method}

Many data reduction methods for calculation of the thermal diffusivity from the response curve in the flash method were proposed in literature during those 57 years of the method existence. Starting from the simplest Parker's formula and ending with complex optimization algorithms. For practical reasons only a handful of those methods were implemented into commercially produced instruments. 

The data reduction method of choice at a time was historically always affected by tools available to experimenters working in the field. In early 60--ties there were no computers, no digital signal acquisition and processing, so the data reduction procedures had to be simple and straightforward. Later in 70-ties and especially 80-ties, as personal computers  became available and more powerful, more advanced and better data reduction methods were introduced. This time one of the most popular and still widely used Clark and Taylor method \cite{clark1975radiation} was introduced. 

Today advanced curve fitting mathematical methods, such as the Least Square Regression, are used to analyze the data from the flash method. Progress is now focused on a development of better and more realistic heat conduction models.      

\subsection{Parker's Formula}

In the flash method experiment, the temperature response is measured on rear side of the specimen. The normalized solution for an ideal response to an instantaneous heat pulse, evenly distributed on the front surface of an adiabatically insulated specimen, is 
\begin{equation}
\label{eq:Tidealdimless}
 V(\omega) = 1 + 2\sum_{n=1}^{\infty} (-1)^n \exp\big[-n^2\pi^2\omega \big].
\end{equation} 
The temperature rise $V$ depends only on the dimensionless time $\omega = at/L^2$. Parker chose  $V=1/2$ and from equation (\ref{eq:Tidealdimless}) he calculated the correspondent dimensionless time
\begin{equation}
\label{eq:halftime}
\omega_{1/2} = \frac{at_{1/2}}{L^2}=\frac{1.37}{\pi^2} = 0.1388,
\end{equation}
where $t_{1/2}$ is experimentally obtained time, called \textit{halftime}, when the response curve rises to 50$\%$ of its final temperature rise.
The unknown thermal diffusivity can then be calculated from equation (\ref{eq:halftime}) as
\begin{equation}
\label{eq:ParkerFormula}
 a=0.1388\frac{L^2}{t_{1/2}},
 \end{equation} 
which is the Parker's formula \cite{parker1961flash}. This simple formula can be used to calculate the thermal diffusivity in case of thin specimens of highly conductive materials, e. g. metals, when heat losses from the specimen surface during the transient period can be neglected.

For other materials and thicker specimens, when the heat losses are not negligible, more sophisticated methods of data reduction have to be used. One of the most often utilized is so called Clark and Taylor method. 

\subsection{Clark and Taylor Method}

It was Cape and Lehman \cite{cape1963temperature}, who in 1963 analyzed a two-dimensional heat conduction model, allowing heat exchange from the edge of the sample as well as the faces. Then they normalized the response curves by dividing $V$ with the maximum rise $V_m$ and time $t$ with the time of half maximum rise $t_{1/2}$, so-called half-max time.

Based on numerical evaluation of Cape and Lehman solution, Taylor and Clark \cite{clark1975radiation} calculated the the dimensionless halftime $\omega_{1/2}= at_{1/2}/L^2$ as a function of three time ratios $t_{0.7}/t_{0.3}$, $t_{0.8}/t_{0.4}$, and $t_{0.8}/t_{0.2}$, for the Biot numbers ranging from $H=0$ to $H=2.0$.

Calculated curves are plotted in Figure \ref{fig:CTcurves}.
\begin{figure}[h]
 \centering
  \includegraphics[scale=0.8]{CTcurves.eps}%
  \caption{Dimensionless halftime  $\omega_{1/2}$ as a function of three different time ratios.}
  \label{fig:CTcurves}%
\end{figure} 

%% graph created using ClarkandTaylorCorrectioncurves.R in HLCorrections (7/6/2016)
Experimenter could select the most convenient time ratio, calculate its value from experimental data and find a correspondent value of $\omega_{1/2}$ using a plot from Figure \ref{fig:CTcurves}. Knowing the experimental half-max time $t_{1/2}$, and $L^2$, the thermal diffusivity is calculated from 

\begin{equation}
a=\omega_{1/2}\frac{L^2}{t_{0.5}}.
\end{equation}

By normalizing the experimental time to its half-max value ($t_{0.5}$) Clark and Taylor solved the problem of finding the characteristic time without prior knowledge of the thermal diffusivity. Furthermore, by using the time ratio of two different time points, instead of a single time point, they bypassed the problem of estimating the heat loss coefficient. The procedure was this way simplified toward the estimation of the thermal diffusivity, as the only desired parameter. 

Both Parker's formula and Clark and Taylor method are using only \textit{few experimental points} of a response curve for the analysis. In case of noisy digital signals, it is difficult to identify these unique points and the results often depend on how the raw signal was smoothed. With increased computational power of personal computers, more precise and robust analytical tools were introduced to data reduction in the flash method. One of the most popular is the Least Square Regression, where \textit{ all points} of the response signal can enter the analysis without the need to smooth or otherwise manipulate the original raw data. 

\subsection{Least Square Regression}

Suppose we are fitting $N$ data points $(t_i,y_i)$, $i=1,2,\ldots, N$ to a model which has $M$ unknown parameters $\alpha_j$, $i=1,2,\ldots, M$. The model predicts a functional relationship between the measured independent variable (time) and dependent variable (temperature rise)  
\begin{equation}
y(t)=y(t|\alpha_1\ldots\alpha_M).
\end{equation}

In order to estimate a set of parameters which will closely fit the model values with the data, we will assume that each data point, $y_i$, has a measurement error that is independently random and normally distributed around the "true" model value $y(t)$. Standard deviations $\sigma$ of these normal distributions are the same for all points. Then the probability of the data point to be within the interval $(y-\Delta y, y+\Delta y)$ is
\begin{equation}
 P_i \propto \exp\bigg[-\frac{[y_i - y(t_i)]^2}{2\sigma^2}\bigg]\Delta y.
\end{equation} 
The probability of the data set is a product of the probabilities of each point
\begin{equation}
\label{eq:ProbabilityAllPointsP}
P \ \propto \prod_{i=1}^N \left\{ \exp\bigg[-\frac{[y_i - y(t_i)]^2}{2\sigma^2}\bigg]\Delta y \right\}.
\end{equation}
Best fit parameters should \textit{maximize} the probability $P$. Maximizing $P$ in (\ref{eq:ProbabilityAllPointsP}) is equivalent to maximizing its logarithm, or minimizing the negative of its logarithm, namely
\begin{equation}
\sum_{i=1}^N \bigg[\frac{[y_i - y(t_i)]^2}{2\sigma^2}\bigg]-N\log\Delta y .
\end{equation} 
Since $N$, $\sigma$, and $\Delta y$ are all constants, minimizing this equation is equivalent to
\begin{equation}
\label{eq:OrdinaryLeastSquareDef}
\text{minimize over }\alpha_1\ldots \alpha_M: \quad \quad S=\sum_{i=1}^N [y_i - y(t_i|\alpha_1\ldots\alpha_M)]^2
\end{equation}

If each data point ($t_i,y_i$) has its own, known standard deviation $\sigma_i$, then equation (\ref{eq:ProbabilityAllPointsP}) is modified by putting a subscript $i$ on the symbol $\sigma$. The quantity to minimize is now called "chi-square" and is defined as
\begin{equation}
\label{eq:ChiSquare}
\chi^2= \sum_{i=1}^N \bigg(\frac{y_i - y(t_i|\alpha_1\ldots\alpha_M)}{\sigma_i^2}\bigg)^2.
\end{equation}

In order to find a minimum of (\ref{eq:ChiSquare}) with respect to the parameters $\alpha_k$, we have to find roots of the first derivatives of $\chi^2$ function
\begin{equation}
\label{eq:RootsDerivativesChiSquare}
0= \sum_{i=1}^N \bigg(\frac{y_i - y(t_i|\alpha_1\ldots\alpha_M)}{\sigma_i^2}\bigg)
\bigg( \frac{\partial y(t_i|\ldots \alpha_k \ldots)}{\partial \alpha_k} \bigg) \quad k=1,2,\ldots M.
\end{equation}
In general, Equation (\ref{eq:RootsDerivativesChiSquare}) is a set of $M$ nonlinear equations for $M$ unknown parameters $\alpha_k$. For certain functions $y$, with a linear dependence on the parameters $\alpha_k$, these equations can be solved explicitly, using a linear regression. For nonlinear functions (or those which cannot be linearized) these equations are solved using iterative methods, called nonlinear regression methods.


\newpage
\section{Discussion}
Mainstream software techniques routinely used to analyze data produced by flash instruments are using analytical models based on the assumption that the thermal conductivity/diffusivity of sample material is isotropic. It is not the case for many of advanced materials, e.g. carbon-carbon composites, graphenes, etc., for which in-plane thermal diffusivity components can be of order of magnitude bigger than through-thickness components. 

We will show that using these data reduction techniques can therefore lead to substantial errors in the thermal diffusivity determination. Magnitude of these errors depends on specimen dimensions, degree of anisotropy of materials, and also experimental conditions like temperature, ambient gas, pressure, etc., which are affecting heat losses from the specimen surfaces and can be expressed as dimensionless Biot numbers.  

It is important to note that if the lateral surfaces of a specimen in the flash method are thermally insulated from the ambient, (Biot numbers $H_r$, for the cylinder, $H_i$, $i=3,4,5,6$, for the parallelepiped, are all zeroes), then the heat flow after a uniform heat pulse is always one--dimensional and the temperature rise follows a one--dimensional response curve, regardless of degree of anisotropy of the specimen material. The effect of anisotropy on the thermal diffusivity determination is therefore proportional to heat losses from the specimen lateral surfaces and is more pronounced at higher temperatures when convective heat losses are augmented with radiation heat losses.  

Basic definition of the Biot number is
\begin{equation}
\label{eq:Biot number definition}
 H=\frac{\text{heat transfer coefficient} \times \text{characteristic length}}{\text{thermal conductivity}}.
\end{equation}
\begin{figure}
 \centering
  \includegraphics[scale=0.8]{anisLamda.eps}%
  \caption{Normalized response curves for a different degree of anisotropy.}
  \label{fig:anisLambda}%
\end{figure} 
The heat transfer coefficient depends on the quality of ambient gas, the gas pressure, ambient temperature, the sample holder design, etc. The characteristic length for lateral surfaces is $b$ or $c$ for a parallelepiped, and the specimen radius $R$ for a cylindrical specimen. The Biot numbers (even if they are estimated for curve fitting purposes during the data reduction) are not the parameters of interest for the experimenters and are usually discarded, since they do not characterize the specimen material. 

From the Biot number definition it is clear that all three factors, the specimen geometry, the heat loss coefficients and the degree of anisotropy, are simultaneously affecting the shape of the response curve.

An example of response curves for different degree of the thermal diffusivity anisotropy is given in Figure  \ref{fig:anisLambda}. The response curves were generated for parallelepiped and cylindrical specimens using Equations (\ref{eq:parallelepiped_Temp_Rise_Anis}) and (\ref{eq:AxialRadialComponentsAnis}), respectively, with dimensions shown in the graph area of the figure. The heat transfer coefficient, $h =  10$ W/(m$^2$ K), was the same for all surfaces. 

As we can see, the anisotropic response curves differ from the isotropic ones when $\lambda_L=\lambda_r= 2$ W/(m K). This is true for both the cylindrical specimen (lines) and also for the parallelepiped specimen(dashed lines). When the ratio of \textit{in-plane/through-thickness} conductivities is smaller than 1, the heat losses are smaller and the response lags behind the isotropical one. For higher than 1 ratios, the response curves are showing signs of a higher heat losses and are leading the isotropical responses. The halfmax values $t_{0.5}$ for these curves are getting smaller with rising degree of anisotropy. Diffusivity results calculated using Parker's formula are therefore higher than those for the isotropic specimen. Taylor and Clark analysis is able to a certain degree to correct for heat losses, but the diffusivity values are still showing non negligible errors, as we can see from Table 1. Specimen shapes, listed in the first column of the table are abbreviated as "C" for a cylinder and "P" for a parallelepiped. 
\bigskip
\begin{center}
\footnotesize
\begin{tabular}{lllllllll}
Table 1\\
\toprule
Shape&$\lambda_L$ & $\lambda_r$ & Parker & C\&T & LSR & Parker [\%] & C\&T [\%] & LSR [\%]\\
\midrule
C&2&0.2&0.319& 0.189&0.203&59.54&-5.43&1.50\\
P&2&0.2&0.316&0.190&0.202&58.18&-5.19&1.00\\
C&2&2&0.330&0.198&0.200&65.24&-0.85&0.00\\
P&2&2&0.325&0.197&0.201&62.34&-1.70&0.50\\
C&2&5&0.353&0.186&0.202&76.25&-6.84&1.02\\
P&2&5&0.346&0.190&0.202&72.96&-4.99&0.92\\
C&2&10&0.359&0.187&0.204&79.68&-6.31&2.00\\
P&2&10&0.363&0.190&0.206&81.44&-5.09&3.00\\
C&2&20&0.363&0.187&0.205&81.44&-6.71&2.50\\
P&2&20&0.363&0.190&0.207&81.44&-5.09&3.67\\
\bottomrule
\end{tabular}
\end{center}
\bigskip  

Thermal conductivities for the in-plane and through-thickness directions are listed in columns 2 and 3, in [W/(m K)]. The diffusivity results for the three above mentioned data reduction techniques are listed in [cm$^2$/s] in columns 4-6. 

LSR results were obtained using the Flash Studio program, developed by the author. Only a isotropic cylinder model was used for the analysis in the Flash Studio.

Actual deviations of the diffusivity results in percent from the know theoretical value used in the simulation ($a = 0.200$ cm$^2$/s) are listed in columns 6 -- 8 of the table. As we can see the deviations are increasing with the degree of anisotropy, and are the biggest for the Parker formula, which is ignoring heat losses from the specimen.

Relative deviations are smaller for the Clark and Taylor analysis, but still about twice of the usually claimed uncertainty of $\pm 3\%$ of the flash method. 

The best results, almost within the claimed uncertainty, are for the Least Square Regression. These results were obtained using a computer program called Flash Studio developed by the author, which is using two--dimensional isotropic cylinder model with heat losses. Only an initial portion of rising part of anisotropic response curves (time range from zero to 0.07 s) were used for the analysis. In these early times heat losses due to the specimen anisotropy are relatively small. If the later parts of the anisotropic response curves were taken into the account (which is usually the case for an automatic data analysis), then relative deviations of diffusivity results were about twice as large as reported in Table 1.

No significant differences were found in the diffusivity results between the cylindrical and parallelepiped shapes of specimens. 

Response curves for different specimen thicknesses are plotted in Figure  \ref{fig:anisDimens}.
\begin{figure}[h]
 \centering
  \includegraphics[scale=0.8]{anisDimens.eps}%
  \caption{Normalized response curves for different specimen thicknesses.}
  \label{fig:anisDimens}%
\end{figure}  
  
In order to show these response curves in a one graph, the time scale in the figure was corrected for different specimen thicknesses by dividing the actual time with the square of the specimen thickness, $time^*=time/L^2$.    

The curves were generated for parallelepiped and cylindrical specimens, using Equations (\ref{eq:parallelepiped_Temp_Rise_Anis}) and (\ref{eq:AxialRadialComponentsAnis}), respectively, with values of parameters shown in the figure area. Curves for isotropical specimens [for $\lambda_L=\lambda_r= 2$ W/(m K)] of parallelepiped shape are depicted as "dot-dash" lines, for the cylindrical shape specimen as "dot" lines. Curves for the anisotropical specimens of parallelepiped shape are continuous lines, and cylindrical shape specimens are shown as "dash" lines. 

It is clear from the graphs that anisotropic response curves differ significantly from the isotropic ones. This is true for both shapes. The differences increase with the increasing ratio of $L/b$ (or $L/R$) as the lateral heat losses are becoming more significant. The halfmax values $t_{0.5}$ for anisotropic curves are getting smaller with rising ratio $L/b$, which is affecting the diffusivity determination using the Parker formula. 

Results of he thermal diffusivity determination for anisotropic specimens (both cylinder "C" and parallelepiped "P") are listed in Table 2.
\bigskip
\begin{center}
\footnotesize
\begin{tabular}{lllllllll}
Table 2\\
\toprule
Shape&$L$& $b$ (or 2$R$) & Parker & C\&T & LSR & Parker [\%] & C\&T [\%] & LSR [\%]\\
\midrule
C&2&10&0.276&0.199&0.201&38.1&-0.50&0.50\\
P&2&10&0.289&0.191&0.206&44.6&-4.56&3.15\\
C&4&10&0.363&0.187&0.205&81.4&-6.71&2.50\\
P&4&10&0.363&0.190&0.207&81.4&-5.09&3.67\\
C&6&10&0.387&0.177&0.201&93.7&-11.63&0.72\\
P&6&10&0.423&0.182&0.204&111.7&-9.10&2.15\\
C&8&10&0.445&0.172&0.201&122.3&-13.83&0.46\\
P&8&10&0.484&0.178&0.202&141.9&-11.14&0.84\\
\bottomrule
\end{tabular}
\end{center}
\bigskip  
The diffusivity results for the three above mentioned data reduction techniques are again listed in [cm$^2$/s] in columns 4-6 of Table 2. The deviations of the diffusivity results in percent from the know theoretical value used in the simulation ($a = 0.200$ cm$^2$/s) are listed in columns 6 -- 8 of the table. 

As expected, the deviations are increasing with as the ratio $L/b$ (or $L/R$) increases, and are the biggest for the Parker formula, which is ignoring heat losses from the specimen.
The relative deviations are much smaller for the Clark and Taylor analysis but still significantly larger than the usually claimed uncertainty of $\pm 3\%$ of the flash method. 

The best results, mostly within the claimed uncertainty, are for the Least Square Regression, even if in all cases the isotropical analytical models were used. These results were obtained using only a small portion of rising part of anisotropic response curves for time interval from zero to ~ $t_{0.3}$, when heat losses due to the specimen anisotropy were relatively small. If the later parts of the anisotropic response curves were taken into the account (which is usually the case for an automatic data analysis), then relative deviations of the results were generally higher.

As in the previous set, no significant differences were found in diffusivity results between the cylindrical and parallelepiped shapes of specimens. 

Using different heat transfer coefficients for different specimen surfaces can lead to a substantial changes in response curves, as we can see in Figure   \ref{fig:different_htc}. The curves were generated for cylindrical specimens with dimensions $L = 4$ mm, $R=5$ mm, using formula (\ref{eq:AxialRadialComponentsAnis}). Thermal conductivities are in W/(m K) and heat transfer coefficients are in W/(m$^2$K). As can be seen from the Figure, curves for the isotropic specimen are less affected than the ones for the anisotropic specimen.

Differences in heat transfer coefficients between lateral and axial specimen surfaces may be caused by a holder design. In the flash method, a heat exchange is more intense on specimen lateral surfaces due to physical contact with holder made of highly conductive material.

\begin{figure}[h]
 \centering
  \includegraphics[scale=0.7]{different_htc.eps}%
  \caption{Normalized response curves for a cylindrical specimen with different heat transfer coefficients.}
  \label{fig:different_htc}%
\end{figure} 
 
\begin{center}
\footnotesize
\begin{tabular}{llllllllll}
Table 3\\
\toprule
$h_z$&$h_r$&$\lambda_z$ & $\lambda_r$ & Parker & C\&T & LSR & Parker [\%] & C\&T [\%] & LSR [\%]\\
\midrule
10&10&2&2&0.330&0.198&0.201&65.24&-0.85&0.50\\
10&20&2&2&0.334&0.189&0.180&67.23&-5.13&-9.72\\
10&50&2&2&0.3436&0.182&0.194&71.36&-8.67&-2.77\\
10&100&2&2&0.343&0.178&0.192&71.36&-10.79&-3.65\\
10&10&2&20&0.363&0.187&0.205&81.44&-6.71&2.50\\
10&20&2&20&0.391&0.175&0.177&95.49&-12.36&-11.47\\
10&50&2&20&0.455&0.167&0.166&127.54&-16.45&-16.68\\
10&100&2&20&0.505&0.165&--&152.36&-17.44&--\\
10&10&2&50&0.365&0.191&0.209&82.5&-4.50&4.50\\
10&20&2&50&0.396&0.178&0.191&98.00&-11.00&-4.50\\
10&50&2&50&0.470&0.170&0.195&135.0&-15.00&-2.50\\
10&100&2&50&0.544&0.153&0.246&172.0&-23.50&23.00\\
10&10&2&100&0.365&0.192&0.2111&82.5&-4.00&5.55\\
10&20&2&100&0.402&0.181&0.191&101.2&-9.47&-4.74\\
10&50&2&100&0.479&0.167&0.200&139.3&-16.67&-0.15\\
10&100&2&100&0.567&0.133&0.260&183.3&-33.30&29.75\\
\bottomrule
\end{tabular}
\end{center}
\bigskip  
The diffusivity results for the three data reduction techniques are again listed in [cm$^2$/s] in columns 5--7 of Table 3. The deviations of the diffusivity results in percent from the know theoretical value used in the simulation ($a = 0.200$ cm$^2$/s) are listed in columns 8 -- 10 of the table. 
\begin{figure}[h]
 \centering
  \includegraphics[scale=0.7]{f1.eps}%
  \caption{Least Square Regression fit. (Output from the Flash Studio.)}
  \label{fig:f1}%
\end{figure}   

The deviations are increasing as ratio $h_r/h_z$ increases, and are much larger for the anisotropic specimen. In the case of anisotropic specimen with $h_r/h_z = 10$, $\lambda_z =2$ W/(m K), $\lambda_r=20$ W/(m K), the LSR technique based on an isotropic model was not able to fit the response curve. Even when the diffusivity results of the LSR analysis are relatively close to the expected theoretical value, curves generated from the fitted parameters were not closely following the simulated responses, which is a clear sign of a model mismatch (see Figure \ref{fig:f1}). The fit has a low value of trustworthiness (goodness-of-fit). 

The relative deviations in thermal diffusivity estimation of three types of data reduction analysis are significantly larger than the usually claimed uncertainty of the flash method, especially for anisotropic materials and increased lateral heat transfer coefficients. 


\section{Conclusion}
We have developed analytical formulas for temperature responses for the flash method specimens of rectangular parallelepiped and cylindrical shapes, with heat losses from specimen surfaces.

Temperature response curves were generated for cylindrical and parallelepiped specimens using these formulas and the thermal diffusivities were estimated using standard data reduction techniques used in the flash method instruments.

It was shown that using these data reduction techniques, based on an assumption that the specimen material is isotropic, can (in certain situations) lead to relative errors which are of order of magnitude larger than usually claimed uncertainty ($\pm 3\%$) of the flash method.

The most important parameters affecting the error in thermal diffusivity determination in the flash method are specimen dimensions, the degree of material anisotropy, and the heat transfer coefficient for specimen lateral surfaces.  


\newpage
\bibliographystyle{ieeetr}
%%\bibliographystyle{}
\bibliography{\detokenize{../../../myFlashMethodTheory/mybook}}

\end{document}