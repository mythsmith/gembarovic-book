% !TEX root = mybook.tex

\chapter{Nonlinear Regression}
\label{Ch:Nonlinear Regression}
\section{Introduction}
Given a set of temperature versus time points from a flash experiment, we want to condense (reduce in size) and summarize the data by fitting it to a model that depends on a set of parameters. The process is called modeling. Model's parameters come from the theory of heat conduction that the data are supposed to satisfy. In our case, the most interesting parameter is the thermal diffusivity of the sample material. In order to reconstruct the temperature rise curve, we also need others the maximum temperature rise, the baseline level, the Biot numbers for sample surfaces, etc.

Modeling can also be used to extend a few data points into a continuous function with a clear underlying idea of what the function should look like, what kind of solution of the heat conduction equation will be used.

The basic general approach is to choose or design a \textit{figure-of-merit function} ("merit function" or "cost" for short) that measures the agreement between the data and the model with a particular choice of parameters. Small values of the merit function represent close agreement. The parameters of the model are adjusted to achieve a minimum of the merit function, yielding \textit{best-fit parameters}\cite{press1988numerical}. 

In this Chapter we will discuss methods used to minimize the merit function and find best-fit parameters for response curves in the flash method. Since typical experimental are subject to \textit{measurement errors} (or "noise"), they never exactly fit the model that is being used, even if the model is correct. We need to asses whether or not the model is appropriate, that is, we need to test the \textit{goodness-of-fit} against some useful statistical standard.

We also need to know the likely errors of the best-fit parameters and if they really represent the global and not a local minimum of the merit function in the parameter space.

A good fitting procedure should provide (i) parameters, (ii) error estimations on the parameters, and (iii) a statistical measure of goodness-of-fit. When the third item suggests that the model is an unlikely match to the data, then items (i) and (ii) are probably worthless. 

\section{Least Square Regression}

Suppose we are fitting $N$ data points $(t_i,y_i)$, $i=1,2,\ldots, N$ to a model that have $M$ unknown parameters $\alpha_j$, $i=1,2,\ldots, M$. The model predicts a functional relationship between the measured independent and dependent variables
\begin{equation}
y(t)=y(t|\alpha_1\ldots\alpha_M).
\end{equation}

How to find a set of parameters which will closely as possible fit the model values with the data? What general principles is the procedure based on? 

We will assume that each data point $y_i$ has a measurement error that is independently random and distributed as a normal (Gaussian) distribution around the "true" model value $y(t)$, and that standard deviations $\sigma$ of these normal distributions are the same for all points. Then the probability of the data point to be within the interval $(y-\Delta y, y+\Delta y)$ is
\begin{equation}
 P_i \propto \exp\bigg[-\frac{[y_i - y(t_i)]^2}{2\sigma^2}\bigg]\Delta y.
\end{equation} 
The probability of the data set is a product of the probabilities of each point
\begin{equation}
\label{eq:ProbabilityAllPointsP}
P \ \propto \prod_{i=1}^N \left\{ \exp\bigg[-\frac{[y_i - y(t_i)]^2}{2\sigma^2}\bigg]\Delta y \right\}.
\end{equation}
Best fit parameters should \textit{maximize} the probability $P$. Maximizing \ref{eq:ProbabilityAllPointsP} is equivalent to maximizing its logarithm, or minimizing the negative of its logarithm, namely
\begin{equation}
\sum_{i=1}^N \bigg[\frac{[y_i - y(t_i)]^2}{2\sigma^2}\bigg]-N\log\Delta y .
\end{equation} 
Since $N$, $\sigma$, and $\Delta y$ are all constants, minimizing this equation is equivalent to
\begin{equation}
\label{eq:OrdinaryLeastSquareDef}
\text{minimize over }\alpha_1\ldots \alpha_M: \quad \quad S=\sum_{i=1}^N [y_i - y(t_i|\alpha_1\ldots\alpha_M)]^2
\end{equation}

If each data point $t_i,y_i)$ has its own, known standard deviation $\sigma_i$, then equation \ref{eq:ProbabilityAllPointsP} is modified by putting a subscript $i$ on the symbol $\sigma$. The quantity to minimize is now called "chi-square" and is defined as
\begin{equation}
\label{eq:ChiSquare}
\chi^2= \sum_{i=1}^N \bigg(\frac{y_i - y(t_i|\alpha_1\ldots\alpha_M)}{\sigma_i^2}\bigg)^2.
\end{equation}

In order to find a minimum of \ref{eq:ChiSquare} with respect to the parameters $\alpha_k$, we have to find roots of the first derivatives of $\chi^2$ function
\begin{equation}
\label{eq:RootsDerivativesChiSquare}
0= \sum_{i=1}^N \bigg(\frac{y_i - y(t_i|\alpha_1\ldots\alpha_M)}{\sigma_i^2}\bigg)
\bigg( \frac{\partial y(t_i|\ldots \alpha_k \ldots)}{\partial \alpha_k} \bigg) \quad k=1,2,\ldots M.
\end{equation}
Equation \ref{eq:RootsDerivativesChiSquare} is, in general, a set of $M$ nonlinear equations for $M$ unknown parameters $\alpha_k$. For certain functions $y$, with a linear dependence on the parameters $\alpha_k$, these equations can be solved explicitly, using a linear regression. For nonlinear functions (or those which cannot be linearized) these equations are solved using iterative methods, called nonlinear regression methods.
 
\section{Fitting Flash Method Data to an Ideal Response}
%\input{IdealLeastSquare}

To demonstrate the use of the least square regression a temperature response curve (shown in Figure \ref{fig:LeastSquareTest}) was generated by a computer. The response contains $N=200$ data points calculated using formula 
\begin{equation}
\label{eq:IdealLeastSquareT}
 y(t_i,a,B)=BV(t_i, a),
\end{equation} 
whith the ideal dimensionless temperature rise (\ref{eq:TidealLt})  
\begin{equation}
\label{eq:VIdeal}
V(t_i,a)=1 + 2\sum_{n=1}^{\infty} (-1)^n \exp\bigg[-\frac{n^2\pi^2 at_i}{L^2}\bigg],
\end{equation}
for the thermal diffusivity $a=1.17$ cm$^2$/s, the adiabatic temperature rise $B=T_{max}=2.2$, the sample thickness $L=0.2$ cm. Normally distributed random noise, with a constant standard deviation $\sigma=0.03$, was added to each point to imitate a noisy experimental signal. The sample temperature before the heat pulse was set to zero. An instantaneous heat pulse started at $t=0 $ s. 
\begin{marginfigure}%
  \includegraphics[scale=0.5]{LeastSquareEcurve.eps}
  \caption{Computer generated response points.}
  \label{fig:LeastSquareTest}
  % generated using LeastSquareIdealVerification.R
\end{marginfigure}
Now, all we know is the baseline temperature level, the sample thickness, the starting time of the heat pulse and we will use the least square regression to estimate the 
"unknown" parameters: the thermal diffusivity $a$, and the maximum adiabatic temperature rise $B$. These two parameters are necessary for a reconstruction of an ideal temperature response. 

\subsection{Brute Force Approach}
One way how to calculate the optimal values would be to divide the parameters phase space $a$ and $B$ into a rectangular 2-dimensional grid (mesh) of discrete points and calculate the sum of square $S$ value for each mesh point. The point, with coordinates $(a^*,B^*)$, in which $S$ reached its minimum, will then be regarded as the optimal choice. 
\begin{marginfigure}
  \includegraphics{3DLogS.eps}
  \caption{Logarithm of the Sum of Squares in the parameter space.}
  \label{fig:3DLogS}
\end{marginfigure}
The graph shown in Figure \ref{fig:3DLogS}, was calculated for a mesh of $(200\times200)$ points, for $a$ from $0.1$ to $2.0$ cm$^2$/s, and $B$ from $1.0$ to $3.0$. The sum of squares $S$ was calculated using \ref{eq:OrdinaryLeastSquareDef}, with the data points from  \ref{fig:LeastSquareTest}. Logarithm of $S$ is plotted for a better resolution. (The minimum of $\text{Log} S$ is also the minimum of $S$.) The minimum of the sum squares is  $S =  0.1880817$, ($\text{Log}S=-1.6708791$), for the thermal diffusivity $a^*= 1.1693$ cm$^2$/s, and the maximum temperature rise $B=2.2060$. Although, the agreement between the theoretical and calculated parameter values is very good, this "brute force" approach is not very smart and is usually very time consuming for higher than two- or three-dimensional phase spaces, where the number of mesh points is rapidly increasing. 

\subsection{Semi-Analytical Approach}

As we know from the mathematical analysis, the necessary conditions for the minimum of the sum of squares function 
\begin{equation}
\label{eq:IdealLeastSquareRT}
S(a,B)=\sum_{i=1}^N \Bigg[ y_i-BV(t_i,a)\Bigg]^2,
\end{equation}
are
\begin{equation}
\label{eq:IdealLeastSquareConditions}
\frac{\partial S(a,B)}{\partial a}=0, \quad \frac{\partial S(a,B)}{\partial B}=0.
\end{equation}
\begin{marginfigure}
  \includegraphics[scale=0.5]{LogSvsa.eps}
  \caption{Logarithm of the sum of squares $S$ versus the thermal diffusivity $a$ .}
  \label{fig:LogSvsa}
   % generated using LeastSquareIdealVerification.R
\end{marginfigure}
If the theoretical model depends linearly on the parameters, then the resulting system of two equations for two unknown parameters can be solved explicitly. In our case, the model function $y(t_i,a,B)=BV(t_i,a)$ depends linearly only on $B$, therefore only the equation for the unknown parameter $B$, can be expressed explicitly.

Performing the indicated partial derivations, and after a lengthy but straightforward set of manipulations, the equations for $a$ and $B$ are
\begin{equation}
\label{eq:IdealLeastSquareEq_a}
\sum_i y_i V(t_i,a)\sum_i V(t_i,a)\frac{\partial V(t_i,a)}{\partial a}-\sum_i y_i\frac{\partial V(t_i, a)}{\partial a}\sum_i V(t_i,a)^2=0,
\end{equation}
\begin{equation}
\label{eq:IdealLeastSquareEq_B}
B=\sum_i y_i V(t_i,a)\bigg( \sum_i V(t_i,a)^2\bigg)^{-1}.
\end{equation}

The optimal values of $a$ and $B$ are obtained as a solution to the equation system \ref{eq:IdealLeastSquareEq_a} and \ref{eq:IdealLeastSquareEq_B},
where $V(t_i,a)$ is given in \ref{eq:VIdeal}, and 

\begin{equation}
\frac{\partial V(t_i,a)}{\partial a} = \frac{\pi^2t_i}{L^2}\sum_{n=1}^{\infty} (-1)^n n^2 \exp\bigg[-\frac{n^2\pi^2 at_i}{L^2}\bigg].
\end{equation}

The problem of finding the optimal values of the parameters is now reduced to solving equation \ref{eq:IdealLeastSquareEq_a} for the thermal diffusivity $a$. A corresponding value of the parameter $B$ can then be calculated from equation \ref{eq:IdealLeastSquareEq_B}. 

\begin{marginfigure}
  \includegraphics[scale=0.5]{FunctionFa.eps}
  \caption{Function F(a).}
  \label{fig:LeastSquareFa}
   % generated using LeastSquareIdealVerification.R
\end{marginfigure}

If we denote the left-hand side of equation \ref{eq:IdealLeastSquareEq_a} as $F(a)$, then as we can see from Figure \ref{fig:LeastSquareFa}, this function has one simple root $a^*$ (except of the trivial one at zero), which correspond to the desired optimal value of the thermal diffusivity. For all $a>a^*$ and $0<a<a^*$, $F(a)>0$ and $F(a)<0$, respectively. This fact facilitates the procedure of computation of $a$ from equation \ref{eq:IdealLeastSquareEq_a} by means of any of the standard numerical methods. In this case, we simply generated 200 data points of $F(a)$  for $0.1 \le a \le 2.0$, shown in Figure \ref{fig:LeastSquareFa}, and then found the first one for which $F(a)>0$. The optimal value of the thermal diffusivity thus found was $a^*=1.1693$ cm$^2$/s. A corresponding value of $B^*=2.2056$. 

As we saw, this algorithm need not to calculate $S$ values for all mesh points of the parameters phase space, as in the brute force approach. Only the parameter $a^*$ is calculated from \ref{eq:IdealLeastSquareEq_a}, where the formula for a normalized theoretical response \ref{eq:VIdeal} is used,  and the parameter $B^*$ is then calculated directly from \ref{eq:IdealLeastSquareEq_B}. The number of model function evaluations is usually much smaller than in the brute force approach. This is how time for the calculation is being saved.  
\begin{marginfigure}
  \includegraphics[width=\linewidth]{LeastSquareReconstructed.eps}
  \caption{Reconstructed response curve.}
  \label{fig:LeastSquareReconstructed}
\end{marginfigure}
A comparison of the experimental points from Figure \ref{fig:LeastSquareTest} with the theoretical curve calculated using \ref{eq:IdealLeastSquareT}, where the optimal values of $a^*$ and $B^*$ were used, is shown in Figure  \ref{fig:LeastSquareReconstructed}. The agreement between the theoretical and experimental points is very good and relative deviations between the reconstructed and the exact parameters are less than 1\%.

\subsection{Levenberg-Marquardt Algorithm}
\
The standard of nonlinear least-squares routines, which works very well in practice, is the Levenberg-Marquardt algorithm (LMA). It was first published in 1944 by Kenneth Levenberg\cite{levenberg1944method} and rediscovered in 1963 by Donald Marquardt\cite{marquardt1963algorithm}, who worked as a statistician at DuPont.
 
The algorithm is based on an iteration process in which the optimal parameters for the minimum of the sum of squares are found by following the path of the steepest descent of the function $S$ in the parameter phase space, with a varying length of steps. A detail description of the algorithm can be found for example in Chapter 15 of Press et. al. book\cite{press1988numerical}. 

The oldest implementation still in use is \texttt{lmdif}, from package MINPACK\cite{more1984minpack}, written in FORTRAN language. MINPACK was also implemented C, C++. LMA is a built-in algorithm in many of today used software, e.g. Mathematica , Matlab, Origin, and LabVIEW. Several high-level languages and mathematical packages have wrappers for the MINPACK routines, among them Python and R-language.

As an example, we will use LMA function \texttt{nls.lm} from R-language package \texttt{minpack.lm} to fit the data from Figure \ref{fig:LeastSquareTest}. 

First, we will define the independent variable \texttt{x} (time vector), dependent variable \texttt{Tdata} (experimental temperatures), the model function \texttt{getPred} (the ideal flash model), and the residual function \texttt{residFun} as a difference between experimental and theoretical temperatures.   

The procedure allows to set up the lower and the upper bounds, individually for each parameter. To start the process of finding the optimal parameters using LMA, the user has to provide an initial guess for the parameter vector, given in \texttt{parStart} list.

The program section with the above described steps is given below: 

\begin{lstlisting}[basicstyle=\tiny]
# define data points for the analysis 
x <- timev # time vector
Tdata <- y1D # temperature rise vector

## 1D model based on a list of parameters 
getPred <- function(parS, xx) {
  alpha   <- parS$Diff 
  deltaT  <- parS$Tmax
  return(flash1D(xx, alpha, thickness, deltaT))
}
## residual function 
residFun <- function(p, observed, xx) observed - getPred(p,xx)

# lower and upper bounds for the parameters
lwr <- c(0,0) 
upr <- c(100,20) 

# initial guesses
parStart <- list(Diff=0.1,Tmax = 1)
\end{lstlisting} 
 
After these initial steps, the function \texttt{nls.lm} is called:

\begin{lstlisting}[language=R ,basicstyle=\tiny]
## perform fit 
nls.out <- nls.lm(par=parStart,lower = lwr[1:length(parStart)], 
                  upper = upr[1:length(parStart)],
                  fn = residFun, observed = Tdata, xx = x, 
                  control = nls.lm.control(nprint=1))
\end{lstlisting}
and the iteration output is printed out as:

\begin{lstlisting}[basicstyle=\tiny]
It.    0, RSS =    523.693, Par. =        0.1    1
It.    1, RSS =    167.666, Par. =   0.261369    5.62344
It.    2, RSS =    107.527, Par. =   0.295532    3.79715
It.    3, RSS =    95.0141, Par. =   0.431114    2.13787
It.    4, RSS =     40.862, Par. =   0.501084    2.70055
It.    5, RSS =    20.9211, Par. =   0.653966    2.33695
It.    6, RSS =    5.37708, Par. =   0.962776    2.10297
It.    7, RSS =   0.224852, Par. =    1.15904    2.18896
It.    8, RSS =   0.186138, Par. =    1.17349    2.20123
It.    9, RSS =   0.186138, Par. =    1.17359    2.20125
It.   10, RSS =   0.186138, Par. =    1.17359    2.20125
\end{lstlisting}
\begin{marginfigure}
  \includegraphics[width=\linewidth]{nlsOutFit.pdf}
  \caption{Reconstructed response curve with its 95\% confidence interval. The optimal parameters were calculated using \texttt{nls.lm}.}
  \label{fig:nlsOutFit}
\end{marginfigure}
with a summary:
\begin{lstlisting}[keywordstyle=\color{black},basicstyle=\tiny]
Parameters:
     Estimate Std. Error t value Pr(>|t|)    
Diff 1.173595   0.005421   216.5   <2e-16 ***
Tmax 2.201245   0.003349   657.2   <2e-16 ***
---
Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1

Residual standard error: 0.03066 on 198 degrees of freedom
Number of iterations to termination: 10 
Reason for termination: 
	Relative error in the sum of squares is at most 'ftol'. 
\end{lstlisting}

As we can see from the output, \texttt{nls.lm} made 10 iterations, and the residual sum of squares $S=\text{RSS}=0.186138$. The function provided  whole information about the fitting process, not only the estimated optimal values ($a=\text{Diff}=1.173595$ cm$^2$/s, and $B=\text{Tmax}= 2.201245$), but also the standard errors for these parameters ($a_{sd}=0.005421$ cm$^2$/s and $B_{sd}=  0.003349$), and the statistical measures of goodness of fit as $\texttt{t value}$, and $\texttt{Pr(>|t|)}$.

The column \texttt{t value} shows the t-test associated with testing the significance of the parameter listed in the first column. For example, the t value of $216.5$ refers to the t-test of the thermal diffusivity $\text{Diff} = 1.173595$, divided by the standard error of that estimate $0.005421$. 

Pr(>|t|) gives the p-value for that t-test (the proportion of the t distribution at that df which is greater than the absolute value of the t statistic), $<2\times 10^{-16}$, in scientific notation. The asterisks following the $\texttt{Pr(>|t|)}$ provide a visually accessible way of assessing whether the statistic met various $\alpha$ criterions. All these tests and measures indicate a very good fit and that the "null-hypothesis" (no correlation between the model parameters and the experimental data) can be safely rejected. 

The function also estimated the residual standard error $\sigma = 0.03066$ on $(N-M) = (200-2) = 198$ degrees of freedom, which is in a very good agreement with the actual value ($0.03$) used for the original data calculation.

Function \texttt{nls.lm} thus provided a complete information: (i) parameters, (ii) error estimations on the parameters, and (iii) statistical measures of goodness-of-fit. The main advantage of this professionally written function is, as we will show later, that it can be used to fit more complicated nonlinear models for large sets of experimental points and with initial guess values far from the optimal.

\subsection{Orthogonal Distance Regression}
\label{ODRPACK}
If the observed (experimental) data are subject to measurement error not only on the $y_i$'s, but also in the independent variable $x_i$'s, then the observed value $y_i$ satisfy
\begin{equation}
\label{eq:ODRconstraints}
y_i=f_i(x_i+\delta_i|\beta_1\ldots \beta_M)-\epsilon_i \quad \quad i=1,\ldots,N,
\end{equation}
where $\delta_i$ and $\epsilon_i$ are unknown errors for $x_i$ and $y_i$ variables, respectively.

The optimal parameters can be found by \textit{orthogonal distance regression} (ODR). If only $y$ is subject to experimental error, and $x$ is observed without error, then the parameters of such an model can be found using \textit{ordinary least square} procedures. 

The explicit ODR problem is to find a set of the parameters $\beta$ for which the sum of squares of $N$ orthogonal distances from the curve $f(x|\beta_1\ldots \beta_M)$ to the $N$ data points is minimized. This is accomplished by the minimization problem
\begin{equation}
\label{eq:ODR}
\min_{\beta,\delta,\epsilon} \sum_{i=1}^N(\epsilon_i^2+\delta_i^2)
\end{equation}
subject to constraints given in \ref{eq:ODRconstraints}. 

Formula \ref{eq:ODR} can be reformulated to
\begin{equation}
\label{eq:ODR}
\min_{\beta,\delta} \sum_{i=1}^N([f_i(x_i+\delta_i|\beta_1\ldots \beta_M)-y_i]^2+\delta_i^2),
\end{equation}
and then generalized to the weighted orthogonal distance regression problem 
\begin{equation}
\label{eq:ODRweighted}
\min_{\beta,\delta} \sum_{i=1}^N(w_{\epsilon_i}[f_i(x_i+\delta_i|\beta_1\ldots \beta_M)-y_i]^2+w_{\delta_i}\delta_i^2)
\end{equation}
by introducing the weights $w_{\epsilon_i}$ and  $w_{\delta_i}$, for $i=1,\ldots,N$, which are sets of non-negative real numbers. The weights can be used to compensate for instances when the data points have unequal precision, or when there are observations which should be excluded from the analysis. (The algorithm can be used as an ordinary least square regression by setting $w_{\delta_i}=0$.)

A numerically stable and efficient algorithm for solving ODR is given in
Boggs et al.\cite{boggs1987stable} and a detailed implementation, called \texttt{ODRPACK}, was published by Boggs\cite{boggs1992user} and his co-workers in 1992. The package, written in FORTRAN, allows a general weighting scheme, provide for finite difference derivatives, and contains extensive checking and report generating facilities. The algorithm is based on the trust region Levenberg-Marquardt algorithm and has a computational effort per step that is the same order as that required for ordinary least squares. An enhancement of \texttt{ODRPACK} is available from Netlib and has been downloaded and used many times by scientists, engineers, and practitioners around the world.

As an example we used \texttt{ODRPACK} implementation in Python package \texttt{scipy} to fit real flash data from austenitic stainless steel sample \texttt{SS93}, tested at $200$ $^\circ$C. The sample thickness was $L= 0.1572$ cm, and the radius $R=0.6365$ cm. Sample surface was irradiated with a coaxial laser beam (pulse duration $0.4$ ms) with radius, $0.590$ cm. The temperature response was measured using an IR detector, which was integrating signal from a concentric area of the opposite sample surface, with radius $0.470$ cm. The sampling frequency of the temperature signal was $15$ kHz.

The response for time interval from $-0.8$ s to $3.0$ s, is shown in Figure \ref{fig:SS93_200C0}. The heat pulse started at time zero, and as can be seen from Figure \ref{fig:SS93_200C003}, there was a sudden 'jump' in the detector signal, a disturbance most probably caused by an electro-mechanical interference. Most disturbed points were excluded and only data points collected within the interval from $0.015$ s  to $3.00$ s were analyzed.

\begin{marginfigure}
  \includegraphics[scale=0.3]{SS93_200C0.eps}
  \caption{Sample \texttt{SS93} temperature response as a function of time.}
  \label{fig:SS93_200C0}
\end{marginfigure}

\begin{marginfigure}
  \includegraphics[scale=0.5]{SS93_200C003.eps}
  \caption{Sample \texttt{SS93} temperature response detail.}
  \label{fig:SS93_200C003}
\end{marginfigure}
Two-dimensional model with heat losses, given in Equation xxx, was used to fit the experimental data. Uncertainties (weights) for the independent variable points (time) were set to zero, \texttt{sx = 0}, and for the dependent variable (temperature) to a constant value, \texttt{sy = 1}. Optimalized parameters were stored in vector $\textbf{Beta}= [T_0, T_m, a, B]$. The initial guess values of these four parameters were given in vector $\textbf{beta0} = [1.0, 2.5, 0.01, 0.002]$. \texttt{ODRPACK} offers a possibility to fix or freeze the parameters by setting vector \texttt{ifixb} value to zero, but in this case, none of our four parameters was fixed.     

\texttt{ODRPACK} can generate two different types of reports. The first is an \textit{error report}, and the second is a \textit{computation report}. Error reports identify incorrect user supplied information passed to \texttt{ODRPACK} that
prevents the computations from beginning. Computation reports are divided into three sections:
\begin{itemize}
\item the initial report,
\item the iteration report, and
\item the final report.
\end{itemize}
These reports are written into a single text file and contain all these important items, which were mentioned in the Introduction: (i) parameters, (ii) error estimations on the parameters, and (iii) a statistical measure of goodness-of-fit. It also contains residuals for all analyzed points and other details of optimization process.
 
 After running the following script:
%\begin{fullwidth}
\begin{lstlisting}[language=Python,basicstyle=\tiny]
# define the model
flash = odrpack.Model(f2DHL) # 2D model with heat losses

#  sx and sy are uncertainties for time and temperature points, resp.
mydata = odrpack.RealData(time, temperature, sx=0, sy=1.0) 
# define ODRPACK input parameters - beta0 are initial guess values
myodr = odrpack.ODR(mydata, flash, beta0=[1.0,2.5, 0.01, 0.002],  
                    iprint=6616, rptfile=filename, ifixb=[1,1,1,1], job=2 )             
                               
myoutput = myodr.run() # run ODRPACK
myoutput.pprint() # print a standard simple output

\end{lstlisting}
\texttt{ODRPACK} returned a standard simple summary:
\begin{lstlisting}[keywordstyle=\color{black},basicstyle=\tiny]
Beta: [ 0.97170796  2.61017082  0.04285324  0.01454185]
Beta Std Error: [  1.21868271e-04   1.17102307e-04   2.00794049e-06   2.44243085e-06]
Beta Covariance: [[  2.46615910e-03  -2.30675898e-03  -2.64860936e-05   3.18494162e-05]
                  [ -2.30675898e-03   2.27704017e-03   2.32581848e-05  -2.26646781e-05]
                  [ -2.64860936e-05   2.32581848e-05   6.69485945e-07  -5.70790868e-07]
                  [  3.18494162e-05  -2.26646781e-05  -5.70790868e-07   9.90568115e-07]]
Residual Variance: 6.022269820879102e-06
Inverse Condition : 0.004075970869871722
Reason(s) for Halting:
  Sum of squares convergence
\end{lstlisting}
%\end{fullwidth}
where the final optimal parameters [$T_0$ $T_m$ $a$ $B$], are listed in vector \texttt{Beta}. The parameters standard deviations are in \texttt{Beta Std Error}. Reconstructed theoretical curve, shown in Figure \ref{fig:ODR_OLS_SS93}, is on top of the experimental data. The optimal thermal diffusivity value $a= 0.0429$ cm$^2$/s is within 1\% of the literature value $0.0427$ cm$^2$/s for Stainless Steel at 200 $^\circ$C. 

\begin{marginfigure}
  \includegraphics[scale=0.5]{ODR_OLS_SS93.eps}
  \caption{Reconstructed response curve with its 95\% confidence interval.}
  \label{fig:ODR_OLS_SS93}
\end{marginfigure}
The noise level for this particular response curve is relatively very low, therefore it is hard to see how well are different portion of the curve fitted with the reconstructed theoretical curve. It is therefore better to inspect the residuals, plotted in Figure \ref{fig:ODR_OLS_SS93_res}, where the 95\% confidence interval is also shown as a pair of horizontal lines around zero. The confidence interval value was calculated as $2\sigma=2\times 0.002454=0.00491$, where the residual standard deviation, $\sigma$, is a square root of the \texttt{Residual Variance} calculated by \texttt{ODRPACK} (see the standard simple summary) from \textit{all data points} taken into the analysis. But, as we can see, the standard deviation and the average value for residuals taken from time interval  $2.0$ s to $2.1$ s, evidently differ from the standard deviation and the average value from $0.5$ s to $0.6$ s. 
\begin{marginfigure}
  \includegraphics[scale=0.5]{ODR_OLS_SS93_res.eps}
  \caption{Residuals with its 95\% confidence interval.}
  \label{fig:ODR_OLS_SS93_res}
\end{marginfigure} 
The actual distribution of residuals is best viewed using a two-dimensional histogram, shown in Figure \ref{fig:myhist2dnearest}, where the distribution density is shown in colors. The area of the graph from Figure \ref{fig:ODR_OLS_SS93_res}, is divided into small rectangular bins and each bin is depicted in a certain color, which depends on how many residuals points are inside the bin.
 
The residuals are relatively small, but they are not uniformly distributed around zero for different portions of the response curve. It means that either, the response curve signal was superimposed with a low frequency 'noise', or that the model is not adequate for fitting the curve. We made some simplifications, assuming for example that the heat loss coefficients are the same for all three sample surfaces, and that the pulse is instantaneous. All these can lead to non-uniformity in the distribution of residuals in time, as observed in our case. But in our case the residual distribution in the tested time interval fit quite well within $3$ or $4$ residual standard deviations $\sigma$, therefore the model inadequacy can be ruled out. 
 \begin{marginfigure}
 \includegraphics[scale=0.5]{myhist2dnearest.eps}
  \caption{Two-dimensional histogram of the residuals.}
  \label{fig:myhist2dnearest}
\end{marginfigure} 
The computation report, generated by  the \texttt{ODRPACK}, contains many other relevant details about the calculation, for example that the procedure fitted $44800$ experimental data points, finished after $7$ iterations, the total number of model function evaluations was $48$, the final weighted sum of squares was $0.269773599$, and since the error of the independent variable (time) was set to zero, the method was explicit OLS.

A good practice is to evaluate optimal parameters for different time intervals of the response curve, if the thermal diffusivity parameter remains approximately the same. It is clear from analysis of the theoretical temperature rise formula that in order to estimate heat losses, later parts (the tail) of the response curve should be included into the optimization. But, if too many tail points are included, then the rising part, which contains information about the thermal diffusivity, is relatively small, and the uncertainty in the thermal diffusivity estimation is therefore increased.

If, on the other hand, the tail portion of the response curve is relatively short, then the Biot number estimation error is increased, and this may also lead to an error in the thermal diffusivity estimation. Fortunately, all these problems can be easily detected by inspecting the standard deviations of the parameters and visually checking the graph of the uniformity of the residuals distribution in time.

 We will talk about these problems in more rigorous terms later, when we will discuss an optimal experiment design.


  