 % !TEX root = mybook.tex

\chapter{Multilayer Specimen}
\label{ch:Multilayers}
%\color{blue}
% text from Flash_Models.doc as 1/31/2017
(ADD LITERATURE!)
\section{Two-layer Model}

\subsection{Problem Formulation}

\begin{marginfigure}%
  \includegraphics{TwoLayersSpecimen.eps}
  \caption{Two layer specimen in cartesian coordinates.}
  \label{fig:TwoLayersSpecimen}
\end{marginfigure}
We will formulate the boundary value problem for temperatures $T_1$ and $T_2$ in the first and the second layer of a two-layer specimen (Figure \ref{fig:TwoLayersSpecimen})under simplified assumptions:
\begin{enumerate}[(a)]
\item	one dimensional heat flow (in \textbf{x} direction),
\item	an instantaneous heat pulse uniformly absorbed on the front surface,
\item	each layer being homogeneous and isotropic, 
\item	constant thermophysical properties of each layer,
\item	heat losses given by the heat loss coefficients $h_1$ and $h_2$ from the front and the rear surface, respectively,
\item	with the thermal contact resistance, $R$, between the layers,
\end{enumerate}
as
\begin{equation}
\label{eq:Twolayer1}
\frac{\partial^2 T_1}{\partial x^2} = \frac{1}{a_1}\frac{\partial T_1}{\partial t},\quad
0 < x < L_1,\quad t>0,
\end{equation}
\begin{equation}
\label{eq:Twolayer2}
\frac{\partial^2 T_2}{\partial x^2} = \frac{1}{a_2}\frac{\partial T_2}{\partial t},\quad
L_1 < x < L_1+L_2, \quad t>0,
\end{equation}

with boundary conditions

\begin{equation}
\label{eq:TwolayerRC1_0}
- k_1\frac{\partial T_1(0,t)}{\partial x} + h_1 T_1(0,t) = 0,
\end{equation}

\begin{equation}
\label{eq:TwolayerRC1_L1_L2}
k_2\frac{\partial T_2(L_1+L_2,t)}{\partial x} + h_2 T_2(L_1+L_2,t) = 0,
\end{equation}
the condition at the interface
\begin{equation}
\label{eq:TwolayerRC1_L1_L2}
\begin{split}
k_1\frac{\partial  T_1(L_1)}{\partial x}=k_2\frac{\partial  T_2(L_1)}{\partial x}\\
k_2\frac{\partial  T_2(L_1)}{\partial x} + \frac{1}{R}\big[T_2(L_1,t)-T_1(L_1,t)\big] = 0,
\end{split}
\end{equation}
and the initial condition
\begin{equation}
\label{eq:TwolayerRC_initial}
T_1(x,0) = T_2(x,0) = \frac{Q}{\rho_1c_1L_1+\rho_2c_2L_2}\delta(x),
\end{equation}
where $Q$ is the amount of heat  per unit area entering the front face of the specimen, $h_i$, $k_i$, $\rho_i$, $c_i$, $(i = 1,2)$, are the heat transfer coefficient, the thermal conductivity, the density and the specific heat of $i$-th layer, respectively. 

The thermal contact resistance $R$ in SI units is 
\begin{equation}
\label{eq:ThermalContactResistanceDimensions}
\big[R\big]=  \frac{\texttt{m}^2 \texttt{K}}{\texttt{W}}
\end{equation} and it describes the rate of heat transfer between the two layers.

\subsection{Temperature Response}

Temperature rise on rear face of the specimen after the pulse \citep{lee1977thermal} is  
\begin{equation}
\label{eq:TwoLayerTemperature}
T_2(L_1+L_2,t)= T_{max}\sum_{n=1}^{\infty‎} \Phi(\gamma_n)\exp{\big[-(\gamma_n \omega_1/\eta_2)^2 t\big]}
\end{equation}
where
\begin{equation}
\label{eq:TwoLayerTemperatureTmax}
T_{max} = \frac{Q}{\rho_1c_1L_1+\rho_2c_2L_2},
\end{equation}
\begin{equation}
\label{eq:TwoLayerTemperatureEtas}
\eta_i=L_i/\sqrt{\alpha_i},\quad  \eta_{ij}=\eta_i/\eta_j, \quad b_i = h_i L_i/\lambda_i, \quad i,j=1,2.
\end{equation}
\begin{equation}
\label{eq:TwoLayerTemperatureXis}
\begin{split}
H_{12}=(\rho_1 c_1 L_1)/(\rho_2 c_2 L_2),\quad R_c=R\rho_1 c_1 \sqrt{\alpha_1 \alpha_2 }/L_2,\\
\chi_1= H_{12}\eta_{21}+1, \quad  \chi_2= H_{12}\eta_{21}-1,\\
q = R_c/(\omega_1 \chi_1),\quad P=\chi_2/\chi_1, \quad b_{12}=b_1/b_2,\\
Y_1=b_2 (b_{12}/\eta_{12} +1),\quad Y_2=b_2 (b_{12}/\eta_{12}-1),\\
 U = \omega_2/\omega_1, \quad Z=b_2^2 (b_{12}/\eta_{12}).
\end{split}
\end{equation}

 $\gamma_n$ are positive roots of the equation
\begin{equation}
\label{eq:TwoLayerTemperatureGammas}
 f_0(\gamma)+f_L(\gamma)+f_R(\gamma)+f_{LR}(\gamma)=0,
\end{equation}
where
\begin{equation}
\label{eq:TwoLayerTemperaturefs}
\begin{split}
f_0(\gamma)=\sin(\gamma)+P\sin(U\gamma),\\
f_R(\gamma)=q\gamma\bigg[\cos(\gamma)-\cos(U\gamma)\bigg],\\
f_L(\gamma)=-\frac{\omega_1}{\gamma}\bigg[Y_1\cos(\gamma)-PY_2\cos(U\gamma)\\
+\frac{Z\omega_1}{\gamma}\bigg(\sin(\gamma)-P\sin(U\gamma)\bigg)\bigg],\\
f_{LR}(\gamma)=q\omega_1\bigg[Y_1\sin(\gamma)-Y_2\sin(U\gamma)\\
-\frac{Z\omega_1}{\gamma}\bigg(\cos(\gamma)+\cos(U\gamma)\bigg)\bigg].
\end{split}
\end{equation}

The function $\Phi(\gamma_n)$ is
\begin{equation}
\label{eq:TwoLayerTemperatureDgamas}
\Phi(\gamma_n )=\frac{2(1+PU)}{\Phi_0(\gamma_n)+\Phi_L(\gamma_n)+\Phi_R(\gamma_n)+\Phi_{LR}(\gamma_n)},
\end{equation}
where
\begin{equation}
\label{eq:TwoLayerTemperatureDfs}
\begin{split}
\Phi_0(\gamma_n)=\cos(\gamma_n)+PU\cos(U\gamma_n),\\
\Phi_R(\gamma_n)=q\bigg[\cos(\gamma_n)-\cos(U\gamma_n)\\
-\gamma_n\bigg(\sin(\gamma_n)-U\sin(U\gamma_n)\bigg)\bigg],\\
\Phi_L(\gamma_n)=\frac{\omega_1}{\gamma_n}\bigg[Y_1\sin(\gamma_n)+PUY_2\sin(U\gamma_n)\bigg]\\
+\frac{\omega_1}{\gamma_n^2} \bigg[Y_1\cos(\gamma_n)+PY_2\cos(U\gamma_n)\bigg]\\
+ \frac{Z\omega_1^2}{\gamma_n^3}\bigg[2\bigg(\sin(\gamma_n)-P\sin(U\gamma_n)\bigg)\\
- \gamma_n\bigg(\cos(\gamma_n)-PU\cos(U\gamma_n)\bigg)\bigg],\\	
\Phi_{LR}(\gamma_n)=\frac{qZ\omega_1^2}{\gamma_n^2}\bigg[\cos(\gamma_n)+\cos(U\gamma_n)\\
+\gamma_n\sin(\gamma_n)+U\sin(U\gamma_n)\bigg]\\
+q\omega_1 \bigg[Y_1\cos(\gamma_n)+UY_2\cos(U\gamma_n)\bigg].
\end{split}
\end{equation}
		
The thermal contact resistance will cause a discontinuity of temperature profile at the interface in the two layer sample. The temperature of the front (heated) layer will be higher than of the rear layer of the sample and the temperature 'jump' on the interface will be proportional to the magnitude of $R$. As we can see from Figure \ref{fig:2LayersTCRs}, where the response curves for a two layer specimen are given for different values of the thermal contact resistance $R$. As we can see, the transient part of response curves for bigger $R$ rises with a smaller slope than curves for a smaller $R$ value. The maximum temperature rise is reached at much longer times. Such a 'sluggish and delayed' response indicates the presence of the thermal contact resistance.
\begin{figure}%
  \includegraphics{2LayersTCRs.eps}
  \caption{Two layer specimen response curve for a different values of the thermal contact resistance $R$ between the layers. The curves were calculated using Equation \ref{eq:TwoLayerTemperature} for parameters: $L_1=0.02$ cm, $L_2=0.1$ cm, $a_1=0.02$ cm$^2$/s, $a_2=1.17$ cm$^2$/s, $\rho_1 = \rho_2=1$ g/cm$^3$, and $c_1=c_2=1$ J/(g K). The values for $R$ are in CGS units: cm$^2$K/W.}
  \label{fig:2LayersTCRs}
\end{figure}
% generated  2LayersWithTCR Pics.R (2/1/2017)
  
If the thermal properties of both layers are known, then the thermal contact resistance $R$ between them can be estimated by comparing the flash method thermogram with the theoretical solution given in Equation \ref{eq:TwoLayerTemperature}. 

Actual value of the thermal contact resistance $R$ depends on quality of the interface between the layers, external pressure, ambient atmosphere, temperature, and possibly other factors.  It is one of the most complex thermal parameters, difficult to predict theoretically and generally very hard to measure.

\section{Three-Layer Model}

\subsection{Problem Formulation}

The boundary value problem for temperatures $T_1$, $T_2$ and $T_3$ in the first, the second and the third layer of a three-layer specimen (Figure \ref{fig:ThreeLayersSpecimen}), is under simplified assumptions:
\begin{enumerate}[(a)]%for small alpha-characters within brackets.
\item	one dimensional heat flow (in \textbf{x} direction),
\item	an instantaneous heat pulse uniformly absorbed on the front surface,
\item	each layer being homogeneous and isotropic, 
\item	constant thermophysical properties of each layer,
\item	no heat losses from the specimen surfaces,
\item	no thermal contact resistance between the layers,
\end{enumerate}
formulated as
\begin{marginfigure}%
  \includegraphics{ThreeLayersSpecimen.eps}
  \caption{Three layer specimen in cartesian coordinates.}
  \label{fig:ThreeLayersSpecimen}
\end{marginfigure}
\begin{equation}
\label{eq:Threelayer1}
\frac{\partial^2 T_1}{\partial x^2} = \frac{1}{a_1}\frac{\partial T_1}{\partial t},\quad
0 < x < L_1,\quad t>0,
\end{equation}
\begin{equation}
\label{eq:Threelayer2}
\frac{\partial^2 T_2}{\partial x^2} = \frac{1}{a_2}\frac{\partial T_2}{\partial t},\quad
L_1 < x < L_1+L_2, \quad t>0,
\end{equation}

\begin{equation}
\label{eq:Threelayer3}
\frac{\partial^2 T_3}{\partial x^2} = \frac{1}{a_3}\frac{\partial T_3}{\partial t},\quad
L_1+L_2 < x < L_1+L_2+L_3, \quad t>0,
\end{equation}

with boundary conditions

\begin{equation}
\label{eq:Threelayer_0}
- k_1\frac{\partial T_1(0,t)}{\partial x} + h_1 T_1(0,t) = 0,
\end{equation}

\begin{equation}
\label{eq:Threelayer_L1_L2_L_3}
k_2\frac{\partial T_2(L_1+L_2+L3,t)}{\partial x} + h_2 T_2(L_1+L_2+L_3,t) = 0,
\end{equation}
the condition at the interfaces
\begin{equation}
\label{eq:Threelayer_L1_L2}
k_1\frac{\partial  T_1(L_1)}{\partial x}=k_2\frac{\partial  T_2(L_1)}{\partial x}
\end{equation}

\begin{equation}
\label{eq:Threelayer_L2_L3}
k_2\frac{\partial  T_2(L_1+L_2)}{\partial x}=k_3\frac{\partial  T_3(L_1+L_2)}{\partial x}
\end{equation}

and the initial condition
\begin{equation}
\label{eq:ThreelayerRC_initial}
\begin{split}
T_1(x,0) = T_2(x,0) = \\
T_3(x,0)=\frac{Q}{\rho_1c_1L_1+\rho_2c_2L_2++\rho_2c_2L_3}\delta(x),
\end{split}
\end{equation}
where $Q$ is the amount of heat per unit area, and  $k_i$, $\rho_i$, $c_i$, $(i = 1,2,3)$, are the thermal conductivity, the density and the specific heat of $i$-th layer, respectively. 

\subsection{Temperature Response}

Under the above mentioned assumptions, the temperature rise on rear face of a three-layer specimen after the pulse \citep{lee1975thermal} is 
\begin{equation}
\label{eq:ThreeLayerTemperature}
T_3(L_1+L_2+L_3, t) = T_{max}\sum_{n=1}^{\infty‎}\Psi(\gamma_n)\exp{\big[-(\gamma_n/\eta_3)^2 t\big]}
\end{equation}
where
\begin{equation}
\label{eq:TwoLayerTemperatureTmax}
T_{max} = \frac{Q}{\rho_1c_1L_1+\rho_2c_2L_2+\rho_3c_3L_3},
\end{equation}
\begin{equation}
\label{eq:ThreeLayerTemperaturePsi}
\Psi(\gamma_n) = \sum_{i=1}^3\omega_i\chi_i\bigg/\sum_{i=1}^4\omega_i\chi_i\cos(\omega_i\gamma_n),
\end{equation}
\begin{equation}
\label{eq:ThreeLayerTemperatureChis}
\begin{split}
\chi_1 = H_{13}\eta_{31}+H_{12}\eta_{21}+H_{23}\eta_{32}+1,\\
\chi_2 = H_{13}\eta_{31}-H_{12}\eta_{21}+H_{23}\eta_{32}-1,\\
\chi_3 = H_{13}\eta_{31}-H_{12}\eta_{21}-H_{23}\eta_{32}+1,\\
\chi_4 = H_{13}\eta_{31}+H_{12}\eta_{21}-H_{23}\eta_{32}-1,
\end{split}
\end{equation}

\begin{equation}
\label{eq:ThreeLayerTemperatureOmegas}
\begin{split}
\omega_1=\eta_{13}+\eta_{23}+1,\\
\omega_2=\eta_{13}+\eta_{23}-1,\\
\omega_3=\eta_{13}-\eta_{23}+1,\\
\omega_4=\eta_{13}-\eta_{23}-1,
\end{split}
\end{equation}
and $\gamma_n$ are positive roots of the following characteristic equation
\begin{equation}
\label{eq:ThreeLayerTemperatureGammas}
\sum_{i=1}^4\omega_i\chi_i\cos(\omega_i\gamma)=0.
\end{equation}

Volumetric heat capacity $H_i$ and square root heat diffusion time $\eta_i$ of the $i$-th layer, are defined as
\begin{equation}
H_i=A\rho_ic_iL_i, \quad \eta_i=L_i\big/\sqrt{\alpha_i}, \quad i=1,2,3,
\end{equation}
where $A$ is the cross sectional area of the specimen.

Relative volumetric heat capacity $H_{ij}$ and relative square root heat diffusion time $\eta_{ij}$, are defined as
\begin{equation}
H_{ij}=H_i\big/H_j, \quad \eta_{ij}=\eta_i\big/\eta_j, \quad i,j=1,2,3.
\end{equation}

%While the temperature rise for two layer sample (Equation \ref{eq:TwoLayerTemperature}) is symmetrical and do not change with reversing layer's order, the formula for three layer samples (Equation \ref{eq:ThreeLayerTemperature}) is not. The temperature response will change if we change the layer's order. This fact is very important for an optimal design of a three layer experiment. % NOT TRUE . See multilayers_tests.R !!
\begin{figure}%
  \includegraphics{3LayersA3s.eps}
  \caption{Three layer specimen response curves for a different values of the thermal diffusivity of the rear layer ($a_3$).}
  \label{fig:3LayersA3s}
\end{figure}
% generated  3LayersPics.R (2/1/2017)
An example of temperature responses for a three-layer specimen for different values of the thermal diffusivity of the third (rear) layer, are given in Figure \ref{fig:3LayersA3s}. The curves were generated using Equation \ref{eq:ThreeLayerTemperature} for parameters of $i$-the layer: the thickness $L_i =(0.02,0.04,0.05)$ cm$^2$/s, the density $\rho_i=(1,1.5,2)$ g/cm$^3$, and the specifit heat $c_i=(1,0.8,0.5)$ J/(g K), for $i=1,2,3$. Thermal diffusivity of the first layer is $a_1 = 0.02$ cm$^2$/s, the second layer, $a_2=1.17$ cm$^2$/s, and there are five different values for the third layer: $a_3 = (0.03, 0.05, 0.2, 0.4, 1.17)$ cm$^2$/s. As expected, the response curves are gradually more and more delayed as the value of the thermal diffusivity of the third layer is getting smaller.  

Analytical models for multi-layer specimens contain complicated mathematical formulas of infinite sums and a need to calculate roots of higher transcendental equations; therefore only simpler experimental conditions can be effectively solved within an acceptable level of uncertainty of calculated parameters. This is why we usually use only one dimensional heat flow models and no contact resistance, no heat losses, in case of the three layer specimen model. All these additional influences can be taken into account by using more realistic numerical models, but due to sensitivity problems related to parameter estimation procedures, the actual benefit of using those more complicated models is often questionable.

\subsection{Symmetry Considerations}

One of important analytical properties of solutions of heat conduction equation is \textit{symmetry}. This can be expressed mathematically simply as:
\begin{equation}
\label{eq:Symmetry}
T(\vec{r},\vec{s},t)=T(\vec{s},\vec{r},t),
\end{equation}
 where $\vec{r}$ is the location where temperature is measured and $\vec{s}$ is the location of the heat source.\sidenote{This property follows from the theory of Green functions -- see for example \citep{hazewinkel2013encyclopaedia}.}
 
  Practically it means that all the formulas for temperature distributions in the flash method experiments remain the same if we exchange front and rear sides of specimens and replace the heat source location with the sensor (viewed area) location and vice versa. 
  
  This property has important implications in case of a one layer specimen. Two-dimensional formulas  - as for example \ref{eq:AxialRadialComponentsDelta} or \ref{eq:AxialRadialPlaneExp} - are also symmetrical to a mutual exchange of heat source and sensor locations. It is usually more convenient for an experimenter to modify the shape of the viewing area on a detector side, than the shape of the heat source, therefore if we want to minimize negative effects of nonuniform heating of specimen, then increasing the viewing area (to say $97\%$ of the specimen rear side area) will practically eliminate possible distortions of the response curve, even in a case of extremely nonuniform heating from a point heat source located anywhere on the specimen front surface. 
  
  Also, the in-plane thermal diffusivity experiment design can be effectively optimized using the symmetry property of formula \ref{eq:InplaneFormula}, where viewed and heat source areas could be mutually exchanged without affecting the response curve. 
  
   Due to the symmetry, the formulas for temperature responses of two- and three-layer specimen do not change with reversing layer's order, e.g. from 1-2, to 2-1, or from 1-2-3 to 3-2-1. Irradiating the front or the rear side of a multi-layer specimen will in theory yield the same results for the thermal parameters. It is therefore practical to irradiate the layer which is made of a material more resilient to a thermal shock coming from the heat source.   
  
%\bigskip
\begin{table}
\caption{ Three-layer specimen parameters.}
\label{tab:3ReversedLayersParams}
\begin{center}
\footnotesize
\begin{tabular}{lllll}
\toprule
 Layer & $L$ & $a$ & $c$ & $\rho$\\
\midrule
No.& cm & cm$^2$s$^{-1}$& J g$^{-1}$ K$^{-1}$ &g cm$^{-3}$ \\
\midrule
\addlinespace
1 & 0.04 & 0.1 & 1.0 & 1.0 \\
2 & 0.05 & 0.2 & 2.0 & 5.0 \\
3 & 0.10 & 0.3 & 3.0 & 9.0 \\
\bottomrule
\end{tabular}
\end{center}
\end{table}
The temperature response curve shape for a three-layer specimen will in general change if we change the layer's order \textit{within} the specimen the way that the rear, or the front layer, will be in the middle (for example, a change from "1-2-3", to "2-1-3").
\begin{marginfigure}%
  \includegraphics{3ReversedLayers.eps}
  \caption{Response curves for a three layer specimen with different order of layers.}
  \label{fig:3ReversedLayers}
\end{marginfigure}  
% created using 3LreversePics.R
An example is in Figure \ref{fig:3ReversedLayers}, where the response curve of a three-layer specimen, calculated using Equation \ref{eq:ThreeLayerTemperature} with parameters given in Table \ref{tab:3ReversedLayersParams}, is compared with the response curve of the same specimen in which the lowest conductive first layer was swapped with the second layer. Response curve "2-1-3" is significantly lagging the curve with the original order "1-2-3". It takes only 43.2 ms for the "1-2-3" specimen to reach 95$\%$ of the a new steady state value, while for specimen "2-1-3", it takes 541.8 ms. For this particular ratio of transient temperature rise, the later specimen, where two neighboring layers were simply swapped, is 13 times slower. 

This fact is very important for an optimal design of a three layer experiment and has also many practical implications. If the transient time is important (a fire protection clothing), then the low conductive layer should be sandwiched between two higher conductive layers. Such three-layer composite will resist sudden increase of temperature longer as if the low conductive layer was either on front, or the rear side of the composite. 

It is important to note that the steady state heat flow through a three-layer composite will be the same regardless of the order of layers. Only a transient behavior can be affected by changing the layers order.
