% !TEX root = mybook.tex

\section{Finite Fourier Transform}
\label{ch:FiniteFourier}

Integral transforms used in cartesian coordinate systems are usually called Fourier transforms because they are derived with Fourier expansion of an arbitrary function in a given interval. In this section we examine the Fourier transform and the corresponding inverse formula for a finite region.
We evaluate the kernels and the eigenvalues for boundary conditions of the third kind at both boundaries. These general relations will be used to evaluate the kernels and eigenvalues for other combinations of boundary conditions. 

We will discuss a boundary value problem
\begin{equation}
\label{eq:1D_HCE}
\frac{\partial^2 T(x,t)}{\partial x^2} = \frac{\partial T(x,t)}{\partial t}, \quad x\in(0,1), \quad t>0,
\end{equation} 
with boundary conditions of the third kind on both $x=0$ and $x=1$
\begin{equation}
\label{eq:FFtbc_0}
-\frac{\partial T(0,t)}{\partial x} + H_0T(0,t) = 0,
\end{equation}
\begin{equation}
\label{eq:FFtbc_1}
\frac{\partial  T(1,t)}{\partial x} + H_1T(1,t) = 0,
\end{equation}
and initial condition
\begin{equation}
T(x,0)=G(x),
\end{equation}
where $H_0=Lh_0/k$ and $H_1=Lh_1/k$, are the Biot numbers for the respective boundaries, and $x$ and $t$ are dimensionless space variable and dimensionless time, defined as $x=x'/L$ and  $t=at'/L^2$.

\subsection{Kernel}
 Following general methodology, described in Chapter \ref{ch:IntegralTransforms}, we will first find a solution of an auxiliary problem (Sturm-Liouville)
 \begin{equation}
\label{eq:SturmLiouvilleXi}
\Phi^{''}(x)  + \beta^2 \Phi(x)= 0, \quad x \in (0,1)
\end{equation}
\begin{equation}
\label{eq:Phi_a}
-\Phi^{'}(0) + H_0\Phi(0) = 0,
\end{equation}
\begin{equation}
\label{eq:Phi_b}
\Phi^{'}(1) + H_1\Phi(1) = 0. 
\end{equation}


If $\Phi_m(x)=\Phi(\beta_m,x)$ are eigenfunction and $\beta_m$ are eigenvalues of the problem \ref{eq:SturmLiouvilleXi} - \ref{eq:Phi_b}, then the kernel of transformation over $x$ is
\begin{equation}
\label{eq:KernelPhi}
K(\beta_m,x)=\frac{\Phi_m(x)}{\sqrt{N_m}},
\end{equation}
where the norm is calculated from 
\begin{equation}
\label{eq:NormPhi}
N_m^2=\int_0^1 \Phi_m^2(x)dx.
\end{equation}
The general solution of Eq. \ref{eq:SturmLiouvilleXi} is 
\begin{equation}
\label{eq:GeneralSolutionXi}
\Phi(x)= A\cos(\beta_m x) + B\sin(\beta_m x). 
\end{equation}
Constants $A$ and $B$, as well as the eigenvalues $\beta_m$, will be calculated using the boundary conditions \ref{eq:Phi_a} and \ref{eq:Phi_b}. 
If we substitute the the general solution into the condition  \ref{eq:Phi_a}, then we have
\begin{equation}
 \label{eq:RatioAB}
 \frac{B}{A}=\frac{H_0}{\beta_m}.
 \end{equation} 
 From the boundary condition \ref{eq:Phi_b} we get
a transcendental equation for the eigenvalues $\beta_m$ in the form
  \begin{equation} 
  \label{eq:betatransc} 
  	\tan\beta = \frac{\beta(H_0+H_1)}{\beta^2-H_0H_1},
  \end{equation} 

Now, using the Eq. \ref{eq:RatioAB}, the eigenfunctions $\Phi_m(x)$ are
\begin{equation}
\label{eq:SolutionXi}
\Phi_m(x)= \beta_m\cos(\beta_m x) + H_0\sin(\beta_m x). 
\end{equation}

The norm $N_m^2$ can be calculated as follows:

By inserting eigenfunctions \ref{eq:SolutionXi} into equation \ref{eq:NormPhi}, and taking into account \ref{eq:SturmLiouvilleXi}, we get
\begin{equation}
\label{eq:NsquareL}
N_m^2=\int_0^1\Phi_m^2dx=-\frac{1}{\beta_m^2}\int_0^1\Phi_m\Phi_m^{''}dx,
\end{equation}
where for a sake of simplicity we used $\Phi_m$ instead of $\Phi_m(x)$, and $\Phi_m^{''}$ instead of $\Phi_m^{''}(x)$.

Integrating \ref{eq:NsquareL} per-partes we have
\begin{equation}
\label{eq:NsquareLperpartes}
N_m^2=-\frac{1}{\beta_m^2}\int_0^1\Phi_m\Phi_m^{''}dx=-\frac{1}{\beta_m^2}\big[\Phi_m\Phi_m^{'}\big]_0^1+\frac{1}{\beta_m^2}\int_0^1\Phi_m^{'2}dx.
\end{equation}

From \ref{eq:SolutionXi} we can calculate $\Phi_m^{'}$ as
\begin{equation}
\label{eq:derPhi}
\frac{1}{\beta_m}\Phi_m^{'}=-\beta_m\sin(\beta_m x)+H_0\cos(\beta_m x).
\end{equation}
Then, by combining \ref{eq:SolutionXi} and \ref{eq:derPhi} we have for
\begin{equation}
\label{eq:derPhi2}
\begin{split}
\Phi_m^2+\frac{1}{\beta_m^2}\Phi_m^{'2}=\\
=\beta_m^2\cos^2(\beta_m x) + H_0^2\sin^2(\beta_m x)+2\beta_m H_0\cos(\beta_m x)\sin(\beta_m x)\\
+\beta_m^2\sin^2(\beta_m x) + H_0^2\cos^2(\beta_m x)-2\beta_m H_0\cos(\beta_m x)\sin(\beta_m x)\\
=\beta_m^2+H_0^2.
\end{split}
\end{equation}

Integrate both sides of \ref{eq:derPhi2} from $0$ to $1$
\begin{equation}
\int_0^1\Phi_m^2dx+\frac{1}{\beta_m^2}\int_0^1\Phi_m^{'2}dx= \beta_m^2+H_0^2,
\end{equation}
and we can see that
\begin{equation}
\label{eq:second Nsquare}
\int_0^1\Phi_m^2dx = N_m^2= \beta_m^2+H_0^2 - \frac{1}{\beta_m^2}\int_0^1\Phi_m^{'2}dx.
\end{equation}

Now add equations \ref{eq:NsquareLperpartes} and \ref{eq:second Nsquare} to get
\begin{equation}
\label{eq:second 2Nsquare}
2N_m^2=\beta_m^2+H_0^2 - \frac{1}{\beta_m^2}\big[\Phi_m\Phi_m^{'}]_0^1.
\end{equation}

Term $\big[\Phi_m\Phi_m^{'}]_0^1$ will be calculated from boundary conditions \ref{eq:Phi_a} and \ref{eq:Phi_b}. 

From \ref{eq:derPhi2}, for $x=0$, we have 
\begin{equation}
\label{eq:Phi0}
\begin{split}
\Phi_m^2(0)+\frac{1}{\beta_m^2}\Phi_m^{'2}(0)=\beta_m^2+H_0^2,\\
\Phi_m^2(0)+\frac{1}{\beta_m^2}H_0^2\Phi_m^{2}(0)=\beta_m^2+H_0^2,\\
\Phi_m^2(0)=\frac{\beta_m^2+H_0^2}{1+H_0^2/\beta_m^2},
\end{split}
\end{equation}
where we used \ref{eq:Phi_a}.

Analogically, using \ref{eq:Phi_b} we get 
\begin{equation}
\label{eq:Phi1}
\Phi_m^2(1)=\frac{\beta_m^2+H_0^2}{1+H_1^2/\beta_m^2}.
\end{equation}

Combining \ref{eq:Phi0} and \ref{eq:Phi1} with the boundary conditions \ref{eq:Phi_a} and \ref{eq:Phi_b}, term $\big[\Phi_m\Phi_m^{'}]_0^1$ is
\begin{equation}
\begin{split}
 \big[\Phi_m\Phi_m^{'}]_0^1=\Phi_m(1)\Phi_m^{'}(1)-\Phi_m(0)\Phi_m^{'}(0)\\
 =-H_1\Phi_m^2(1)-H_0\Phi_m^2(0)\\
 =-H_1\frac{\beta_m^2+H_0^2}{1+H_1^2/\beta_m^2}-H_0\frac{\beta_m^2+H_0^2}{1+H_0^2/\beta_m^2},
\end{split}
\end{equation} 
which can be simplified as
\begin{equation}
\label{eq:PhiPhi01}
 \big[\Phi_m\Phi_m^{'}]_0^1=-\beta_m^2\bigg[H_1\bigg( \frac{\beta_m^2+H_0^2}{\beta_m^2+H_1^2}\bigg) + H_0\bigg].
\end{equation} 

Finally, if we substitute \ref{eq:PhiPhi01} into \ref{eq:second 2Nsquare}
\begin{equation}
2N_m^2=\beta_m^2+H_0^2 + H_1\bigg( \frac{\beta_m^2+H_0^2}{\beta_m^2+H_1^2}\bigg) + H_0,
\end{equation}
and we have
\begin{equation}
\label{eq:second 2NsquareFinal}
N_m^2 = \frac{(\beta_m^2+H_0^2)\bigg(1 +\frac{H_1}{\beta_m^2+H_1^2}\bigg)+H_0}{2}.
\end{equation}

The kernel $K(\beta_m,x)$ of the Finite Fourier Transform of Eq. \ref{eq:GeneralFormulaHCE}, with the boundary conditions \ref{eq:FFtbc_0} and \ref{eq:FFtbc_1}, is
\begin{equation}
\label{eq:KernelFiniteFT}
K(\beta_m,x) = \sqrt{2} \frac{\beta_m\cos(\beta_m x) + H_0\sin(\beta_m x)}{\bigg[(\beta_m^2+H_0^2)\bigg(1 +\frac{H_1}{\beta_m^2+H_1^2}\bigg)+H_0\bigg]^{1/2}},
\end{equation}
where $\beta_m, m = 1,2,\ldots $, are positive roots of the transcendental equation \ref{eq:betatransc}.

The kernel given in equation \ref{eq:KernelFiniteFT} was derived for the boundary conditions of the third kind for both $x=0$ and $x=1$. Boundary condition of the first and of the second kind can also be calculated using the same kernel. All nine combinations of the boundary conditions of the first, second and third kind, are easily obtained from Eqs. \ref{eq:KernelFiniteFT} and  \ref{eq:betatransc} by choosing the values of $H_0$ and $H_1$ as zero, finite or infinite.

If, for example, we have at $x=0$ the boundary condition of the first kind (zero temperature), and at  $x=1$ we have the boundary condition of the third kind, then we have to set $H_0=\infty$ and the boundary value problem has to be solved using the kernel   
\begin{equation}
\label{eq: KernelFiniteFTIand III}
K(\beta_m,x) = \sqrt{2}\bigg[\frac{\beta_m^2+H_1^2}{\beta_m^2+H_1^2+H_1} \bigg]^{\frac{1}{2}}\sin(\beta_m x),
\end{equation}
where $\beta_m$ are positive roots of the equation
  \begin{equation} 
  \label{eq:betatranscIandIII} 
  	\beta \cot \beta = -H_1.
  \end{equation} 
  
If the boundary conditions for both $x=0$ and $x=1$ are of the second kind (zero heat fluxes), then both $H_0$ and $H_1$ have to be set to zero, and the kernel of the finite Fourier transform is
\begin{equation}
\label{eq: KernelFiniteFTIIandII}
K(\beta_m,x) = \sqrt{2}\cos(\beta_m x),
\end{equation}
where $\beta_m$ are non-negative roots of the equation
  \begin{equation*}  
  	\sin \beta = 0,
  \end{equation*} 
i.e.
  \begin{equation*}  
  	\beta_m = \pi m, \quad \text{for }\quad m=0,1,2\ldots.
  \end{equation*} 
For $\beta_0=0$, the term  $\sqrt{2}$ in the kernel given in Eq. \ref{eq: KernelFiniteFTIIandII}, have to be replaced with $1$ .

The representation of an arbitrary function $T(x)$, as given above, is valid if function $T(x)$ satisfies \textit{Dirichlet conditions} at each point of the interval $x \in (0,1)$ where the function is continuous\cite{sneddon1951fourier}. We say that the function $T(x)$ satisfies the Dirichlet's conditions in the interval $x \in (0,1)$ if
\begin{enumerate}[(a)]
\item{$T(x)$ has only a finite number of maxima and minima on $x \in (0,1)$},
\item{$T(x)$ has only a finite number of discontinuities on $x \in(0,1)$ 
and no infinite discontinuities}.
\end{enumerate}

\subsection{Example}

 Now, if we multiply both sides of Eq. \ref{eq:1D_HCE} with the kernel $K(\beta_m, x)$ from \ref{eq:KernelFiniteFT} and integrate both sides for $x$ from $0$ to $1$, we will get
 \begin{equation}
\label{eq:Transformed1DHCE}
-\beta_m^2\bar{T} = \frac{d\bar{T}}{dt}, \quad t>0,
\end{equation} 
with the initial condition 
\begin{equation}
\bar{T}=\bar{G},\quad t=0,
\end{equation}
where 
\begin{equation}
\bar{T}(\beta_m,t) =\int_0^1 K(\beta_m,x') T(x',t)dx',
\end{equation}
The solution of \ref{eq:Transformed1DHCE} is
\begin{equation}
\bar{T}(\beta_m,t)=\bar{G}e^{-\beta_m^2t}.
\end{equation}

Temperature $T(x,t)$ can then be calculated using the inversion formula \ref{eq:FiniteIntInversionT} as
 \begin{equation}
 T(x,t)=C\sum_{m=1}^\infty K_m(\beta_m,x)\bar{G}e^{-\beta_m^2t}.
 \end{equation}
If, for example, we have an instantaneous heat source at $x'$, $0<x'<1$ so
\begin{equation}
G(x)=\delta(x-x'), \quad t=0
\end{equation}
then
\begin{equation}
\bar{G}=\int_0^1 K(\beta_m,x') \delta(x-x')dx'=K(\beta_m,x')
\end{equation}
and the temperature is
\begin{equation}
\begin{split}
 T(x,t)=\sum_{m=1}^\infty K(\beta_m,x)K(\beta_m,x')e^{-\beta_m^2t} \\
 =2\sum_{m=1}^\infty\frac{[\beta_m\cos(\beta_m x) + H_0\sin(\beta_m x)][\beta_m\cos(\beta_m x') + H_0\sin(\beta_m x')]}{(\beta_m^2+H_0^2)\bigg(1 +\frac{H_1}{\beta_m^2+H_1^2}\bigg)+H_0}e^{-\beta_m^2t}.
\end{split}
\end{equation}