% !TEX root = mybook.tex

\chapter{Three Points (3P) Method}
\label{3P Method}

\section{Introduction}

Thermal diffusivity measurements, using the flash method, became the dominant path for determining thermal conductivity of many types of solid materials over the last 50 years. Practical considerations concerning the nature of the sample materials, however, do limit the use of the flash technique.

Thermal conductivity of thermal insulations is usually measured using steady state methods, e.g. guarded hot plate (GHP) method \cite{standard2010c177}, heat flow meter technique\cite{standard2010c518}\cite{standard1991thermal}, etc. These methods are well established, highly standardized and reasonably precise in temperature range from -50 $^{\circ}$C up to 600 $^{\circ}$C. Obvious disadvantages of the steady state methods are large samples and long measurement times (sometimes days). Rapid transient methods (e.g. flash method, hot wire method, transient hot strip, or transient hot disc method \cite{gustafsson1991transient}, etc.), which in recent fifty years almost completely replaced the older steady state methods and became the dominant path for determining thermal conductivity of many types of solid materials, are seldom used for the thermal insulations, despite the fact that they use much smaller samples, the measurement takes only minutes, and they can be applied to a broader range of temperatures. Application of the transient methods to the thermal insulations is hampered by multi-component heat transfer and structural complexity and diversity of today commonly used thermal insulations. Poorly conducting transparent non-homogeneous samples with ill-defined surfaces, such as loosely packed fibrous materials, porous insulations, aerogels, etc., still present real challenges for experimenters.

Need for reliable thermophysical data of high temperature insulation materials in 60-ties and 70-ties resulted in developing and using the transient thermal diffusivity methods for the thermal conductivity determination of thermal insulations. First, the flash method was modified by substituting step heating for flash heating, i.e. replacing the laser with a lamp or film heater. In the step-heat technique, presented by Bittle and Taylor\cite{bittle1985thermal}, a constant heat flux condition was imposed on the front surface of the sample and the rear surface was considered to be adiabatic. The thermal diffusivity was calculated from collected temperature-time data and the corresponding solution of the heat equation using parameter estimation methods. The samples used in the method were relatively larger than the laser flash samples, but small compared to guarded hot plate samples.
 
 The step-heat method was later modified for fibrous insulations by Rook and Taylor\cite{rooke1988transient}. The constant heat flux condition on the front surface and the adiabatic condition on the rear surface were replaced by measured time-temperature curves. The sample temperatures were measured at the front and rear face of the specimen and in one or two known locations within the sample. The sample, of the size of the step heat sample, was sandwiched between two thin metal plates of diameter equal to the sample diameter. The plates provided sample support, absorb light used to heat up the sample and secured the thermocouple's position and contact with the sample material. The temperature-time data from the internal thermocouples were then used to calculate the thermal diffusivity of the sample using parameter estimation method. Boundary temperatures data were used to calculate the temperature distribution in the sample using one dimensional Crank-Nicholson finite difference scheme.  

The number of thermocouples used in the modified step heat method was later reduced to three and the apparatus\cite{rooke1988transient} was redesigned for measurements in vacuum or special atmospheres. The procedure of the thermal diffusivity measurement was described by Taylor\cite{taylor1993diffusivity}. Digital multi-meter in combination with PC was used for temperature reading and recording. Two incandescent lamps were used for the radiant heating of sample. This significantly improved the accuracy and reliability of the results, especially at higher temperatures\cite{gembarovic2007method}. 

%This paper summarizes further development and improvements of the method for use in high temperature thermal diffusivity measurement of thermal insulation.  In order to differentiate between the earlier versions, we will call it Three Points (3P) method in this paper. Some of the 3P method results are compared with those from the steady state methods.

\section{Theory}

Temperature distribution $T(z,t)$, $0\leq z \leq L$, $t>0$, in a homogeneous and isotropic sample (depicted in Figure \ref{fig:3Psample}), with constant thermophysical properties, initially at temperature $T_0$, is a solution of one-dimensional heat conduction problem with boundary conditions of the first kind:
\begin{marginfigure}
  \includegraphics[scale=0.9]{graphics/3Psample.eps}%
  \caption{Sample in the 3P method.}
  \label{fig:3Psample}%
\end{marginfigure} 
 \begin{equation}
\label{eq:HCE 3P}
 \frac{\partial^2 T}{\partial z^2} = \frac{1}{a}\frac{\partial T}{\partial t},
\end{equation}
\begin{equation}
\label{eq:3Pboundary1}
T(0,t) = \phi_1(t),
\end{equation}
\begin{equation}
\label{eq:3Pboundary2}
T(L,t) = \phi_2(t), 
\end{equation}
and initial condition
\begin{equation}
\label{eq:3Pinitial}
T(z,0)=T_0,  
\end{equation}
where $a$  is the effective thermal diffusivity and $\phi_j(t)$, $j = 1, 2, 3,$ are the temperature responses of j-th thermocouple. 
Analytical solution of \ref{eq:HCE 3P} with conditions \ref{eq:3Pboundary1} - \ref{eq:3Pinitial} is given by a formula\cite{CJp103}  
\begin{equation}
\label{eq:3PSolution}
\begin{split}
T(z,t)=T_0+\frac{2a\pi}{L^2}\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎} n\sin \frac{n\pi z}{L} \\
\times \int_0^t e^{-n^2\pi^2a(t-t')/L^2}\bigg[\phi_1(t')-(-1)^n\phi_2(t')\bigg]dt'.
\end{split}
\end{equation} 	
Since the analytical solution, given by \ref{eq:3PSolution}, is not practical for numerical calculations, our Damped Heat Wave (DHW) algorithm (described in subsection \ref{DHW Algorithm}) is used to calculate the temperature within the sample. DHW algorithm is more precise and more stable than the finite difference scheme used in the modified step heat method. In the DHW algorithm, a finite homogeneous medium of the thickness $L$ is divided into $N$ equal slabs of the thickness $\Delta l = L/N$ . These slabs are replaced by a perfect conductor of the same heat capacity separated by the thermal resistance $\Delta l/k$, (where $k$  is the thermal conductivity of the medium), so the temperature within a slab at any given time is constant. Heat propagates through the medium due to a temperature difference between the slabs. A certain portion (given by the inner transfer coefficient, $\xi$) of the excessive heat energy moves from one slab to the next one, thus lowering the temperature difference between the two neighbor slabs. This redistribution process (the damped heat wave) starts from the left boundary slab and marches in space from one pair of slabs to another. When the wave reaches the boundary of the medium, it bounces back and moves in the opposite direction in a perpetual manner. 

The inner transfer coefficient $\xi$  is a dimensionless quantity given by
\begin{equation}
\label{InnerHeatTranferCoeff}
\xi=\frac{\mu}{\mu+2},
\end{equation}
where
\begin{equation}
\label{MeshRatio}
\mu=\frac{a\Delta t}{\Delta l^2},
\end{equation}
is the mesh ratio and $\Delta t$ is time step. 

The effective thermal diffusivity of the sample is calculated from the interior position (thermocouple at  $z=L_3$) temperature data $\phi_3(t)$ which are compared to the solution of the direct heat conduction problem $T(L_3, t)$. A software package ODRPACK (described in subsection \ref{ODRPACK}) is used for the thermal diffusivity calculation using an ordinary least square method. 

The thermal conductivity $k$ of the sample material is then calculated using $k = a \rho c_p$, where $\rho$ is the bulk density, and $c_p$ is the specific heat at constant pressure.

\section{Experiment}

\subsection{Apparatus Description}

The 3P method apparatus consists of vacuum chamber, which is housing furnace and sample holder. A 300 W quartz-iodide, tungsten element bulb, mounted within an aluminum parabolic reflector, is is used as the heat source. 

The experimental data are collected using multi-channel A/D converter with 12-bit resolution and direct thermocouple inputs. The sample temperature is measured using Chromel-Alumel thermocouples (thickness $7.62\times10^{-2}$ mm). The experiment is controlled and data are collected automatically using Labview$^{TM}$ software.

Cylindrical sample of 50 mm in diameter and thickness ranging from $2$ mm to $15$ mm is sandwiched between two thin metal plates (see Figure \ref{fig:3Psample}). The plates support the sample and prevent light from penetrating into the sample material. Thin spacer screens inserted between the sample and metal plates insulate the sample material and the surface thermocouples from the metal plates.
 
\subsection{Procedures}
It has to be noted that $\phi_1(t)$  and  $\phi_2(t)$ at the sample boundaries in Equations \ref{eq:3Pboundary1} and \ref{eq:3Pboundary2} are arbitrary functions of time. From the experimental point of view it means that we do not need to generate a particular shape of heat pulse. Any thermal disturbance, (arbitrary heat pulse), strong enough to generate a transient state reliably detected by the thermocouples in the sample can be utilized for the thermal diffusivity determination. The disturbance should not be too strong, in order not to overheat the sample and to keep valid the assumption of constant thermal properties during the test. Contrary to the step heating method and the flash method, there is no need for the disturbance to start at the particular known time instant and there is also no need for the sample surfaces to be thermally insulated from the surrounding. The lamp can be turn on and off any time and the only requirement for the data acquisition system is to record the sample temperatures with no relative delays between the thermocouples.   

From the nature of the heat conduction formulation it follows that the thermocouples 1 and 2 need not to be placed exactly at the physical boundaries of the sample. All three thermocouples should be at the same radial (preferably on the main axis), but different axial positions within the sample. Only relative axial distances between them enter into the diffusivity calculation.

Before the diffusivity measurement, the sample is usually evacuated inside the vacuum chamber for about 1 hour in order to dry the sample material. Then the chamber is filled with a gas and the sample temperature is stabilized at room temperature for one hour. Nitrogen atmosphere is often used instead of air in order to prevent corrosion of furnace element at higher temperatures. Thermal diffusivity values measured in nitrogen are the same as in air, within the experimental uncertainty of 3P method.
\begin{figure}
  \includegraphics[scale=1.5]{3PtypResponse.pdf}%
  \caption{Typical Response Curve in 3P method.}
  \label{fig:3PTypResponse}%
\end{figure}
% generated using /Literature/3P method/3P_fit_minpack_CJ_lvm_curves.R  
Steady state (baseline) sample temperature is recorded in the first 10 s, and then the lamp is turned on for a period up to 2 s to generate a test heat pulse. Actual measurement time ranges from 60 s to 450 s and depend on the sample thickness and the thermal diffusivity of the sample material. Maximum temperature rise at the irradiated sample boundary is about 5 K. The inside thermocouple rise due to the pulse is about 1 K. The opposite boundary temperature rise is only of order of 0.1 K. Typical temperature response curves for an insulation sample (diameter $D = 50.13$ mm, $L_1 = 0$, $L_2 = 6.968$ mm, $L_3 = 3.216$ mm, $T_0 = 605.3$ $^\circ$C) are shown in Figure \ref{fig:3PTypResponse}.
\begin{figure}
  \includegraphics[scale=0.6]{3PResiduals.eps}%
  \caption{Residuals for the third thermocouple data from Figure \ref{fig:3PTypResponse}.}
  \label{fig:3PResiduals}%
\end{figure} 
% generated using /Literature/3P method/3P_fit_minpack_CJ_lvm_curves.R   
 Theoretical curve was fitted using Levenberg-Marquardt algoritm implemented in R package \texttt{minpack.lm}. The differences between the theoretical curve with $a = 0.451$ mm$^2$/s and the experimental curve $\phi_3(t)$ (residuals) are shown in Figure \ref{fig:3PResiduals}. Analysis of residuals is a powerful tool for checking the overall quality of the experimental data and also the fitting process. In this particular case the residuals are distributed randomly around zero, along whole time interval, resembling normal or Gaussian distribution with zero mean. This indicates that the experiment was conducted correctly and mathematical model used in the direct heat conduction problem is close to reality.

\subsection{Uncertainty}

All possible sources of uncertainty\cite{ISOstandardGUM} of the 3P method can be distributed into five major categories:

\begin{enumerate}
\item Measurement Means

This category represents the devices used for the measurement of the physical quantities involved in the determination of the thermal diffusivity (sample thickness, thermocouple positions, voltages delivered by the thermocouples, time, baseline temperature value, baseline stability).

\item Method

Uncertainties mainly due to the differences between experimental conditions and the assumptions upon which the model of the method was based. Badly controlled initial and boundary conditions, especially non-constant sample temperature before the thermal disturbance, two or three dimensional heat flow in the sample, multi-component heat flow in the sample  (radiation and convection), represent significant sources of uncertainty. Optimally designed experiment can minimize these effects. 

\item Material

Uncertainties related to geometrical quality of the sample (optimal sample thickness, flatness and parallelism of the surfaces) and its chemical, optical and thermophysical properties (isotropy, homogeneity, opacity, etc.)  The sample dimensions are measured using a calibrated micrometer at room temperature. An additional uncertainty due to sample and sample holder thermal expansion has to be taken into account at higher than room temperatures. Thermal insulation materials are often highly anisotropic, non-homogeneous and translucent, therefore uncertainties due to radiation and convection heat transfer pose often a serious problem. 

\item Surroundings

The uncertainty sources related to the sample boundary conditions, especially furnace design, the furnace temperature homogeneity and stability. The nature of the atmosphere (inert gas at certain pressure, or vacuum) can also contribute to excessive heat losses and sample temperature fluctuations. The baseline temperature uncertainty due to instability during the measurement can be prevented by a good furnace design and careful temperature stabilization during the experiment. The uncertainty is usually higher at high temperatures due to higher signal to noise ratio and furnace temperature fluctuations, especially when measured in gas atmospheres at higher pressures.

\item User

The uncertainty in this category depends on the operator's skills and expertise. The sources are related mainly to the quality of the sample preparation, the attaching and the inserting the thermocouples, the selection of time limit for the experiment and the correction for the sample baseline temperature drift. 

\end{enumerate}

The relative expanded uncertainty (coverage factor $k = 2$) of the thermal diffusivity determination $U_r(a)$ is estimated to be from $\pm6\%$ to $\pm15\%$, depending on material and temperature. The uncertainty components having the most weight are those related to sample thickness, thermocouple positions, baseline drift and (at higher temperatures) those related to multi-component heat transfer in the sample.    

Since the thermal conductivity $k$  is the product of the thermal diffusivity, the specific heat and the density, the combined relative expanded uncertainty of the thermal conductivity determination $U_r(k)$  is given by
\begin{equation}
\label{eq:3PUncertainty}
U_r(k)^2=U_r(a)^2+U_r(c_p)^2+U_r(\rho)^2.
\end{equation}
The relative expanded uncertainty of the specific heat determination $U_r(c_p)$ by using differential scanning calorimetry (DSC) is estimated to be $\pm 5\%$ and relative expanded uncertainty of the density $U_r(\rho)$  is $\pm3\%$.  The combined relative expanded uncertainty of the thermal conductivity determination is therefore estimated to be from $\pm 8.4 \%$ to $\pm 16.1 \%$. 

\subsection{Comparison with a Steady State Method}

In order to check the results with a steady state method, the thermal conductivity of Microtherm HT (sample diameter $D = 48.65$ mm, thickness $L = 4.074$ mm, mass $m = 2.5960$ g, density  $\rho = 342.8$ kg m$^{-3}$) was measured in air at normal pressure at room temperature by the 3P method and also by the heat flow meter  technique. Specific heat of the sample material was measured by the DSC. The results of the thermal conductivity determination are listed in Table \ref{tab:3ReversedLayersParams}. The difference between the two thermal conductivity values was less than $1\%$.
\begin{table}
\caption{ Thermal Conductivity Results for Microtherm HT.}
\label{tab:3ReversedLayersParams}
\begin{center}
\footnotesize
\begin{tabular}{lllll}
\toprule
 Method & $\rho$ & $a$& $c_p$ & $k$\\
\midrule
  & kg m$^{-3}$ & mm$^2$s$^{-1}$& J g$^{-1}$K$^{-1}$ & mW cm$^{-1}$K$^{-1}$\\
\midrule
\addlinespace
3P & $342.8 \pm 10.3$ & $0.108 \pm 0.006$ & $0.703 \pm 0.035$ & $26.0 \pm 2.2$ \\
Heat Flow Meter &$342.8 \pm 10.3$&  &  & $25.8 \pm 2.1$ \\
\bottomrule
\end{tabular}
\end{center}
\end{table}

The method was used to measure a great variety of highly anisotropic, extremely low conductive and non-homogeneous insulating materials, for example aerogels, honeycomb structures, metallic foams, micro and nano-porous insulation and carbon fiber insulation at temperatures ranging  from temperature of liquid Nitrogen ($-195~^\circ$C) up to $1200~^\circ$C. 


