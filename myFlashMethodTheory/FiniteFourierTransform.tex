\section{Finite Fourier Transform}
\label{ch:FiniteFourier}

In this chapter we will derive a formula for the transform kernel of the Finite Fourier transform and its application in the solving heat conduction boundary value problems related to the flash method.

\subsection*{Kernel}
The integral transformsfor used in cartesian coordinate systems are usually called Fourier transforms because they are derived with Fourier expansion of an arbitrary function in a given interval. In this section we examine the Fourier transform and the corresponding inverse formula for a finite region.
We now evaluate the kernels and the eigenvalues, for generality, for boundary conditions of the third kind at both boundaries. These general relations will be used to evaluate the kernels and eigenvalues for other combinations of boundary conditions. 

We will discuss a solution of the heat conduction equation
\begin{equation}
\label{eq:GeneralFormulaHCE}
\frac{\partial^2 T}{\partial x^2} + \frac{\partial^2 T}{\partial y^2} +\frac{\partial^2 T}{\partial z^2} = \frac{1}{a}\frac{\partial T}{\partial t},
\end{equation}
and we will find a kernel of finite integral transform over the variable $x$, if $x \in (a,b)$, 
with boundary conditions of the third kind on both $x=a$ and $x=b$
\begin{equation}
\label{eq:FFtbc_a}
-k\frac{\partial T(a,y,z,t)}{\partial x} + h_1(T(a,y,z,t)-T_1(y,z,t)) = 0,
\end{equation}
\begin{equation}
\label{eq:FFtbc_b}
k\frac{\partial  T(b,y,z,t)}{\partial x} + h_2(T(b,y,z,t)-T_1(y,z,t)) = 0. 
\end{equation}
 Following general methodology, described in Chapter \ref{ch:IntegralTransforms}, we will first find a solution of an auxiliary problem (Sturm-Liouville)
 \begin{equation}
\label{eq:SturmLiouvilleXi}
\frac{\partial^2  \Phi(x)}{\partial x^2}  + \beta^2 \Phi(x)= 0, \quad x \in (a,b)
\end{equation}
\begin{equation}
\label{eq:Phi_a}
-\frac{\partial  \Phi(a)}{\partial x} + H_1\Phi(a) = 0,
\end{equation}
\begin{equation}
\label{eq:Phi_b}
\frac{\partial  \Phi(b,t)}{\partial x} + H_2\Phi(b) = 0. 
\end{equation}
where $H_1=h_1/k$ and $H_2=h_2/k$.

If $\Phi_m(x)=\Phi(\beta_m,x)$ are eigenfunction and $\beta_m$ are eigenvalues of the problem \ref{eq:SturmLiouvilleXi} - \ref{eq:Phi_b}, then the kernel of transformation over $x$ is
\begin{equation}
\label{eq:KernelPhi}
K(\beta_m,x)=\frac{\Phi_m(x)}{\sqrt{N_m}},
\end{equation}
where the norm is calculated from 
\begin{equation}
\label{eq:NormPhi}
N_m=\int_D \Phi_m^2(x)d\xi.
\end{equation}
The general solution of Eq. \ref{eq:SturmLiouvilleXi} is 
\begin{equation}
\label{eq:GeneralSolutionXi}
\Phi(x)= A\cos(\beta_m x) + B\sin(\beta_m x). 
\end{equation}
Constants $A$ and $B$, as well as the eigenvalues $\beta_m$, will be calculated using the boundary conditions \ref{eq:Phi_a} and \ref{eq:Phi_b}. 
If we substitute the the general solution into the condition  \ref{eq:Phi_a}, then we have
\begin{equation}
 \label{eq:RatioAB}
 \frac{B}{A}=\frac{\beta_m\sin(\beta_m a) + H_1\cos(\beta_m a)}{\beta_m\cos(\beta_m a) - H_1\sin(\beta_m a)}.
 \end{equation} 
 From the second boundary condition \ref{eq:Phi_b} we get
a transcendental equation for the eigenvalues $\beta_m$ in the form
  \begin{equation} 
  \label{eq:betatransc} 
  	\tan\beta_m(b-a) = \frac{\beta_m(H_1+H_2)}{\beta_m^2-H_1H_2},
  \end{equation} 

Now using the Eq. \ref{eq:RatioAB}, where we put $A=1$, the eigenfunctions $\Phi_m(x)$ are
\begin{equation}
\label{eq:SolutionXi}
\Phi_m(x)= \cos(\beta_m x) + \frac{\beta_m\sin(\beta_m a) + H_1\cos(\beta_m a)}{\beta_m\cos(\beta_m a) - H_1\sin(\beta_m a)} \sin(\beta_m x). 
\end{equation}

The norm $N$ can be now calculated inserting these eigenfunctions into Eq. \ref{eq:NormPhi}.
After a long but straightforward set of manipulations, we will get
\begin{equation*}
\begin{split}
N = \frac{(\beta_n^2+H_1^2)\bigg( b-a +\frac{H_2}{\beta_n^2+H_2^2}\bigg)+H_1}{2[\beta_m\cos(\beta_m a) - H_1\sin(\beta_m a)]},
\end{split}
\end{equation*}
so the kernel $K(\beta_m,x)$ of the Finite Fourier Transform of Eq. \ref{eq:GeneralFormulaHCE}, with the boundary conditions \ref{eq:FFtbc_a} and \ref{eq:FFtbc_b}, is
\begin{equation}
\label{eq: KernelFiniteFT}
K(\beta_m,x) = \sqrt{2} \frac{\cos(\beta_m x) + \frac{\beta_m\sin(\beta_m a) + H_1\cos(\beta_m a)}{\beta_m\cos(\beta_m a) - H_1\sin(\beta_m a)} \sin(\beta_m x)}{\bigg[(\beta_n^2+H_1^2)\bigg( b-a +\frac{H_2}{\beta_n^2+H_2^2}\bigg)+H_1\bigg]^{1/2}},
\end{equation}
where $\beta_m, m = 1,2,\ldots $, are positive roots of the transcendental equation \ref{eq:betatransc}.

For the case when $a=0$, the kernel $K(\beta_m,x)$ is simply
\begin{equation}
\label{eq: KernelFiniteFTa0}
K(\beta_m,x) = \sqrt{2} \frac{\beta_m\cos(\beta_m x) + H_1\sin(\beta_m x)}{\bigg[(\beta_n^2+H_1^2)\bigg( b+\frac{H_2}{\beta_n^2+H_2^2}\bigg)+H_1\bigg]^{1/2}},
\end{equation}
where $\beta_m$ are positive roots of the equation
  \begin{equation} 
  \label{eq:betatransca0} 
  	\tan(\beta_m b) = \frac{\beta_m(H_1+H_2)}{\beta_m^2-H_1H_2}.
  \end{equation} 

We summarize the above results as follows:

The integral transform and the inversion formula with respect to the space variable $x$ of a function $T(x)$ in the interval $a<x<b$ subject to the boundary conditions of the third kind at both ends are given as
\begin{equation}
\label{eq:FourierFiniteFTransform}
\bar{T}(x) =\int_a^b K(\beta_m,x') T(\beta_m,x')dx',
\end{equation}
and 
\begin{equation}
\label{eq:InvFiniteFTransform}
T(x) =\sum_{m=1}^\infty K(\beta_m,x) \bar{T}(\beta_m,x),
\end{equation}
where the summation is taken over all eigenvalues $\beta_m$, which are the positive roots of the transcendental equation
\begin{equation} 
  \label{eq:betasgeneral} 
  	\tan\beta_m(b-a) = \frac{\beta_m(H_1+H_2)}{\beta_m^2-H_1H_2}.
\end{equation} 
The kernel $K(\beta_m,x)$ is given as
\begin{equation}
\label{eq: KernelFiniteFTgeneral}
K(\beta_m,x) = \sqrt{2} \frac{\cos(\beta_m x) + \frac{\beta_m\sin(\beta_m a) + H_1\cos(\beta_m a)}{\beta_m\cos(\beta_m a) - H_1\sin(\beta_m a)} \sin(\beta_m x)}{\bigg[(\beta_n^2+H_1^2)\bigg( b-a +\frac{H_2}{\beta_n^2+H_2^2}\bigg)+H_1\bigg]^{1/2}},
\end{equation}
where
\begin{equation}
H_1=\frac{h_1}{k}, \quad H_2=\frac{h_2}{k}.
\end{equation}

 Now, if we multiply both sides of Eq. \ref{eq:GeneralFormulaHCE} with the kernel $K(\beta_m, x)$ from \ref{eq: KernelFiniteFTgeneral} and integrate both sides for $x$ from $a$ to $b$, we will get
 \begin{equation*}
\begin{split}
-\beta_m^2\bar{T} + H_1 K(\beta_m, a) T_1(y,z,t)+H_2 K(\beta_m, b) T_2(y,z,t) + \\
+\frac{\partial^2\bar{T}}{\partial y^2} +\frac{\partial^2 \bar{T}}{\partial z^2} = \frac{1}{a}\frac{\partial \bar{T}}{\partial t},
\end{split}
\end{equation*} 
where 
\begin{equation*}
\bar{T} =\int_a^b K(\beta_m,x') T(\beta_m,x',y,z,t)dx',
\end{equation*}
with additional conditions for the spatial variables $y,z$ and given initial condition for time $t$.

The kernel given in Eq. \ref{eq: KernelFiniteFTgeneral} (along with \ref{eq:betasgeneral}) was derived for the boundary conditions of the third kind for both $x=a$ and $x=b$. Boundary condition of the first and of the second kind can also be calculated using the same kernel. All combinations of the boundary conditions of the first, second and third kinds are easily obtained from Eqs. \ref{eq: KernelFiniteFTgeneral} and  \ref{eq:betasgeneral} by choosing the values of $H_1$ and $H_2$ as zero, finite or infinite.

If, for example, we have at $x=a$ the boundary condition of the first kind (zero temperature), and at  $x=b$ we have the boundary condition of the third kind, then we have to set $H_1=\infty$ and the boundary value problem has to be solved using the kernel   
\begin{equation}
\label{eq: KernelFiniteFTIand III}
K(\beta_m,x) = \sqrt{2}\bigg[\frac{\beta_n^2+H_1^2}{(\beta_n^2+H_1^2)( b-a)+H_2} \bigg]^{\frac{1}{2}}\sin\beta_m(x-a),
\end{equation}
where $\beta_m$ are positive roots of the equation
  \begin{equation} 
  \label{eq:betatranscIandIII} 
  	\beta_m \cot \beta_m (b-a) = -H_2.
  \end{equation} 
  
If the boundary conditions for both $x=a$ and $x=b$ are of the second kind (zero heat fluxes), then both $H_1$ and $H_2$ have to be set to zero, and the kernel of the finite Fourier transform is
\begin{equation}
\label{eq: KernelFiniteFTIIandII}
K(\beta_m,x) = \sqrt{\frac{2}{b-a}}\cos\beta_m(x-a),
\end{equation}
where $\beta_m$ are positive roots of the equation
  \begin{equation*}  
  	\sin \beta_m (b-a) = 0,
  \end{equation*} 
i.e.
  \begin{equation*}  
  	\beta_m = \frac{\pi m}{b-a}, \quad \text{for } m=0,1,2\ldots.
  \end{equation*} 
For $\beta_0=0$, the term  $\sqrt{\frac{2}{b-a}}$ in the kernel given in Eq. \ref{eq: KernelFiniteFTIIandII}, have to be replaced with $\sqrt{\frac{1}{b-a}}$ .

The representation of an arbitrary function $T(x)$ as given above is valid if function $T(x)$ satisfies \textit{Dirichlet} conditions at each point of the interval $(a,b)$ where the function is continuous\cite{sneddon1951fourier}. We say that the function $T(x)$ satisfies Dirichlet's conditions in the interval $(a,b)$ if
\begin{enumerate}[(a)]
\item{$T(x)$ has only a finite number of maxima and minima on $(a,b)$},
\item{$T(x)$ has only a finite number of discontinuities on $(a,b)$ 
and no infinite discontinuities}.
\end{enumerate}
