% !TEX root = mybook.tex

\section{Heat Loss Corrections}
\subsection{Parker}
In their first article\cite{parker1961flash}, Parker and his coworkers showed that they can routinely measure the thermal diffusivity of different solid materials between room temperature and 135 $^\circ$C in a quiescent atmosphere using formula \ref{eq:ParkerFormula}, which assumes that there are no heat losses from the tested sample. Since they were trying to measure the bismuth telluride sample exposed to an intensive electron beam, which has to be cooled by a forced air flow, they had to find out how to calculate the thermal diffusivity under these conditions. Parker developed a procedure for the thermal diffusivity and the specific heat identification from experimental data using a more realistic model with linearized heat losses he found on page 119 of the Carslaw and Jaeger book\cite{CJ59}.

It follows from \ref{eq:1DHLsolution} that the temperature rise at rear side of sample ($x=L$), is now a function of time and the Biot number $B$
\begin{equation}
\label{eq:1DHLsolutionRear}
 T(t, B)= \frac{2Q}{\rho c_p L} \mathlarger{\mathlarger{\sum}}_{n=1}^{\infty}\frac{\beta_n^2\cos\beta_n}{\beta_n^2+B^2+2B}\exp\bigg[{-\frac{\beta_n^2 a}{L^2}t}\bigg],
\end{equation}
where  $B=B_0=B_L$ and $\beta_n$ are positive roots of the transcendental equation
 \begin{equation} 
  \label{eq:Btranscendental} 
  	(\beta^2-B^2)\tan(\beta) - 2\beta B = 0.
  \end{equation}  
The dimensionless Biot number, which characterizes the magnitude of heat losses, is for a defined as
\begin{equation}
 B=\frac{hL}{k},
 \end{equation} 
 where $L$ is the sample thickness and $k$ is the thermal conductivity.

The dimensionless temperature rise at $x=L$ is
\begin{equation}
\label{eq:1DHLsolutionRearDimless}
 V(\omega, B)= \frac{T(\omega,B)}{T_m}=2\mathlarger{\mathlarger{\sum}}_{n=1}^{\infty}\frac{\beta_n^2\cos\beta_n}{\beta_n^2+B^2+2B}\exp\bigg[{-\beta_n^2 \omega}\bigg],
\end{equation}
where $T_m=Q/(\rho c_p L)$ is the maximum temperature rise for a thermally insulated sample, and $\omega=at/L^2$ is the Fourier number. 
\begin{marginfigure}
  \includegraphics[scale=0.8]{VvsB.eps}%
  \caption{Example of curves with heat losses.}%
  \label{fig:VvsB}%
\end{marginfigure} 
%% Figures generated using HLcurves_dimless.R 6/28/2016
For $B=0$, formula \ref{eq:1DHLsolutionRearDimless} is equal to the ideal solution
\begin{equation}
 V(\omega)= 1 + 2\sum_{n=1}^{\infty} (-1)^n \exp\big[-n^2\pi^2\omega \big],
\end{equation}
since $\beta_n= n\pi$, for $n=0,1,2,\ldots$.
 
Graph of the temperature rise $V$ as function of dimensionless time $\omega$ for different values of the Biot number $B$ is in Figure \ref{fig:VvsB}. The maximum value of $V$ decreases with increasing $B$ and the response curves are exponentially decreasing with time after the maximum is reached. 

We have to keep in mind that if we want to analyze an experimentally obtained response, the only tell-tale sign of heat loss presence is the decreasing signal after the maximum. The response curve "tail" decreases with time. The actual experimentally obtained maximum depends on many other parameters (for example the sample surface emissivity, temperature sensor sensitivity, etc.) and cannot be used for the diffusivity estimation. Only the shape of the response curve is important in this case. 
\begin{marginfigure}
  \includegraphics[scale=0.8]{VmvsB.eps}%
  \caption{Maximum temperature rise $V_m$ as a function of the Biot number $B$.}%
  \label{fig:VmvsB}%
\end{marginfigure}
Parker realized this and using the formula \ref{eq:1DHLsolutionRearDimless} he calculated the maximum  $V_m$ of theoretically obtained temperature rise as a function of the Biot number $B$, in the range from $0$ to $2$. Theoretically calculated $V_m$ values are shown in Figure  \ref{fig:VmvsB}. In this graph, $V_m = 1$ for a thermally insulated sample and is lower for $B>0$.    

%% this picture was generated using HLcurves_dimless.R 6/29/2016
Then he "normalized" the response temperature rise by dividing $V$ with its maximum value $V_m$ for the particular $B$. Thus normalized curves from Figure \ref{fig:VmvsB} are shown in Figure \ref{fig:VVmvsomega}. The dimensionless halftime values are getting smaller with increasing heat losses. It means that the Parker formula \ref{eq:ParkerFormula}
would yield higher than true values of the thermal diffusivity for non-zero heat losses.
\begin{marginfigure}
  \includegraphics[scale=0.8]{VVmvsomega.eps}%
  \caption{Normalized temperature rise $V/V_m$ as a function of $\omega$, for different Biot numbers $B$.}
  \label{fig:VVmvsomega}%
\end{marginfigure}
%% this picture was generated using HLcurves_dimless.R 6/29/2016
  In order to calculate the thermal diffusivity from a normalized temperature rise using the experimental halftime $t_{1/2}$, the numerical factor $\omega_{1/2}$, in his formula
\begin{equation}
\label{eq:ParkerBiot}
 a=\omega_{1/2}\frac{L^2}{t_{1/2}},
\end{equation} 
had to be determined as a function of the Biot number $B$, to reflect changed halftime values. Values of $\omega_{1/2}$, calculated from the normalized theoretical response curves, are plotted in Figure \ref{fig:omega12vsB}. If $B=0$ then $\omega_{1/2}=0.1388$. The values for $B>0$ decrease with $B$. 
\begin{marginfigure}
  \includegraphics[scale=0.8]{omega12vsB.eps}%
  \caption{Numerical factor in the Parker's formula \ref{eq:ParkerBiot} as a function of the Biot number.}%
  \label{fig:omega12vsB}%
\end{marginfigure}
%% pictures generated using Parker_1.R 6/28/2016 9Julka sa stahuje do noveho domu)
 

Parker estimated the magnitude of heat losses from the logarithmic decrement of the measured response curve at time beyond $4t_{1/2}$, which is the time when the response curve reaches within $1\%$ of its asymptotic value in no-loss case and no more but the first term of series in \ref{eq:1DHLsolutionRearDimless} can be used for the temperature calculation. For temperatures $T_1=T(t_1,B)$ and $T_2=T(t_2,B)$, at two different times $t_1$ and $t_2$, when $t_2>t_1$ and $4at_1/L^2>0.56$, we have
\begin{equation}
\begin{split}
  T_1= T_m\frac{\beta_1^2\cos\beta_1}{\beta_1^2+B^2+2B}\exp\bigg[{-\frac{\beta_1^2 a}{L^2}t_1}\bigg],\\
  T_2= T_m\frac{\beta_1^2\cos\beta_1}{\beta_1^2+B^2+2B}\exp\bigg[{-\frac{\beta_1^2 a}{L^2}t_2}\bigg],
\end{split}
\end{equation}
and natural logarithm of the ratio of these two temperatures is
\begin{equation}
  \ln (T_1/T_2)=\frac{\beta_1^2 a}{L^2}(t_2 - t_1).
  \end{equation}  
From this equation the first root $\beta_1$ is 
\begin{equation}
\label{eq:beta1}
 \beta_1^2=\frac{\ln (T_1/T_2)L^2}{a(t_2 - t_1)}.
 \end{equation} 
Since the transcendental equation \ref{eq:Btranscendental} is equivalent to 
\begin{equation}
\big(B\sin(\beta/2)+\beta\cos(\beta/2)\big)\big(B\cos(\beta/2)-\beta\sin(\beta/2)\big)=0,
\end{equation}
its positive roots comprise the positive roots of the two equations
\begin{equation}
\label{eq:TrancendentalSplit}
\begin{split}
  \beta\tan(\beta/2)-B= 0,\\
  \beta\cot(\beta/2)+B= 0.
\end{split}
\end{equation} 
It follows from \ref{eq:TrancendentalSplit} that the root $\beta_1$ is related to the Biot number $B$ as 
\begin{equation}
\label{eq:Biotbeta}
 B=\beta_1\tan(\beta_1/2).
 \end{equation} 

When heat losses needed to be taken into account, they had to find the Biot number value from cooling part of the normalized temperature rise. Problem is that the formula \ref{eq:beta1}, which had to be used in the process, actually contains the unknown thermal diffusivity. It was therefore impossible to use these formulas to calculate the thermal diffusivity directly. Instead, they used an iterative procedure. First, they determined $B$ value from \ref{eq:Biotbeta} and \ref{eq:beta1} using an estimated value of the thermal diffusivity, calculated using the original Parker's formula. Then they picked the value of $\omega_{1/2}$ from Figure  \ref{fig:omega12vsB}, and recalculated the thermal diffusivity. Reportedly, only a few iterations were needed to determine the final values of $a$ and $B$.

Then, the volumetric specific heat value $C$, calculated from original formula (xxx) was corrected using
\begin{equation}
  C=\rho c_p = \frac{Q}{LT_m}V_m,
  \end{equation}  
where $V_m$ was, for a known value of the Biot number $B$, found from Figure  \ref{fig:VmvsB}. Finally, the thermal conductivity was calculated as
\begin{equation}
k=aC.
\end{equation}
\begin{figure}
  \includegraphics[scale=0.8]{BiTeParkerResults.pdf}%
  \caption{Thermal diffusivity of p-type longitudinally cut bismuth telluride}%
  \label{fig:BITeParkerResults}%
\end{figure}
%% copied from the Parkers Lecture in Thermal Conductivity 24, p. KL-16
This iterative procedure was used to measure the the thermal diffusivity of bismuth telluride sample exposed to different levels of air coolings and the results from room temperature to 310 $^\circ$C, shown in Figure \ref{fig:BITeParkerResults}, were published\cite{parker1962thermal} in 1962.   

\subsection{Cowan}
In 1963, Robert Cowan\cite{cowan1963pulse} considered the problem of heat loss in the thermal diffusivity determination by analyzing of the cooling part of temperature response curve, similarly to Parker. He considered one-dimensional heat conduction with linearized radiation loss for a finite square shape heat pulse at the front face of a sample. He presented a relationship, in dimensionless form, which represented the behavior of the cooling part of the response curve, measured at rear face of a sample, for varying heat losses. Novelty of his approach lies in the fact that he normalized not only the temperature response, but also time. Using the analytical solution, he tabulated relations for the dimensionless half time $ \omega_{1/2}= at_{1/2}/L^2$ versus the ratios of normalized temperatures at five and ten times halftime, $V(5t_{1/2})/V(t_{1/2})$ and $V(10t_{1/2})/V(t_{1/2})$, for different heat losses. Experimenters could use either five or ten half-time ratios from their response curves to find a correspondent value of $\omega_{1/2}$, and then use the formula
\begin{equation}
 a=\omega_{1/2}\frac{L^2}{t_{1/2}},
\end{equation} 
to calculate the thermal diffusivity of the sample. For small heat losses, the $V(10t_{1/2})/V(t_{1/2})$ curve was obviously more sensitive than the  $V(5t_{1/2})/V(t_{1/2})$ curve.

 Cowan's correction was simpler, faster and more straightforward than the iterative procedure proposed by Parker.
 
 Similar  heat loss correction was later proposed by Heckman\cite{heckman1973finite}, who  presented dimensionless times for the sample heating as well as cooling portions of the temperature response curve. For different values and ratios of the heat loss parameters for the front and rear faces Heckman presented numerical values of $t/t_c$, where $t_c=L^2/(\pi^2a)$. 
 
\subsection{Clark and Taylor} 
\label{ClarkAndTaylor}
It was Cape and Lehman\cite{cape1963temperature}, who in 1963 analyzed a two-dimensional heat conduction model, allowing heat exchange from the edge of the sample as well as the faces. Then they normalized the response curves by dividing $V$ with the maximum rise $V_m$ and time $t$ with the time of half maximum rise $t_{1/2}$, so-called half-max time. Plots of the $V/V_m$ versus $t/t_{1/2}$, for various heat losses, are shown in Figure \ref{fig:DoubleNormalizedV}.
\begin{marginfigure}
  \includegraphics[scale=0.8]{DoubleNormalizedV.eps}%
  \caption{Normalized temperature rise $V/V_m$ as a function of normalized time $t/t_{1/2}$.}
  \label{fig:DoubleNormalizedV}%
\end{marginfigure}
%% this picture was generated using ClarkAndTaylorcurves.R 6/30/2016
From these plots it can be seen that the ratio of time at higher percent rise to a time at lower percent rise decreases with increasing heat loss coefficient. Curves with heat losses are lagging behind the ideal response curve at lower percentage rise, while leading the ideal response curve at higher percentage rise. The ratio of time at higher percent rise to a time at lower percent rise can be used to calculate the thermal diffusivity from the heating part of the response curve. The heating part is less affected by heat losses and other adverse effects (heat conduction through the holder, nonuniform heating, etc.), than the cooling part. Based on numerical evaluation of Cape and Lehman solution, Taylor and Clark\cite{clark1975radiation} calculated the the dimensionless halftime $\omega_{1/2}= at_{1/2}/L^2$ as a function of three time ratios $t_{0.7}/t_{0.3}$, $t_{0.8}/t_{0.4}$, and $t_{0.8}/t_{0.2}$, for heat losses ranging from $B=0$ to $B=2.0$. Calculated curves are plotted in Figure \ref{fig:CTcurves}.\begin{marginfigure}
  \includegraphics[scale=0.8]{CTcurves.eps}%
  \caption{Dimensionless halftime  $\omega_{1/2}$ as a function of three different time ratios $t_{0.7}/t_{0.3}$, $t_{0.8}/t_{0.4}$, $t_{0.8}/t_{0.2}$.}
  \label{fig:CTcurves}%
\end{marginfigure} 

%% graph created using ClarkandTaylorCorrectioncurves.R in HLCorrections (7/6/2016)
Experimenter could select the most convenient time ratio, calculate its value from experimental data and find a correspondent value of $\omega_{1/2}$ using a plot from Figure \ref{fig:CTcurves}. Knowing the experimental half-max time $t_{1/2}$, and $L^2$, the thermal diffusivity is calculated from 
\begin{equation}
\label{eq:ClarkTaylor}
a=\omega_{1/2}\frac{L^2}{t_{1/2}}.
\end{equation}

By normalizing the experimental time to its half-max value\sidenote{The half-max time was used for the final calculation because the response curve is the most sensitive to the thermal diffusivity change around this time. Principally, any other fraction time $t_x$, where $0<x<1$, can be used, but then the dimensionless time $\omega_x$ has to be also recalculated for this particular fraction time.}, Clark and Taylor bypassed the problem of finding the characteristic time without prior knowledge of the thermal diffusivity, and by using the time ratio of two different time points, instead of a single time point, they bypassed the problem of estimating the heat loss coefficient. The procedure was this way simplified toward the estimation of the thermal diffusivity, as the only desired parameter. 

In their original article from 1975, they used this procedure to correct for heat losses from graphite samples tested from 1700 K to 2058 K. It was shown that errors in the thermal diffusivity calculation up to 40$\%$ could be corrected using average values of all three proposed time ratios.

Two years later, in 1977, Degiovanni\cite{degiovanni1977diffusivite} published the set of formulas that give three thermal diffusivity values computed using three different time ratios  
\begin{equation}
•a=\frac{L^2}{t_{5/6}}P_{x/y}
\end{equation}
with
\begin{equation}
P_{1/3}=0.8498-1.8451\frac{t_{1/3}}{t_{5/6}}+1.0315\bigg(\frac{t_{1/3}}{t_{5/6}}\bigg)^2,
\end{equation}
\begin{equation}
P_{1/2}=0.9680-1.6382\frac{t_{1/2}}{t_{5/6}}+0.6148\bigg(\frac{t_{1/2}}{t_{5/6}}\bigg)^2,
\end{equation}
\begin{equation}
P_{2/3}=5.1365-11.955\frac{t_{2/3}}{t_{5/6}}+7.1793\bigg(\frac{t_{2/3}}{t_{5/6}}\bigg)^2. 
\end{equation}
The times $t_{1/3}$, $t_{1/2}$, $t_{2/3}$, and $t_{5/6}$ correspond to $V/V_{max} = 1/3$, $1/2$, $2/3$, and $5/6$, respectively. 
%The principle of these data-reduction procedures is in accordance with that suggested by ASTM.

One of the heat loss correction procedures, now recommended in the ASTM standard for the flash method ($E1461$), is based on a modified Clark and Taylor technique.  Ratio $t_{3/4}/t_{1/4}$ is used to calculate the correction factor $K_r$ from 
\begin{equation}
K_r=- 0.3461467 + 0.361578 \frac{t_{3/4}}{t_{1/4}} - 0.06520543 \bigg(\frac{t_{3/4}}{t_{1/4}}\bigg)^2
\end{equation}
and the thermal diffusivity is then calculated as
\begin{equation}
a_c = a_{1/2}K_r/0.13885,
\end{equation}
where $a_{1/2}$ is the thermal diffusivity calculated using the Parker's formula \ref{eq:ParkerFormula}.

\subsection{Partial Temporal Moments}
In 1986, Degiovanni and Laurent\cite{degiovanni1986nouvelle} proposed an original procedure for the heat loss correction based on partial temporal moments. 

Partial temporal moment $m_i$ of function $f(t)$ of order $i$, is defined  as
\begin{equation}
m_i=\int_{t_{\alpha}}^{t_{\beta}}t^if(t)dt,
\end{equation}
where $i$ is a positive or negative integer, and the partial times $t_{\alpha}$ and $t_{\beta}$  are the times when the function $f$ reaches   
\begin{equation}
\begin{split}
f(t_{\alpha})=\alpha, \\
f(t_{\beta})=\beta.
\end{split}
\end{equation}

Dimensionless partial temporal moment $m_i^*$ of a dimensionless function $f^*(t^*)=f(t^*)/\max(f)$, is similarly defined as
\begin{equation}
m_i^*=\int_{t_{\alpha}^*}^{t_{\beta}^*}t^{*i}f^*(t^*)dt^*,
\end{equation}
where the dimensionless time $t^*=t/t_c$, where  $t_c$ the time constant (a certain characteristic time). Partial dimensionless times $t_{\alpha}^*$ and $t_{\beta}^*$,  are  defined as
\begin{equation}
\begin{split}
f^*(t_{\alpha}^*)=\alpha, \\
f^*(t_{\beta}^*)=\beta,
\end{split}
\end{equation} 
respectively, and $\alpha$, $\beta$, are real numbers from $0$ to $1$. 

If the dimensionless function $f$ represents the temperature  rise in a two dimensional model, with the temperature rise expressed generally as a sum of exponentials
\begin{equation}
f(t)=\sum_{n=1}^\infty A_n \exp(-\gamma_n^2 at/L^2).
\end{equation}
Function $f(t)$ depends on both thermal diffusivity, $a$, and heat losses, characterized by the Biot number $B$. Its temporal moments are
\begin{equation}
m_i=\int_{t_{\alpha}}^{t_{\beta}}t^i\sum_{n=1}^\infty A_n \exp(-\gamma_n^2 at/L^2)dt.
\end{equation}
After the substitution, $t^*=at/L^2$, we have for $i\geq -1$
\begin{equation}
m_i=\int_{t_{\alpha}^*}^{t_{\beta}^*}\bigg(\frac{L^2}{a}t^*\bigg)^i\sum_{n=1}^\infty A_n \exp(-\gamma_n^2 t^*)\bigg(\frac{L^2}{a}\bigg)dt^*.
\end{equation}
or, after a simplification
\begin{equation}
\label{eq:TempMoments}
m_i=\bigg(\frac{L^2}{a}\bigg)^{i+1} m_i^* \quad \text{for} \quad i\geq -1.
\end{equation}
 It follows directly from \ref{eq:TempMoments} that 
\begin{equation}
\label{eq:MinusOneMoment}
m_{-1}=m_{-1}^*.
\end{equation}
It means that the temporal moment $m_{-1}$ is actually independent of the thermal diffusivity ($L^2/a$), and depends only on the Biot number. 

From \ref{eq:TempMoments} we have
\begin{equation}
m_0=\bigg(\frac{L^2}{a}\bigg) m_0^* 
\end{equation}
and the thermal diffusivity can be expressed as
\begin{equation}
 a=L^2\bigg( \frac{m_0^*}{m_0}\bigg).
 \end{equation} 

Based on numerical simulations,  Degiovanni and Laurent, choose $\alpha=0.1$, $\beta=0.8$, and established a relationship between the temporal moments $m_0^*$ and $m_{-1}^*$, as a function
\begin{equation}
m_0^*=F(m_{-1}^*).
\end{equation}
It follows from \ref{eq:MinusOneMoment} that $F(m_{-1}^*)=F(m_{-1})$,
and the thermal diffusivity can be evaluated from
\begin{equation}
\label{eq:DegioAlpha}
 a=L^2\frac{F(m_{-1})}{m_0}.
\end{equation}

Degiovanni and Laurent proposed a formula for the correction function in \ref{eq:DegioAlpha} 
\begin{equation} 
\label{eq:DegioF}
  F(m_{-1})= 0.08548-0.314(0.5486-m_{-1})+0.5(0.5486-m_{-1})^{2.63},
\end{equation}  
which can be used in a broad range of sample sizes and heat losses.
 Since, in a limiting case of zero heat losses
\begin{equation}
\lim_{B \to 0}m_{-1}=0.5486,
\end{equation}
for a thermally insulated sample $F(m_{-1})\approx 0.0855$ and the formula for the thermal diffusivity, is
\begin{equation}
 a=0.0855\frac{L^2}{m_0}.
\end{equation}
\begin{marginfigure}
  \includegraphics[scale=0.8]{Degio1.pdf}%
  \caption{Temperature response curves $f(t)$ and $f(t)/t$ for the Biot number $B=0.5$.}
  \label{fig:Degio}%
\end{marginfigure} 
%%%% generated using DegiovanniCurvesFig.R 7/13/2016 %%%%
An example of temperature response curves is in Figure \ref{fig:Degio}. Two dimensional model, with $B=0.5$ (equal for all sample surfaces), $a=0.5$ cm$^2$/s, $L=1$ cm, $R=0.635$ cm, was used to calculate $f(t)$ and $f(t)/t$, depicted as red and blue lines, respectively. Red shaded area is equal to the temporal moment $m_0$, and blue shaded area represents $m_{-1}$. Numerical integration yielded  $m_{-1}=0.3887$ s$^{-1}$ and $m_0=0.07921$.

The thermal diffusivity value, calculated using \ref{eq:DegioAlpha} and \ref{eq:DegioF}, is $a = 0.4960$ cm$^2$/s, which is less than 1\% below the theoretical value used in the simulation. 

\subsection{Logarithmic Transform}
Since the temperature rise of the rear face is less affected by heat losses in early times after the flash, then for a typical sample, the fast converging solution suitable for short times,  can be used to describe the temperature response. By taking into consideration only the first term of the infinite series in Equation \ref{eq:SmallTimes1dimless}, we have
%	T^*=T_lim  (2L^2)/√(πt ) e^(-L^2/4αt).	
\begin{equation}
\label{eq:SmallTimeFirstTerm}
T(t)=T_m\frac{2L}{\sqrt{\pi at}}\exp\bigg[{-\frac{L^2}{4at}\bigg]} .
\end{equation}  
This formula can be transformed to a linear function of the inverse time $\tau=1/t$. After a logarithmic transform, defined as
%	Θ(1⁄t)≡ln(T^* √t),	
\begin{equation}
	\Phi(1/t) \equiv \ln(T\sqrt{t}),
\end{equation}	
the transformed Equation \ref{eq:SmallTimeFirstTerm} becomes a linear function of the inverse time
%	Θ(τ)=-L^2/4α τ+ln(T_lim  (2L^2)/√(π )).	
\begin{equation}
	\Phi(\tau)=A\tau + B,
\end{equation}	
where
\begin{equation}
\label{eq:SlopeA}
 A=-L^2/(4a)=\frac{d\Phi}{d\tau},
 \end{equation} 
is the slope, and  $B= \ln \big( 2 T_m L/\sqrt{\pi a}\big)$, is the intercept.
	
The thermal diffusivity is then calculated from the experimentally obtained slope of the transformed temperature response curve using 
%	α=-L^2⁄((4 dΘ/dτ) ).	
\begin{equation}
	a=-\frac{L^2}{4}\bigg(\frac{d\Phi}{d\tau}\bigg)^{-1}.
	\end{equation}	

Takahashi and his coworkers\cite{takahashi1988usefulness} showed that this method provides far better thermal diffusivity results than the Parker method, even when significant heat losses are involved. It does not require the estimation of the rise curve maximum and is almost not affected by the spatial non-uniformity of the flash pulse. 

For thermal responses with heat losses calculated thermal diffusivity values depend on the range of time chosen for the evaluation. Since the beginning of temperature response, during and immediately after the heat pulse, is often distorted due to an electromagnetic interference, usually the analysis cannot start from the very first point after the pulse, and the starting point ($i_0$) must be shifted to an undisturbed part of the response curve. Also the end time point is not defined by the method. This 'subjectivity' in the time range choice is an obvious weakness of the logarithmic transformation method and may lead to a significant error in the diffusivity evaluation. 
\begin{marginfigure}
  \includegraphics[scale=0.8]{LogTransApparentA.pdf}%
  \caption{Apparent thermal diffusivity values, calculated from two different starting points $i_0$ and for different total number of points $N$.}
  \label{fig:LogTransApparentA}%
  % generated using LogarithmicTransformFig.R  (7/14/2016)
\end{marginfigure} 
As we can see from Figure \ref{fig:LogTransApparentA}, where the temperature response $f(t)$ from Figure \ref{fig:Degio} is analyzed and calculated thermal diffusivity values for two different starting points $i_0=1,20$ are plotted as functions of total number of points $N$ taken into the evaluation. Resulting "apparent" thermal diffusivity values $a^*$, even for the case of an undisturbed signal, with the starting point at $i_0=1$, differ from the theoretical value $a=0.500$ cm$^2$/s. The error in the calculated $a^*$ increases with increasing number of points $N$. The apparent thermal diffusivity values, calculated for the starting point $i_0=20$, differ even more than those starting at $i_0=1$. As we can see from Figure \ref{fig:LogTransApparentA}, the apparent thermal diffusivity values are always bigger than the theoretical value, but if properly extrapolated to $N \to 1$, the apparent diffusivity converges to the theoretical value. 

The extrapolation of apparent diffusivity values calculated using the logarithmic transformation method to the time origin was proposed in 1991 by Voz\'ar et al\cite{vozar1991new}. The method was used for the estimation of the thermal diffusivity and also the maximum adiabatic temperature rise ($T_m$) in a presence of strong heat losses.

\section{Finite Pulse Corrections}

In original theoretical models, proposed for the flash method sample temperature response, the pulse was regarded as an instantaneous, described analytically using the Dirac delta function $\delta(t)$. In real experiments the heat pulses generated either by a laser or a flash tube, have a finite duration with a distribution described by a function of time $f(t)$. Analytical solutions ($V(t)$), which were derived for an instantaneous pulse, can be relatively simply transformed to solutions for a finite pulse duration using the general formula
\begin{equation}
 V_{f}(t) =  \frac{\int_0^t f(t')V(t-t')dt'}{\int_0^t f(t')dt'} \quad \mbox{if} \quad 0<t<\tau, 
\end{equation}
or
\begin{equation}
 V_{f}(t) =\frac{\int_0^{\tau}f(t')V(t-t')dt'}{\int_0^{\tau}f(t')dt'} \quad \mbox{if}  \quad t>\tau,
\end{equation} 
where $\tau$ is the pulse duration and the pulse distribution function is  
\begin{equation}
 f(t) = \begin{cases}  f(t) &\mbox{if } 0<t<\tau \\ 
0 & \mbox{if }  t>\tau. \end{cases}
\end{equation}
An example of response curves generated for finite heat pulses with distribution function
\begin{equation}
 f(t) = \begin{cases}  1 &\mbox{if } 0<t<\tau \\ 
0 & \mbox{if }  t>\tau. \end{cases}
\end{equation}
for a different pulse duration $\tau$, is in Figure \ref{fig:RectangularIdeal}.
\begin{marginfigure}
  \includegraphics[scale=1.2]{RectangularIdeal.eps}%
  \caption{Response curves for different pulse durations.}
  \label{fig:RectangularIdeal}%
  % generated using FinitePulseFig.R  (7/15/2016)
\end{marginfigure}

As we can see from Table \ref{tab:FPTeffectHalftimes}, the halftime value for the response curve with $\tau=0.02$ is only 3.44\% off the curve for the instantaneous pulse ($\tau=0$), with half-maximum time $t_{1/2}\approx 0.280$ s. Halftimes of response curves with longer pulse time duration are shifted to longer times, and if the pulse duration is neglected, may lead to much smaller estimated diffusivity values. 
\begin{margintable}
\caption{ Halftime values }
\label{tab:FPTeffectHalftimes}
%\begin{center}
\footnotesize
\begin{tabular}{lll}
\toprule
$\tau$ & $t_{1/2}$ [s] & $\Delta$ [\%]\\ 
\midrule
0	&	0.280&	0	\\
0.02&	0.289&	3.44\\
0.1 &	0.331&	18.3\\
0.2	&	0.386&	37.8\\
0.3	&	0.443&	58.4\\
0.4	&	0.501&	79.0\\
0.5	&	0.559&	99.6\\
\bottomrule
\end{tabular}
%\end{center}
\end{margintable}
Other than rectangular shapes of heat pulses (e.g. triangular, exponential) have been analyzed in the very first decade of the flash method existence (see Chapter Thermal Diffusivity\cite{maglic1984compendium} and literature therein). Various look-up tables and correction formulas were proposed and verified experimentally. It was also shown that the pulse duration can be safely neglected, if ratio of the experimental half-maximum and the pulse duration is   
\begin{equation}
\frac{t_{1/2}}{\tau} \geq 50.
\end{equation}

%Response curves are distorted by the finite pulse effect (without other effects being present) that the experimental curve normalized to halftime lags the ideal curve in the lower part and leads the ideal curve in the upper part of the rise. A long, flat maximum is observed (see Figure  \ref{fig:RectangularNorm}).
%\begin{marginfigure}
%  \includegraphics[scale=1.2]{NormFinitePulseCurves.eps}%
%  \caption{Normalized response curves with different pulse duration.}
%  \label{fig:RectangularNorm}%
  % generated using FinitePulseFig.R  (7/18/2016)
%\end{marginfigure} 

If the heat pulse duration is not short in comparison with the experimental half-maximum time of a response curve, then the pulse duration and the pulse time distribution have to be taken into account in the thermal diffusivity calculations.

\subsection{Azumi and Takahashi}

 A simple and effective method for eliminating the finite pulse time effect was proposed in 1981 by Azumi and Takahashi\cite{azumi1981novel}.
 In the method, the time origin is first shifted from the beginning to the "center of gravity" ($t_{cg}$) of the pulse, defined as
 \begin{equation}
 t_{cg}=\frac{\int_0^t t'f(t')dt'}{\int_0^t f(t')dt'}
 \end{equation}
and then any of the previously described thermal diffusivity identification methods can be applied.\begin{margintable}
\caption{ Halftime values corrected using the Azumi and Takahashi method}
\label{tab:AzumiHalftimes}
%\begin{center}
\footnotesize
\begin{tabular}{lll}
\toprule
$\tau$ & $t_{1/2}-t_{cg}$ [s] & $\Delta$ [\%]\\ 
\midrule
0	&	0.280&  0\\
0.02&	0.279& -0.138\\
0.1 &	0.281&  0.454\\
0.2	&	0.286&  2.05\\
0.3	&	0.293&  4.80\\
0.4	&	0.301&  7.54\\
0.5	&	0.309&  10.3\\
\bottomrule
\end{tabular}
%\end{center}
\end{margintable} This technique was introduced for the adiabatic sample (ideal model), but is very appealing since it is not restricted to a particular pulse shape and can be used in combination with any other correction methods. 

As we can see from Table \ref{tab:AzumiHalftimes}, corrected halftime values from our previous example, are now within 5\% of the value for the instantaneous response curve, for all pulses up to $\tau=0.3$ s. Shifted response curves are shown in Figure \ref{fig:AzumiFTPcorr}. 
\begin{marginfigure}
  \includegraphics[scale=1.2]{AzumiFPTcorr.eps}%
  \caption{Response curves with the shifted time origins.}
  \label{fig:AzumiFTPcorr}%
  % generated using FinitePulseFig.R  (7/15/2016)
\end{marginfigure}

\section{Non-uniform Heating Corrections}
 All above described methods for thermal diffusivity determination are based on analytical models in which the heat pulse is uniformly distributed over the sample surface. Due to either a non-uniformity of the light beam, or a non-uniformity of the sample surface emissivity, this assumption is usually not fulfilled and the actual heat source distribution is non-uniform.

While heat losses are problem only for thick samples of a low conductive material, and the finite pulse time effect only for thin samples of highly conductive materials, non-uniform heating can occur during any flash diffusivity experiment. 
 
%Relatively very little has been published about the effect of nonuniform heating of the sample in the flash experiment. Beedham and Dalrymple, McKay and Schriempf\cite{mckay1976corrections}, and Taylor\cite{taylor1975critical} have described the results for certain non-uniformities and suggested ways how to mitigate them. These results show that when the heating is uniform over the central portion of the sample, reasonably good results can be obtained.  However, a continuous nonuniformity over the central portion can lead to errors of at least ten times the determinate error.   
 
Beedham and Dalrymple\cite{beedham1970measurement} suggested a simple but powerful method for the non-uniform heating correction when they suggested to normalize the temperature rise curve to its maximum rise ($T_{max}$), instead of the steady state value after the pulse $T_m$. 
\begin{marginfigure}
  \includegraphics[scale=1.0]{BeedhamDalrympleCurves.eps}%
  \caption{Response curves with nonuniform heating.}
  \label{fig:BeedhamDalrympleCurves}%
  % generated using NonuniformFig.R  (7/20/2016)
\end{marginfigure}

An example of temperature rise curves calculated using 2D flash model, with $L=0.2$ cm, $a=0.5$ cm$^2$/s, $R=0.635$ cm, is in Figure \ref{fig:BeedhamDalrympleCurves}. The sample is thermally insulated ($B=0$) and the central portion is irradiated with a coaxial beam with the outer radius, $R_{io}$. Six different temperature responses are shown, for different values of $R_{io}$, ranging from $0.5R$, to $1.0R$. Temperature rise is measured in the center of the rear side of sample. Halftime values, $t_{0.5}$, calculated from curves normalized to $T_m$, are listed in the second column of Table \ref{tab:BeedhamHalftimes}. As we can see, using $t_{0.5}$ values for diffusivity calculations can lead to a significant error, even in a case when $R_{io}=0.9R$, and the beam is perfectly covering more than 81\% of the sample central area. 
\begin{table}
\caption{ Halftime values for non-uniform heating}
\label{tab:BeedhamHalftimes}
\begin{center}
\footnotesize
\begin{tabular}{lllll}
\toprule
$R/R_{io}$ & $t_{0.5}$ [s] & $\Delta$ [\%]& $t_{1/2}$ [s] & $\Delta$ [\%]\\ 
\midrule
1.0	&	0.0111&  	0		&	0.0111	&	0		 \\
0.9 &	0.00960&  -13.6	&	0.0109	&	-2.14\\
0.8 &	0.00840&  -24.4	&	0.0107	&	-3.58\\
0.7	&	0.00737&  -33.7	&	0.0104	&	-6.09\\
0.6	&	0.00649&  -41.6	&	0.0100	&	-9.67\\
0.5	&	0.00570&  -48.7	&	0.00948	&	-14.7\\
\bottomrule
\end{tabular}
\end{center}
\end{table}

% numbers and Figures generated using NonuniformFig.R (7/20/2016)
The half-maximum values, $t_{1/2}$, calculated from the curves normalized to the maximum rise values $T_{max}$, are listed in the fourth column of Table \ref{tab:BeedhamHalftimes}. Response curves, normalized to $T_{max}$, are shown in Figure \ref{fig:BeedhamDalrympleCorr}.
\begin{marginfigure}
  \includegraphics[scale=1.0]{BeedhamDalrympleCorr.eps}%
  \caption{Response curves normalized to $T_{max}$ values.}
  \label{fig:BeedhamDalrympleCorr}%
\end{marginfigure} As we can see from the fifth column of the table, the relative deviations of $t_{1/2}$ values, caused by the non-uniformity of the beam, are much smaller and do not exceed 10\% even if only 36\% of the sample area is actually irradiated by the beam. Since all these relative deviations are negative, this type of beam non-uniformity (so called "hot center"), if not corrected, leads to higher diffusivity values. 

Sample design is also playing an important role in mitigating or completely eliminating the effect of non-uniform heating. Generally, we may say that thinner and wider samples are more sensitive than the thicker and narrower ones. Theoretical studies\cite{platunov1982nonuniform, gembarovic1984} of two dimensional models with non-uniform heat source distributions led to a conclusion that if the ratio of the sample thickness to the diameter is 
\begin{equation}
\frac{L}{R} \geq 2,
\end{equation}
\begin{marginfigure}
  \includegraphics[scale=1.0]{LoverRatio.eps}%
  \caption{Response curves normalized to $T_{m}$ for different $L/R$ values.}
  \label{fig:LoverRatio}%
\end{marginfigure}
then the non-uniform heating of the sample can be neglected.  In other words, if the sample is as tall as wide, then the heat pulse non-uniformity is able to  'homogenize' while diffusing through the sample length. This statement is valid for any heat source distribution on the front surface of a cylindrical shape sample, and any location of the temperature sensor on the opposite surface. The effect of increasing the ratio $L/R$ values, is illustrated in Figure \ref{fig:LoverRatio}. The curve for $L/R=2.0$ is identical with the ideal response for a uniform pulse distribution.


Unfortunately, real samples are usually designed with the ratio $L/R < 1$, with regard to heat losses. 

Degiovanni et al\cite{degiovanni1985heat} showed that the effect of non-uniformities on thermal diffusivity evaluation can be minimized if the response signal is recorded as an average value over the whole rear side area of the sample. As we can see from Figure \ref{fig:WholeAreaViewed}, where response curves for different ratios of the viewed area outer radius over the sample radius $R$ are shown, the curves for $R_{vo}/R \to 1$, are converging to the ideal response curve for a uniformly distributed heat pulse, even if the actual heat source outer radius for all curves is $R_{io}= 0.3R$.
In contrast with the previously described method, increasing the viewing area of an infra-red detector, can be easily accomplished.
 \begin{marginfigure}
  \includegraphics[scale=1.0]{WholeAreaViewed.eps}%
  \caption{Response curves for different outer viewed radius values.}
  \label{fig:WholeAreaViewed}%
\end{marginfigure} 
% generated using NonuniformFig.R  (7/20/2016)