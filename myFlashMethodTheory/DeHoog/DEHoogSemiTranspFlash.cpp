/*----------------------------------------------------------------
Laplace Inversion Source Code
Copyright � 2010 James R. Craig, University of Waterloo
----------------------------------------------------------------*/
#include <math.h>
#include <complex>
#include <iostream>
#include <fstream>

using namespace std;

typedef complex<double>              cmplex;

const double PI = 3.141592653589793238462643;      // pi 
const int    MAX_LAPORDER = 60;

int test_case = 4;
inline void  upperswap(double &u, const double v){ if (v>u){ u = v; } }

/***********************************************************************
LaplaceInversion
************************************************************************
Returns Inverse Laplace transform f(t) of function F(s),
where s is complex, evaluated at time t

f(t) = 1/2*PI*i \intfrmto{gamma-i\infty}{gamma+i\infty} exp(st)*F(s) ds

Based upon De Hoog et al., 1982, An improved method for numerical inversion of
Laplace transforms, SIAM J. Sci. Stat. Comput.

Speed of algorithm is primarily a function of M, the order of the
Taylor series expansion.

Inputs: F(s), a function pointer to some complex function of a single
complex variable
t is the desired time of evaluation of f(t)
tolerance is the required accuracy of f(t)
-----------------------------------------------------------------------*/
double LaplaceInversion(cmplex(*F)(const cmplex &s),
	const double &t,
	const double tolerance)
{
	//--Variable declaration---------------------------------
	int    i, n, m, r;          //counters & intermediate array indices
	int    M(40);            //order of Taylor Expansion (must be less than MAX_LAPORDER)
	double DeHoogFactor(4.0);//DeHoog time factor
	double T;                //Period of DeHoog Inversion formula
	double gamma;            //Integration limit parameter
	cmplex h2M, R2M, z, dz, s;   //Temporary variables

	static cmplex Fctrl[2 * MAX_LAPORDER + 1];
	static cmplex e[2 * MAX_LAPORDER][MAX_LAPORDER];
	static cmplex q[2 * MAX_LAPORDER][MAX_LAPORDER];
	static cmplex d[2 * MAX_LAPORDER + 1];
	static cmplex A[2 * MAX_LAPORDER + 2];
	static cmplex B[2 * MAX_LAPORDER + 2];

	//Calculate period and integration limits------------------------------------
	T = DeHoogFactor*t;
	gamma = -0.5*log(tolerance) / T;

	//Calculate F(s) at evalution points gamma+IM*i*PI/T for i=0 to 2*M-1--------
	//This is likely the most time consuming portion of the DeHoog algorithm
	Fctrl[0] = 0.5*F(gamma);
	for (i = 1; i <= 2 * M; i++)
	{
		s = cmplex(gamma, i*PI / T);
		Fctrl[i] = F(s);
	}

	//Evaluate e and q ----------------------------------------------------------
	//eqn 20 of De Hoog et al 1982
	for (i = 0; i<2 * M; i++)
	{
		e[i][0] = 0.0;
		q[i][1] = Fctrl[i + 1] / Fctrl[i];
	}
	e[2 * M][0] = 0.0;

	for (r = 1; r <= M - 1; r++) //one minor correction - does not work for r<=M, as suggested in paper
	{
		for (i = 2 * (M - r); i >= 0; i--)
		{
			if ((i<2 * (M - r)) && (r>1)){
				q[i][r] = q[i + 1][r - 1] * e[i + 1][r - 1] / e[i][r - 1];
			}
			e[i][r] = q[i + 1][r] - q[i][r] + e[i + 1][r - 1];
		}
	}

	//Populate d vector----------------------------------------------------------- 
	d[0] = Fctrl[0];
	for (m = 1; m <= M; m++)
	{
		d[2 * m - 1] = -q[0][m];
		d[2 * m] = -e[0][m];
	}

	//Evaluate A, B---------------------------------------------------------------
	//Eqn. 21 in De Hoog et al.
	z = cmplex(cos(PI*t / T), sin(PI*t / T));

	A[0] = 0.0; B[0] = 1.0; //A_{-1},B_{-1} in De Hoog
	A[1] = d[0]; B[1] = 1.0;
	for (n = 2; n <= 2 * M + 1; n++)
	{
		dz = d[n - 1] * z;
		A[n] = A[n - 1] + dz*A[n - 2];
		B[n] = B[n - 1] + dz*B[n - 2];
	}

	//Eqn. 23 in De Hoog et al.
	h2M = 0.5*(1.0 + z*(d[2 * M - 1] - d[2 * M]));
	R2M = -h2M*(1.0 - sqrt(1.0 + (z*d[2 * M] / h2M / h2M)));

	//Eqn. 24 in De Hoog et al.
	A[2 * M + 1] = A[2 * M] + R2M*A[2 * M - 1];
	B[2 * M + 1] = B[2 * M] + R2M*B[2 * M - 1];

	//Final result: A[2*M]/B[2*M]=sum [F(gamma+itheta)*exp(itheta)]-------------
	return 1.0 / T*exp(gamma*t)*(A[2 * M + 1] / B[2 * M + 1]).real();
}
//-----------------------------------------------------------------------
double testf(const double &t)
{
	double a1 = 0.333, b1 = 4.0;
	double D = 0.25; // Diffusivity 
	double L = 0.5; // Thickness
	switch (test_case){
	case(0) : {return a1 / pow(4 * PI*pow(t, 3.0), 0.5)*exp(-a1*a1 / 4.0 / t); break; }
	case(1) : {return sin(a1*t + b1);                                 break; }
	case(2) : {return exp(-a1*t);                                  break; }
	case(3) : {return t;                                          break; }
	case(5) : {return 2.0 / pow(PI*t*D, 0.5)*(exp(-1.0 / 4.0 / t / D) + exp(-9.0 *L*L / 4.0 / t / D) + exp(-25.0*L*L / 4.0 / t / D) + exp(-49.0 *L*L / 4.0 / t / D) + exp(-81.0*L*L / 4.0 / t / D));  break; }
	case(6) : {return 2*L/ pow(PI*t*D, 0.5)*(exp(-1.0 *L*L / 4.0 / t / D) + exp(-9.0*L*L / 4.0 / t / D) + exp(-25.0*L*L / 4.0 / t / D) + exp(-49.0*L*L / 4.0 / t / D) + exp(-81.0 *L*L / 4.0 / t / D));  break; }
	default:{return 1;                                          break; }
	}
}
//-----------------------------------------------------------------------
cmplex testF(const cmplex &s)
{
	double a1 = 0.333, b1 = 4.0; int n;
	cmplex q; double alpha = 1.0; double th = 1.0; cmplex suma;
	double h = 0.0; // Biot number
	switch (test_case){
	case(0) : {return exp(-a1*pow(s, 0.5));                         break; }
	case(1) : {return (sin(b1)*s + a1*cos(b1)) / (s*s + a1*a1);              break; }
	case(2) : {return 1.0 / (s + a1);                                  break; }
	case(3) : {return 1.0 / s / s;                                    break; }
	case(4) : {return 1.0 / pow(s, 0.5) / sinh(pow(s, 0.5));             break; }
	case(5) : { 
		q = pow(s / alpha, 0.5);
		for (n = 0; n < 11; n++){
			suma = suma + exp(-2.0 * n*th*q)*pow(q - h, 2 * n) / pow(q + h, 2 * n + 2.0);
		}
				
		return 2.0*q*exp(-th*q)*suma;             break; }
	case(6) : {
				  double D = 0.25; // Diffusivity 
				  double L = 0.5; // Thickness
				  double h = 0.0; // heat loss coeff
				  double K = 0.5; // thermal conductivity
				  double a = 10.0; // absorption coeff
				  double b = 1000.0; // effective IR absorbtion coeff
				  double R = 0.3; // reflection coeff
				  double Q0 = 1.0; // energy of the pulse per unit area
				  double P0 = Q0; // Dirac delta pulse shape
				  double F = 1.0; //  constant
				  double B = h / K; // heat loss variable

				  cmplex A0, A1, B0, B1, E, E0, C, S1, S2, S; // auxiliary variables

				  q = pow(s / D, 0.5);
				  
				  C = P0*(1.0 - R)*a / (2 * K * (pow(q, 2) - pow(a, 2)));
				  E = P0*(1.0 - R)*a*R*exp(-2 * a*L) / (2 * K * (pow(q, 2) - pow(a, 2)));

				  A0 = exp(-q*L)*(q - B)*(a + B) + exp(-a*L)*(q + B)*(-a + B);
				  B0 = exp( q*L)*(q + B)*(a + B) + exp(-a*L)*(q - B)*(-a + B);

				  A1 = exp(-q*L)*(q - B)*(-a + B) + exp(a*L)*(q + B)*(a + B);
				  B1 = exp( q*L)*(q + B)*(-a + B) + exp(a*L)*(q - B)*(a + B);

				  E0 = exp(-q*L)*pow((q - B),2) - exp(q*L)*pow((q + B),2);

				  S1 =     -A0 / (q + b)*(1.0 - exp((b + q)*L));
				  S1 = S1 + B0 / (q - b)*(1.0 - exp((b - q)*L));
				  S1 = S1 + E0 / (a - b)*(1.0 - exp((b - a)*L));
				  S1 = F*C*b*exp(-b*L) / E0 * S1;

				  S2 =     -A1 / (q + b)*(1.0 - exp((b + q)*L));
				  S2 = S2 + B1 / (q - b)*(1.0 - exp((b - q)*L));
				  S2 = S2 - E0 / (a + b)*(1.0 - exp((b + a)*L));
				  S2 = F*E*b*exp(-b*L) / E0 * S2;

				  S = S1 + S2; // tested 4/30/2015 OK JG
				  
				  // this theory is in the article:
				  // Salazar, A., Mendioroz, A., Api�aniz, E., Pradere, C., No�l, F., and Batsale, J.-C., 2014,
				  // "Extending the flash method to measure the thermal diffusivity of semitransparent solids,"
				  // Measurement Science and Technology, 25(3), p. 035604.

				  return S;             break; }

	default:{return 1.0 / s;                                      break; }
	}
}
//-----------------------------------------------------------------------
void main()
{
	double F, f, max_err(0), t;
	char str;
	test_case = 6;
	cout << "Testing inverse Laplace transform..." << endl;
	cout << "Function :" << test_case << endl;
	ofstream LAPTEST;
	LAPTEST.open("SemitransparentTest2.csv");
	LAPTEST << "t,f(t) analytic,f(t) numerical, error" << endl;
	LAPTEST.precision(15);
	for (t = 0.006; t<0.6; t += 0.006)
	{
		F = LaplaceInversion(testF, t, 1e-15);
		f = testf(t);
		upperswap(max_err, fabs(F - f));
		LAPTEST << t << "," << f << "," << F << "," << F - f << endl;
		cout << t << "," << f << "," << F << "," << F - f << endl;
	//	cout << t << "," << F << endl;
	}
	LAPTEST.close();
	cout << "...Inverse Laplace transform evaluated. Max Error= " << max_err << endl;
	cout << " Press n to exit ..." << endl;
	cin >> str;
}
//-----------------------------------------------------------------------