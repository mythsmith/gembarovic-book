% !TEX root = mybook.tex

\chapter{Non-Fourier Heat Conduction}

\section{Hyperbolic Heat Conduction Equation }
Experiments in search of so called "second sound" clearly demonstrated that for situations involving very short pulses\cite{greenstein1982propagation} and temperatures near absolute zero the Fourier Law
\begin{equation}
\label{eq:HHCE Fourier Law}
\vec{q}=-k\nabla T
\end{equation}
and the classical heat conduction equation
\begin{equation}
 \label{eq:HHCE Classical HCE}
 \frac{\partial T}{\partial t}= a\Delta T, 
\end{equation}
become invalid for the calculation of temperature distribution in sample, because they predict an infinite speed of propagation for the heat pulse. Any thermal disturbance which originated in point $P_0$, at time $t=t_0$, is immediately affecting temperature at any point $P$, in arbitrary long distance from $P_0$, at any time $t > t_0$. This clearly contradict the special theory of relativity and all known mechanisms of heat transport.

A modified "non-Fourier" heat flux law, (originally proposed by Maxwell\cite{Maxwell1867gases}, and later also by Vernotte \cite{vernotte1958paradoxes} and Cattaneo\cite{cattaneo1958forme}), is
\begin{equation}
\label{eq:HHCE Heat Flux Law}
\tau\frac{\partial \vec{q}}{\partial t}+\vec{q}=-k\nabla T,
\end{equation}
where $\tau$ is the thermal relaxation time. The term with partial derivation of the heat flux in time, which was added to the Fourier law, causes that the heat flux due to temperature gradient rises exponentially from initial zero to its steady state value, $-k\nabla T$. The Fourier law, in contrast, assumes that the heat flux rises \textit{instantaneously} to the steady state value, when the temperature gradient is created. This is the reason why the Fourier law leads to an infinite speed of heat propagation.  

When equation \ref{eq:HHCE Heat Flux Law} is incorporated into the continuity equation 
\begin{equation}
\label{eq:HHCE Continuity}
\nabla\cdot \vec{q}=-c\rho\frac{\partial T}{\partial t},
\end{equation}
where $c$ is the specific heat and $\rho$ is the density, the hyperbolic heat conduction equation (HHCE) is obtained in the form
\begin{equation}
 \label{eq:Hyperbolic HCE}
 \tau\frac{\partial^2 T}{\partial t^2} + \frac{\partial T}{\partial t}= a\Delta T.
\end{equation}
The presence of the second partial derivation of temperature in time term guarantee that the speed of the thermal disturbance propagation in a medium is now finite, given by
\begin{equation}
   \nu=\sqrt{\frac{a}{\tau}}.
\end{equation}   
Points, in a distance bigger than $\nu(t-t_0)$, are not affected by the thermal disturbance at time $t$. 

In what follows we will derive a solution of the hyperbolic heat conduction equation  \ref{eq:Hyperbolic HCE}, in a one dimensional medium, with initial and boundary conditions typical for the flash method.

\subsection{Analysis}

The sample will be represented as a finite plate of thickness $L$, initially at zero temperature. Temperature distribution, after a heat pulse at $x=0$, will be a solution of one dimensional hyperbolic heat conduction equation 
\begin{equation}
 \label{eq:1D Hyperbolic HCE}
 \tau\frac{\partial^2 T}{\partial t^2} + \frac{\partial T}{\partial t}= a\frac{\partial^2 T}{\partial x^2},  \quad 0 \leq x\leq L, \; t\geq 0,
\end{equation}
with zero initial temperature condition
\begin{equation}
\label{eq:HHCE T init zero}
T(x,0)=0,
\end{equation}
zero time change of the initial temperature
\begin{equation}
\label{eq:HHCE T time init zero}
\frac{\partial T}{\partial t}(x,0)=0,
\end{equation}
zero initial heat flux 
\begin{equation}
\label{eq:HHCE q init zero}
q(x,0)=0,
\end{equation}
The heat pulse is instantaneous, and the boundary conditions for thermally insulated surfaces are
\begin{equation}
\label{eq:HHCE boundary cond1}
q(0,t)= -k\frac{\partial T}{\partial x}(0,t)-\tau\frac{\partial q}{\partial t}(0,t) =Q\delta(t),
\end{equation}
\begin{equation}
\label{eq:HHCE boundary cond2}
q(L,t)= -k\frac{\partial T}{\partial x}(L,t)=0 \quad \text{or} \quad q(L,t) =0.
\end{equation}
Here $Q$ is a constant, and $\delta(t)$ is the Dirac delta function. 

If the Laplace transformation (given in \ref{eq:LaplaceTransfDef}) is applied to equation \ref{eq:1D Hyperbolic HCE}, by taking into account the initial conditions 
\ref{eq:HHCE T init zero} -- \ref{eq:HHCE q init zero}, following subsidiary equation is obtained
\begin{equation}
\label{eq:HHCE subsidiary}
a\frac{d^2 \bar{T}}{d x^2}-p(\tau p+1)\bar{T}=0, \quad 0 \leq x\leq L,
\end{equation}
with conditions
\begin{equation}
\label{eq:HHCE subsid cond1}
\frac{d \bar{T}}{d x}=-\frac{Q}{k}(1+\tau p), \quad \text{at} \; x=0,
\end{equation}
\begin{equation}
\label{eq:HHCE subsid cond2}
\frac{d \bar{T}}{d x}=0, \quad \text{at} \; x=L,
\end{equation}
where
\begin{equation}
 \bar{T}(x,p)=\int_0^\infty T(x,t)e^{-pt}dt.
 \end{equation} 
is the Laplace image of the temperature $T(x,t)$.

The solution of the subsidiary equation \ref{eq:HHCE subsidiary} with respect to the transformed boundary conditions in \ref{eq:HHCE subsid cond1} and \ref{eq:HHCE subsid cond2} is
\begin{equation}
\label{eq:HHCE L solution subsid}
\bar{T}=\frac{Q}{kh}\frac{(1+p\tau)}{(1-e^{-2hL})}\big[ e^{-hx} +e^{-h(2L-x)}\big],
\end{equation}
where
\begin{equation}
h=\sqrt{\frac{\tau p(p+1/\tau)}{a}}.
\end{equation}

Since, we seek a solution of equation \ref{eq:Hyperbolic HCE} for short times, we expand the term $(1 - e^{-2hL})^{-1}$ into a series for a large Laplace variable $h$. Here, the right-hand side of equation \ref{eq:HHCE L solution subsid} has the form
\begin{equation}
\label{eq:HHCE L solution small t}
\bar{T}=\frac{Q}{k}(1+p\tau)\sum_{m=0}^\infty \frac{1}{h}\big\{ e^{-h(2mL+x)} +e^{-h(2mL+2L-x)}\big\}.
\end{equation}
Using a table of Laplace transforms [27] we can find the inverse tranformation of equation  \ref{eq:HHCE L solution small t}
\begin{equation}
\label{eq:HHCE solution small t}
T(x,t)=H(x,t)+\tau\frac{\partial H(x,t)}{\partial t}
\end{equation}
where
\begin{equation}
\label{eq:HHCE H solution small t}
\begin{split}
H(x,t)=\frac{Q}{k}\sqrt{\frac{a}{\tau}}\exp \big( -\frac{t}{2\tau}\big)\times\\
\times\sum_{m=0}^\infty \bigg\{ I_0\big( \frac{1}{2\tau}\sqrt{t^2-(2mL+x)^2\frac{\tau}{a}}\big)\\
\times u\bigg[t - \sqrt{\frac{\tau}{a}}(2mL+x)\bigg]\\
\times I_0\big( \frac{1}{2\tau}\sqrt{t^2-(2mL+2L-x)^2\frac{\tau}{a}}\big)\\
\times u\bigg[t - \sqrt{\frac{\tau}{a}}(2mL+2L-x))\bigg]
\bigg\},
\end{split}
\end{equation}
and $I_0(z)$ is the modified Bessel function of the first kind of zero order, and $u(z)$ is the Heaviside step function
\begin{equation}
u(z)= \begin{cases}  0 & z< 0\\ \frac{1}{2} &  z=0\\ 
1 &  z>0. \end{cases}
\end{equation}

The first term in equation \ref{eq:HHCE H solution small t} describes the flow of thermal waves from the plane $x=0$ to the right, and the second one describes the waves reflected from the plane $x=L$ moving tho the left.

\subsection{Extended Pulse}
Now suppose that the instantaneous pulse occurs at time $t=t'$, i.e. we shift the time origin to $-t'$ by writing $(t-t')$ in place of $t$. Suppose also that there is a sequence of instantaneous sources $Q/\rho c$ in the interval $0$ to $t_1$. Let this time sequence be of magnitude proportional to $f(t')$, where $f(t')$ is a non-dimensional function. To obtain the temperature distribution at time $t$, we integrate the solution in \ref{eq:HHCE solution small t} from $0$ to $t$, when $t<t_1$, or from $0$ to $t_1$, when $t>t_1$. In latter case we have
 \begin{equation}
 \label{eq:HHCExtPulse}
T(x,t)=\int_0^{t_1} f(t')\bigg[H(x,t-t')+\tau\frac{\partial H(x,t-t')}{\partial t}\bigg]dt'
\end{equation}


\subsection{Non-dimensional formulas}
If we want to express the temperature in a medium as a fraction of the steady--state temperature after the pulse, i.e. 
\begin{equation}
T_m=\frac{Q}{\rho cL}\int_0^{t_1}f(t')dt'
\end{equation}
then the equation  \ref{eq:HHCExtPulse} has the form
 \begin{equation}
 \label{eq:HHCExtPulseDimless}
V(x,t)=\frac{\int_0^{t_1} f(t')\bigg[H(x,t-t')+\tau\frac{\partial H(x,t-t')}{\partial t}\bigg]dt'}{\int_0^{t_1}f(t')dt}
\end{equation}
and the dimensionless form of $H(x,t)$ is
\begin{equation}
\label{eq:HHCEH}
\begin{split}
H(x,t)=\frac{L}{\sqrt{a\tau}}\exp \bigg( -\frac{t}{2\tau}\bigg)\times\\
\times\sum_{m=0}^\infty \bigg\{ I_0\bigg( \frac{1}{2\tau}\sqrt{t^2-(2mL+x)^2\frac{\tau}{a}}\bigg) u\bigg[t - \sqrt{\frac{\tau}{a}}(2mL+x)\bigg]\\
+ I_0\bigg( \frac{1}{2\tau}\sqrt{t^2-(2mL+2L-x)^2\frac{\tau}{a}}\bigg)u\bigg[t - \sqrt{\frac{\tau}{a}}(2mL+2L-x))\bigg]
\bigg\}.
\end{split}
\end{equation}

For convenience in the numerical analysis, the following dimensionless quantities are introduced:
\begin{equation}
\label{eq:HHCE Fourier No}
\begin{split}
\omega=\frac{at}{L^2}, \quad \text{Fourier number},\\
Ve=\frac{\sqrt{a\tau}}{L}, \quad \text{Vernotte number},\\
X=\frac{x}{L}, \quad \text{dimensionless coordinate}.
\end{split}
\end{equation}

After these substitutions, function $H$, given in equation \ref{eq:HHCEH}, takes the form
\begin{equation}
\label{eq:HHCEHDimless}
\begin{split}
H(X,\omega)=\frac{1}{Ve}\exp\bigg(-\frac{\omega}{2Ve^2}\bigg)\times\\
\times\sum_{m=0}^\infty \bigg\{ I_0\big( \frac{1}{2Ve^2}\sqrt{\omega^2-(2m+X)^2 Ve^2}\big)u\bigg[\omega - (2m+X)Ve\bigg]\\
+ I_0\big( \frac{1}{2Ve^2}\sqrt{\omega^2-(2m+2-X)^2 Ve^2}\big) u\bigg[\omega - (2m+2-X)Ve)\bigg]
\bigg\}.
\end{split}
\end{equation}

\subsection{Rectangular Pulse}

If a plate of thickness $L$ is irradiated with a rectangular heat pulse of duration $\omega_1$, described using two Heaviside step functions
\begin{equation}
\label{eq:HHCE square pulse}
 f(\omega)=u(\omega)-u(\omega-\omega_1),
\end{equation}
  then it follows from \ref{eq:HHCExtPulseDimless} that the dimensionless temperature distribution $V$ for time $\omega > \omega_1$, is
 \begin{equation}
 \label{eq:HHCFo1Dimless}
V(X,t)=\frac{1}{\omega_1}\bigg\{\int_0^{\omega_1}H(X,\omega-\omega')d\omega' + Ve^2[H(X,\omega)-H(X,\omega-\omega_1)]\bigg\},
\end{equation}
where $H(X,\omega)$ is given in \ref{eq:HHCEHDimless}.

\begin{marginfigure}
\includegraphics[scale=0.8]{NonFourieriVe4}
\caption{\label{fig:HHCEiVe4} Temperature distributions for different times $\omega$, calculated using equation \ref{eq:HHCFo1Dimless} with the pulse duration $\omega_1=0.0125$, and $1/Ve = 4$.}
\end{marginfigure}

The analytical solution for a finite medium ($L = 1$ cm, $a =
0.625$ cm$^2$/s, $\tau = 0.1$ s) heated by a square (stepwise) pulse of
duration $t_1 = 0.02$ s, is shown in Figure \ref{fig:HHCEiVe4}.
Temperature profiles are calculated using Equation
\ref{eq:HHCFo1Dimless}, with the dimensionless pulse duration $\omega_1=0.0125$, the Vernott number $Ve=0.25$, at eight different dimensionless times $\omega$, starting from $0.05$ to $0.40$. 

Dominant feature of this of the HHCE solution is a
thermal wave, which is travelling through the medium with a final speed, bouncing
back and forth from the boundaries, decaying exponentially with
time and dissipating its energy along its path. The wave front is at $X=0.2$, in $\omega = 0.05$, and the part of the medium for $X>0.2$ is not affected by the pulse. The wave reaches the boundary at $X=1$ in $\omega=0.25$, then turns around and moves toward the other boundary ($X=0$).
   
The dimensionless width of this wave, $\Delta X$, depends on the dimensionless pulse duration $\omega_1$, and the speed of propagation of heat ($\propto 1/Ve$), namely
\begin{equation}
\label{eq:HHC DeltaX}
 \Delta X = \frac{\omega_1}{Ve}.
 \end{equation} 
The height of the discontinuity on the front side of the wave $\Delta V_f$ decays exponentially with time $\omega$ and can be calculated from 
\begin{equation}
\label{eq:HHC Vf}
 \Delta V_f = \frac{Ve}{\omega_1}\exp\bigg(-\frac{\omega}{2Ve^2}\bigg).
 \end{equation} 
Similarly, again from  \ref{eq:HHCFo1Dimless}, we can calculate the peak of discontinuity on the back side of the wave
\begin{equation}
\label{eq:HHC Vr}
 \Delta V_r = \frac{Ve}{\omega_1}\exp\bigg(-\frac{\omega-\omega_1}{2Ve^2}\bigg),
\end{equation}
which also decays exponentially with time.
The wave is traveling back and forth through the medium, dissipating its energy along its path.

Dimensionless temperature distributions, at four different dimensionless times $\omega$, are  shown in Figure \ref{fig:NonFourieriVe10}. HHCE profiles were calculated using \ref{eq:HHCFo1Dimless} for the pulse of duration $\omega_1=0.05$, and $1/Ve = 10$.  HCE temperature profiles were calculated using the classical (Fourier) solution
\begin{marginfigure}
\includegraphics[scale=0.6]{NonFourieriVe10}
\caption{\label{fig:NonFourieriVe10} Comparison of HHCE and HCE temperature distributions for different times $\omega$.}
\end{marginfigure}
\begin{equation}
\label{eq:NonFourierIdealSquare}
V(X,\omega)=\frac{1}{\omega_1}\int_0^{\omega_1}T(X,\omega-z)dz,
\end{equation}
where $T(X,\omega)$, is the small times formula from \ref{eq:SmallTimesxdimless},
\begin{equation}
%\label{eq:SmallTimesxdimless}
\begin{split}
T(X,\omega)= \frac{1}{\sqrt{\pi \omega}}\mathlarger{\sum}_{m=0}^\infty\bigg\{\exp\bigg[{-\frac{(2m+X)^2}{4\omega}\bigg]}
\\
+ \exp\bigg[{-\frac{(2m+2-X)^2}{4\omega}\bigg]} \bigg\}.
\end{split}	
\end{equation} 
In the first set of profiles (for $\omega=0.025$), the heat wave front is in $X=0.25$ and region for $X>0.25$ is not affected by the heat pulse. The difference between the HHCE(dot-dashed line) and HCE (solid line) profiles is clearly visible. The wave discontinuities diminishes quickly with time and HHCE curves converge with HCE profiles.

%Another comparison of classical and non-Fourier distributions, for the same pulse duration as in Figure \ref{fig:NonFourieriVe10}, but for a smaller speed of propagation ($1/Ve = 6.325$), is in Figure \ref{fig:NonFourieriVe6}. We can see that the wave character of HHCE curves is more pronounced, as the wave speed of propagation ($1/Ve$) decreases. The curves 
 
%\begin{marginfigure}
%\includegraphics[scale=0.6]{NonFourieriVe6}
%\caption{\label{fig:NonFourieriVe6} A comparison of temperature distributions for different times $\omega$, calculated using solution of classical heat conduction equation ($1/Ve = 6.2$).}
%\end{marginfigure}
%(Add description and Discussion!)

\subsection{Parameter Estimation}
The flash method can be used for the parameter estimation using the non-Fourier heat conduction model.
 
Temperature rise of the rear side of a thermally insulated sample, after the front side was heated with an extended heat pulse described by the function $f(\omega)$,  is
\begin{equation}
 \label{eq:HHCV1}
V(1,\omega)=\begin{cases} \frac{\int_0^{\omega_1} f(\omega')\bigg[H(1,\omega-\omega')+\tau\frac{\partial H(1,\omega-\omega')}{\partial \omega}\bigg]d\omega'}{\int_0^{\omega_1}f(\omega')d\omega'}, \quad \text{for}\; \omega >\omega_1\\
\frac{\int_0^{\omega} f(\omega')\bigg[H(1,\omega-\omega')+\tau\frac{\partial H(1,\omega-\omega')}{\partial \omega}\bigg]d\omega'}{\int_0^{\omega}f(\omega')d\omega'}, \quad \text{for}\; 0<\omega <\omega_1
\end{cases}
\end{equation}
where
\begin{equation}
\omega_1=\frac{at_1}{L^2}, \quad \omega'=\frac{at'}{L^2}.
\end{equation}

In the case of a rectangular heat pulse, given in equation \ref{eq:HHCE square pulse},
the solution of \ref{eq:HHCV1}, for $\omega > \omega_1$, is
\begin{equation}
\label{eq:HHCVRect}
\begin{split}
V(1,\omega)= \frac{Ve^2}{\omega_1} \sum_{m=0}^\infty \bigg\{\frac{1}{Ve^2}\int_0^{\omega_1}\exp \bigg( -\frac{\omega-\omega'}{2Ve}\bigg)\\
I_0\bigg( \frac{1}{2Ve^2}\sqrt{(\omega-\omega')^2-(2m+1)^2Ve^2}\bigg)d\omega'\\
\times u\big[\omega - (2m+1)Ve\big]\\
+\exp \bigg( -\frac{\omega}{2Ve}\bigg)I_0\bigg( \frac{1}{2Ve^2}\sqrt{(\omega^2-(2m+1)^2 Ve^2}\bigg)\\
\times u\big[\omega - (2m+1)Ve)\big]\\
-\exp \bigg( -\frac{\omega-\omega_1}{2Ve}\bigg)I_0\bigg( \frac{1}{2Ve^2}\sqrt{(\omega-\omega_1)^2-(2m+1)^2Ve^2}\bigg)\\
\times u\big[\omega-\omega_1 - (2m+1)Ve\big]
\bigg\}.
\end{split}
\end{equation}
%If dimensionless time is shorter than the pulse duration, ($\omega<\omega_1$), then in equation \ref{eq:HHCVRect} $\omega$ occurs instead of $\omega_1$, and the last term becomes $0$.

\begin{figure}
\includegraphics[scale=0.6]{NonFourierRear}
\caption{\label{fig:NonFourierRear} Temperature rise curves calculated using HHCE solution  \ref{eq:HHCVRect}, for different values of $1/Ve$.}
\end{figure}
The effect of Vernotte number on temperature rise curves at $X=1$ is demonstrated in Figure \ref{fig:NonFourierRear}. Here we are observing temperature responses on a rectangular pulse at $X=0$, calculated using HHCE solution \ref{eq:HHCVRect}, with the pulse duration $\omega_1=1.0 \times 10^{-5}$. As we can see, the curves with $1/Ve > 20$ are practically identical with the ideal classical response curve (with $1/Ve \to \infty$), calculated using equation \ref{eq:NonFourierIdealSquare}. 
The response curve ($1/Ve=15.0$) differs from the ideal response that it stays at zero for $\omega < Ve$ since the heat wave has not arrived to $X=1$. At $\omega = Ve$, the curve abruptly jumps to a value slightly higher than the ideal response and rises ahead of the ideal response for the rest of time. The first jump appearance is obviously getting longer as the value $1/Ve$ decreases. The response curve for $1/Ve = 4.0$ is decreasing after the first jump at $\omega=0.25$, and a second jump at $\omega=0.75$, corresponding to the second appearance of the heat wave, which now traveled the distance $3L$ between the sample surfaces.  There are four jumps in the curve for $1/Ve = 1.5$, visible in this time range. The last response curve, for $1/Ve=0.8$, rises in a 'tilted staircase' manner and reaches the stationary value at much later times. 

Time $t^*$ of the first jump appearance in the sample plane $x=L$, if properly measured, can serve for an estimation of the actual speed of heat propagation in the sample using $\nu = L/t^*$.
The formula \ref{eq:HHCEH} can be rewritten into
\begin{equation}
\label{eq:HHCEH_speed}
\begin{split}
H(L,t)=\frac{2L\nu}{a}\exp \bigg( -\frac{\nu^2t}{2a}\bigg)\times\\
\times\sum_{m=0}^\infty \bigg\{ I_0\bigg( \frac{\nu^2}{2a}\sqrt{t^2-(2m+1)^2\frac{L^2}{\nu^2}}\bigg) u\bigg[t - \frac{L}{\nu}(2m+1)\bigg]
\bigg\},
\end{split}
\end{equation}
where the only unknown parameter is the thermal diffusivity $a$. This formula, in combination with equation \ref{eq:HHCExtPulseDimless}, can then be used to fit the experimental data and to estimate the thermal diffusivity. 
\begin{figure}
\includegraphics[scale=1.0]{NonFourierRearPDuration2}
\caption{\label{fig:NonFourierRearSidePDurationEffect} Temperature rise curves calculated using HHCE solution  \ref{eq:HHCVRect}, for $Ve=0.1$ and for different pulse durations $\omega_1$.}
\end{figure}
The effect of the pulse duration $\omega_1$ is demonstrated  in Figure {\ref{fig:NonFourierRearSidePDurationEffect}, where temperature rise at the boundary plane $X=1$ is shown. The HHCE curves, calculated using \ref{eq:HHCVRect} ($Ve=0.1$ and $\omega_1$ ranging from $0.001$ to $0.1$) are compared with classical (HCE) responses. It follows from  \ref{eq:HHC Vf} and \ref{eq:HHC Vr} the discontinuities at front and rear side of the heat wave for time $\omega = Ve$, when it reaches the plane $X=1$, are inversely proportional to the pulse duration, $\omega_1$. While the response curve $\omega_1=0.001$ has a narrow and high jump, starting in $\omega=Ve=0.1$, the jumps are diminishing as the pulse duration increases. HHCE responses for $\omega_1 > 0.075$ lose their discontinuous character and closely resemble the classical HCE responses for the same pulse duration. 


The common value for $1/Ve$ at room temperature and for the typical solids ($L \approx 1$ cm, $\tau \approx 10^{-11}$ s, $a \approx 10^{-5}$ cm$^2$s$^{-1}$) is of the order of $10^6$. This means that the effect of the finite speed of propagation of the thermal disturbance in common materials with typical dimensions of samples does not appear, or in other words, can be safely neglected. However, at the low temperature the values of $\tau$ and $a$ increase, therefore at certain values of $L$ the conditions can become favorable for the observation of temperature responses at the short pulses which are close to the response curves predicted by the HHCE.  
