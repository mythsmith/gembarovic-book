% !TEX root = mybook.tex
\section{Notes on Measurement of Thermal Conductivity}


 Everybody who was ever involved in the thermal conductivity measurement will agree that it is a nontrivial experimental task, much harder to conduct that for example measurement of the electrical conductivity.
 
Instead of describing all existing experimental methods, we will first try to answer two basic questions, regarding the thermal conductivity measurement.

Question 1:
\textit{Why it is difficult to measure the thermal conductivity of real materials?}

\begin{itemize}
\item	The heat energy in real materials is usually transferred not only by the conduction (diffusion), but also by the radiation and/or the convection. Including of later two modes requires completely different approach and different mathematical models. Radiation heat transfer can be modeled only numerically using 3D models and additional material characteristics (like absorption coefficient, emissivity, dispersion coefficient, etc., which are complicated functions of temperature and wavelength) are necessary to define it even in the simplest models.  Convection transport is also mathematically different from the conduction, and, as the radiation, cannot be simply 'incorporated' into the conduction.

\item	Even in the case of a pure conduction, it is very difficult to design an ideal experiment, where the tested sample will be completely insulated from the ambient. In contrast with the electrical conductivity, there is no perfect thermal insulator, nor a perfect thermal conductor. Thermal conductivities of all known materials range only 7 orders of magnitude (from $0.001$ W/(m K) to $10000$ W/(m K)). Even vacuum in is a quite good heat conductor at high temperatures, when radiation is a prevailing mode of heat transfer.  

\item	To measure the thermal conductivity basically means we have to obtain experiment data about the sample temperature, geometry and heat fluxes, and then solve an inverse heat conduction problem of finding the parameter lambda. This, so called parameter estimation problem, is inherently "ill posed". It means small changes (perturbations, noise) in measured temperatures can lead to big changes in backward estimated thermal conductivity values. There is nothing we can do about it. It is in the nature of the heat conduction equation. The problem can be only mitigated by a clever experimental design.

\item	In order to calculate temperature distribution and heat fluxes in tested sample a solution of the heat conduction equation has to be found for a particular set of boundary and initial conditions. We have to know, for example, if the sample is in good thermal contact with a heater, or if there is a thermal contact resistance between them, if there are heat losses from sample's lateral surfaces, what is the nature of heat losses (if the Newton law of cooling can be applied, or the losses are nonlinear function of temperature difference, etc). The problem is that we actually do not know these boundary and initial conditions, and the analytical solution is different for different set conditions.  There is no general solution for the heat conduction equation, which would cover all possible conditions. The solutions for more general conditions (e.g. combination of heat losses with non-zero thermal contact resistance), are mathematically very complex, with many additional unknown parameters to be determined on top of the thermal conductivity, to a point they are practically useless.

\item	Like in quantum mechanics, there is the "uncertainty principle" in the thermal measurement which is substantial even on macroscopic scales. The act of thermal measurement is affecting the measured object. Our way of measuring the temperature is an indirect measurement, where the temperature value is estimated from its effect on a sensor. For example, a thermocouple will generate electrical current if there is a difference between its hot and cold junctions, or a resistor will change its resistance with temperature, or the sensor material will expand with temperature, etc. The least invasive IR detectors are limited to a detection of the infra-red radiation emitted from sample surface.  Also heat flux is notoriously difficult to measure. Heat flux sensors distort heat flow pattern in the measured object therefore introducing an experimental error which is affecting the thermal conductivity measurements. 

\item There is no universal standard material, which can serve as a calibration of preciseness of existing methodologies. The thermal conductivity of all existing materials is easily affected or alternated by mechanical or electrical treatment, heating, or cooling, irradiation, etc.
\end{itemize}

Question 2:
\textit{Why there are so many methods for the thermal conductivity measurements?}

With ever increasing demand for more precise conductivity data of newly developed materials and with improved data acquisition and data reduction possibilities by using computers in the middle of the 20th century, there was an explosion of new methods for the thermal conductivity measurements. As we can find in scientific and technical literature there were hundreds of new or newly updated methods used worldwide and it is not far from true that every experimenter working in this field has been using his own, or somewhat modified version of an existing method. In those years, if you were an engineer designing a part of machinery from say titanium, you could find a value of thermal conductivity for the titanium of any of your choice within two orders of magnitude, if you were patient and dig deep enough into a literature. 
The situation was improved when certain groups of researchers (e.g. CINDAS) in USA and later other countries started collecting published thermophysical properties data, comparing them, and critically evaluating its reliability. Resulted monographs, like Toloukian's TPRC Series of 12 volumes of thermophysical data, are highly cited to these days in scientific journals around the world.  This duty was later officially transferred to national institutes for standards and measures, which are now trying to review and regulate the process of publishing new experimental data, based on existing databases.  These regulations, along with global international effort for unification and standardization, stymied the rapid development of new experimental procedures used in industry. With all newly developed materials nano-structures, thin layers, ceramic composites, graphene materials, super insulation materials, high temperature insulation materials, etc., which are in many instances impossible to test using existing methods, there is a constant need for innovation. Process of developing new or modified innovative techniques is far from over.   

(Sestak book excerpt)
A problem is the tendency of recent instruments to become oversophisticated, in the form of impersonal "black boxes" (or less provoking "gray boxes") that are easy to operate providing computed or, at least, pre-analyzed results and nicely smoothed and methodically granted data. Attached computers, equipped with powerful programs, allow instantaneous analysis and solution of most mathematical and interpretative tasks. Nonetheless, it occurs persuasive but effortless to pass scientist's responsibility for the results evaluation and interpretation straightforwardly in the hands of computers. If a user agrees with such a policy, that means he accepts the ready-calculated data without taking notice of the error analysis and interpretative ambitions, he becomes inattentive to the instrument calibration and performance. In extreme, he does not even aspire to learn what physical-chemical processes have been really measured, which it is not the manufacture's, instrument's or computer's fault, but it is a sluggishness of the scientific operator.

In this respect, progress in thermophysical and thermo-analytical instrumentation is apparently proceeding in two opposite directions: 

(i) towards "push-button" instruments with more and more functions being delegated to the computer, and, in a somewhat opposite direction,

(ii) towards more "transparent" (flexible and easy to observe) instruments, with well allocated all functions that still require the researcher's involvement, experience and thus his explicit "know-how" and a perceptive scientific "sense".

While the first type have a place in a routine analysis, mostly required in the industry, the transparent-type piece of equipment (substituting once popular, laboratory self-made instruments, where each piece was intimately known in its operative functioning) should become more appreciated in the scientific and industrial research.

\subsection{Two Basic Modes of Measurement}
	From its definition as a proportionality coefficient between the heat flux vector and the temperature gradient, the thermal conductivity is generally represented by a tensor of the second rank. In a simplified situation of a homogeneous and isotropic material, the thermal conductivity $k$ is expressed as a scalar (real, positive number), calculated from the Fourier Law, where absolute values (or modulus) of the heat flux and temperature gradient are taken into account
\begin{equation}
 k=\frac{|\vec{q}|}{|\text{grad}T|}.
 \end{equation}

In order to measure the thermal conductivity experimentally, we have to create a known temperature gradient and measure resulting heat flux in the tested material.  Information about the temperature gradient, i.e. temperature distribution in a sample, is obtained using temperature sensors positioned on, or inside the sample.  Heat flux is measured either by so called heat flux meters, or from knowing total energy input into the sample body during the experiment.

Historically, existing experimental methods for the thermal conductivity determination are divided into two groups:  so called \textit{steady state} methods, when sample temperature, during the test, is only a function of position and it is not changing in time, and \textit{transient} methods, when the sample temperature during the test is a function of a position and also a function of time. The Fourier Law is valid for both a steady state and transient conditions.

\subsection{Steady State Method Principle}

The steady state methods are based on one dimensional Fourier Law:
\begin{equation}
\label{eq:1D Fourier Law}
 q=-k\frac{\partial T}{\partial x}
 \end{equation}
where $q$ is the heat flux and $\frac{\partial T}{\partial x}$  is the temperature gradient component in $x$ direction. 

If the heat flux in a tested body remains constant, independent of time and position, then it follows from Equation \ref{eq:1D Fourier Law} that the temperature gradient has to be a constant, too. In one dimensional case it means that the temperature distribution is a simple linear function of position $T(x)=a+bx$, where $a$ and $b$ are constants, calculated from boundary temperatures. A typical sample in steady state method is a slab with dimensions 50 cm x 50 cm and thickness $L \approx 2$ cm. The upper surface of the sample is in thermal contact with a heater, kept at a constant temperature $T_H$, and the sample bottom is attached to a cooler, with constant temperature $T_L$. In a steady state (after long enough time), the temperature profile within the homogeneous sample becomes linear   \begin{equation}
T(x)=T_H-\frac{T_H-T_L}{L}x, \quad 0\le x \le L,
\end{equation}
and the temperature gradient can be expressed simply as
\begin{equation}
 \frac{\partial T}{\partial x}= \frac{T_H-T_L}{L}.
 \end{equation}
If the heat flux $q$ value is known, then the thermal conductivity, $k$, of the sample, can be simply calculated as
\begin{equation}
k=q\frac{L}{T_H-T_L}.
\end{equation}

From the experimental point of view, the steady state methodology is very simple. Temperature distribution in the sample is known and all we have to measure, are the boundary temperatures and the heat flux. They remain constant during the experiment and we have plenty of time to measure them. 

Problems with the above described steady state methods are that the sample surface temperatures are due to thermal contact resistance usually not equal to the temperatures of the heater, nor the cooler, and there are parasitic heat flows not accounted for in the analysis. Despite of continuous progress in sensor development, measurement of heat flow remains a challenging experimental problem. 

The steady state measurements are usually very time consuming and temperature range of these methods is  restricted to low temperatures.

\subsection{Transient Method Principle}

 
If we have information about the temperature and the heat flux in every point and time of tested body, then we can calculate the thermal conductivity using time dependent heat conduction equation
\begin{equation}
 c\rho \frac{\partial T}{\partial t}= k\Delta T, 
\end{equation}
It is a partial differential equation (PDE) of second order and temperature distribution $T(\vec{r},t)$ can be found as a solution of this PDE with boundary and initial conditions. Boundary conditions define the heat fluxes, or temperatures at the sample surfaces, and the temperature distribution at time zero, $T(\vec{r},0)$, is given by the initial condition.

To monitor and measure local temperatures and heat fluxes during the transient state is experimentally challenging. Even today, with using sophisticated temperature sensors and advanced digital data acquisition hardware, it is not a trivial task. It is therefore much easier, to wait long enough for the sample temperature to reach a steady state distribution when assumptions about a constant heat flux, independent of position, and linear temperature distribution independent of time in the sample, are valid. This is a reason why steady state methods prevailed in the past. 

Also, data reduction process, when the thermal conductivity is calculated from the temperature and heat flux distributions data, is much more difficult using transient data. There is no general universal solution of transient heat conduction equation and the actual theoretical model used for the parameter estimation (data reduction) depends on the sample geometry, heat source distribution, and boundary and initial conditions. The particular solution depends heavily on all above mentioned factors and conditions, and from a mathematical point of view, it is sometimes an "ill-posed" problem.

(\textbf{ Preformuluj to, mnohe tieto idei su nepresne, tazkopadne a nedaju sa v knihe pouzit!})


