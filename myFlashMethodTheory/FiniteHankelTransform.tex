% !TEX root = mybook.tex

\section{Finite Hankel Transform} 
\label{sec:FiniteHankelTransform}

Consider a long solid cylinder of radius $b$, which is initially at temperature $F(r)$. For times $t>0$ heat is dissipated by convection from the boundary surface into a surrounding medium at zero temperature. (See Figure .) The boundary value problem of heat conduction in the dimensionless radial coordinate $r$, is given as 
\begin{equation}
\label{eq:CylHCE1D}
\frac{\partial^2 T}{\partial r^2}+\frac{1}{r}\frac{\partial T}{\partial r} = \frac{b^2}{a}\frac{\partial T}{\partial t},\quad \text{in} \quad 0\le r \le 1,\quad t>0,
\end{equation}
\begin{equation}
\label{eq:CylHCE1Dbc}
\frac{\partial T}{\partial r} +H_r T =0,\quad \text{at} \quad r=1,\quad t>0,
\end{equation}
\begin{equation}
\label{eq:CylHCE1Dinitc}
T=F(r),\quad \text{in} \quad 0\le r \le 1,\quad t=0,
\end{equation}
where $T=T(r,t)$, and $H_r = bh_r/k$, is the dimensionless Biot number.

Assume solution in the form
\begin{equation*}
T(r,t)=\Gamma(t) R(r).
\end{equation*}
The space variable function $R(r)$ satisfies following eigenvalue problem:
\begin{equation}
\label{eq:CylHCE1DR}
\frac{d^2 R}{d r^2}+\frac{1}{r}\frac{d R}{d r} +\sigma^2 R=0,\quad \text{in} \quad 0\le r \le 1,
\end{equation}
\begin{equation}
\label{eq:CylHCE1DRbcb}
\frac{d R}{dr} +H_r R =0,\quad \text{at} \quad r=1.
\end{equation}

\begin{equation}
\label{eq:CylHCE1DRbc0}
R < \infty,\quad \text{at} \quad r=0.
\end{equation}
The differential equation \ref{eq:CylHCE1DR} is a special case of the \textit{Bessel's differential equation} of order $\nu$ 
\begin{equation}
\label{eq:CylR}
\bigg(\frac{d^2R}{dr^2} + \frac{1}{r}\frac{d R}{d r}\bigg)+\bigg(\sigma^2 - \frac{\nu^2}{r^2}\bigg)R=0,
\end{equation}
with the particular solution in the form
\begin{equation}
\label{eq:CylParticularBessel}
R(r) = c_1 J_{\nu}(\sigma r)+c_2 Y_{\nu}(\sigma r),
\end{equation}
where $c_1$ and $c_2$ are constants which depend on boundary conditions.
The functions $J_{\nu}(\sigma r)$ and $Y_{\nu}(\sigma r)$ are the \textit{Bessel functions} of order $\nu$ of the first and second kind respectively.

The eigenfunctions of the Bessel differential equation \ref{eq:CylHCE1DR}, where $\nu=0$, are
\begin{equation}
 R(\sigma_m,r)=J_0(\sigma_m r),
 \end{equation} 
where we used the particular solution given in  \ref{eq:CylParticularBessel}, and excluded $Y_0(\sigma_m r)$, since it is infinite at $r=0$, which is in a contradiction with the boundary condition \ref{eq:CylHCE1DRbc0}.

The eigenvalues, $\sigma_m$, determined from the boundary condition \ref{eq:CylHCE1DRbcb}, are the positive roots of the transcendental equation
\begin{equation}
\sigma J_1(\sigma) - H_rJ_0(\sigma)=0,
\end{equation}
where we used 
\begin{equation}
J'_0(\sigma)=\bigg[\frac{dJ_0(r)}{dr}\bigg]_{r=\sigma}=-J_1(\sigma).
\end{equation}

The norm $N$ is evaluated from
\begin{equation}
N=\int_0^1 rJ_0^2(\sigma_m r)dr=\frac{1}{2}\frac{H_r^2+\sigma_m^2}{\sigma_m^2}J_0^2(\sigma_m).
\end{equation}

The kernel $K_0(\sigma_m,r)$ and the eigenvalues $\sigma_m$ of Hankel transform 
for a finite region $0 \le r \le 1$ subjected to a boundary conditions of the third kind at $r=b$ are given as
\begin{equation}
\label{eq:CylK0definition}
K_0(\sigma_m,r)=\frac{R(\sigma_m,r)}{\sqrt{N}}=\sqrt{2}\frac{\sigma_m}{\sqrt{H_r^2 + \sigma_m^2}}\frac{J_0(\sigma_m r)}{J_0(\sigma_m)}
\end{equation}
and the eigenvalues $\sigma_m$ are positive roots of the equation
\begin{equation}
\sigma J_1(\sigma) - H_rJ_0(\sigma)=0. 
\end{equation}

Finite Hankel transform of order zero for an arbitrary function $F(r)$ in a finite region $0\le r \le 1$ is defined as
\begin{equation}
\bar{F}(\sigma_m)=\int_0^1 r' K_0(\sigma_m,r) F(r') dr',
\end{equation}
and the inversion formula is
\begin{equation}
\label{eq:HankelTransormInversion}
F(r)=\sum_{m=1}^\infty K_0(\sigma_m,r) \bar{F}(\sigma_m).
\end{equation}

Now, back to the our boundary value problem from the beginning of this Section. If we multiply both sides of Eq. \ref{eq:CylHCE1D} with $r K_0(\sigma_m,r)$ and integrate it over $r$, from $0$ to $1$, then for the finite Hankel transform of zero order of function $T(r,t)$, we get
\begin{equation}
\label{eq:CylHCE1DRtransf}
-\sigma^2 \bar{T}= \frac{b^2}{a}\frac{d \bar{T}}{d t},
\end{equation}
subject to the initial condition 
\begin{equation}
\bar{T}=\int_0^1 r' K_0(\sigma_m,r) F(r') dr'=\bar{F}.
\end{equation}

The solution of \ref{eq:CylHCE1DRtransf} is 
\begin{equation}
\bar{T}=\bar{F}\exp{\bigg[-\sigma_m^2\frac{a t}{b^2}\bigg]}.
\end{equation}


The solution for temperature $T(r,t)$ can be calculated using the inversion formula \ref{eq:HankelTransormInversion} as
\begin{equation}
\label{eq:GeneralRTerm}
T(r,t)=2\sum_{m=1}^\infty \exp{\bigg[-\sigma_m^2\frac{a t}{b^2}\bigg]}\frac{\sigma_m^2}{H_r^2+\sigma_m^2}\frac{J_0(\sigma_m r)}{J_0^2(\sigma_m)}\int_0^1 r' J_0(\sigma_m r') F(r') dr'.
\end{equation}
If the surface at $r=1$ is thermally insulated, then $H_r = 0$, and a term
\begin{equation}
2\int_0^1r' J_0(\sigma_m r') F(r') dr'
\end{equation}
must be added to the right-hand side of equation \ref{eq:GeneralRTerm}.
\begin{enumerate}[I.]
\item
For a uniform source distribution $F(r)=1$, we have
\begin{equation}
\label{eq:integral_rJ0}
\int_0^1 r' J_0(\sigma_m r') F(r') dr' = \frac{1}{\sigma_m}J_1(\sigma_m) = \frac{H_r}{\sigma_m^2}J_0(\sigma_m)
\end{equation}
and the solution of Eq. \ref{eq:CylHCE1D}, with the conditions \ref{eq:CylHCE1Dbc} and \ref{eq:CylHCE1Dinitc}, is
\begin{equation}
T(r,t) = 2\sum_{m=1}^\infty \frac{H_r}{H_r^2+\gamma_m^2}\frac{J_0(\gamma_m r)}{J_0(\gamma_m)}\exp{\bigg[-\gamma_m^2\frac{a t}{b^2}\bigg]},
\end{equation}
where  $\gamma_m$ are positive roots of the equation
\begin{equation}
\label{eq:RadialTranscendental}
\gamma J_1(\gamma) - H_rJ_0(\gamma)=0. 
\end{equation}
\item
For a unit instantaneous cylindrical surface source per unit axial length, radius $r = r',  (0 < r' < 1)$, at $t = 0$, with $F(r)=\delta(r-r')/2\pi r'$, we have
\begin{equation}
\int_0^1 r' J_0(\gamma_m r) \frac{\delta(r-r')}{2\pi r'} dr' = \frac{J_0(\gamma_m r')}{2\pi},
\end{equation}
and the solution is
\begin{equation}
\label{eq:DimlessGreenFunctionR}
T(r,r',t) = \frac{1}{\pi}\sum_{m=1}^\infty\frac{\gamma_m^2}{H_r^2+\gamma_m^2}\frac{J_0(\gamma_m r)J_0(\gamma_m r')}{J_0^2(\gamma_m)}\exp{\bigg[-\gamma_m^2\frac{a t}{b^2}\bigg]},
\end{equation}
where  $\gamma_m$ are positive roots of equation \ref{eq:RadialTranscendental}.

If  the boundary surface is thermally insulated ($H_r = 0$),  then
\begin{equation}
T(r,r',t) = \frac{1}{\pi}\bigg\{1+\sum_{m=1}^\infty\frac{J_0(\gamma_m r)J_0(\gamma_m r')}{J_0^2(\gamma_m)}\exp{\bigg[-\gamma_m^2\frac{a t}{b^2}\bigg]}\bigg\}.
\end{equation}

If we express \ref{eq:DimlessGreenFunctionR} in real (not dimensionless) coordinates $0 \leq r \le b$ and $0 \leq r' \leq b$, whith $Q$ units of heat energy supplied per unit area, than the temperature of the cylinder is
\begin{equation}
\label{eq:GreenFunctionR}
T(r,r',t) = \frac{Q}{\rho c}\frac{1}{\pi b^2}\sum_{m=1}^\infty\frac{\gamma_m^2}{H_r^2+\gamma_m^2}\frac{J_0(\gamma_m r/b)J_0(\gamma_m r'/b)}{J_0^2(\gamma_m)}e^{-\gamma_m^2a t/b^2},
\end{equation}
The source strength is defined as $Q/(\rho c)$ temperature units per unit area. The unit source strength is thus the rise in temperature when one unit of heat is distributed uniformly in unit volume of the solid. $Q/(\rho c)$ has dimensions [temperature] [length]$^3$.

\end{enumerate}

%\item
%For a radial distribution of unit instantaneous sources with strength proportional to $g(r')$, we take the integral over the whole circular cross section:
%\begin{equation}
%\begin{split}
%T(r,r',t) = \frac{1}{\pi}\sum_{m=1}^\infty\frac{\gamma_m^2}{H_r^2+\gamma_m^2}\frac{J_0(\gamma_m r)}{J_0^2(\gamma_m b)}\exp{\bigg[-\gamma_m^2\frac{a t}{b^2}\bigg]}\int_0^1 2\pi r' J_0(\gamma_m r') g(r') dr'\\
%=2\sum_{m=1}^\infty\frac{\gamma_m^2}{H_r^2+\gamma_m^2}\frac{J_0(\gamma_m r)}{J_0^2(\gamma_m b)}\exp{\bigg[-\gamma_m^2\frac{a t}{b^2}\bigg]}\int_0^1 r' J_0(\gamma_m r') g(r') dr'
%\end{split}
%\end{equation} % OK checked it is the same as for F(r')

