
\chapter{Integral Transform Techniques}
\label{ch:IntegralTransforms}

In this chapter we will discuss the solution of transient HCE with integral transforms in a finite regions. We will not discuss problems in semi-infinite and infinite regions.

\section{General Formulation}
Consider a solid region $D$, with continuous bounded surfaces $S_k$, $k=1,2,\ldots,6$, in an orthogonal system of coordinates ($\xi_1, \xi_2, \xi_3$). Assume that the solid  is stationary, isotropic, homogeneous and has constant thermal properties. Then we have to solve the boundary value problem in a form

\begin{equation}
\label{eq:GeneralFormulaHCE}
\Delta T(\vec{r},t)= \frac{1}{a}\frac{\partial T(\vec{r},t)}{\partial t} + \frac{w(\vec{r},t)}{k}, \quad \vec{r} \in D, \quad t>0,
\end{equation}
with boundary conditions
\begin{equation}
\label{eq:GeneralFormulaHCEboundc}
\frac{\partial  T(\vec{r},t)}{\partial n_k} + B_k  T(\vec{r},t) = \phi_k(\vec{r},t), \quad \vec{r} \in S_k,  \quad t>0,
\end{equation}
and with the initial condition
\begin{equation}
\label{eq:GeneralFormulaHCEinitc}
T(\vec{r},t) = F(\vec{r}), \quad P \in D,  \quad t=0,
\end{equation}
where is the Laplace's operator, and $\frac{\partial}{\partial n_k}$ is the differentiation along the outward-drawn normal to the bounding surface.

As a first step of solving the boundary value problem (\ref{eq:GeneralFormulaHCE}-\ref{eq:GeneralFormulaHCEinitc}), we consider the consider the following \textit{auxilliary} homogeneous eigenvalue problem for the space variable $\Phi(\vec{r})$, in the same region $D$, subject to homogeneous boundary conditions.
\begin{equation}
\label{eq:SturmLiouvillePhi}
\Delta \Phi(\vec{r}) + \lambda^2 \Phi(\vec{r})= 0, \quad \vec{r} \in D
\end{equation}

\begin{equation}
\label{eq:SturmLiouvilleBc}
\frac{\partial  \Phi(\vec{r_k})}{\partial n_k} + B_k  \Phi(\vec{r_k}) = 0, \quad \vec{r_k} \in S_k.
\end{equation}

This system of equation is similar to the eigenvalue problems considered in our previous sections. Let $\Phi_m(\vec{r})=\Phi(\lambda_m,\vec{r})$ be the eigenfunctions and $\lambda_m$ the eigenvalues of the system (\ref{eq:SturmLiouvillePhi}-\ref{eq:SturmLiouvilleBc}). 

We take the eigenfunctions $\Phi_m(\vec{r})$ as the \textit{kernel} and define a three-dimensional \textit{finite integral transform} of the temperature $T(\vec{r},t)$ as

\begin{equation}
 \label{eq:integraltransformT}
 \bar{T}(\lambda_m,t)=\iiint_D \Phi_m(\vec{r'})T(\vec{r'},t)d\vec{r'}
 \end{equation} 
 and the \textit{inversion formula} as
 \begin{equation}
 \label{eq:inversionformulaT}
 T(\vec{r},t)=\sum_{m=1}^\infty C_m \Phi_m(\vec{r'})\bar{T}(\lambda_m,t),
 \end{equation}
 where the summation is taken over all eigenvalues; for the three dimensional problems the summation is a triple infinite series. 
 The unknown coefficients $C_m$ in equation (\ref{eq:inversionformulaT}) can be determined if the eigenfunctions $\Phi_m(\vec{r})$ constitute an orthogonal set in the region under consideration, i.e.
 \begin{equation}
 \label{eq:orthogonalityPhi}
 \iiint_D \Phi_m(\vec{r})\Phi_n(\vec{r})d\vec{r}=0,\quad \text{when}\quad m\neq n
 \end{equation}
Multiplying both sides of equation (\ref{eq:inversionformulaT}) by $\Phi_n(\vec{r})$, integrating it over the region, and by making use of the orthogonality of the eigenfunctions, we obtain
\begin{equation}
\label{eq:Norm}
C_m=\bigg[\iiint_D \Phi_m^2(\vec{r})d\vec{r} \bigg]^{-1}=1/N_m.
\end{equation}
The unknown coefficient $C_m$ is equal to the reciprocal of \textit{norm} $N_m$.

Using the \textit{normalized kernel} $K(\lambda_m,\vec{r})$ defined as
\begin{equation}
\label{eq:NormalizedKernel}
K(\lambda_m,\vec{r})=\frac{\Phi_m(\vec{r})}{\sqrt{N_m}},
\end{equation}
the finite integral transform and inversion formula for temperature is defined as
\begin{equation}
\label{eq:FiniteIntTranformT}
 \bar{T}(\lambda_m,t)=\iiint_D K(\lambda_m,\vec{r'})T(\vec{r'},t)d\vec{r'},
\end{equation}

 \begin{equation}
 \label{eq:FiniteIntInversionT}
 T(\vec{r},t)=\sum_{m=1}^\infty K_m(\lambda_m,\vec{r'})\bar{T}(\lambda_m,t).
 \end{equation}

Now, we proceed with the solution of the boundary value problem (\ref{eq:GeneralFormulaHCEboundc}-\ref{eq:GeneralFormulaHCEinitc}). By applying the integral transform (\ref{eq:FiniteIntTranformT}) on the equation (\ref{eq:GeneralFormulaHCEboundc}), we get

\begin{equation*}
\begin{split}
\iiint_D K(\lambda_m,\vec{r'})\Delta T(\vec{r'},t)d\vec{r'} + \frac{1}{k}\iiint_D K(\lambda_m,\vec{r'})w(\vec{r'},t)d\vec{r'}\\
=  \frac{1}{a}\iiint_D K(\lambda_m,\vec{r'})\frac{\partial T(\vec{r'},t)}{\partial t}d\vec{r'},
\end{split}
\end{equation*}
which is written in the form
\begin{equation}
\label{eq:TransformedHCE}
\iiint_D K(\lambda_m,\vec{r'})\Delta T(\vec{r'},t)d\vec{r'} + \frac{1}{k}\bar{w}(\lambda_m,t)
=  \frac{1}{a} \frac{\partial \bar{T}(\vec{r'},t)}{\partial t},
\end{equation}
where quantities with bars (i.e. $\bar{w}, \bar{T}$) refer to the integral transform according to (\ref{eq:FiniteIntTranformT}).

The first integral on the left-hand side of (\ref{eq:TransformedHCE}) can be evaluated using the Green's theorem and can be written in the form
\begin{equation}
\label{eq:AuxilliaryGreenF}
\begin{split}
\iiint_D K_m\Delta T d\vec{r'}= \iiint_D T\Delta K_md\vec{r'}+ \sum_{k=1}^{6}\iint_{S_k}\bigg[
K_m\frac{\partial T}{\partial n_k}-T\frac{\partial K_m}{\partial n_k}\bigg]ds_k,
\end{split}
\end{equation}
where $K_m=K(\lambda_m,\vec{r})$, and the summation is taken over all continuous bounding surfaces. 

The first term on right-hand side is obtained by multiplying the differential equation (\ref{eq:SturmLiouvillePhi}) by $T$ and integrating it over the region $D$,
\begin{equation}
 \label{eq:TheFirstTerm}
 \iiint_D T\Delta K_m d\vec{r'}=-\lambda_m^2\iiint_D K_m T d\vec{r'}=-\lambda_m^2 \bar{T}(\lambda_m,t).
 \end{equation}

The second term on right-hand side of (\ref{eq:AuxilliaryGreenF}) is evaluated using the boundary conditions (\ref{eq:GeneralFormulaHCEboundc}) and (\ref{eq:SturmLiouvilleBc}). We obtain
\begin{equation}
\label{eq:TheSecondTerm}
\begin{split}
\bigg[K_m\frac{\partial T}{\partial n_k}-T\frac{\partial K_m}{\partial n_k}\bigg]=K(\lambda_m,\vec{r_k})\phi_k(\vec{r_k},t).
\end{split}
\end{equation}
Substituting equations (\ref{eq:TheFirstTerm}) and (\ref{eq:TheSecondTerm}) into (\ref{eq:AuxilliaryGreenF}),
\begin{equation}
\label{eq:AuxilliaryGreenFsimple}
\begin{split}
\iiint_D K_m\Delta T d\vec{r'}=-\lambda_m^2 \bar{T}(\lambda_m,t) + \sum_{k=1}^{6}\iint_{S_k}K(\lambda_m,\vec{r_k})\phi_k(\vec{r_k},t)ds_k,
\end{split}
\end{equation}
and then substituting this into Eq. \ref{eq:TransformedHCE}, we have
\begin{equation}
\label{eq:TransformedHCEsimpl}
\frac{d\bar{T}(\lambda_m, t)}{dt}+a\lambda_m^2 \bar{T}(\lambda_m, t)=R(\lambda_m, t),
\end{equation}
where
\begin{equation}
\label{eq:AinTransfHCE}
R(\lambda_m,t)= \frac{a}{k}\bar{w}(\lambda_m,t)+ a\sum_{k=1}^{6}\iint_{S_k} K(\lambda_m,\vec{r_k})\phi_k(\vec{r_k},t)ds_k. 
\end{equation}

As we can see from the above analysis the integral transform removed the second partial derivative with respect to spatial variables from HCE  \ref{eq:GeneralFormulaHCE} and reduced it to a first order, linear, ordinary differential equation for the integral transform of temperature $\bar{T}(\lambda_m, t)$ as given in \ref{eq:TransformedHCEsimpl}. In order to find a solution of Eq. \ref{eq:TransformedHCEsimpl} an initial condition is needed, which is obtained by taking the integral transform of the initial condition of the original problem in Eq. \ref{eq:GeneralFormulaHCEinitc},

\begin{equation}
\label{eq:TransformedInitc}
\bar{T}(\vec{r},t) = \iiint_D K(\lambda_m,\vec{r}) F(\vec{r})d\vec{r}=\bar{F}(\lambda_m),  \quad t=0.
\end{equation}

The solution of the differential equation \ref{eq:TransformedHCEsimpl} subject to the initial condition \ref{eq:TransformedInitc} is

\begin{equation}
\label{eq:SolutionTransT}
\bar{T}(\vec{r},t) = e^{-a\lambda_m^2t}\bigg[ \bar{F}(\lambda_m)+\int_0^t  e^{a\lambda_m^2t'} R(\lambda_m,t')dt'\bigg].
\end{equation}
and substituting the integral transform of temperature $\bar{T}(\vec{r},t)$ into the inversion formula \ref{eq:FiniteIntInversionT} we obtain the solution of the boundary value problem of heat conduction in the form

\begin{equation}
\label{eq:InverseT}
T(\vec{r},t) =\sum_{m=1}^\infty e^{-a\lambda_m^2t} K(\lambda_m,\vec{r})\bigg[ \bar{F}(\lambda_m)+\int_0^t  e^{a\lambda_m^2t'} R(\lambda_m,t')dt'\bigg].
\end{equation}

