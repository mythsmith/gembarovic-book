% !TEX root = mybook.tex


\section{Negative Jumps in the Flash Method}

\subsection{Introduction}

If the temperature response of a sample tested using the laser flash method [Parker] is measured on an opposite (rear) face of a disc shape sample using an IR detector, then the experimental temperature response signal during and immediately after the heat pulse is usually slightly distorted from a theoretically expected shape. Usually, there is a short positive spike during the pulse, followed by a slow recovery back to the baseline level before the pulse. There are many theories particular to the instrument and sample quality, used to explain the origins of such a disturbances, ranging from a direct electromagnetic interactions between the laser and detector electrical circuits, to a scattered laser radiation hitting the sample rear surface, the laser beam penetration, or mechanical effects. The positive spikes were studied in the laser flash literature for translucent samples with conduction and radiation heat transfer [*]. Due to its complexity and great variances with different instruments, the problem was rarely mentioned for opaque samples. Despite the deficiency, the experimenters involved in the flash method and the instrument makers paid a great deal of attention to this problem and they found that the effect of this type of disturbances can be relatively simply mitigated or almost eliminated by the design of the flash instrument (shielding the electronic circuits and preventing scattered light reaching the rear sample surface and IR detector) and also the experiment design by using thicker, better prepared samples, with highly emissive surfaces, for which the signal will have more time to recover from the disturbances. Commercial analytical software used for the thermal diffusivity calculations, will usually jump over the distorted region and use only portions of the experimental signal undisturbed by the heat pulse, with the baseline level prior the pulse.    

In this article, we will describe another type of disturbance, which may occur in a response signal from IR detector if the rear sample surface is of a low emissivity and also highly reflective. In this case, instead of a positive spike, the signal will during and after the pulse suddenly decrease to a level lower than the original baseline before the pulse. The signal will then only gradually recover to a pre-flash level and in the process will be overlapped with the actual rise due the sample temperature increase. If the positive disturbances can be relatively easily accounted for in the thermal diffusivity calculations, the negative ones are extremely difficult to deal with, and total error of simply ignoring them and use the baseline level before the flash for the calculation, is usually leading to errors in the thermal diffusivity determination, which are often multiples of the accepted levels.

Obvious solutions, like increasing the sample surface roughness and emissivity, by using a sandpaper, sand-blasting them, or spraying them with black paints, are not always applicable. In a case of testing shiny, highly polished samples, or molten metals, we can not alter the sample surfaces, so we have to deal with the negative jumps in the response signals, especially at higher temperatures. This problem, although witnessed by many flash method practitioners, was never described, nor discussed in existing literature dealing with the laser flash method.

\subsection{Instrument Description}
\label{sec:Instrument}

TA Instrument DLF2-EM1600 laser flash instrument was used in our experiments. ... description, see brochure .... Furnace, measurement chamber and IR detector schematics, is shown in Figure \ref{fig:Furnace}. A detail view of the sample holder is in Figure \ref{fig:Holder}. The holder is made from a translucent Alumina material.   

\begin{figure}
\begin{center}
\includegraphics[scale=0.5]{Literature/Jump/FurnaceInside.eps}
\caption{\label{fig:Furnace} Furnace and Detector Schematics.}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=0.3]{Literature/Jump/SampleHolderLaser2.eps}
\caption{\label{fig:Holder} Sample and Holder Detail View.}
\end{center}
\end{figure}


\section{Experimental Evidence}
\label{sec:ExperEvid}


%\usepackage{graphicx}
%\usepackage{subcaption}

