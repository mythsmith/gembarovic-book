% !TEX root = mybook.tex

\chapter{Rectangular Parallelepiped Sample}
\label{ch:FiniteFourierExample}

\subsection{Problem Formulation}
As an example of using the Finite Fourier Transform we will find a solution of the heat conduction equation for a three dimensional rectangular parallelepiped sample (see Figure \ref{fig:RectangularSample}), initially at temperature $T_0(x,y,z)$, with boundary conditions of the third kind.

The boundary value problem is formulated as

\begin{marginfigure}
  \includegraphics[scale=0.8]{graphics/parallelepiped.eps}%
  \caption{Rectangular Sample with Heat Losses.}%
  \label{fig:RectangularSample}%
\end{marginfigure}
 
\begin{equation}
\label{eq:RectangularHCE}
\begin{split}
\frac{\partial^2 T}{\partial x^2}+\frac{\partial^2 T}{\partial y^2}+\frac{\partial^2 T}{\partial z^2} = \frac{1}{a}\frac{\partial T}{\partial t}, \\
x\in (0,L), y\in (0,b), z \in (0,c), t>0,
\end{split}
\end{equation}

with boundary conditions

\begin{equation}
\label{eq:RectHCE_x0}
-\frac{\partial  T(0,y,z,t)}{\partial x} + H_1 T(0,y,z,t) = 0,
\end{equation}

\begin{equation}
\label{eq:RectHCE_xa}
\frac{\partial  T(L,y,z,t)}{\partial x} + H_2 T(L,y,z,t) = 0,
\end{equation}


\begin{equation}
\label{eq:RectHCE_y0}
-\frac{\partial  T(x,0,z,t)}{\partial y} + H_3 T(x,0,z,t) = 0,
\end{equation}
\begin{equation}
\label{eq:RectHCE_yb}
\frac{\partial  T(x,b,z,t)}{\partial x} + H_4 T(x,b,z,t) = 0,
\end{equation}

\begin{equation}
\label{eq:RectHCE_z0}
-\frac{\partial  T(x,y,0,t)}{\partial z} + H_5 T(x,y,0,t) = 0,
\end{equation}

\begin{equation}
\label{eq:RectHCE_zc}
\frac{\partial  T(x,y,c,t)}{\partial z} + H_6 T(x,y,c,t) = 0,
\end{equation}
and the initial condition
\begin{equation}
\label{eq:RectHCE_initial}
T(x,y,z,0)= T_0(x,y,z),
\end{equation}
Coefficients $H_i, (i=1,2,\ldots, 6)$, are defined for all six sample surfaces as a ratio of the heat transfer coefficien,t $h_i$ (multiplied with the specimen's length), and the sample thermal conductivity $k$.

\subsection{A Solution Using Finite Fourier Transform}
We will define triple integral transform and the corresponding inversion formula for the $x,y,z$ variables of the temperature function $T(x,y,z,t)$ as

\begin{equation}
\label{eq:TripleIntTranformT}
 \bar{T}(\beta_m, \nu_n,\omega_k,t)=\int_0^L\int_0^b\int_0^c K(\beta_m,x)K(\nu_n,y)K(\omega_k,z)T(x,y,z,t)dxdydz,
\end{equation}

 \begin{equation}
 \label{eq:TripleInversionT}
 T(x,y,z,t)=\sum_{m=1}^\infty \sum_{n=1}^\infty \sum_{k=1}^\infty K(\beta_m,x)K(\nu_n,y)K(\omega_k,z)\bar{T}(\beta_m,, \nu_n, \omega_k,t).
 \end{equation}
 
We now take the the integral transform of the partial differential equation \ref{eq:RectangularHCE} by applying the triple integral transform \ref{eq:TripleIntTranformT},
\begin{equation}
\label{eq:TripleIntTranformHCE}
\begin{split}
\int_0^L\int_0^b\int_0^c K(\beta_m,x)K(\nu_n,y)K(\omega_k,z)\bigg( \frac{\partial^2 T}{\partial x^2}+\frac{\partial^2 T}{\partial y^2}+\frac{\partial^2 T}{\partial z^2}\bigg)dxdydz\\
=\frac{1}{a}\frac{d  \bar{T}(\beta_m, \nu_n,\omega_k,t)}{dt},
\end{split}
\end{equation}
which is reduced to an ordinary differential equation for the transformed temperature $\bar{T}(\beta_m, \nu_n,\omega_k,t)$ 

\begin{equation}
\label{eq:Tranformed3DHCE}
\frac{d \bar{T}(\beta_m,\nu_n,\omega_k,t)}{d t}+ a[\beta_m^2 + \nu_n^2 + \omega_k^2] \bar{T}(\beta_m, \nu_n,\omega_k,t)= 0,
\end{equation}
subject to the initial condition
\begin{equation}
\label{eq:TranformedInitT}
 \bar{T}(\beta_m, \nu_n,\omega_k,0)=\bar{T_0}(\beta_m, \nu_n,\omega_k)
\end{equation}
where
\begin{equation}
\bar{T_0}(\beta_m, \nu_n,\omega_k) =\int_0^L\int_0^b\int_0^c K(\beta_m,x)K(\nu_n,y)K(\omega_k,z)T_0(x,y,z)dxdydz .
\end{equation}
The solution of the equation \ref{eq:Tranformed3DHCE} subject to the initial condition \ref{eq:TranformedInitT} is straightforward and give the triple transform of temperature $\bar{T}$. Substituting the resulting triple transform into the inversion formula \ref{eq:TripleInversionT}, the solution of the problem of rectangular parallelepiped sample is
 \begin{equation}
 \label{eq:GenSolutionT}
 T(x,y,z,t)=\sum_{m=1}^\infty \sum_{n=1}^\infty \sum_{k=1}^\infty e^{-a[\beta_m^2 + \nu_n^2 + \omega_k^2]t} K(\beta_m,x)K(\nu_n,y)K(\omega_k,z)\bar{T_0}.
 \end{equation}
 The kernels $K(\beta_m,x)K(\nu_n,y)K(\omega_k,z)$ and the corresponding eigenvalues $\beta_m,\nu_n,\omega_k$ are calculated from the formulas
\begin{equation}
\label{eq: Kernel_m}
K(\beta_m,x) = \sqrt{2} \frac{\beta_m\cos(\beta_m x) + H_1\sin(\beta_m x)}{\bigg[(\beta_n^2+H_1^2)\bigg(L+\frac{H_2}{\beta_n^2+H_2^2}\bigg)+H_1\bigg]^{1/2}},
\end{equation}
\begin{equation}
\label{eq: Kernel_n}
K(\nu_k,y) = \sqrt{2} \frac{\nu_n\cos(\nu_n y) + H_3\sin(\nu_n y)}{\bigg[(\nu_n^2+H_3^2)\bigg( b+\frac{H_4}{\nu_n^2+H_4^2}\bigg)+H_3\bigg]^{1/2}},
\end{equation}
\begin{equation}
\label{eq: Kernel_k}
K(\omega_k,z) = \sqrt{2} \frac{\omega_k\cos(\omega_k z) + H_5\sin(\omega_k z)}{\bigg[(\omega_k^2+H_5^2)\bigg( c+\frac{H_6}{\omega_k^2+H_6^2}\bigg)+H_5\bigg]^{1/2}},
\end{equation}

where $\beta_m$, $\nu_n$, and $\omega_k$, are positive roots of transcendental equations
  \begin{equation} 
  \label{eq:beta_m} 
  	\tan(\beta_m L) = \frac{\beta_m(H_1+H_2)}{\beta_m^2-H_1H_2},
  \end{equation} 
 
  \begin{equation} 
  \label{eq:nu_n} 
  	\tan(\nu_n b) = \frac{\nu_n(H_3+H_4)}{\nu_n^2-H_3H_4},
  \end{equation} 
  
  \begin{equation} 
  \label{eq:omega_k} 
  	\tan(\omega_k c) = \frac{\omega_k(H_5+H_6)}{\omega_k^2-H_5H_6}.
  \end{equation}  

If  the initial condition is for an instantaneous uniform heat pulse at $x=0$ , so
\begin{equation}
\label{eq:Rect_Initial_deltax}
T(x,y,z,0)= T_0(x,y,z)=\delta(x),
\end{equation}
then 
\begin{equation}
\label{eq:Rect_barT_0}
\begin{split}
\bar{T_0}=\int_0^L\int_0^b\int_0^c K(\beta_m,x)K(\nu_n,y)K(\omega_k,z)\delta(x)dxdydz =\\
=K(\beta_m,0)\int_0^b\int_0^c K(\nu_n,y)K(\omega_k,z)dydz = \\
=\frac{2^{\frac{3}{2}}\beta_m}{\sqrt{\bigg[(\beta_n^2+H_1^2)\bigg(L+\frac{H_2}{\beta_n^2+H_2^2}\bigg)+H_1\bigg]}}\\
\times\frac{ \big(\sin(\nu_n b)+ \frac{H_3}{\nu_n}[1- \cos(\nu_nb)]\big)}{\sqrt{\bigg[(\nu_n^2+H_3^2)\bigg( b+\frac{H_4}{\nu_n^2+H_4^2}\bigg)+H_3\bigg]}}\\
\times\frac{\big(\sin(\omega_kc)+ \frac{H_5}{\omega_k}[1- \cos(\omega_kc)]\big)}{\sqrt{\bigg[(\omega_k^2+H_5^2)\bigg( c+\frac{H_6}{\omega_k^2+H_6^2}\bigg)+H_5\bigg]}}.
\end{split}
\end{equation}

Substituting $\bar{T_0}$ into the inversion formula \ref{eq:GenSolutionT} we have the solution for the temperature distribution in our sample
 \begin{equation}
  T(x,y,z,t)=\sum_{m=1}^\infty \sum_{n=1}^\infty\sum_{k=1}^\infty  e^{-a[\beta_m^2 + \nu_n^2 + \omega_k^2]t}  X(\beta_m, x) Y(\nu_n,y)Z(\omega_k,z),
 \end{equation}
 where
\begin{equation}
\label{eq:parallelepipedX}
  X(\beta_m, x) = \frac{2\beta_m[\beta_m \cos(\beta_m x) +H_1\sin(\beta_m x)]}{(\beta_n^2+H_1^2)\bigg(L+\frac{H_2}{\beta_m^2+H_2^2}\bigg)+H_1},
\end{equation}
\begin{equation}
\label{eq:parallelepipedY}
  Y(\nu_n, y) = \frac{2\nu_n[\sin(\nu_n b) +\frac{H_3}{\nu_n}(1-\cos(\nu_n b))][\cos(\nu_n y) +H_3\sin(\nu_n y)]}{(\nu_n^2+H_3^2)\bigg(b+\frac{H_4}{\nu_n^2+H_4^2}\bigg)+H_3},
\end{equation}
\begin{equation}
\label{eq:parallelepipedZ}
  Z(\omega_k, z) = \frac{2\omega_k[\sin(\omega_k c) +\frac{H_5}{\omega_k}(1-\cos(\omega_k c))][\cos(\omega_k z) +H_5\sin(\omega_k z)]}{(\omega_k^2+H_5^2)\bigg(c+\frac{H_6}{\omega_k^2+H_6^2}\bigg)+H_5},
\end{equation}
and $\beta_m$, $\nu_n$, and $\omega_k$, are positive roots of transcendental equations \ref{eq:beta_m}, \ref{eq:nu_n} and \ref{eq:omega_k}.  

If we measure the temperature rise in the center of the rear side (opposite to the heated side) of the sample, then the temperature in $x=L, y=b/2, z=c/2$, after the pulse, is
 \begin{equation}
 \label{eq:Parallelepiped}
  T(t)=\sum_{m=1}^\infty \sum_{n=1}^\infty\sum_{k=1}^\infty  e^{-a[\beta_m^2 + \nu_n^2 + \omega_k^2]t}  X(\beta_m, L) Y(\nu_n,b/2)Z(\omega_k,c/2),
\end{equation}
\begin{marginfigure}%
  \includegraphics[width=\linewidth]{Parallel_H1.eps}
  \caption{Response curves for a rectangular parallelepiped sample with different values of the Biot number $H_1$. }
  \label{fig:Parallel_H1}
\end{marginfigure}  
% generated using SquareSampleRead.R (10/4/2016)

Temperature responses for a rectangular parallelepiped sample for different values of the Biot number $H_1$ , are plotted in Figure \ref{fig:Parallel_H1}.
The curves were calculated using formula \ref{eq:Parallelepiped} for a sample with dimensions $L=0.1$ cm, $b=0.4$ cm and $c=0.4$ cm, with the thermal diffusivity, $a=0.01$ cm$^2$/s, and the Biot numbers $H_2=H_1$, $H_3=H_4=H_5=H_6=4H_1$. 

Temperature responses for rectangular parallelepiped and cylindrical shape samples are compared in Figure \ref{fig:Parallel_Square_comp}. The responses were 
calculated using formulas \ref{eq:Parallelepiped} and xxx. The curves plotted as dot-dashed lines are for the the cylindrical shape sample. The first rectangular parallelepiped sample (\texttt{0.01p}) dimensions were $L=0.1$ cm, $b=c=0.4$ cm, the second (\texttt{0.1p})  $L=0.1$ cm, $b=c=0.2$ cm, and the third (\texttt{0.2p}) $L=0.1$ cm, $b=c=0.1$ cm. Dimensions for the correspondent cylindrical samples were for the first sample (\texttt{0.01c}) $L=0.1$ cm, $R=0.2$ cm, for the second (\texttt{0.1c}) $L=0.1$ cm, $R=0.1$ cm, and for the third (\texttt{0.2c}) $L=0.1$ cm, $R=0.05$ cm. The Biot numbers for the front and rear sample surfaces were $0.01, 0.1$ and $0.2$. 
\begin{marginfigure}%
  \includegraphics[width=\linewidth]{Parallel_Square_comp.eps}
  \caption{Response curves for  rectangular parallelepiped and cylindrical samples for different values of the Biot number $H$ and different dimensions. }
  \label{fig:Parallel_Square_comp}
\end{marginfigure}  
% generated using SquareSampleRead.R (10/4/2016)
The response curves for for relatively thin samples (when the sample thickness is smaller than the other dimensions), the responses for cylindrical samples were the same or just slightly below those for the parallelepiped samples. Only in the case of a cubical parallelepiped sample is the response curve for the corresponded cylindrical sample of the same thickness with the diameter equal to the cubical sample side (dot-dashed curve \texttt{0.2c}) higher that the one for the rectangular parallelepiped sample (\texttt{0.2p}). 

\subsection{Extended and Nonuniform Pulses} 

If instead of one instantaneous heat source there is a sequence of instantaneous sources $(Q/\rho c_p)g_y(y')g_z(z')$ in the interval $t_1$ to  $t_2$, for $t>0$, with magnitudes proportional to the dimensionless function $\psi(t')$, then in order to obtain the temperature distribution at time $t$, we have to integrate from $t_1$, to $t$ when $t_1 < t < t_2$, or from $t_1$, to $t_2$, when $t > t_2$. 

The temperature at $t>t_2$ is
\begin{equation}
\label{eq:XYZComponentsPsi}
\begin{split}
T(x,y,z,t)=\mathlarger{\mathlarger{‎‎\sum}}_{m=1}^{\infty‎}\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\mathlarger{\mathlarger{‎‎\sum}}_{k=1}^{\infty‎} X(\beta_m x)
 \int_0^b g_y(y')Y(\nu_n,y')dy'\\\times \int_0^c   g_z(z')Z(\omega_k,z')dz'\int_{t_1}^{t_2}\psi(t')e^{-a[\beta_m^2 + \nu_n^2 + \omega_k^2](t-t')}dt',
\end{split}
\end{equation}
where functions $X$, $Y$ and $Z$ are defined in equations \ref{eq:parallelepipedX}, \ref{eq:parallelepipedY}, and \ref{eq:parallelepipedZ}, respectively.

    