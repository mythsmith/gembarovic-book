% !TEX root = mybook.tex

\chapter{Optimal Experimental Design}

Decisions made in designing experiment in the flash method include choosing specimen shape and dimensions, specifying a length of time for the experiment to be performed, number of points to be collected, physical model to be used for analysis, environmental considerations, etc. 

\section{Sensitivity Analysis}

\subsection{ One Point Analysis}

When Parker choose half of the maximum temperature rise and correspondent halftime $t_{1/2}$ for the thermal diffusivity $a$ determination from a response curve, using 
\begin{equation}
a=0.1388\frac{L^2}{t_{1/2}},
\end{equation}
the decision was probably guided only by his intuition. Theoretically, he could choose any other fraction $\beta$ of the rise\sidenote{Values of $\beta < 0.1$, or $\beta>0.9$, are not practical, because of uncertainty in determining corresponding $t_\beta$ due to a noise in response curves.}, calculate a corresponding value of $\omega_\beta$ as a root of 
\begin{equation}
\label{eq:omega_ideal_model}
\beta - 1 - 2\sum_{n=1}^{\infty} (-1)^n \exp\big[-n^2\pi^2\omega_\beta \big]=0,
\end{equation}
and then calculate the thermal diffusivity as
\begin{equation}
a=\omega_\beta\frac{L^2}{t_\beta}.
\end{equation}
Values of $\omega_{\beta}$ as a function of $\beta$, calculated using formula \ref{eq:omega_ideal_model} for the ideal model, are plotted in Figure \ref{fig:omega_beta}.
\begin{marginfigure}
 \centering
  \includegraphics[scale=1.0]{Literature/Sensitivity/omega_beta.eps}%
  \caption{Calculated values of $\omega_\beta$ as a function of $\beta$  for the Ideal Model.}
  \label{fig:omega_beta}%
\end{marginfigure}
%generated using sensit_flash0omega.R (7/24/2017) 

 How we can find the best, optimal value of $\beta$ for the analysis?
  
The optimal value of $\beta$ should be located at the point where a small change of the thermal diffusivity value would lead to the biggest change of the response temperature. In other words; it is the point where temperature response curve is the most \textit{sensitive} to the thermal diffusivity. 

%Since the thermal diffusivity is in the nominator of the dimensionless time (Fourier number), we will find the point where the response curve is most sensitive to $\omega$.

In order to find the optimal point, we will calculate function $S$ as a partial derivation of the normalized response curve $V$ with respect to $a$, multiplied with $a$: 
\begin{equation}
\label{eq:SensitivityOmega}
 S=a\frac{\partial V}{\partial a},
\end{equation} 
which is by definition directly proportional to the sensitivity of $V$ to $a$. The optimal $\beta$ is then calculated as $\beta = V(\omega^*)$, where $\omega^*$ is the dimensionless time point where function $S$ has its maximum.

For the normalized ideal response, given in formula \ref{eq:Tidealdimless}, function $S$ is  
\begin{equation}
\label{eq:Somega}
\begin{split}
S = a\frac{\partial}{\partial a}\bigg[1 + 2\sum_{n=1}^{\infty} (-1)^n \exp\big[-n^2\pi^2at/L^2\big]\bigg]\\
 =-2\pi^2\omega\sum_{n=1}^{\infty} (-1)^n n^2\exp\big[-n^2\pi^2\omega \big],
 \end{split}
\end{equation} 
where $\omega=at/L^2$ is the dimensionless time.

A graph of $S$ as a function of $\omega$ is in Figure \ref{fig:sensitIdeal}. In this graph, $\omega$ is divided by dimensionless halftime value $\omega_{1/2}=0.1388$. The ideal temperature response curve is also plotted for a comparison. As we can see, the maximum of function $S$ lies very close to the value originally proposed by Parker. An exact analytical treatment of \ref{eq:Somega} will show that the maximum of $S$ is in $\omega^*=0.134$, which corresponds to $\beta=0.477$.  
\begin{marginfigure}
 \centering
  \includegraphics[scale=1.0]{Literature/Sensitivity/sensitIdealomega.eps}%
  \caption{Function $S$ for the Ideal Model.}
  \label{fig:sensitIdeal}%
\end{marginfigure}
%generated using sensit_flash01.R (8/23/2017) 
After all, Parker's guess value $\beta = 1/2$, was quite close to the optimal value!

\subsection{Ideal Model Response Curve Reconstruction}

It is important to note that Parker and other practitioners of the flash method in the sixties were not \textit{reconstructing} the response curves in order to estimate the thermal diffusivity of sample material. They were looking for a one point of the response curve - the experimental halftime. Usually, they took a photograph of an oscilloscope trace of the signal from temperature sensor. On the photograph, they draw a horizontal line as a baseline, draw second horizontal line on the maximum temperature rise level, and then draw the third horizontal line halfway between those two lines and estimated the halftime value at the intersection of the third line with the response curve trace. In case of a noisy signal, there was an uncertainty in finding all these lines and the point for estimating the halftime value.

With introducing digital data acquisition systems and use of computers for signal analysis in eighties and nineties, experimenters started to use more sophisticated mathematical curve-fitting methods and they were able to reconstruct the response curves in the flash method. It resulted in much more objective and more precise evaluation of the thermal diffusivity because experimenters gained a feedback when they could see and numerically evaluate how well reconstructed response curves fitted experimental points. 

Even if we want to reconstruct response curve using the simplest formula in the ideal model, we have to know the sample thickness ($L$) and calculate values of three parameters: the baseline level ($T_0$), the maximum temperature rise ($T_{max}$), and the thermal diffusivity ($a$).

The baseline level can be estimated from the signal recorded before the flash using a linear regression curve fitting technique. Once $T_0$ is known and fixed then the maximum temperature rise and the thermal diffusivity can be estimated from the temperature response signal using one of the nonlinear regression methods described in Chapter \ref{Ch:Nonlinear Regression}. 

In a digital acquisition system the response curve is usually recorded for quite a long time after the transient temperature response. In order to optimize the parameter estimation process, we have to decide what part of the response curve should be taken into the calculation. If e.g. we take only a short time interval after the flash, the calculation may take a short time, but we could have a large uncertainty in estimating of the response curve maximum, $T_{max}$. 

If, on the other hand, we choose time interval which is too long, the calculation takes a long time, and the transient (rising) points of the response curve may represent only a small portion of the total number of points. In this case, we can have a large uncertainty in the thermal diffusivity estimation. Evidently, there is a time interval (time window) of the response curve, which is optimal with regards to the calculation duration and the uncertainty in both desired parameters, $T_{max}$ and $a$.

An important question is also if the both desired parameters can be estimated \textit{simultaneously} and \textit{unambiguously}? Are they identifiable?        

According to the theory of parameter estimation, described by Beck and Arnold\cite{beck1977parameter}, model parameters can be determined simultaneously and unambiguously only within a time window, where the sensitivity coefficients of the parameters are not linearly dependent.

Sensitivity coefficient for parameter $p_i$ is defined as
\begin{equation}
\label{eq:SensitivityCoefficientDefinition}
S_i = p_i\frac{\partial T}{\partial p_i},
\end{equation}
where $T=T(\vec{r},t,\vec{p})$  is the temperature response function, with independent variables of position and time $\vec{r}, t$ and model parameter vector $\vec{p} = (p_1, p_2, \dots, p_n)$.

Sensitivity coefficients $S_i$, $i=1,2,\dots, n$, are linearly dependent when 
\begin{equation}
\label{eq:Linear Dependence S}
C_1S_1+C_2S_2+\dots +C_nS_n=0
\end{equation}
is true for all observed points and not all arbitrary constants $C_i$ equal to zero. 

\begin{marginfigure}
  \includegraphics[scale=1.0]{Literature/Sensitivity/sensitideal_Tmax_a.eps}%
  \caption{Sensitivity Coefficients $S_a$ and $S_{Tmax}$ for the Ideal Model.}
  \label{fig:sensitIdealomegaTm}%
\end{marginfigure}
%generated using sensit_flash01.R (8/13/2017) 
As we can see from Figure \ref{fig:sensitIdealomegaTm}, sensitivity coefficient curves for the thermal diffusivity $S_a$ and the maximum temperature rise $S_{Tmax}$ in the ideal model are not linearly dependent in the time window from zero to $7\omega_{1/2}$. There is no way we can multiply them with constant numbers (to stretch or squeeze them) to fit each other. These two parameters therefore can be estimated from a response curve simultaneously and unambiguously if we take into account points close to the beginning of the response, up to at least 3 or 4 halftime value.

\subsection{Sample Thickness Estimation}

Sample thickness should be small enough to minimize experimental time and to prevent heat losses from the sample surface, but big enough to avoid the finite pulse time effect.

The optimal sample thickness for a material with approximately known thermal diffusivity $a$, can be estimated from the Parker's formula \ref{eq:ParkerFormula}, as 
\begin{equation}
\label{eq:ThicknessEstimate}
  L\approx \sqrt{\frac{at^*_{1/2}}{0.1388}}.
\end{equation}  
where $t^*_{1/2}$ is the expected halftime. Typical laser pulse duration is $0.3$ ms, so in order to avoid problems with the finite pulse time effect, $t^*_{1/2}$ should be bigger than $30$ ms. On the other hand, if we want to minimize heat losses, $t^*_{1/2}$ value should be smaller than $80$ ms.

If, for example, sample material has estimated thermal diffusivity $a=0.1$ cm$^2$/s, and by setting $t^*_{1/2}=30$ ms, then from \ref{eq:ThicknessEstimate} we get for the sample thickness $L\approx 1.5$ mm. 

For low thermal diffusivity materials the sample thickness is often limited by the nature of material. If the estimated thermal diffusivity is $a=0.001$ cm$^2$/s, then by setting $t^*_{1/2}=30$ ms, we get for the sample thickness value $0.15$ mm. This "hair-thin" value is not realistic because most of these low conductive materials are non-homogeneous and minimal sample thickness has to be chosen big enough to have a representative layer of the material in the trough-thickness direction.  

%: ...  The estimated halftime value $t^*_{1/2}$ represents time interval where large enough number of temperature points, say $N=500$, is collected by the data acquisition system. It depends on the actual frequency of data collection used in the experiment. For example, if A/D converter is generating $10^4$ points per second (frequency $f=10^4$ Hz), then the estimated halftime value should be $t_{1/2} = N/f = 0.050$ s. If the sample material has estimated thermal diffusivity $a=0.1$ cm$^2$/s, then from \ref{eq:ThicknessEstimate} we get for the sample thickness $L\approx 1.9$ mm. 

\subsection{Multi-parameter Identification in Other Models} 
 
If we want to fit response curves with a more realistic one dimensional model with heat losses, described by \ref{eq:1DHLsolutionRearDimless},
\begin{equation}
\label{eq:1DHLsolutionRearT}
 T(t,T_0,T_m,a,H)= T_0+2T_m\mathlarger{\mathlarger{\sum}}_{n=1}^{\infty}\frac{\beta_n^2\cos\beta_n}{\beta_n^2+H^2+2H}\exp\bigg[{-\frac{\beta_n^2 a}{L^2}t}\bigg],
\end{equation}
where $H$ is the Biot number for both front and rear surfaces, and $\beta_n$ are positive roots of the transcendental equation
 \begin{equation} 
  \label{eq:BtranscendentalT} 
  	(\beta^2-H^2)\tan(\beta) - 2\beta H = 0,
  \end{equation} 
we have to identify four parameters:  the baseline level ($T_0$), the maximum temperature rise ($T_m$), the thermal diffusivity ($a$), and the Biot number ($H$). As in the ideal model, $T_0$ value can be again calculated from the signal recorded before the flash using a linear regression curve fitting technique. There are now three parameters, which have to be found from the response curve: $T_m$, $a$ and $H$. 

Can all these three parameters be estimated from a response curve simultaneously?
\begin{marginfigure}
 \centering
  \includegraphics[scale=1.0]{Literature/Sensitivity/sensit_1DHL_b1.eps}%
  \caption{Sensitivity Coefficients for a one dimensional model with heat losses given in \ref{eq:1DHLsolutionRearT} and \ref{eq:BtranscendentalT}.}
  \label{fig:sensit1DHL}%
\end{marginfigure}
%generated using sensit_flashb1.R (8/22/2017)
Sensitivity coefficient $S_{Tm}$, $S_a$ and $S_H$ for a one dimensional model with heat losses, calculated using four different values of the Biot number, are shown in Figure \ref{fig:sensit1DHL}. We intentionally chose four values of the Biot number in order to show differences in sensitivities for a case of small heat losses ($H=0.01$), intermediate heat losses ($H=0.1, 0.5$), and large heat losses ($H=1$). Sensitivity to the Biot number $S_H$ is relatively very small for a small heat losses in the shown time interval. It is therefore difficult to estimate heat loss magnitude, and it is better to use an ideal model instead of the model with heat losses.

As we can see from Figure  \ref{fig:sensit1DHL}, sensitivity curves for intermediate and large Biot numbers are not linearly dependent in time interval $0<\omega<1$. If we start the analysis in a later time, say $\omega>0.5$, then all three sensitivity coefficient curves have quite similar shapes, which is an indication of their linear dependence. Only later parts of the response curve therefore cannot be used for a simultaneous identification of all three parameters. 

 
Sensitivity to heat losses increases with increasing Biot number value. It is interesting to note that later parts of the response curve in this model are also quite sensitive to the thermal diffusivity determination. 
  
We can therefore conclude that both small times, as well as later times of the response curve have to be included into a regression analysis if we want to estimate the thermal diffusivity value and fully reconstruct the response curve.

Optimal design of experiments where two or three-dimensional models are used, or models for samples with multiple layers, requires sensitivity analysis which is usually much more complicated, because the number of optimized parameters increases. Despite of its complexity, it is still often used method for experiment optimization. 

\section{Bayesian Approach}

Read and make references to Chapter \ref{ch:BayesianInference}!

The basic idea in experimental design is that statistical inference about the quantities of interest can be improved by appropriately selecting the values of control variables. These values should be chosen to achieve small variability for the estimator of interest -- thermal diffusivity. Decisions must be made before data collection. Because specific information is usually available prior to experiment, Bayesian inference technique can play an important role. 
(excerpts from BayesianDesignReview1995.pdf Kathruine Chaloner)