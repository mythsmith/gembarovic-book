% !TEX root = mybook.tex
\section{Laplace Transform}
The Laplace transform, described in this section, when applied to problems involving only one space variable, have multiple advantages over the Fourier and Hankel transforms, namely [CJ59 p.455] 
\begin{enumerate}[(i)]
\item
it provides a routine procedure in the same way to all problems
\item
applies indifferently to all boundary conditions, so the necessity of developing a new kernel for each type of boundary conditions is avoided
\item
convergence difficulties of resulting formulas are avoided
\item
large body of simple theorems which can be used to produce new results and transforms.
\end{enumerate}
For problems involving more than one space variables the advantages are not so  obvious, and either the Fourier or the Hankel transforms are used on space variables after the time variable is removed using the Laplace transform.

\subsection{Definition}


Suppose that $v(x,y,z,t)$ is a piecewise continuous function. The Laplace transform of $v(x,y,z,t)$ is denoted $\mathcal{L}\big\{v\big\}$, or $\bar{v}$  and defined as
\begin{equation}
\label{eq:LaplaceTransfDef}
 \mathcal{L} \big\{v(x,y,z,t)\big\}=\int_0^\infty e^{-pt} v(x, y, z, t) dt = \bar{v}.
\end{equation}
It takes a function of a positive real variable $t$ (time) to a function of a complex variable $p$ (often frequency).

\subsection{Basic Properties}
We will write only three most important theorems, which will be used later. Interested  reader can find more detail description of the transform properties in  CJ59 p.300-301.
\begin{enumerate}[\textsc{Theorem} I.]
\item %Theorem I.
%Linearity
\begin{equation}
\label{eq:LaplaceTheoremI}
\mathcal{L}\{v_1+v_2\}=\mathcal{L}\{v_1\}+\mathcal{L}\{v_2\}.
\end{equation}
\item %Theorem II.
%Transform of time derivation
\begin{equation}
\label{eq:LaplaceTheoremII}
\mathcal{L}\bigg\{\frac{\partial v}{\partial t}\bigg\}=p\mathcal{L}\{v\}-v_0,
\end{equation}
where $v_0$ is
\begin{equation*}
v_0=\lim_{t \to +0}v.
\end{equation*}
The result \ref{eq:LaplaceTheoremII} follows immediately on integrating by parts, since
\begin{equation*}
\int_0^\infty e^{-pt}\frac{\partial v}{\partial t}dt=\big[e^{-pt}v\big]_0^\infty + p\int_0^\infty e^{-pt}v dt=-v_0 + p\bar{v}.
\end{equation*}
\item %Theorem III.
%Transform of space derivation
\begin{equation}
\label{eq:LaplaceTheoremIII}
\mathcal{L}\bigg\{\frac{\partial^n v}{\partial x^n}\bigg\}=\frac{\partial^n \bar{v}}{\partial x^n},
\end{equation}
with similar results for the other space variables. This is equivalent to
\begin{equation}
\int_0^\infty e^{-pt}\frac{\partial^n v}{\partial x^n}dt=\frac{\partial^n}{\partial x^n}\int_0^\infty e^{-pt}vdt,
\end{equation}
where we assumed that function $v$ to be such that the orders of integration and derivation can be interchanged in this way.
\end{enumerate}

\subsection{Inverse Transform}
The inverse Laplace transform is defined as
\begin{equation}
\label{eq:InvLaplaceTransfDef}
 v(x,y,z,t)=\mathcal{L}^{-1}\big\{\bar{v}\big\}=\frac{1}{2\pi i} \int_{\alpha-i\infty}^{\alpha+i\infty}\bar{v}e^{pt}dp,
\end{equation} 
where $\alpha$ is a real number so that the contour path of integration is in the region of convergence of $\bar{v}$.

To evaluate the inverse Laplace transform using \ref{eq:InvLaplaceTransfDef} is often a very difficult task. In practice, it is typically more convenient to decompose a Laplace transform into known transforms of functions obtained from a table of Laplace transforms\cite{oberhettinger2012tables} and construct the inverse by inspection.
 
The inverse transform can be also found using numerical approximation algorithms, for example, the Stehfest algorithm\cite{stehfest1970algorithm}, or the improved De Hoog algorithm\cite{de1982improved}.

\subsection{Example}

Boundary value problem for a thermally insulated sample, initially at zero temperature, irradiated with an instantaneous heat source, can be reformulated as
\begin{equation}
\label{eq:1DLaplace}
	\frac{\partial T(x,t)}{\partial t}=a\frac{\partial^2 T(x,t)}{\partial x^2} , \quad 0<x<L, \quad t>0,
\end{equation}
with the initial condition 
\begin{equation}
\label{eq:initZero}
 T(x,0) = 0,
\end{equation}
and the boundary conditions
\begin{equation}
\label{eq:NeumanLaplace}
-k\frac{\partial T(0,t)}{\partial x}=Q\delta(t),  \quad \frac{\partial T(L,t)}{\partial x}=0,
\end{equation}
where $Q$ is the amount of heat generated by the heat pulse per unit area of the sample surface, and $\delta(t)$ is the Dirac delta function.

We will now find a solution of this boundary value problem using the Laplace transform technique.

Multiply both sides of  \ref{eq:1DLaplace} with $e^{-pt}$ and integrate from $0$ to $\infty$
\begin{equation}
\label{eq:1DLaplace}
	\int_0^{\infty}\frac{\partial T(x,t)}{\partial t}e^{-pt}dt=a\int_0^{\infty}\frac{\partial^2 T(x,t)}{\partial x^2}e^{-pt}dt.
\end{equation}
After applying \textsc{Theorems} \ref{eq:LaplaceTheoremII} and \ref{eq:LaplaceTheoremIII}, the equation \ref{eq:1DLaplace} will be
\begin{equation}
\label{eq:1DLaplaceTransformed0}
	p\bar{T}(x,p) - 0 = a\frac{\partial^2 \bar{T}(x,p)}{\partial x^2},
\end{equation}
where $\bar{T}$ is the Laplace transformation

\begin{equation}
\label{eq:LTransformed}
	\bar{T}(x,p)=\int_0^{\infty} e^{-pt}T(x,t)dt.
\end{equation}
Now, if we define 
\begin{equation*}
	q=\sqrt{p/a}.
\end{equation*}
then equation \ref{eq:1DLaplaceTransformed0} can be simplified as
\begin{equation}
\label{eq:1DLaplaceTransformed}
  \frac{\partial^2 \bar{T}(x,p)}{\partial x^2}= q^2\bar{T}(x,p).
\end{equation}
The boundary conditions \ref{eq:NeumanLaplace} are transformed to  
\begin{equation}
\label{eq:TNeumanLaplace}
-k\frac{\partial\bar{T}(0,t)}{\partial x}=Q,  \quad \frac{\partial\bar{T}(L,t)}{\partial x}=0.
\end{equation}

The solution of (\ref{eq:1DLaplaceTransformed}), with conditions (\ref{eq:TNeumanLaplace}), is

\begin{equation}
\label{eq:1DLaplaceSolution}
\begin{split}
	\bar{T}(x,p)=\frac{Q}{qk}\bigg(\frac{e^{-qx}}{1-e^{-2qL}} + \frac{e^{qx}}{e^{2qL}-1}\bigg)= \\
	=\frac{Q}{k}\mathlarger{\sum}_{m=0}^\infty \bigg(\frac{e^{-qx-2mqL}}{q} + \frac{e^{qx-2qL-2mqL}}{q}\bigg)= \\
	=\frac{Q}{k}\mathlarger{\sum}_{m=0}^\infty \bigg(\frac{e^{-q(2mL-x)}}{q} + \frac{e^{-q(2mL+2L -x)}}{q}\bigg),
\end{split}	
\end{equation}
where we used the expansions
\begin{equation*}
\frac{1}{1-e^{-2gL}} = \mathlarger{\sum}_{m=0}^\infty e^{-2mqL},\quad \frac{1}{e^{2gL}-1} = \sum_{m=0}^\infty e^{-q(2mL+2L)}. 
\end{equation*}

From the table of Laplace transforms\cite{oberhettinger2012tables} we will find the inverse transform as
\begin{equation}
\label{eq:Inverse_eq_over_q}
\mathcal{L}^{-1}\bigg\{\frac{e^{-qx}}{q}\bigg\}=\sqrt{\frac{a}{\pi t}}\exp{\bigg[-\frac{x^2}{4at}\bigg]}.
\end{equation}

Inverting the terms in \ref{eq:1DLaplaceSolution}, using \ref{eq:Inverse_eq_over_q}, we have

\begin{equation}
\label{eq:SmallTimesFormulax}
\begin{split}
T(x,t)= \frac{Q}{k}\sqrt{\frac{a}{\pi t}}\mathlarger{\sum}_{m=0}^\infty\bigg\{\exp\bigg[{-\frac{(2mL+x)^2}{4at}\bigg]}
\\
+ \exp\bigg[{-\frac{(2mL+2L-x)^2}{4at}\bigg]} \bigg\}.
\end{split}	
\end{equation} 

If we use dimensionless time $\omega=at/L^2$, then
\begin{equation}
\label{eq:SmallTimesxdimless}
\begin{split}
T(x,\omega)= \frac{Q}{\rho c L}\frac{1}{\sqrt{\pi \omega}}\mathlarger{\sum}_{m=0}^\infty\bigg\{\exp\bigg[{-\frac{(2m+x/L)^2}{4\omega}\bigg]}
\\
+ \exp\bigg[{-\frac{(2m+2-x/L)^2}{4\omega}\bigg]} \bigg\}.
\end{split}	
\end{equation} 
This solutions can be easily visualized. Temperature distribution within the region is a sum of terms which are all based on a solution for a thermally insulated semi-infinite space, initially at zero temperature, with a unit instantaneous heat source at $x=0$,  
\begin{equation}
\label{eq:semi-infiniteT}
T(x,\omega)=\frac{1}{\sqrt{\pi \omega}}\exp\bigg[{-\frac{(x/L)^2}{4\omega}}\bigg], \quad x\geq 0, \quad \omega>0
\end{equation}
where $L$ stands for a unit of length. The solution for a finite medium can be imagined as if we "folded" the curve for a semi-infinite solution at $x=1$, as if the boundary plane reflected the wave back toward the origin. Then we fold the curve again at the boundary plane $x=0$, then again at $x=1$, etc. The semi-infinite curve will be this way folded between $x=0$ and $x=1$ boundaries. The temperature for such a finite medium will be the sum of these folded curves. Theoretically, the semi-infinite curve should be folded many (infinite) times, since the curve is only asymptotically closing to zero. In practice, only a small number of folded curves (terms) is necessary, due to a fast convergence of \ref{eq:SmallTimesxdimless}, where the dimensionless time $\omega$ is in denominator of an exponential function. 
\begin{marginfigure}%
  \includegraphics[width=70mm,scale=0.4]{FoldedWaveBlue.eps}
  \caption{Folded Curves for a Semi-Infinite Medium.}
  \label{fig:FoldedWave}
\end{marginfigure}
The process of folding is illustrated in Figure \ref{fig:FoldedWave}, where the semi-infinite temperature curve, calculated using equation \ref{eq:semi-infiniteT}, is depicted in $0<x<4$ for $\omega=0.4$. Four folded curves (which represent terms for $m = 0,1$ from \ref{eq:SmallTimesxdimless}) are shown within $0<x<1$, and the resulting temperature for a finite medium, calculated as a sum of those four folded curves is on top of the graph as a red line. From this graph it is clear that not many terms are actually needed to calculate the resulting temperature in the finite medium, since for small times ($\omega<0.5$) the terms in \ref{eq:SmallTimesxdimless} converge rapidly.    

For the normalized temperature rise on the sample's rear surface, where $x/L=1$, we have 

\begin{equation}
\label{eq:SmallTimes1dimless}
V(\omega)=\frac{T(\omega)}{T_m} =\frac{2}{\sqrt{\pi \omega}}\mathlarger{\sum}_{m=0}^\infty \exp\bigg[{-\frac{(2m+1)^2}{4\omega}\bigg]} .
\end{equation}  
For example, to calculate the value of $V$ for $\omega = 0.01$, with error less than $10^{-3}$, it is enough to take only the first term for $m=0$ of the solution \ref{eq:SmallTimes1dimless}, because the contribution of the second term for $m=1$ is of order $10^{-97}$. The convergence is slower for large times and we have to summarize much more terms than if using the formula \ref{eq:TidealLt}. (elaborate!!!). 

Formula \ref{eq:SmallTimes1dimless} therefore represents an alternative formula to the previously given solution \ref{eq:TidealLt}, which was obtained using the separation of variables. 
