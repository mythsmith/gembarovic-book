\chapter{Using the Fourier Transform for Data Reduction}

In this method, experimental temperature vs. time data are first properly periodized, then transformed by using the discrete Fourier transformation, and then they are fitted with an appropriate theoretical formula. The procedure allows to treat a perturbed signals, even in the case when the data are partly statistically correlated, i.e. the noise is not Gaussian. The mean advantage of the procedure is that the thermal diffusivity calculation actually does not depend on temperature level before flash (baseline), and can be applied also in the cases when the the measured signal of temperature of the sample is superimposed with an arbitrary, but linearly rising, or falling signal, with no needs to know the parameters of this imposed signal. This is an unique, completely new feature of this procedure, which has no analogy among the others existing method of data reduction. Practically it means, that the thermal diffusivity of the sample can be measured by the flash method under arbitrary dynamically (linearly) changing condition of its temperature. Experimentally, the data reduction procedure has been tested for a correction of the effect of an electronic noise imposed on temperature vs. time signal in the flash method. The results show that the accuracy of the procedure is comparable with other data reduction methods. 

\section{Discrete Fourier Transform}
The discrete Fourier transform (DFT) is a relatively new transform. A major development of discrete Fourier transforms appeared in 1965, when J. W. Cooley and J. W. Tukey \cite{cooley1965algorithm} described a very fast algorithm for computation known as the Fast Fourier Transform (FFT). Thanks to this algorithm the DFT became the most widespread means of numerical computation of the Fourier transform and of numerical harmonic analysis.

Direct DFT of a sequence of $N$ finite complex numbers $x_i$, $i=0,1,2,\ldots,N-1$, is defined \cite{oppenheim1999discrete} as a sequence of values $X_k$ given by
\begin{equation}
\label{eq:DFT}
 X_k=\sum_{i=0}^{N-1} x_le^{-ijk2\pi /N},\quad k=0,1,2,\ldots,N-1,
\end{equation}
where $j=\sqrt{-1}$. We shall call the sequence $X_k$ the image of the sequence $x_i$. 
If $x_k$, $k=0,1,2,\ldots,N-1$, is a sequence of $N$  finite complex numbers, then its inverse DFT is a sequence of $N$ complex numbers expressed by the relation
\begin{equation}
\label{eq:DFTinverse}
x_i=\frac{1}{N}\sum_{k=0}^{N-1} X_ke^{ijk2\pi /N},\quad i=0,1,2,\ldots,N-1.
\end{equation}
In the case where a sequence $X_k$ is an image of a sequence $x_i$, the relation \ref{eq:DFTinverse} expresses the original sequence $x_i$, and is inverse to the relation \ref{eq:DFT}.

DFT converts the sampled function from its original time domain to the frequency domain, where components $k$ are multiples of the fundamental frequency.

\subsection{DFT of the Ideal Temperature Response}

Experimental points of measured temperature data are in the ideal model represented by a sequence of  $N$  real numbers $T_i$ given by
\begin{equation}
\label{eq:LTDR Tideal Discrete}
 T_i =T_m\Bigg\{ 1 + 2\sum_{n=1}^{\infty} (-1)^n \exp\bigg[-\frac{n^2\pi^2 a}{L^2}(i+1)\vartheta \bigg] \Bigg\}, \quad i=0,1,2,\ldots, N-1,
\end{equation} 
where $T_m$ is the maximum adiabatic temperature rise, and $\vartheta$ is time between two consecutive temperature points.

Before computing the formula for theoretical ideal image of the temperature response with using Eq. \ref{eq:DFT},  we will define the sequence $x_i$ as a composition of two sequences -- the original $E_i$ and $E_i$ in reverse order 
\begin{equation}
\label{eq:LTDR T periodization}
 x_i = \begin{cases}  T_i, \quad  \quad \quad \quad i=0,1,2,\ldots, N-1,\\ 
 T_{2N-1-i}, \quad  i=N, N+1, N+2,\ldots,2N-1. \end{cases}
\end{equation}
Such a "periodized" sequence of total length $2N$ is more suitable for the digital Fourier transformation than the original sequence, since the periodization minimizes the effect of leakage\cite{harris1978use} responsible for appearance of false spectral contributions due to discontinuities at the boundaries of the original sequence.

This periodization of temperature response enables to use this data reduction method also in the case when the experimental response is superimposed by a linearly raising, or falling signal.
 
After inserting the sequence $x_i$ from Equation \ref{eq:LTDR T periodization} into Equation \ref{eq:DFT}, with using the formula for summation of a geometrical sequence, the image  sequence $X_k$ can be written in the form
\begin{equation}
\label{eq:LTDR Xk}
\begin{split}
 X_k =T_m\Bigg\{ N\delta_k + 2\sum_{n=1}^{\infty} (-1)^ne^{\zeta_n\vartheta}\Bigg[ \frac{1-\exp(-\zeta_n\vartheta N/2 - jk\pi)}{1-\exp(-\zeta_n\vartheta - jk2\pi/N)}+ \\
 +\frac{\exp(-\zeta_n\vartheta N/2 - jk\pi)-\exp(- jk2\pi)}{\exp(-\zeta_n\vartheta)-\exp(- jk2\pi/N)} \Bigg] \Bigg\},
 \end{split}
\end{equation} 
where
\begin{equation}
\delta_k=\begin{cases} 1, \quad k=0, \\
0, \quad k\ne 0,\end{cases} \quad \zeta_n=\frac{n^2\pi^2a}{L^2}, \quad k=0,1,2,\ldots, N-1.
\end{equation}

As we can see from \ref{eq:LTDR Xk}, each term of the image sequence $X_k$ is a unique function of the thermal diffusivity, $a$, and can be used for computing of the desired value of thermal diffusivity. 

It should be noted that although the theoretical formulas derived for the image temperature here are for the simplest ideal model, it is easy to derive similar ones for more complicated models, including heat losses from the sample, or for multi-layer samples. 

\section{Data Reduction Procedure -- Principle}

Experimental temperature data can be represented as a sequence of $M$ numbers $E_i$, where $E_i$ is the temperature in known time instant, $t=(i+1)\vartheta$, and $i=0,1,2,\ldots, M-1$. 
%If $M=2^K$, where $K$ is an integer number (usually greater then 6), then the response signal can be transformed by using the well-known standard FFT algorithm. The same periodization of $E_i$  has to be done before transformation, as this given by Eq. \ref{eq:LTDR T periodization}

Because $E_i$  is a sequence of real numbers, the image $\bar{E}_k(N,a)$  can be expressed as a sequence of  $(N/2 - 1)$ complex numbers. Due to a fact that the higher terms are more sensitive to a noise, for data reduction it is reasonable to choose as low term as possible. Zero term of the image temperature (for $k=0$) is affected by  presence of a shift of the response curve up, or down as a whole (by adding a constant to the signal), while the first term is sensitive on superimposed linear rising or falling signal. This is why we will use for the fitting procedure the real part of the second term of the image temperature $\bar{E}_2(N,a)$. 


From the theoretical formula given by Eq.\ref{eq:LTDR Xk}, for the second term (k  = 2) we have

\begin{equation}
\label{eq:LTDR Xk 2nd term}
\begin{split}
 Re\{X_2(N,a)\} =2T_m\big(1+C(N)\big)\times \\
 \times \sum_{n=1}^{m} (-1)^n\frac{W(a,n)\big(1-W(a,n)\big)\big(1-W^{N/2}(a,n)\big)}{1+W^2(a,n)-2C(N)W(a,n)},
 \end{split}
\end{equation} 
where
\begin{equation}
W(a,n)=\exp\bigg(-\frac{n^2\pi^2a\vartheta}{L^2}\bigg), \quad C(N)=\cos\frac{4\pi}{N}.
\end{equation}



As can be seen from Equation \ref{eq:LTDR Xk 2nd term}, the second unknown parameter, the steady state temperature after the pulse, $T_m$, can be eliminated by dividing two values of the transformed temperature calculated for different values of $N$, i. e. for two different time intervals. Comparison of these rates of the transformed experimental data with the theoretical one is the basis for the so-called "two points" algorithm.
\begin{marginfigure}
\includegraphics[scale=0.8]{FTDRE256}
\caption{\label{fig:FTDRE256} Example of the response curve.}
\end{marginfigure} 
 The function $F(a)$, defined as
 \begin{equation}
 \label{eq:LTDR Fa Two Points}
 \begin{split}
 F(a)=Re\{\bar{E}_2(N)\}\ Re\{X_2(N/2,a)\}+\\
 -Re\{X_2(N/2,a)\}\ Re\{\bar{E}_2(N)\},
 \end{split}
 \end{equation}
has a simple root, $a^*$, which is the desired thermal diffusivity of the sample. 

% eps Figures were generated using FiguresFTDRnew.R
\subsection{Example 1}
\begin{marginfigure}
\includegraphics[scale=0.6]{FTDRfftE256128}
\caption{\label{fig:FTDRfftE256128} Fourier spectrum of the periodized response curves.}
\end{marginfigure}
In order to illustrate the procedure for identification of the thermal diffusivity from an experimental signal, using the Fourier discrete transform technique, a simulated sequence $E_{256}$ of $256$ temperature vs. time points $(t_i, E_i)$, shown in Figure \ref{fig:FTDRE256}, was generated by a computer using formula \ref{eq:LTDR Tideal Discrete}, where $a = 1.17$ cm$^2$/s  , $L = 0.500$ cm,   $T_m = 1.0$,  $\vartheta = 1/1000$ s. The number of points was conveniently chosen to be equal to $256$, since most of the existing FFT computer routines accept only $2^K$ points, where $K$ is a positive integer (usually $K \geq 9$).  
\begin{marginfigure}
\includegraphics[scale=0.6]{FTDRpT256128}
\caption{\label{fig:FTDRpT256128} Periodized response curves.}
\end{marginfigure}
Graph of the original sequence $E_{256}$, and a shorter sequence, $E_{128}$, which consists of only the first $N/2$ points, is shown in Figure \ref{fig:FTDRpT256128}. Both sequences were periodized using formula \ref{eq:LTDR T periodization}.
\begin{marginfigure}
\includegraphics[scale=0.6]{FTDRFaE256128}
\caption{\label{fig:FTDRFaE256128} Function $F(a)$, for the sequences from the Figure \ref{fig:FTDRpT256128}}
\end{marginfigure}
The sequences were then transformed using the FFT procedure (function \textit{fft}, from \textbf{R} package \textit{stats}) and real parts of the Fourier spectrum, $\text{Re}|\bar{E}_k|$, $k=1,2,\ldots, 20$, of the sequences $E_{256}$ and $E_{128}$, are shown in Figure \ref{fig:FTDRfftE256128}.

Values of the real part of the second terms of the image temperatures $\bar{E}_2(256)$ and $\bar{E}_2(128)$ were then substituted into function $F(a)$ defined in  \ref{eq:LTDR Fa Two Points}, where $N=256$. 
Graph of function $F(a)$ for ($ 0.5 < a < 2.5$), is shown Figure \ref{fig:FTDRFaE256128}. 
%file:///C:/Users/jgembarovic/Documents/repos/latexy/myFlashMethodTheory/Literature/Fourier Data Red Old/fftdr/058/FTDRFaE256128.eps

In search for roots of the $F(a)$, we have to start with a value of $a$ much bigger than the expected value, and the first root found on the way to smaller values, is the desired thermal diffusivity of the sample material. In this example, the program found the root $a^*=1.17$ cm$^2$/s, which is exactly equal to the expected value of the thermal diffusivity used to generate the original sequence $E_{256}$.
\newpage

% eps Figures were generated using FigFTDRnew_shifted.R
\subsection{Example 2}

\begin{marginfigure}
\includegraphics[scale=0.8]{FTDREs}
\caption{\label{fig:FTDREs} An example of noisy, shifted response curve, superimposed with linearly falling signal.}
\end{marginfigure} 
\begin{marginfigure}
\includegraphics[scale=0.6]{FTDREshifted}
\caption{\label{fig:FTDREshifted} Periodized response curves of the signals $Es_{256}$ and $Es_{128}$.}
\end{marginfigure}
As it was indicated, the procedure is able to identify the thermal diffusivity from a noisy signal, which is superimposed with an arbitrary linearly rising, or falling signal. To demonstrate this, a sequence of $256$ temperature-time points, $(t_i,Es_i)$, is calculated from the one, given in the previous example, using 
\begin{equation}
\label{eq:FTDRshiftedEs}
Es_i^* = a +bE_i + ct_i + noise, \quad i=0,1,2,\ldots, 255,
\end{equation}
where the shift $a=-2$, the $T_m$ multiplier is $b = 1.5$, and the slope of the superimposed linearly falling with time signal is $c= -5.0$ s$^{-1}$. Noise was generated using a random number generator with the normal distribution (the average value set to zero, and standard deviation $0.02$). Graph of the response curve $Es_{256}$ is shown in Figure \ref{fig:FTDREs}. 


  After the periodization, given by Equation \ref{eq:LTDR T periodization}, both curves $Es_{265}$ and $Es_{128}$, are shown on Figure \ref{fig:FTDREshifted}.

Real parts of the first 20 components of image sequences of the ideal response curves $E_{256}$ and $E_{128}$, from the previous example, and the noisy, shifted curves $Es_{256}$ and $Es_{128}$, are compared in Figure \ref{fig:CompFTDROrigShifted}.
\begin{marginfigure}
\includegraphics[scale=0.6]{CompFTDROrigShifted}
\caption{\label{fig:CompFTDROrigShifted} Comparison of the image sequences of the ideal, and the noisy, shifted temperature responses.}
\end{marginfigure}
    
Image sequences of the periodized sequences $Es_{256}$ and $Es_{128}$ are superposition of images of the periodized ideal signal $E$, and the tooth-like signal. The later, due to its simple shape, have real parts of their odd terms equal zero. It means, that the presence of a linear superimposed signal affects only the zero, and odd terms, and can be fully ignored if anyone of the even terms is used for the diffusivity identification.

All terms of the sequences $\bar{Es}_{256}$ and $\bar{Es}_{128}$ are affected by the presence of noise in the signal, especially higher ones. As can be seen from this comparison, the real parts of image sequences of the both signals differ significantly for all components, except the second component $k=2$. 



\begin{marginfigure}
\includegraphics[scale=0.6]{FTDRFaShifted}
\caption{\label{fig:FTDRFaShifted} Function $F(a)$ for  $\bar{Es}_{256}$.}
\end{marginfigure}
Graph of the function $F(a)$, calculated using the second components of the Fourier image sequences $\bar{Es}_{256}$ and $\bar{Es}_{128}$, is shown Figure \ref{fig:FTDRFaShifted}. The thermal diffusivity value, $a^*=1.1657$ cm$^2$/s, calculated as a root of the function $F(a)$,  differs less than $1$\% from the exact value of $a=1.17$ cm$^2$/s.  

\newpage

\subsection{Real data Example}

\begin{marginfigure}
\includegraphics[scale=0.6]{AllfiveCurves3}
\caption{\label{fig:AllFive} Response curves superimposed with a different electronic noise.}
\end{marginfigure}
To demonstrate the use of this data reduction procedure in the flash method, the effect of electronic noise on the reproducibility of thermal diffusivity calculations from temperature response signal will be discussed here in detail.

 A set of five response curves was experimentally generated by the flash method with different  degree of superimposed electronic noise. All five curves, shown in Figure \ref{fig:AllFive}, were obtained using the same sample, at the same temperature and all other experimental conditions, except that different electronic (non-gaussian) noise was superimposed on the response temperature signal. The sample thickness was 0.105 cm, and sampling frequency ($1/\vartheta$) of the signal was 1530 Hz. 512 (and 256) points after the flash at $t=0$ s, were used in data reduction calculations. As can be seen from Table \ref{tab:FFT Comparison}, the results for the calculated thermal diffusivity values are all within $\pm 2.1\%$ from that for the first data set, which represents the smoothest data. 
%\bigskip
\begin{table}
\caption{ Results of the test of reproducibility (the value $\Delta$ is calculated as a relative difference between the computed and the reference value of the thermal diffusivity $a$. The reference thermal diffusivity value is the first value in the column - for Data No. 1.)}
\label{tab:FFT Comparison}
\begin{center}
\footnotesize
\begin{tabular}{lll}
\toprule
Data No.& $a$ [cm$^2/$s] & $\Delta \%$	\\ 
\midrule
1&	0.0383&	0.0\\
2&	0.0380&	-0.8\\
3&	0.0386&	0.7\\
4&	0.0373&	-2.1\\
5&	0.0388&	1.3\\
\bottomrule
\end{tabular}
\end{center}
\end{table}

\subsection{Discussion}
%Fourier cosine transform, defined as 

%can be used instead of formula \ref{eq:DFT} to derive a formula for the Fourier image %of the periodized theoretical temperature response \ref{eq:LTDR Tideal Discrete}.

Our testing of the algorithm shows that the number of points $N$  taken into computation should be $N \ge 2^8$. Upper boundary of the time interval, $N\vartheta$ should be within a range 
\begin{equation}
\frac{aN\vartheta}{L^2} \in (0.15, 1.0).
\end{equation}
The lower limit is given by the onset of the transient rise in temperature vs. time curve, and the upper limit  is given by the possibility to meet approximately the ideal adiabatic boundary condition during a real experiment.


Because of slow convergence of the sum for theoretical amplitude given by Equation \ref{eq:LTDR Xk 2nd term}, the upper limit of the summation index $n$,  has to be $m \ge 30$.

