% !TEX root = mybook.tex

\chapter{Cylindrical Sample}

In this chapter we examine the solution of boundary-value problem in circular cylindrical coordinate system, which is used to describe temperature distribution in cylindrical shape samples (see Figure \ref{fig:CylCoordinateSystem}). The solutions obtained using the Finite Fourier and the finite Bessel transforms given in the previous Chapter, will be used.
\begin{marginfigure}%
  \includegraphics[width=\linewidth]{cylindrical_coordinates.eps}
  \caption{Sample in a Cylindrical Coordinate System.}
  \label{fig:CylCoordinateSystem}
\end{marginfigure}

\section{Problem Formulation}
Consider three dimensional, homogeneous partial differential equation of heat conduction in cylindrical coordinate system,\sidenote{ In the cylindrical coordinate system point $P$ position is described with three coordinates $(r, \phi, z)$. Coordinate $r$ is radial distance (or radius) from the longitude axis $L$, $\phi$ is  the angular position or the azimuth, and the third coordinate $z$ is the height, or the axial position. The polar axis $A$, is the ray that lies in the reference plane $z=0$, starting at the origin and pointing in the reference direction. (see Wikipedia for a nice pics!}

\begin{equation}
\label{eq:CylindricalHCE}
\frac{\partial^2 T}{\partial r^2}+\frac{1}{r}\frac{\partial T}{\partial r}+\frac{1}{r^2}\frac{\partial^2 T}{\partial \phi^2}+ \frac{\partial^2 T}{\partial z^2} = \frac{1}{a}\frac{\partial T}{\partial t},
\end{equation}
where $T=T(r,\phi,z)$.

Assume a separation of variables in the form
\begin{equation}
T(r,\phi,z)=R(r)\Phi(\phi)Z(z)\Gamma(t).
\end{equation}

Then equation  \ref{eq:CylindricalHCE} becomes
\begin{equation}
\label{eq:CylSepHCE}
\frac{1}{R}\bigg[\frac{d^2 R}{d r^2}+\frac{1}{r}\frac{d R}{d r}\bigg]+\frac{1}{r^2}\frac{1}{\Phi}\frac{d^2 \Phi}{d \phi^2}+ \frac{1}{Z}\frac{d^2 Z}{d z^2} = \frac{1}{a}\frac{1}{\Gamma}\frac{d \Gamma}{d t},
\end{equation}
Since the functions $R$, $\Phi$, $Z$, and $\Gamma$ are independent of each other, the only way Eq. \ref{eq:CylSepHCE} is satisfied if each of the groups equals to a constant; that is
\begin{equation}
\label{eq:CylGamma}
\frac{1}{a}\frac{1}{\Gamma}\frac{d\Gamma}{d t}=-\lambda^2 \quad \text{or} \quad \frac{d\Gamma}{dt}+a\lambda^2\Gamma=0
\end{equation}

\begin{equation}
\label{eq:CylPhi}
\frac{1}{\Phi}\frac{d^2\Phi}{d \phi^2}=-\nu^2 \quad \text{or} \quad \frac{d^2\Phi}{d\phi^2}+\nu^2\Phi=0
\end{equation}

\begin{equation}
\label{eq:CylZ}
\frac{1}{Z}\frac{d^2 Z}{d z^2}=-\eta^2 \quad \text{or} \quad \frac{d^2Z}{dz^2}+\eta^2 Z=0
\end{equation}
where $\lambda$, $\nu$, and $\eta$ are separation parameters and are constants. Then $R$ function satisfies
\begin{equation}
\label{eq:CylR}
\bigg(\frac{d^2R}{dr^2} + \frac{1}{r}\frac{d R}{d r}\bigg)+\bigg(\beta^2 - \frac{\nu^2}{r^2}\bigg)R=0
\end{equation}
where $\beta^2=\lambda^2-\eta^2$.

Equations \ref{eq:CylGamma},  \ref{eq:CylPhi},  \ref{eq:CylZ} have particular solutions in the form

\begin{equation}
\label{eq:CylParticularSols}
\begin{split}
\Gamma(t) = c_1e^{-a\lambda t} \\
\Phi(\phi) = c_2\sin(\nu\phi)+c_3\cos(\nu\phi)\\
Z(z) = c_4\sin(\eta z)+c_5\cos(\eta z)
\end{split}
\end{equation}
where $c_k$, $k=1,2,\ldots 5$, are constants.

The differential equation \ref{eq:CylR} for $R$ function, is called \textit{Bessel's differential equation} of order $\nu$ and the particular solution of which is in the form
\begin{equation}
\label{eq:CylParticularBessel}
R(r) = c_6 J_{\nu}(\beta r)+c_7Y_{\nu}(\beta r)
\end{equation}
where $c_6$ and $c_7$ are constants.
The functions $J_{\nu}(\beta r)$ and $Y_{\nu}(\beta r)$ are colled \textit{Bessel functions} of order $\nu$ of the first and second kind respectively.

In a situation typical to the flash method, sample temperature has no angle $\phi$ dependence, and Equation 
\ref{eq:CylindricalHCE} becomes
\begin{equation}
\label{eq:CylHCEs}
\frac{\partial^2 T}{\partial r^2}+\frac{1}{r}\frac{\partial T}{\partial r}+ \frac{\partial^2 T}{\partial z^2} = \frac{1}{a}\frac{\partial T}{\partial t},
\end{equation}
where $T=T(r,z)$. Now the separated functions satisfy

\begin{equation}
\frac{d\Gamma}{dt}+a\lambda^2\Gamma = 0,
\end{equation}
\begin{equation}
\frac{d^2R}{dr^2} + \frac{1}{r}\frac{d R}{d r}+\beta^2 R = 0,
\end{equation}
and
\begin{equation}
\frac{d^2Z}{dz^2}+\eta^2 Z = 0.
\end{equation}

The particular solutions of separation equations are now
\begin{equation}
\label{eq:CylHCEsParticulars}
\begin{split}
\Gamma(t) = c_1e^{-a\lambda t} \\
R(r) = c_2 J_{0}(\beta r)+c_3Y_{0}(\beta r)\\
Z(z) = c_4\sin(\eta z)+c_5\cos(\eta z)
\end{split}
\end{equation}
where $c_k$, $k=1,2,\ldots 5$, are constants. The particular solution for $R$ function is a zero-order Bessel function, because with no $\phi$ dependence we have $\nu=0$. 

The derivatives with respect to the space variable $r$ can be removed from the differential equation of heat conduction by applying an integral transform generally called \textit{Hankel Transform}. Such transforms and the inversion formulas are derived from the expansion of an arbitrary function over a given interval in an infinite series of Bessel functions. In following section we examine the construction of Hankel transform and the corresponding inversion formula for finite regions for boundary conditions of the third kind.

