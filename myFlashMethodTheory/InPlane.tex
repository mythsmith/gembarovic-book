% !TEX root = mybook.tex

\section{In-Plane Method}

\label{ch:InPlane}

The flash method has been extended to measure the thermal diffusivity in the radial direction of thin films, with substantial surface heat losses. In so--called \textit{In Plane} method, a small doughnut shape portion of the front surface of the sample is exposed to the laser light and resulting temperature rise is recorded on a small radius area on the opposite side of the sample (Figure \ref{fig:InPlaneSample}). The thermal diffusivity is  calculated using a mathematical model based on the \textit{fin approximation}, when the axial component of the temperature gradient in the sample is negligible small in comparison with the radial component. 
\begin{marginfigure}%
  \includegraphics[width=\linewidth]{inPlaneSample.eps}
  \caption{\label{fig:InPlaneSample}Coordinate System for the In--Plane Sample.}
  \label{fig:InPlaneSample}
\end{marginfigure}
%The technique was experimentally verified on Copper foils of available thicknesses ranging from 25 $\mu$m to 0.5 mm.  Results show that in-plane thermal diffusivity of thin film samples can be measured without accounting for actual sample thickness. This technique has been applied to determine in-plane thermal diffusivity of graphite (or graphene) thin film materials with many applications in research and industry. 

Theoretical model for the measurement of the thermal diffusivity of thin samples in the in--plane direction, can be derived from the flash method theory under simplified assumptions:
\begin{itemize}
\item{an instantaneous pulse,}
\item{negligible small axial (through--thickness) component of the temperature gradient,}
\item{one dimensional (radial) heat flow}
\end{itemize}

Analytical formula for the temperature response at the rear side of a very thin sample to an instantaneous (doughnat shape) heat pulse at the front side for $t>0$ is
\begin{equation}
\label{eq:InplaneFormula}
\begin{split}
T(t,a)=T_0+T_{max} \bigg[1+ \frac{4R^4}{r_0^2(R_o^2-R_i^2)} \sum_{i=1}^ \infty  \Phi( \beta_i)\times \\  \times\exp{ \bigg(- \beta_i^2 \frac{at}{R^2}} \bigg) \bigg] \exp{ \bigg(-m^2  \frac{at}{R^2} \bigg)},
\end{split}
\end{equation}
where $a$ is the thermal diffusivity in the sample in-plane (radial) direction, $T_0$ is the initial (baseline) temperature level, $T_{max}$ is the normalized temperature rise of adiabatically insulated sample after the flash, $R$ is the sample radius, $R_i$ and $R_o$ are the heat pulse inner and outer radii, respectively, $r_o$ is the viewed area outer radius, and
\begin{equation}
     \Phi( \beta_i)=  \frac{r_0J_1 \big( \beta_i \frac{r_o}{R} \big) \big[R_oJ_1 \big( \beta_i \frac{R_o}{R} \big)-R_iJ_1 \big( \beta_i \frac{R_i}{R} \big)\big]}{R^2 \beta_i^2J_0( \beta_i)^2},
\end{equation}    
where $ \beta_i$, $i=0,1,2,3, \dots$, are positive roots of 
\begin{equation}
 J_1( \beta)=0. 
\end{equation}
$J_0(x)$ and $J_1(x)$ are Bessel functions of the 1st kind. 

The heat pulse is coaxial to the sample main axis and the viewed area center is in the center of the rear face of the sample. Heat losses from the sample surfaces are characterized by the dimensionless parameter $m$, which is proportional to the ratio of the heat transfer coefficient to the thermal conductivity of the sample.

In what follows we will derive formula \ref{eq:InplaneFormula} from the heat conduction equation.
     
     
\subsection{Fin Approximation}
Heat conduction equation for temperature distribution in a cylindrical sample shown schematically in Figure \ref{fig:InPlaneSample}, is
\begin{equation}
\label{eq:InplaneHCE}
\frac{\partial^2 T}{\partial r^2}+\frac{1}{r}\frac{\partial T}{\partial r}+\frac{1}{r^2}\frac{\partial^2 T}{\partial \phi^2}+ \frac{\partial^2 T}{\partial z^2} = \frac{1}{a}\frac{\partial T}{\partial t},
\end{equation}
where $T=T(r,\phi,z,t)$.

If the thickness of the cylindrical sample is much smaller than the sample radius, then the $z$-term, characterizing the temperature change in the through--thickness direction, $\partial^2 T/\partial z^2$, in equation \ref{eq:InplaneHCE}, can be replaced with a source--like term $-m^2T/R^2$, where $m^2$ is a positive constant characterizing heat losses from the sample surface, and $R$ is the sample radius. 

After this substitution, equation \ref{eq:InplaneHCE} is
\begin{equation}
\label{eq:InplaneHCEm}
\frac{\partial^2 T}{\partial r^2}+\frac{1}{r}\frac{\partial T}{\partial r}+\frac{1}{r^2}\frac{\partial^2 T}{\partial \phi^2}-\frac{m^2}{R^2}T = \frac{1}{a}\frac{\partial T}{\partial t},
\end{equation}
where $T=T(r,\phi,t)$.

We will assume that the sample was initially at zero temperature, and the heat pulse is radially symmetrical, so the sample temperature does not depend on angle coordinate $\phi$, and is only a function of $r$ and $t$.

Let define function $W(r,t)$, related to $T(r,t)$ by
\begin{equation}
\label{eq:InplaneSubst}
T(r,t)=W(r,t)e^{-m^2 a t/R^2}.
\end{equation}
Substitution \ref{eq:InplaneSubst} into \ref{eq:InplaneHCEm} will yield
\begin{equation*}
\begin{split}
\frac{\partial^2 W}{\partial r^2}e^{-m^2 a t/R^2}+\frac{1}{r}\frac{\partial W}{\partial r}e^{-m^2 a t/R^2}-\frac{m^2}{R^2}We^{-m^2 a t/R^2} = \\
= \frac{1}{a}\frac{\partial W}{\partial t}e^{-m^2 a t/R^2} - \frac{m^2}{R^2}We^{-m^2 a t/R^2}
\end{split}
\end{equation*} 

\begin{equation}
\label{eq:InplaneHCEnom}
\frac{\partial^2 W}{\partial r^2}+\frac{1}{r}\frac{\partial W}{\partial r} = \frac{1}{a}\frac{\partial W}{\partial t},
\end{equation} 
where the terms containing $m^2$ canceled out.

For a zero heat flow condition at $r=R$, we have
\begin{equation}
\label{eq:InplaneBoundaryCond R}
\frac{\partial T}{\partial r} = \frac{\partial W}{\partial r} = 0. 
\end{equation}

The solution of equation \ref{eq:InplaneHCEnom}, with the boundary condition \ref{eq:InplaneBoundaryCond R}, and the initial condition for an unit instantaneous heat source in $r=r'$,
\begin{equation}
W(r,0) = F(r)= \delta(r'),
\end{equation}
 is
\begin{equation}
W(r,t|r') = \frac{1}{\pi R^2}\bigg[1+\sum_{n=1}^\infty exp\bigg(-\beta_n^2\frac{at}{R^2}\bigg)K_0(\beta_n,r)K_0(\beta_n,r')\bigg],
\end{equation}
where $\beta_n$ are positive roots of the equation
\begin{equation}
J_1(\beta R)=0. 
\end{equation}

After substitution for the kernels $K_0$ from \ref{eq:CylK0definition}, we have
\begin{equation}
W(r,t|r') = \frac{1}{\pi R^2}\bigg[1+\sum_{n=1}^\infty \exp\bigg(-\beta_n^2\frac{at}{R^2}\bigg)\frac{J_0(\beta_n r)J_0(\beta_n r')}{J_0^2(\beta_n R)}\bigg].
\end{equation}

The temperature distribution $T$, as defined in \ref{eq:InplaneSubst}, is
\begin{equation}
\begin{split}
T(r,t|r') = \frac{1}{\pi R^2}\bigg\{1+\sum_{n=1}^\infty \exp\bigg(-\beta_n^2\frac{at}{R^2}\bigg)\times\\\times\frac{J_0(\beta_n r)J_0(\beta_n r')}{J_0^2(\beta_n R)}\bigg\}\exp{\bigg[-m^2 \frac{at}{R^2}\bigg]}.
\end{split}
\end{equation}

If the heat source is constant (unit) for $r_i < r' < r_o$, at time $t=0$, then the temperature in the sample for $t>0$ is
\begin{equation}
\begin{split}
 T(r,t)=2\pi\int_{r_i}^{r_o} r'T(r,t|r')dr' = \\
 = \bigg\{\frac{r_o^2-r_i^2}{R^2}+2\sum_{n=1}^\infty  \exp\bigg(-\beta_n^2\frac{at}{R^2}\bigg)\times\\
 \times\frac{J_0(\beta_n r)\big[r_oJ_1(\beta_n r_o)-r_iJ_1(\beta_n r_i)\big]}{\beta_n R^2 J_0^2(\beta_n R)}\bigg\}\exp{\bigg[-m^2 \frac{at}{R^2}\bigg]}.
\end{split}
\end{equation} 

Finally, in real experiments, the temperature sensor (IR detector) is collecting signal from a central circular area of the sample, for $0<r<r_v$, and the integrated\sidenote{In these integrals we used the formula  for the integral of the Bessel function of the first kind, zero order \begin{equation*}
\begin{split}
\int_a^b xJ_0(\beta x)dx=\left.\frac{xJ_1(\beta x)}{\beta}\right|_a^b= \\
=\frac{b}{\beta} J_1(\beta b)-\frac{a}{\beta} J_1(\beta a).
\end{split}
\end{equation*} } temperature  $T(r_v,t)$ is
\begin{equation}
\label{eq:InplaneFinalTemp}
\begin{split}
 T(t)=\frac{2\pi\int_{0}^{r_v} r'T(r',t)dr'}{\pi r_v^2} = \\
 = \bigg\{\frac{r_o^2-r_i^2}{R^2}+4\sum_{n=1}^\infty \exp\bigg(-\beta_n^2\frac{at}{R^2}\bigg)\times\\
 \times\frac{J_1(\beta_n r_v)\big[r_oJ_1(\beta_n r_o)-r_iJ_1(\beta_n r_i)\big]}{\beta_n^2 r_v J_0^2(\beta_n R)}\bigg\}\exp{\bigg[-m^2 \frac{at}{R^2}\bigg]}.
\end{split}
\end{equation} 
For a numerical treatment of the formulas it is advantageous to define dimensionless geometrical parameters of the sample as follows
\begin{equation}
 R_i=\frac{r_i}{R}, \quad R_o=\frac{r_o}{R}, \quad R_v=\frac{r_v}{R}, 
\end{equation} 
and dimensionless time (Fourier number), as
\begin{equation}
Fo=\frac{at}{R^2}.
\end{equation}

The dimensionless temperature rise, $V(Fo)$ is 
\begin{equation}
\label{eq:InplaneDimlessTemp}
\begin{split}
 V(Fo)=\frac{T(Fo)}{T_{max}} = \\
 = \bigg\{1+\frac{4}{R_o^2-R_i^2}\sum_{n=1}^\infty \exp\bigg(-\beta_n^2 Fo\bigg)\times \\
 \times\frac{J_1(\beta_n R_v)\big[R_oJ_1(\beta_n R_o)-R_iJ_1(\beta_n R_i)\big]}{\beta_n^2 R_v J_0^2(\beta_n)}\bigg\}\exp{\bigg[-m^2 Fo\bigg]},
\end{split}
\end{equation} 
where $T_{max}=R_v^2-R_i^2$, is the normalized temperature rise of adiabatically insulated sample, and $\beta$'s are positive roots of 
\begin{equation}
 J_1( \beta)=0. 
\end{equation}
\begin{marginfigure}%
  \includegraphics[width=\linewidth]{InplaneM.eps}
  \caption{\label{fig:InplaneM}Temperature Responses for different $m$ values.}
\end{marginfigure}
An example of temperature responses for different values of parameter $m$ is given in Figure \ref{fig:InplaneM}. The curves were generated using formula \ref{eq:InplaneDimlessTemp}, with an instantaneous, doughnut shape heat pulse of the outer and inner radii $R_o=0.9$, and $R_i=0.8$, respectively, and for a concentric viewed area with $R_v=0.4$. The response curve for $m=0.01$ corresponds to a case, when the sample heat losses are minimal (e.g. in vacuum) and the curve closely resembles the ideal response curve in the flash method. (Warning: In this case, the Parker's formula can not be used for the diffusivity calculation!) The response curves for bigger $m$ values are more close resembling the curves obtained by measuring the samples in air, at normal pressure. A sharp maximum and a falling "tail" for longer times, are characteristic signs of heat loss presence. 

The dimensionless parameter $m$ should not be mistaken for the Biot number, which is also related to the heat loss magnitude, as we saw in the previous paragraphs. The Biot number is a ratio of the heat loss coefficient and the material thermal conductivity, multiplied with a characteristic length of the medium, usually the sample thickness, or the radius. Parameter $m$ is characterizing the heat losses from surfaces of a sample which has theoretically zero thickness.   
\begin{marginfigure}%
  \includegraphics[width=\linewidth]{InplaneVRO.eps}
  \caption{\label{fig:InplaneVRO}Temperature Responses for different viewed radii $R_v$.}
  % the curves generated using Literature/In-Plane/inplanePics.R (9/29/2016)
\end{marginfigure}
Temperature responses for different values of the viewed radius $R_v$ are plotted in Figure \ref{fig:InplaneVRO}. The curves were generated  using formula \ref{eq:InplaneDimlessTemp}, for the same heat source as in the previous example, for $m=0.7$, and for $R_v=(0.001, 0.3, 0.4, 0.5, 0.6)$. Curves for $R_v < 0.1$ are almost identical with the response curve for $R_v=0.001$. As the viewed radius is nearing the heat pulse inner radius $R_i$, the response curve transient part is starting sooner and is rising more steeply then in the case of smaller $R_v$'s, and the curves are shifted toward time beginning.

