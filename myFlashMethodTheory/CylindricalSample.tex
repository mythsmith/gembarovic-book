% !TEX root = mybook.tex

\chapter{Cylindrical Sample}
\label{Ch:Cylindrical Sample}

In this chapter we derive the solution for temperature distribution in a solid disc shape sample in circular cylindrical coordinate system (see Figure \ref{fig:CylCoordinateSystem}). In the cylindrical coordinate system point $P$ position is described with three coordinates $(r, \phi, z)$. Coordinate $r$ is radial distance (or radius) from the longitude axis $z$, $\phi$ is  the angular position or the azimuth, and the third coordinate $z$ is the height, or the axial position. The polar axis $O$, is the ray that lies in the reference plane $z=0$, starting at the origin and pointing in the reference direction. (see Wikipedia for a nice pics!)
\begin{marginfigure}%
  \includegraphics[width=30mm,scale=0.4]{cylindrical_coordinates.eps}
  \caption{Cylindrical Coordinate System.}
  \label{fig:CylCoordinateSystem}
\end{marginfigure}
The solutions obtained using the finite Fourier and the finite Bessel transforms, given in the previous Chapter, will be used.


\section{Problem Formulation}

The system of radial and axial coordinates and dimensions is illustrated in Figure \ref{fig:2D sample}. Generally, heat conduction in the sample can be described using a three dimensional, homogeneous partial differential equation in cylindrical coordinate system,
\begin{equation}
\label{eq:CylindricalHCE}
\frac{\partial^2 T}{\partial r^2}+\frac{1}{r}\frac{\partial T}{\partial r}+\frac{1}{r^2}\frac{\partial^2 T}{\partial \phi^2}+ \frac{\partial^2 T}{\partial z^2} = \frac{1}{a}\frac{\partial T}{\partial t},
\end{equation}
where $T=T(r,\phi,z,t)$.

With axial symmetry the terms in $\phi$ do not appear and will be omitted. Equation \ref{eq:CylindricalHCE} becomes
\begin{equation}
\label{eq:CylindricalHCE}
\frac{\partial^2 T}{\partial r^2}+\frac{1}{r}\frac{\partial T}{\partial r}+ \frac{\partial^2 T}{\partial z^2} = \frac{1}{a}\frac{\partial T}{\partial t},
\end{equation}
\begin{marginfigure}%
  \includegraphics[width=30mm,scale=0.4]{2Dsample2.eps}
  \caption{Sample in a Cylindrical Coordinate System.}
  \label{fig:2D sample}
\end{marginfigure}
This equation will be solved for boundary conditions of the third kind
\begin{equation}
\label{eq:CylHCEboundary}
\frac{\partial T}{\partial n} +H T =0,\quad t>0,
\end{equation}
for the end and lateral sample surfaces, and for an instantaneous heat source distribution
\begin{equation}
\label{eq:2DHCinit}
g(z, r)=g_1(z)g_r(r),\quad t=0.
\end{equation}  

For a right circular cylinder with axial symmetry of initial temperature distribution, or equivalent instantaneous source distribution, the boundary conditions are independently related to the axial, $\Psi(z,t)$, and radial, $\Phi(r,t)$, components of temperature and heat flow. Thus, the solution of equation \ref{eq:CylindricalHCE} can be found as product of these two components
\begin{equation}
T(r,z,t)=\Phi(r,t)\Psi(z,t).
\end{equation}

For the axial component $\Psi(z, t)$, for $(t > 0)$, and $(0 < z < L)$
\begin{equation}
\label{eq:CylindricalHCE A}
 \frac{\partial^2 \Psi}{\partial z^2} = \frac{1}{a}\frac{\partial \Psi}{\partial t},
\end{equation}
with boundary conditions
\begin{equation}
\label{eq:CylAboundary1}
-k\frac{\partial \Psi}{\partial z} + h_0 \Psi = 0,\quad \text{at} \quad z=0,
\end{equation}
\begin{equation}
\label{eq:CylAboundary2}
k\frac{\partial  \Psi}{\partial z} + h_L \Psi = 0,\quad \text{at} \quad z=L, 
\end{equation}
and initial condition
\begin{equation}
\label{eq:CylAinitial}
\Psi=g_1(z), \quad \text{at} \quad  t=0. 
\end{equation}

For the radial component $\Phi(r, t)$, for $(t > 0)$, and $(0 < r < R)$
\begin{equation}
\label{eq:CylindricalHCE R}
\frac{\partial^2 \Phi}{\partial r^2}+\frac{1}{r}\frac{\partial \Phi}{\partial r}= \frac{1}{a}\frac{\partial \Phi}{\partial t},
\end{equation}
with boundary conditions
\begin{equation}
\label{eq:CylRboundary0}
\Phi < \infty,\quad \text{at} \quad r=0.
\end{equation}
\begin{equation}
\label{eq:CylRboundary}
-k\frac{\partial \Phi}{\partial r} + h_r \Phi = 0,\quad \text{at} \quad r=R,
\end{equation}
and initial condition
\begin{equation}
\label{eq:CylAinitial}
\Phi=g_r(r), \quad \text{at} \quad  t=0. 
\end{equation}
Equations \ref{eq:CylindricalHCE A} and \ref{eq:CylindricalHCE R} may be solved by using the Fourier and Hankel transforms described in Chapter \ref{ch:IntegralTransforms}. The extension to pulses of finite duration is obtained by integration of these results with respect to time.

\section{Axial Component}
\label{sec:Axial Component}
First, find the temperature distribution in the finite region $0 < z < L$, due to unit instantaneous plane source per unit area at $z'$ $(0 < z' < L)$ and at $t = 0$, with general boundary conditions \ref{eq:CylAboundary1} and \ref{eq:CylAboundary2}. This boundary value problem can be formulates as follows:

\begin{equation}
\label{eq:CylindricalHCE Az}
 \frac{\partial^2 \Psi}{\partial z^2} = \frac{L^2}{a}\frac{\partial \Psi}{\partial t},
\end{equation}
with boundary conditions
\begin{equation}
\label{eq:CylAboundary1z}
-k\frac{\partial \Psi}{\partial z} + h_0 \Psi = 0,\quad \text{at} \quad z=0,
\end{equation}
\begin{equation}
\label{eq:CylAboundary2z}
k\frac{\partial  \Psi}{\partial z} + h_L \Psi = 0,\quad \text{at} \quad z=L, 
\end{equation}
and initial condition
\begin{equation}
\label{eq:CylAinitialz}
\Psi=\delta(z-z'), \quad \text{at} \quad  t=0. 
\end{equation}
The solution can be found using the Finite Fourier transform. 

Multiply both sides of \ref{eq:CylindricalHCE Az} with the kernel \ref{eq:KernelFiniteFT} and integrate both sides for $z$ from $0$ to $L$. Then we get
\begin{equation}
\label{eq:AxialDtime}
-\beta_m^2\bar{\Psi} = \frac{L^2}{a}\frac{\partial \bar{\Psi}}{\partial t},
\end{equation} 
where 
\begin{equation*}
\bar{\Psi} =\int_0^L K(\beta_m,z) \Psi(z,t)dz,
\end{equation*}

\begin{equation}
\label{eq: KernelFiniteFTa0z}
K(\beta_m,z) = \sqrt{2} \frac{\beta_m\cos(\beta_m z/L) + H_0\sin(\beta_m z/L)}{\bigg[(\beta_n^2+H_0^2)\bigg(1 + \frac{H_L}{\beta_n^2+H_L^2}\bigg)+H_0\bigg]^{1/2}},
\end{equation}
$\beta_m$ are positive roots of the equation
  \begin{equation} 
  \label{eq:betatransca0} 
  	\tan(\beta) = \frac{\beta(H_0+H_L)}{\beta^2-H_0H_L},
  \end{equation} 
and
\begin{equation}
H_0=\frac{h_0 L}{k}, \quad H_L=\frac{h_L L}{k}.
\end{equation}
Equation \ref{eq:AxialDtime} has a solution
\begin{equation}
\bar{\Psi}=\bar{\Psi}(0)e^{-\beta_m^2at/L^2},
\end{equation}
and, as follows from the initial condition in \ref{eq:CylAinitialz}, 
\begin{equation}
 \bar{\Psi}(0)=\int_0^L\delta(z-z')K(\beta_m,z)dz=K(\beta_m,z').
\end{equation}

If, for the sake of simplicity, we use $F_m(z)=K(\beta_m,z)$, then the finite Fourier transform of the axial component is
\begin{equation}
\bar{\Psi}=F_m(z')e^{-\beta_m^2at/L^2}.
\end{equation}
The original axial temperature can be calculated using the inversion formula \ref{eq:InvFiniteFTransform} in the form
\begin{equation}
\label{eq:AxialSolution_delta}
\Psi(z,t)=\mathlarger{\mathlarger{‎‎\sum}}_{m=1}^{\infty‎} F_m(z)F_m(z')\exp{\bigg[-\beta_m^2\frac{a t}{L^2}\bigg]},
\end{equation}
where 
\begin{equation}
F_m(z)=\sqrt{2} \frac{\beta_m\cos(\beta_m z/L) + H_0\sin(\beta_m z/L)}{\bigg[(\beta_n^2+H_0^2)\bigg(1 + \frac{H_L}{\beta_n^2+H_L^2}\bigg)+H_0\bigg]^{1/2}},
\end{equation}
and $\beta_m$ are positive roots of the equation \ref{eq:betatransca0}.

To obtain the result for an axial distribution of source strength proportional to $g_1(z')$ , we take the integral of the result in \ref{eq:AxialSolution_delta} for the plane source over the interval $z' = 0$ to $L$. $Q/pc$ is an amplitude and  $g_1(z')$ is non-dimensional. Then
\begin{equation}
\label{eq:AxialSolution_g1}
\Psi(z,t)=\frac{Q}{\rho c_p L}\mathlarger{\mathlarger{‎‎\sum}}_{m=1}^{\infty‎} F_m(z)\exp{\bigg[-\beta_m^2\frac{a t}{L^2}\bigg]} \int_0^Lg_1(z')F_m(z')dz',
\end{equation} 
and if $H_0=H_L=0$, then a term
\begin{equation}
\frac{Q}{\rho c_p L}\int_0^L g_1(z')dz',
\end{equation}
has to be added to the right-hand side of equation \ref{eq:AxialSolution_g1}.


\section{Radial Component}
\label{sec:Radial Component}
As in the case of the axial component, we will first find a solution for the temperature distribution in a cylinder of radius $R$ (see Figure  \ref{fig:2D sample}), for a unit instantaneous cylindrical surface source per unit axial length, radius $r = r'$ at $t=0$. As we have already shown in Section \ref{sec:FiniteHankelTransform}, equation \ref{eq:GreenFunctionR}, the solution is
\begin{equation}
\label{eq:RadialComponentDelta}
\Phi(r,t) = \frac{1}{\pi R^2}\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)J_0(\gamma_n r'/R)}{J_0^2(\gamma_n)}\exp{\bigg[-\gamma_n^2\frac{a t}{R^2}\bigg]},
\end{equation}
where  $\gamma_n$ are positive roots of the equation
\begin{equation}
\label{eq:RadialTranscendental}
\gamma J_1(\gamma) - H_r J_0(\gamma)=0, 
\end{equation}
and 
\begin{equation}
H_r = \frac{h_r R}{k},
\end{equation}
is the Biot number for the lateral surface at $r=R$.

For a radial distribution of source strength proportional to $g_r(r')$, we take the integral over the whole circular cross section. $Q/pc$ is here an amplitude and $g_r(r')$ is non-dimensional. Then we have
\begin{equation}
\begin{split}
\Phi(r,t) = \frac{Q}{\rho c_p}\frac{1}{\pi R^2}\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)}{J_0^2(\gamma_n)}\exp{\bigg[-\gamma_n^2\frac{a t}{R^2}\bigg]}\\
\times\int_0^R2\pi r' g_r(r')J_0(\gamma_n r'/R)dr',
\end{split}
\end{equation}
or
\begin{equation}
\label{eq:RadialSolution_gr}
\begin{split}
\Phi(r,t) = \frac{Q}{\rho c_p}\frac{2}{R^2}\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)}{J_0^2(\gamma_n)}\exp{\bigg[-\gamma_n^2\frac{a t}{R^2}\bigg]}\\
\times\int_0^R r' g_r(r')J_0(\gamma_n r'/R)dr'.
\end{split}
\end{equation}

If $H_r=0$, then a term
\begin{equation}
\frac{Q}{\rho c_p}\frac{2}{R^2}\int_0^R r'g_r(r')dr',
\end{equation}
has to be added to the right-hand side of equation \ref{eq:RadialSolution_gr}.

For a uniformly distributed heat source, $g_r(r')=1$, the radial component is
\begin{equation}
\label{eq:RadialSolutiong1}
\begin{split}
\Phi(r,t) = \frac{Q}{\rho c_p}\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{2\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)}{J_0^2(\gamma_n)}\exp{\bigg[-\gamma_n^2\frac{a t}{R^2}\bigg]},
\end{split}
\end{equation}
where the recurrent formula for integration of the Bessel function,  given in \ref{eq:integral_rJ0},
was used. For $t=0$ the radial component $\Phi(r,0)=1$, and we have a useful relation for the numerical analysis
\begin{equation}
\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{2\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)}{J_0^2(\gamma_n)}=1,
\end{equation}
which may be used to indicate how many terms of the sum have to be taken into the evaluation in order to keep the error below a certain limit. 


\section{Combined Components}
\label{sec:Combined Components}

\subsection{Instantaneous Pulses}
\label{subsec:Instantaneous Pulses}

The temperature distribution in a circular cylinder, with surfaces $z = 0$, $z = L$ and $r = R$, due to an instantaneous co-axial \textit{circular line source} at $x = z'$, $r = r'$ and $t = 0$ is a product of the axial component given in \ref{eq:AxialSolution_delta} and the radial component \ref{eq:RadialComponentDelta}:
\begin{equation}
\label{eq:AxialRadialComponentsDelta}
\begin{split}
T(r,z,t)=\Phi(r,t)\Psi(z,t)=\frac{Q}{\rho c_p L}\mathlarger{\mathlarger{‎‎\sum}}_{m=1}^{\infty‎}F_m(z)F_m(z')\\
\times\frac{1}{\pi R^2}\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)J_0(\gamma_n r'/R)}{J_0^2(\gamma_n)}\\
\times\exp{\bigg[-\beta_m^2\frac{a t}{L^2} - \gamma_n^2\frac{a t}{R^2}\bigg]},
\end{split}
\end{equation}
where $\beta_m$ and $\gamma_n$ are positive roots of the transcendental equations  \ref{eq:betatransca0} and \ref{eq:RadialTranscendental}, respectively.

Similarly, for a distribution of instantaneous sources 
\begin{equation*}
\frac{Q}{\rho c_p} g_1(z')g_r(r'), \quad \text{at} \quad t=0,
\end{equation*}
within the solid, the temperature distribution for $t > 0$, with the boundary conditions of the third kind at the boundaries, will be the product of equations \ref{eq:AxialSolution_g1} and \ref{eq:RadialSolution_gr}, but with $Q/(\rho c_p)$ as the common amplitude.


\subsection{Extended Pulses}
\label{subsec:Extended Pulses}
If the instantaneous sources occur not at time zero, but at $t = t'$, the time origin has to be shifted to $-t'$ by writing $(t - t')$ in place of $t$. If instead of one instantaneous heat source there is a sequence of instantaneous sources $(Q/\rho c_p)g_1(z')g_r(r')$ in the interval $t_1$ to  $t_2$, for $t>0$, with magnitudes proportional to the dimensionless function $\psi(t')$, then in order to obtain the temperature distribution at time $t$, we have to integrate from $t_1$, to $t$ when $t_1 < t < t_2$, or from $t_1$, to $t_2$, when $t > t_2$. 

The temperature at $t>t_2$ is
\begin{equation}
\label{eq:AxialRadialComponentsPsi}
\begin{split}
T(r,z,t)=\frac{Q}{\rho c_p L}\mathlarger{\mathlarger{‎‎\sum}}_{m=1}^{\infty‎} F_m(z) \int_0^Lg_1(z')F_m(z')dz'\\
\times\frac{1}{\pi R^2}\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)}{J_0^2(\gamma_n)}\int_0^R r' g_r(r')J_0(\gamma_n r'/R)dr'\\
\times\int_{t_1}^{t_2}\psi(t')\exp{\bigg[-\bigg(\frac{\beta_m^2}{L^2} + \frac{\gamma_n^2}{R^2}\bigg)a(t-t')\bigg]}dt',
\end{split}
\end{equation}
where $Q/(\rho c_p)$ has the dimensions \texttt{[temperature][length]}$^3$\texttt{[time]}$^{-1}$.

\subsection{Uniform Plane Pulses}
\label{subsec:Uniform Plane Pulses}

In case of a uniform plane heat source at one end of the sample, $z' = 0$ and $g_r(r') = 1$, $g_1=\delta(z')$, with radiation boundary conditions, for an instantaneous pulse at $t' = 0$ with $Q/(\rho c_p)$ as the common amplitude, the temperature for $t>0$ is the product of equations \ref{eq:AxialSolution_delta} and \ref{eq:RadialSolutiong1}:
\begin{equation}
\label{eq:AxialRadialPlanedelta}
\begin{split}
T(r,z,t)=\frac{Q}{\rho c_p L}\mathlarger{\mathlarger{‎‎\sum}}_{m=1}^{\infty‎} F_m(z)F_m(0)
\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{2\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)}{J_0^2(\gamma_n)}\\
\times\exp{\bigg[-\bigg(\frac{\beta_m^2}{L^2} + \frac{\gamma_n^2}{R^2}\bigg)at\bigg]},
\end{split}
\end{equation}

For a similar pulse of a finite duration $\tau$, ($t_1=0$, $t_2=\tau$), for $t>\tau$ we get 
\begin{equation}
\label{eq:AxialRadialPlaneTau}
\begin{split}
T(r,z,t)=\frac{Q}{\rho c_p L}\mathlarger{\mathlarger{‎‎\sum}}_{m=1}^{\infty‎} F_m(z)F_m(0)
\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{2\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)}{J_0^2(\gamma_n)}\\
\times\int_{0}^{\tau}\psi(t')\exp{\bigg[-\bigg(\frac{\beta_m^2}{L^2} + \frac{\gamma_n^2}{R^2}\bigg)a(t-t')\bigg]}dt'.
\end{split}
\end{equation}
In this case the dimensions of $Q/(\rho c_p)$ are \texttt{[temperature][length]}\texttt{[time]}$^{-1}$.

\subsection{Exponential Pulses} 
\label{Exponential Pulses Cylinder}

The most commonly used heat pulse shape, which is closely following time evolution of the pulse generated by a flash lamp, is described using an exponential function as 
\begin{equation}
\label{eq:exponential pulse}
 \psi(t)=\frac{t}{t_p^2}e^{-t/t_p},
\end{equation}
where $t_p$ is the pulse maximum. Normalized shapes of the exponential function $\psi$,  for four different pulse maximum, $t_p = (0.0005, 0.001, 0.002, 0.003)$ s, are shown in Figure \ref{fig:functionPsi}. 
\begin{marginfigure}%
  \includegraphics{functionPsi.eps}
  \caption{Normalized pulse shape for different $t_p$ values.}
  \label{fig:functionPsi}
\end{marginfigure}
If $\psi$ from \ref{eq:exponential pulse} is substituted into \ref{eq:AxialRadialPlaneTau}, then the temperature response to a uniform plane source is calculated as
\begin{equation*}
\begin{split}
T(r,z,t)=\frac{Q}{\rho c_p L}\mathlarger{\mathlarger{‎‎\sum}}_{m=1}^{\infty‎} F_m(z)F_m(0)
\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{2\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)}{J_0^2(\gamma_n)}\\
\times\int_{0}^{t}\frac{t}{t_p^2}e^{-t'/t_p}\exp{\bigg[-\bigg(\frac{\beta_m^2}{L^2} + \frac{\gamma_n^2}{R^2}\bigg)a(t-t')\bigg]}dt',
\end{split}
\end{equation*}
and after the integration, this can be simplified into
\begin{equation}
\label{eq:AxialRadialPlaneExp}
\begin{split}
T(r,z,t)=\frac{Q}{\rho c_p L}\mathlarger{\mathlarger{‎‎\sum}}_{m=1}^{\infty‎} F_m(z)F_m(0)
\mathlarger{\mathlarger{‎‎\sum}}_{n=1}^{\infty‎}\frac{2\gamma_n^2}{H_r^2+\gamma_n^2}\frac{J_0(\gamma_n r/R)}{J_0^2(\gamma_n)}\Gamma_{mn}(t),
\end{split}
\end{equation}
where, the time component is
\begin{equation}
\label{eq:2Dexptimefunction}
\Gamma_{mn}(t)=\bigg(\frac{L^2}{at_p}\bigg)^2\frac{e^{-t/t_p}}{\sigma_{mn}^2}\bigg[\exp{\bigg(-\sigma_{mn}\frac{at}{L^2}\bigg)} +\sigma_{mn}\frac{at}{L^2} - 1\bigg],
\end{equation}
\begin{equation}
\sigma_{mn}=\beta_m^2+\gamma_n^2\frac{L^2}{R^2}-\frac{L^2}{at_p},
\end{equation}
$\beta_m$ are positive roots of 
\begin{equation}
(\beta^2-H_0H_L)\sin{\beta} - \beta(H_0+H_L)\cos{\beta}=0,
\end{equation}
and $\gamma_n$ are positive roots of
\begin{equation}
\gamma J_1(\gamma) - H_rJ_0(\gamma)=0.
\end{equation}

If the heat pulse is not evenly distributed over the sample front area, but has a doughnut-like shape, with the inner and outer radii $R_0$ and $R_1$, then the analytical formula for the temperature response at the rear face of a cylindrical sample is be calculated as an integral over the pulse radii
\begin{equation}
\label{eq:T_R}
 T_R(r,z,t)=\frac{2\pi\int_{R_0}^{R_1}T(r',z,t)r'dr'}{\pi(R_1^2-R_0^2)},
\end{equation}
where $T(r,z,t)$ is given in \ref{eq:AxialRadialPlaneExp}.

If the temperature response is sensed by an IR detector, with a concentric viewed area bounded with inner and outer radii, $r_0$ and $r_1$, respectively, then the formula \ref{eq:T_R} has to be integrated again, now over the whole viewed area
 \begin{equation}
\label{eq:T_R2}
 T(r,z,t)=\frac{2\pi\int_{r_0}^{r_1}T_R(r',z,t)r'dr'}{\pi(r_1^2-r_0^2)}.
\end{equation}
Resulting temperature response at $z=L$ is   
\begin{equation}
\label{eq:2Dexponetial}
 T(t) = \frac{Q}{\rho c_p L}\sum_{m=0}^\infty \sum_{n=0}^\infty \Phi_m\Psi_n\Gamma_{mn}(t), 
\end{equation}
where 
\begin{equation}
\label{eq:CoAxialTermCylinder}
\Phi_m = \frac{ 2\beta_m^2(\beta_m^2+H_L^2)\big(\cos{\beta_m}+\frac{H_0}{\beta_m}\sin{\beta_m}\big)}{(\beta_m^2+H_0^2)(\beta_m^2+H_L^2+H_L)+H_0(\beta_m^2+H_L^2)}
\end{equation}
and
\begin{equation}
\label{eq:CoRadialTermCylinder}
\Psi_n=\frac{4\big(r_1J_1(\gamma_nr_1)-r_0J_1(\gamma_nr_0)\big)\big(R_1J_1(\gamma_nR_1)-R_0J_1(\gamma_nR_0)\big)}{(\gamma_n^2+H_r^2)J_0(\gamma_n)^2(R_1^2-R_0^2)(r_1^2-r_0^2)},
\end{equation} 
and $\Gamma_{mn}(t)$ given in equation \ref{eq:2Dexptimefunction}. The heat pulse and viewed area radii, $R_0$, $R_1$, $r_0$, and $r_1$, are dimensionless quantities, relative to the sample radius $R$. 

\begin{marginfigure}%
  \includegraphics{ExpResp4VRO.eps}
  \caption{ Temperature Responses for different $r_1$ values.}
  \label{fig:ExpResp4VRO}
\end{marginfigure}
  % the curves generated using repos\latexy\myFlashMethodTheory\Literature \Exponential Pulse\ExponentialPulsePics.R (9/29/2016)
Temperature responses for different viewed radius values $r_1=(0.001, 0.25, 0.5, 0.75, 0.9999)$ are plotted in Figure \ref{fig:ExpResp4VRO}. The curves were generated using formula \ref{eq:2Dexponetial} for a pure copper sample of thickness $L=0.2608$ cm, radius $R=0.637$ cm, and Biot numbers $H_0=H_1=H_rR/L=0.01$. The sample halftime $t_{1/2}$ was around $10$ ms. Only $25$\% of the sample surface ($R_1=0.5$) was heated by an exponential pulse with maximum at $t_p=1.0$ ms, and as we can see from Figure \ref{fig:ExpResp4VRO}, the shape of temperature response curves is very sensitive on the viewed radius value. There is a distinctive "hump" for small viewed radii, and even for $r_1=0.75$, the response curve differs significantly from the response curve for $r_1=0.9999$, when the detector signal is collected from almost whole rear sample surface. As we can see, the effect of non-uniform heating can be effectively eliminated by collecting the response signal from the whole rear sample face.
\begin{marginfigure}%
  \includegraphics{ExpResp4tp.eps}
  \caption{ Temperature Responses for different $t_p$ values.}
  \label{fig:ExpResp4tp}
\end{marginfigure}
% the curves generated using repos\latexy\myFlashMethodTheory\Literature \Exponential Pulse\ExponentialPulsePics.R (9/29/2016)
 Temperature responses for  the same sample, now with fixed viewed radius $r_1=0.95$ for exponential pulses with different time of the maximum $t_p= 0.0005, 0.001, 0.002, 0.003$ s, are plotted in Figure  \ref{fig:ExpResp4tp}. The response curves were generated using formula \ref{eq:2Dexponetial} and it is clear that the increasing $t_p$ value is shifting the response curves toward longer times. If this influence of the heat pulse shape is neglected, it leads to smaller calculated thermal diffusivity values.  

 \begin{marginfigure}%
  \includegraphics{ThinSampleExp.eps}
  \caption{ Thin sample Temperature Responses for different $t_p$ values.}
  \label{fig:ThinSampleExp}
  % the curves generated using repos\latexy\myFlashMethodTheory\Literature \Exponential Pulse\ExponentialPulsePics.R (9/29/2016)
\end{marginfigure}  This effect is more damaging for thin highly conductive samples, as it is illustrated in Figure   \ref{fig:ThinSampleExp}. The response curves for $0.1$ mm thick copper sample, calculated using formula \ref{eq:2Dexponetial}, for the Biot numbers  $H_0=H_1=H_rR/L=0.0005$, are plotted for different $t_p$ values. The pulse maximum values are also shown as dotted vertical lines, of the same color, as correspondent response curves. The sample halftime for an instantaneous uniform plane heat source, calculated using the Parker's formula \ref{eq:ParkerFormula}, is $t_{1/2}\approx 12$ $\mu$s. It is clear that in this case an accurate information about the heat pulse is crucial for estimation of the sample thermal diffusivity value.  

   As we will see in Data Reduction chapter, neglecting the influence of the heat pulse distribution in time and space, or detector's field of view, can lead to significant errors in the parameter estimation.    