##################################################################
### SENSITIVITY ANALYSIS
##################################################################
#
# sensit_flash1.R 
# (c) Jozef Gembarovic 6/6/2017

# sensitivity coefficients for 1D model with heat losses

require(myFlash3)
library(ggplot2)
library(minpack.lm)
library(reshape2)

# Parameters
tmax=1
sd=0.01
time <- seq(0.001, tmax, length = 800);Nmax <- c(100)
 b1 <- 0.1; pd <- 0.001 

alpha     <- 1 
thickness <- 1

T_m <- 1

# adiabatic temperature rise Tm sensitivity
x <- time
y <- flash1DHLsq(x,alpha,thickness,T_m,pd,b1)
sTm <- data.frame(time,y, "Tm")
names(sTm) <- c("Fo","V","name")

# thermal diffusivity sensitivity
ya <- flash1DHLsq(x,alpha*1.01,thickness,T_m,pd,b1)
sa <- data.frame(time,(ya-y)*100, "a")
names(sa) <- c("Fo","V","name")

# Biot number sensitivity
yb <- flash1DHLsq(x,alpha,thickness,T_m,pd,b1*1.01)
sb <- data.frame(time,(yb-y)*100, "H")
names(sb) <- c("Fo","V","name")

S <- rbind(sTm, sa, sb)
names(S) <- c("Fo","V","Name")

# plot all
coeff <- expression(~italic(S[a]*","~S[Tm]*","~S[H]))
Fou <- expression(~italic(Fo))
  
ggplot(S, aes(x=Fo,y=V, colour=Name))+
  geom_line()+
  scale_y_continuous(breaks = round(seq(min(-0.2), max(1), by = 0.2),1)) +
  theme_bw()+
  labs( x =Fou, y = coeff, color = "Parameter")
  

# End 