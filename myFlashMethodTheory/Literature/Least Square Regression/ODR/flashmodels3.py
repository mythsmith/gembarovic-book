
# coding: utf-8

# # Flash Method MODELS
#  (c) Jozef Gembarovic 2015
## File:  flashmodels.py

get_ipython().magic(u'pylab inline')

import numpy as np # for Numerical Python
import scipy.special as scs # for Bessel functions 
from scipy.optimize import brenth # for finding zeroes of a function
import scipy.odr.odrpack as odrpack #  for ODRPACK
import os # for files manipulatios (operating system package)
from matplotlib import pyplot # for plots
from matplotlib import gridspec # for resizing figures



def flash1D(t, a, L): # One dimensional ideal model (an instantaneous heat pulse, no heat losses)
    suma =0.0
    for n in range(1, 50):
        suma = suma + (-1)**n*np.exp(-((n*np.pi)**2*a*t/L**2))
    return (1+2.0*suma)

def flash1Dexp(t, a, L, tau): # One dimensional ideal model (an exponential heat pulse, no heat losses)
                              # pulse shape (t/tau^2)exp(-t/tau), where tau is the maximum of the pulse
                              # K.B. Larson and K. Koyama, JAP 38, 1967, pp. 465-474
    gamma = L**2/a/tau
    gsr = np.sqrt(gamma)
    suma = 0.0
    for n in range(1, 50):
        suma = suma + (-1)**n*np.exp(-((n*np.pi)**2*a*t/L**2))/(gamma-(n*np.pi)**2)**2
    return (1.0+2.0*gamma**2*suma - gsr*np.exp(-t/tau)/(2*np.sin(gsr))*(1.0 + 2*t/tau + gsr/np.tan(gsr)))

def rootsearch(f,a,b,Bio,dx):
    x1 = a; f1 = f(a, Bio)
    x2 = a + dx; f2 = f(x2, Bio)
    while f1*f2 > 0.0:
        if x1 >= b:
            return None,None
        x1 = x2; f1 = f2
        x2 = x1 + dx; f2 = f(x2, Bio)
    return x1,x2


def mroots(f, Bio, a, b, eps=1e-2):
    #print ('The roots on the interval [%f, %f] are:' % (a,b))
    i=0
    myroots=[]
    while 1:
        x1,x2 = rootsearch(f,a,b,Bio,eps)
        if x1 != None:
            a = x2
            #root = bisect(f,x1,x2,1)
            root = brenth(f,x1,x2, Bio, xtol=1e-16)
            if root != None:
                pass
       #     print ('root',i, root, f(root, Bio))
            myroots.append(root)
            i = i+1
        else:
            #print ('\nDone')
            return myroots
            
            break

# transcendental functions for eigenvalues calculations
def funA(x,Bio): 
    return (x*x-Bio[0]*Bio[1])*np.sin(x) - (Bio[0]+Bio[1])*x*np.cos(x)
    
def funR(x,Bio): 
    return (x*scs.j1(x) - Bio[2]*scs.j0(x))



def flash1DHL(t, a, L, Bio, gamas): # One dimensional model with heat losses

    Bi1=Bio[0]
    Bi2=Bio[1]

    g2 = list(np.array(gamas)**2)
    suma=0.0
    for i in range(0, len(gamas)):
       suma = suma + 2*g2[i]*(g2[i]+Bi2**2)/((g2[i]+Bi1**2)*(g2[i]+Bi2**2)
       +(Bi1+Bi2)*(g2[i]+Bi1*Bi2))*(np.cos(gamas[i])+Bi1/gamas[i]
       *np.sin(gamas[i]))*np.exp(-g2[i]*a*t/L**2)  
    
    return suma


 # Two dimensional model with heat losses (instantaneous pulse)
def flash2DHL(t, a, L, R, VRID, VROD, IRID, IROD, Bio, gamas, betas):
    Bi1=Bio[0]
    Bi2=Bio[1]
    Bi3=Bio[2]
    
    g2 = list(np.array(gamas)**2)
    b2 = list(np.array(betas)**2)

    sumaA = 0.0
    for i in range(0, len(gamas)):
       sumaA = sumaA + 2*g2[i]*(g2[i]+Bi2**2)/((g2[i]+Bi1**2)*(g2[i]+Bi2**2)
       +(Bi1+Bi2)*(g2[i]+Bi1*Bi2))*(np.cos(gamas[i])+Bi1/gamas[i]
       *np.sin(gamas[i]))*np.exp(-g2[i]*a*t/L**2)

    sumaR = 0.0
    for i in range(0, len(betas)):
        sumaR = sumaR + 4*(VRID*scs.j1(betas[i]*VRID)
        -VROD*scs.j1(betas[i]*VROD))*(IRID*scs.j1(betas[i]*IRID)
        -IROD*scs.j1(betas[i]*IROD))/((b2[i]+Bi3**2)*scs.j0(betas[i])**2
        *(IROD**2-IRID**2)*(VROD**2-VRID**2))*np.exp(-b2[i]*a*t/R**2)
    
    return sumaA * sumaR

def Axi(gama, b1):

    b2 = b1
    b1s = b1*b1
    b2s = b2*b2
    b12 = b1*b2
  
    gs = gama * gama;
    an = 2 * gs*(gs + b2s)/((gs + b1s)*(gs + b2s) + (b1 + b2)*(gs + b12))
  
    An = np.cos(gama) + b1 / gama * np.sin(gama) 
  
    axi = an*An             
  
    return axi

# radial components at rear side and store them in a vector
def Rad ( beta, b3, IRId, IROd, VRId, VROd):

    b3s = b3*b3;
  
    bs = beta * beta;
    ra = VRId*scs.j1(beta * VRId) - VROd*scs.j1(beta * VROd);
    ra = ra*(IRId*scs.j1(beta * IRId) - IROd*scs.j1(beta * IROd))
    rad = ra / ((bs + b3s)*scs.j0(beta)*scs.j0(beta))
  
    return rad

# the exponential pulse term
def Expon(sgama, sbeta, ssigma, t, ipulsemax):
    sbs = sgama+sbeta*ssigma;
    Ea = ipulsemax*ipulsemax/(ipulsemax-sbs)/(ipulsemax-sbs)
    Ea = Ea*(np.exp(-sbs*t) - np.exp(-ipulsemax*t)*(1+(ipulsemax-sbs)*t))
    return(Ea)

# Two dimensional model with heat losses and an exponential heat pulse
def flash2DHLexp(t, a, L, R, pulsemax, VRId, VROd, IRId, IROd, Bio, gamas, betas):
    b1=Bio[0]
    b2=Bio[1]
    b3=Bio[2]

    Rs    = R*R
    thickness = L
    alpha = a
    ipulsemax=thickness*thickness/(pulsemax*alpha)
    ssigma=(thickness/R)*(thickness/R)
    avi = []
    rvj = []
    for n in range(0, len(gamas)):
        avi.append(Axi(gamas[n], b1))
        rvj.append(Rad(betas[n], b3, IRId, IROd, VRId, VROd))
  
    td = t*alpha/thickness/thickness

    sumaij = 0
    for i in range(0, len(gamas)):
        gi = gamas[i]
        sgi = gi*gi
        for j in range(0, len(gamas)):
            bj  = betas[j]
            sbj = bj*bj
            sumaij = sumaij+avi[i]*rvj[j]*Expon(sgi, sbj, ssigma, td, ipulsemax) 
    
    return sumaij*4/(IROd*IROd - IRId*IRId)/(VROd*VROd - VRId*VRId)


# the square pulse term
def Square(sgama, sbeta, ssigma, t, pdur):
    
    denom = sgama+sbeta*ssigma
  
    if t <= 0.0:
        Sq = 0
    if t>0:
        if t < pdur:
            Sq = (1.0 - np.exp(-denom*t))/denom/pdur
        else:
            Sq = (np.exp(-denom*(t-pdur)) - np.exp(-denom*t))/(denom*pdur)
    return Sq

# Two dimensional model with heat losses and a square heat pulse
def flash2DHLsq(t, a, L, R, pd, VRId, VROd, IRId, IROd, Bio, gamas, betas):
    b1=Bio[0]
    b2=Bio[1]
    b3=Bio[2]

    Rs    = R*R
    thickness = L
    alpha = a
    pdur = pd*alpha/(thickness*thickness)
    ssigma=(thickness/R)*(thickness/R)
  
  # calculate axial and radial component vectors first
    for n in range(0, len(gamas)):
        avi[n]=Axi(gamas[n], b1)
        rvj[n]=Rad(betas[n], b3, IRId, IROd, VRId, VROd)
  
    td = list(np.array(t)*alpha/thickness/thickness)
    sumaij = 0
    for i in range(0, len(gamas)):
        gi = gamas[i]
        sgi = gi*gi
        for j in range(0, len(gamas)):
            bj  = betas[j]
            sbj = bj*bj
            sumaij = sumaij+avi[i]*rvj[j]*Square(sgi, sbj, ssigma, td, pdur)
    
    return sumaij*4/(IROd*IROd - IRId*IRId)/(VROd*VROd - VRId*VRId)




