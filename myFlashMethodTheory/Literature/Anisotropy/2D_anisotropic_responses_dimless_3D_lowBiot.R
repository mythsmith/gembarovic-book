#' ---
#' title: "Package myFlash3 Example"
#' author: "Jozef Gembarovic"
#' date: "May 18th, 2017"
#' ---
#' 

# flash2DHLe example (dimensionless times)

# with 3D graphs added

require(myFlash3)
library(ggplot2)
library(reshape2)

time <- seq(0.001, 2.5, length = 100)
Nmax <- c(100)
alpha     <- c(1, 1) 
thickness <- c(1)

R <- c(6)

b1 <- c(0.01)

pd <- c(0.001) 

IRI <- c(0)
IRO <- c(R*0.85)
VRI <- c(0)
VRO <- c(R*0.15)
baseline <- c(0)
deltaT <- c(1)
slope <- c(0.0)

# calculations
strt <- Sys.time()

# multiple responses for different alphaRs
coeffs <- c(0.001,(1:5)/5,1.5,(2:10))
alphaR <- alpha[1]*coeffs 
ch <- as.character(coeffs)

# isotropic case 
ye <- sapply(alphaR, flash2DHLe, time=time, Nmax=Nmax, alphaA=alpha[1],
             thickness=thickness,
             pd=pd,b1=b1, ba=1, R=R, IRI=IRI, IRO=IRO, 
             VRI=VRI,VRO=VRO,baseline=baseline, 
             deltaT=deltaT, slope=slope)
colnames(ye) <- ch
ye <- as.data.frame(ye)

halftime <- 0
yen1 <- ye
for(i in 1:ncol(ye)) yen1[,i] <- ye[,i]/max(ye[,i])
#for(i in 1:ncol(ye)) halftime[i] <- time[which(yen1[,i] >= 0.5)][1]
y <- cbind("1",time, yen1)
colnames(y) <-c("hanisotropy","time", ch)

# plot(coeffs, halftime/max(halftime),"l", col="tomato")

# different Biot number
ys <- sapply(alphaR, flash2DHLe, time=time, Nmax=Nmax,  alphaA=alpha[1],
             thickness=thickness,
             pd=pd,b1=b1, ba=5, R=R, IRI=IRI, IRO=IRO, 
             VRI=VRI,VRO=VRO,baseline=baseline, 
             deltaT=deltaT, slope=slope)
colnames(ys) <- ch
ys<- as.data.frame(ys)
ysn5 <- ys
for(i in 1:ncol(ys)) ysn5[,i] <- ys[,i]/max(ys[,i])
ys <- cbind("5",time, ysn5)
colnames(ys) <-c("hanisotropy","time",ch)
y <- rbind(y, ys)

# ONE MORE different Biot number
ys <- sapply(alphaR, flash2DHLe, time=time, Nmax=Nmax,  alphaA=alpha[1],
             thickness=thickness,
             pd=pd,b1=b1, ba=10, R=R, IRI=IRI, IRO=IRO, 
             VRI=VRI,VRO=VRO,baseline=baseline, 
             deltaT=deltaT, slope=slope)
colnames(ys) <- ch
ys<- as.data.frame(ys)
ysn10 <- ys
for(i in 1:ncol(ys)) ysn10[,i] <- ys[,i]/max(ys[,i])
ys <- cbind("10",time, ysn10)
colnames(ys) <-c("hanisotropy","time",ch)
y <- rbind(y, ys)

# ONE MORE different Biot number
ys <- sapply(alphaR, flash2DHLe, time=time, Nmax=Nmax,  alphaA=alpha[1],
             thickness=thickness,
             pd=pd,b1=b1, ba=20, R=R, IRI=IRI, IRO=IRO, 
             VRI=VRI,VRO=VRO,baseline=baseline, 
             deltaT=deltaT, slope=slope)
colnames(ys) <- ch
ys<- as.data.frame(ys)
ysn20 <- ys
for(i in 1:ncol(ys)) ysn20[,i] <- ys[,i]/max(ys[,i])
ys <- cbind("20",time, ysn20)
colnames(ys) <-c("hanisotropy","time",ch)
y <- rbind(y, ys)

# ONE MORE different Biot number
ys <- sapply(alphaR, flash2DHLe, time=time, Nmax=Nmax,  alphaA=alpha[1],
             thickness=thickness,
             pd=pd,b1=b1, ba=50, R=R, IRI=IRI, IRO=IRO, 
             VRI=VRI,VRO=VRO,baseline=baseline, 
             deltaT=deltaT, slope=slope)
colnames(ys) <- ch
ys<- as.data.frame(ys)
ysn50 <- ys
for(i in 1:ncol(ys)) ysn50[,i] <- ys[,i]/max(ys[,i])
ys <- cbind("50",time, ysn50)
colnames(ys) <-c("hanisotropy","time",ch)
y <- rbind(y, ys)

# ONE MORE different Biot number
ys <- sapply(alphaR, flash2DHLe, time=time, Nmax=Nmax,  alphaA=alpha[1],
             thickness=thickness,
             pd=pd,b1=b1, ba=100, R=R, IRI=IRI, IRO=IRO, 
             VRI=VRI,VRO=VRO,baseline=baseline, 
             deltaT=deltaT, slope=slope)
colnames(ys) <- ch
ys<- as.data.frame(ys)
ysn100 <- ys
for(i in 1:ncol(ys)) ysn100[,i] <- ys[,i]/max(ys[,i])
ys <- cbind("100",time, ysn100)
colnames(ys) <-c("hanisotropy","time",ch)
y <- rbind(y, ys)

# calculations are finished 
print(Sys.time()-strt)

ym <- melt(y, id = c("hanisotropy", "time"), 
           measure.vars = ch, 
           value.name="Temperature")

# ggplot all normalized curves
diff <- expression(~italic(a[r]/a[z]))
hloss <- expression(~italic(h[r]/h[0]))
ggplot(ym,aes(x=time, y=Temperature, colour=variable, linetype=hanisotropy))+
  geom_line(cex=0.6)+
  labs( x ="time", y = "V", color = diff,  linetype=hloss)

# ggplot just those normalized curves
ym1 <- ym[ym$hanisotropy==1,]
ggplot(ym1,aes(x=time, y=Temperature, colour=variable, linetype=hanisotropy))+
  geom_line(cex=0.6)+#scale_y_log10()+
  labs( x ="time", y = "V", color = diff,  linetype=hloss)

ym5 <- ym[ym$hanisotropy==5,]
ggplot(ym5,aes(x=time, y=Temperature, colour=variable, linetype=hanisotropy))+
  geom_line(cex=0.6)+
  labs( x ="time", y = "V", color = diff,  linetype=hloss)

ym10 <- ym[ym$hanisotropy==10,]
ggplot(ym10,aes(x=time, y=Temperature, colour=variable, linetype=hanisotropy))+
  geom_line(cex=0.6)+
  labs( x ="t [s]", y = "V", color = diff,  linetype=hloss)

ym20 <- ym[ym$hanisotropy==20,]
ggplot(ym20,aes(x=time, y=Temperature, colour=variable, linetype=hanisotropy))+
  geom_line(cex=0.6)+
  labs( x ="time", y = "V", color = diff,  linetype=hloss)

ym50 <- ym[ym$hanisotropy==50,]
ggplot(ym50,aes(x=time, y=Temperature, colour=variable, linetype=hanisotropy))+
  geom_line(cex=0.6)+
  labs( x ="time", y = "V", color = diff,  linetype=hloss)

ym50 <- ym[ym$hanisotropy==100,]
ggplot(ym100,aes(x=time, y=Temperature, colour=variable, linetype=hanisotropy))+
  geom_line(cex=0.6)+
  labs( x ="time", y = "V", color = diff,  linetype=hloss)

# plot3D graphs
library(rgl)
library(plot3D)
z1 <- as.matrix(yen1)
z5 <- as.matrix(ysn5)
z10 <- as.matrix(ysn10)
z20 <- as.matrix(ysn20)
z50 <- as.matrix(ysn50)
z100 <- as.matrix(ysn100)
X <- time
Y <- alphaR

par(mfrow = c(3, 2), mar=c(1, 1, 1, 0) )
obj = list(b1=b1)
persp3D(X,Y,z1, axis=T, clab = "V",
        xlab = "time", ylab = " ",zlab = "V", r=4,
        facets =T,plot = T, #aspect = "iso",
        phi = 20, theta = 50, ticktype="detailed", nticks = 5,  contour = T,
        image=T, colkey=F, main=bquote(italic(h[r]/h[0])==1~~italic(H[0])== .(obj$b1)))
text3D(3.9, 0.6, 0.29, labels = expression(a[r]/a[z]), add = TRUE, adj = 1)

persp3D(X,Y,z5, axis=T, clab = "V",
        xlab = "time", ylab = " ",zlab = "V", r=4,
        facets =T,plot = T, aspect = "iso", smooth=T,
        phi = 20, theta = 50, ticktype="detailed", nticks = 5,  contour = T,
        image=T, colkey=F,main=bquote(italic(h[r]/h[0])==5~~italic(H[0])==.(obj$b1)))
text3D(3.9, 0.6, 0.29, labels = expression(a[r]/a[z]), add = TRUE, adj = 1)

persp3D(X,Y,z10, axis=T, clab = "V",
        xlab = "time", ylab = " ",zlab = "V", r=4,
        facets =T,plot = T, aspect = "iso",
        phi = 20, theta = 50, ticktype="detailed", nticks = 5,  contour = T,
        image=T, colkey=F, main=bquote(italic(h[r]/h[0])==10~~italic(H[0])==.(obj$b1)))
text3D(3.9, 0.6, 0.29, labels = expression(a[r]/a[z]), add = TRUE, adj = 1)

persp3D(X,Y,z20, axis=T, clab = "V",
        xlab = "time", ylab = " ",zlab = "V", r=4,
        facets =T,plot = T, aspect = "iso",
        phi = 20, theta = 50, ticktype="detailed", nticks = 5,  contour = T,
        image=T, colkey=F, main=bquote(italic(h[R]/h[A])==20~~italic(H[0])==.(obj$b1)))
text3D(3.9, 0.6, 0.29, labels = expression(a[R]/a[A]), add = TRUE, adj = 1)

persp3D(X,Y,z50, axis=T, clab = "V",
        xlab = "time", ylab = " ",zlab = "V", r=4,
        facets =T,plot = T, aspect = "iso",
        phi = 20, theta = 50, ticktype="detailed", nticks = 5,  contour = T,
        image=T, colkey=F, main=bquote(italic(h[r]/h[0])==50~~~italic(H[0])==.(obj$b1)))
text3D(3.9, 0.6, 0.29, labels = expression(a[r]/a[z]), add = TRUE, adj = 1)

persp3D(X,Y,z100, axis=T, clab = "V",
        xlab = "time", ylab = " ",zlab = "V", r=4,
        facets =T,plot = T, aspect = "iso",
        phi = 20, theta = 50, ticktype="detailed", nticks = 5,  contour = T,
        image=T, colkey=F, main=bquote(italic(h[r]/h[0])==100~~~italic(H[0])==.(obj$b1)))
text3D(3.9, 0.6, 0.29, labels = expression(a[r]/a[z]), add = TRUE, adj = 1)
# save the image to eps using Rstudio Export !


# End 