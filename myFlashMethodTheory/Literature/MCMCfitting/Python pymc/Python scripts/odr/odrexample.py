# -*- coding: utf-8 -*-
"""
Created on Fri Dec 18 17:52:55 2015

@author: Jozef
"""

import numpy as np
import scipy.odr.odrpack as odrpack
np.random.seed(1)

N = 100
x = np.linspace(0,10,N)
y = 3*x - 1 + np.random.random(N)/2
sx = np.random.random(N)/2
sy = np.random.random(N)/2

def f(B, x):
    return B[0]*x + B[1]
linear = odrpack.Model(f)
# mydata = odrpack.Data(x, y, wd=1./np.power(sx,2), we=1./np.power(sy,2))
mydata = odrpack.RealData(x, y, sx=sx, sy=sy)
myodr = odrpack.ODR(mydata, linear, beta0=[10., 2.])
myoutput = myodr.run()
myoutput.pprint()