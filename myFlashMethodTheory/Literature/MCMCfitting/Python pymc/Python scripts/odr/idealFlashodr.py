# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 17:30:39 2015

@author: Jozef
"""

import numpy as np
import scipy.odr.odrpack as odrpack
import os
 
## get input ##
filename="results.txt"
 
## delete only if file exists ##
if os.path.exists(filename):
    os.remove(filename)
else:
    print("Sorry, I can not remove %s file." % filename)
 
np.random.seed(1)
#import matplotlib
from matplotlib import pyplot
N=100
def f1D(t,a,L):
    suma = 0
    for i in range (1,100):
        suma += (-1)**i * np.exp(-(i * np.pi)**2 * a * t / L**2)
    return 1 + 2 * suma
    
#def f1Dn(t,a,L):
#    n1=np.arange(1,101)
#    mone=(-1)**n1
#    expf=np.exp(-(n1*np.pi)**2*t*a/L**2)
#    return 1 + 2 * np.dot(mone,expf)
    
noise=0.5
time = np.linspace(0.001,1.2,N)
temperature = 4.5*(f1D(time, 1.17, 1) + (np.random.random(N)-0.5)*noise) -3.0
pyplot.plot(time, temperature)


def f(B, x):
    return B[0]+B[1]*f1D(x,B[2],1)
flash = odrpack.Model(f)

mydata = odrpack.RealData(time, temperature, sx=0.00001, sy=0.0001)

myodr = odrpack.ODR(mydata, flash, beta0=[-1.,1.5, 1.3], ifixb=[1,1,1], iprint=6616,rptfile=filename )
myoutput = myodr.run()
myoutput.pprint()

B = myoutput.beta
theory = B[0]+B[1]*f1D(time,B[2],1)
pyplot.plot(time, temperature, 'gd')
pyplot.plot(time, theory,'r')
pyplot.plot(time, temperature - theory, 'b+')
pyplot.xlabel("time")
pyplot.ylabel("temperature")
pyplot.show()

