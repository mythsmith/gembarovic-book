# -*- coding: utf-8 -*-
"""
Created on Tue Dec 22 10:51:51 2015

@author: Jozef
"""
import numpy as np
import scipy.special as scs
from scipy.optimize import brenth
import scipy.odr.odrpack as odrpack
import os

## define values of parameters (CGS units)
a = 1 #  thermal diffusivity
L = 1# sample thickness
R = 6.35 # sample radius
IRI = 0.0 # irradiated radius inner
IRO = 6.35 # irradiated radius outer
VRI = 0.0 # viewed radius inner
VRO = 5.5 # viewed radius outer

#dimensionless radii
IRID = IRI/R
IROD = IRO/R
VRID = VRI/R
VROD = VRO/R

# Theoretical values
Biot_theor  = 0.2 # dimensionless parameter m
td_theor = 1.17 # thermal diffusivity
Tmax_theor = 3.0  # Tmax (adiabatic)
baseline_theor = -3.0

theors = [baseline_theor, Tmax_theor, td_theor, Biot_theor]

# Biot numbers, front, rear, radial
Bio = [Biot_theor, Biot_theor, Biot_theor*L/R]


def rootsearch(f,a,b,Bio,dx):
    x1 = a; f1 = f(a, Bio)
    x2 = a + dx; f2 = f(x2, Bio)
    while f1*f2 > 0.0:
        if x1 >= b:
            return None,None
        x1 = x2; f1 = f2
        x2 = x1 + dx; f2 = f(x2, Bio)
    return x1,x2


def roots(f, Bio, a, b, eps=1e-2):
    #print ('The roots on the interval [%f, %f] are:' % (a,b))
    i=0
    myroots=[]
    while 1:
        x1,x2 = rootsearch(f,a,b,Bio,eps)
        if x1 != None:
            a = x2
            #root = bisect(f,x1,x2,1)
            root = brenth(f,x1,x2, Bio, xtol=1e-16)
            if root != None:
                pass
       #     print ('root',i, root, f(root, Bio))
            myroots.append(root)
            i = i+1
        else:
            #print ('\nDone')
            return myroots
            
            break

#f=lambda x:x*math.cos(x-4)
#fa = lambda x: (x*x-4)*np.sin(x) - (4)*x*np.cos(x)
#fr = lambda x: (x*scs.j1(x) - 0.001*scs.j0(x))
def funA(x,Bio): 
    return (x*x-Bio[0]*Bio[1])*np.sin(x) - (Bio[0]+Bio[1])*x*np.cos(x)
    
def funR(x,Bio): 
    return (x*scs.j1(x) - Bio[2]*scs.j0(x))

gamas = roots(funA, Bio, 0.0001, 200)
betas = roots(funR, Bio, 0.0001, 200)


#funA = function (x, b1, b2) (x*x-b1*b2)*sin(x) - (b1+b2)*x*cos(x)
#funR = function (x, b3) (x*besselJ(x,1) - b3*besselJ(x,0))


def flash1D(t, a, Tmax):
    suma =0.0
    for n in range(1, 50):
        suma = suma + (-1)**n*np.exp(-((n*np.pi)**2*a*t/L**2))
    return Tmax*(1+2.0*suma)

def flash1DHL(t, a, Bio, Tmax): 

    Bi1=Bio[0]
    Bi2=Bio[1]

 #   gamas = roots(funA, Bio, 0.0001, 200)

    g2 = list(np.array(gamas)**2)
    suma=0.0
    for i in range(0, len(gamas)):
       suma = suma + 2*g2[i]*(g2[i]+Bi2**2)/((g2[i]+Bi1**2)*(g2[i]+Bi2**2)
       +(Bi1+Bi2)*(g2[i]+Bi1*Bi2))*(np.cos(gamas[i])+Bi1/gamas[i]
       *np.sin(gamas[i]))*np.exp(-g2[i]*a*t/L**2)  
    
    return Tmax * suma
 
def flash2DHL(t, a, Bio, Tmax): 

    Bi1=Bio[0]
    Bi2=Bio[1]
    Bi3=Bio[2]
    
#    gamas = roots(funA, Bio, 0.0001, 200)
#    betas = roots(funR, Bio, 0.0001, 200)

    g2 = list(np.array(gamas)**2)
    b2 = list(np.array(betas)**2)

    sumaA = 0.0
    for i in range(0, len(gamas)):
       sumaA = sumaA + 2*g2[i]*(g2[i]+Bi2**2)/((g2[i]+Bi1**2)*(g2[i]+Bi2**2)
       +(Bi1+Bi2)*(g2[i]+Bi1*Bi2))*(np.cos(gamas[i])+Bi1/gamas[i]
       *np.sin(gamas[i]))*np.exp(-g2[i]*a*t/L**2)

    sumaR = 0.0
    for i in range(0, len(betas)):
        sumaR = sumaR + 4*(VRID*scs.j1(betas[i]*VRID)
        -VROD*scs.j1(betas[i]*VROD))*(IRID*scs.j1(betas[i]*IRID)
        -IROD*scs.j1(betas[i]*IROD))/((b2[i]+Bi3**2)*scs.j0(betas[i])**2
        *(IROD**2-IRID**2)*(VROD**2-VRID**2))*np.exp(-b2[i]*a*t/R**2)
    
    return Tmax * sumaA * sumaR

#flash2DHL(0.139, 1.0, Bio, 1.0)
#flash2DHL(time, 1.0, Bio, 1.0)

#pyplot.plot(time, flash2DHL(time, 1.0, Bio, 1.0))

#flash1DHL(0.139, 1.0, Bio, 1.0)
#flash1DHL(time, 1.0, Bio, 1.0)

#flash1D(0.139, 1.0, 1.0)
#flash1D(time, 1.0, 1.0)

from matplotlib import pyplot
N=1000
time = np.linspace(0.001,1.2,N)
noise = 0.1
temperature=baseline_theor+Tmax_theor*(flash2DHL(time, td_theor, Bio, 1.0) + np.random.normal(0,noise,N))
pyplot.plot(time, temperature)


 ## ODRPACK application
## get input ##
filename="results2Dodr.txt"
 
## delete only if file exists ##
if os.path.exists(filename):
    os.remove(filename)
else:
    print("Sorry, I can not remove %s file." % filename)
    
def f2DHL(B, x):
    Bio = [B[3], B[3], B[3]*L/R]
    gamas = roots(funA, Bio, 0.0001, 200)
    betas = roots(funR, Bio, 0.0001, 200) 
    return B[0]+B[1]*flash2DHL(x, B[2], Bio, 1.0) 
    
flash = odrpack.Model(f2DHL)

mydata = odrpack.RealData(time, temperature, sx=0.00001, sy=0.0001)

myodr = odrpack.ODR(mydata, flash, beta0=[-4.,1.5, 1.3, 0.2], ifixb=[1,1,1,1],
                    iprint=6616, rptfile=filename )
myoutput = myodr.run()
myoutput.pprint()

B = myoutput.beta
Bio=[B[3],B[3],B[3]*L/R]
theory = B[0]+B[1]*flash2DHL(time, B[2], Bio, 1.0) 
pyplot.plot(time, temperature, 'gd')
pyplot.plot(time, theory,'r')
pyplot.plot(time, temperature - theory, 'b+')
pyplot.xlabel("time")
pyplot.ylabel("temperature")
pyplot.show()

errors = (B-theors)/theors*100 
print('Errors in percent are :',errors)
