# -*- coding: utf-8 -*-
"""
Created on Tue Dec 22 10:51:51 2015

@author: Jozef
"""
import numpy as np
import scipy.special as scs
from scipy.optimize import brenth
import scipy.odr.odrpack as odrpack
import os
 

    
## define values of parameters (CGS units)
a = 1 #  thermal diffusivity
L = 1# sample thickness
R = 6.35 # sample radius
IRI = 0.0 # irradiated radius inner
IRO = 6.35 # irradiated radius outer
VRI = 0.0 # viewed radius inner
VRO = 5.5 # viewed radius outer

#dimensionless radii
IRID = IRI/R
IROD = IRO/R
VRID = VRI/R
VROD = VRO/R

# Theoretical values
Biot_theor  = 0.9 # dimensionless parameter m
td_theor = 0.9 # thermal diffusivity
Tmax_theor = 3.0  # Tmax (adiabatic)
theors = [td_theor, Biot_theor, Tmax_theor]

# Biot numbers, front, rear, radial
Bio = 0.1


def rootsearch(f,a,b,Bio,dx):
    x1 = a; f1 = f(a, Bio)
    x2 = a + dx; f2 = f(x2, Bio)
    while f1*f2 > 0.0:
        if x1 >= b:
            return None,None
        x1 = x2; f1 = f2
        x2 = x1 + dx; f2 = f(x2, Bio)
    return x1,x2


def roots(f, Bio, a, b, eps=1e-2):
    #print ('The roots on the interval [%f, %f] are:' % (a,b))
    i=0
    myroots=[]
    while 1:
        x1,x2 = rootsearch(f,a,b,Bio,eps)
        if x1 != None:
            a = x2
            #root = bisect(f,x1,x2,1)
            root = brenth(f,x1,x2, Bio, xtol=1e-16)
            if root != None:
                pass
       #     print ('root',i, root, f(root, Bio))
            myroots.append(root)
            i = i+1
        else:
            #print ('\nDone')
            return myroots
            
            break

#f=lambda x:x*math.cos(x-4)
#fa = lambda x: (x*x-4)*np.sin(x) - (4)*x*np.cos(x)
#fr = lambda x: (x*scs.j1(x) - 0.001*scs.j0(x))
def funA(x,Bio): 
    return (x*x-Bio*Bio)*np.sin(x) - (Bio+Bio)*x*np.cos(x)
    
def funR(x,Bio): 
    return (x*scs.j1(x) - Bio*scs.j0(x))

gamas = roots(funA, Bio, 0.0001, 200)
betas = roots(funR, Bio, 0.0001, 200)


#funA = function (x, b1, b2) (x*x-b1*b2)*sin(x) - (b1+b2)*x*cos(x)
#funR = function (x, b3) (x*besselJ(x,1) - b3*besselJ(x,0))


def flash1D(t, a, Tmax):
    n=np.arange(1,50)
    term = ((-1)**(n)*np.exp(-(n)**2*np.pi**2*a*t/L**2))
    return Tmax*(1+2*np.sum(term))

def flash1DHL(t, a, Bio, Tmax, gamas): 

    Bi1=Bio
    Bi2=Bio

 #   gamas = roots(funA, Bio, 0.0001, 200)

    g2 = list(np.array(gamas)**2)

    Ax=[]
    for i in range(0, len(gamas)):
       Ax.append(2*g2[i]*(g2[i]+Bi2**2)/((g2[i]+Bi1**2)*(g2[i]+Bi2**2)
       +(Bi1+Bi2)*(g2[i]+Bi1*Bi2))*(np.cos(gamas[i])+Bi1/gamas[i]
       *np.sin(gamas[i]))*np.exp(-g2[i]*a*t/L**2))   
    
    return Tmax * np.sum(Ax)
 
def flash2DHL(t, a, Bio, Tmax, gamas, betas): 

    Bi1=Bio
    Bi2=Bio
    Bi3=Bio*L/R
    
#    gamas = roots(funA, Bio, 0.0001, 200)
#    betas = roots(funR, Bio, 0.0001, 200)

    g2 = list(np.array(gamas)**2)
    b2 = list(np.array(betas)**2)

    Ax=[]
    for i in range(0, len(gamas)):
       Ax.append(2*g2[i]*(g2[i]+Bi2**2)/((g2[i]+Bi1**2)*(g2[i]+Bi2**2)
       +(Bi1+Bi2)*(g2[i]+Bi1*Bi2))*(np.cos(gamas[i])+Bi1/gamas[i]
       *np.sin(gamas[i]))*np.exp(-g2[i]*a*t/L**2))   

    Ar=[]
    for i in range(0, len(betas)):
        Ar.append(4*(VRID*scs.j1(betas[i]*VRID)-VROD*scs.j1(betas[i]*VROD))
        *(IRID*scs.j1(betas[i]*IRID)-IROD*scs.j1(betas[i]*IROD))
        /((b2[i]+Bi3**2)*scs.j0(betas[i])**2*(IROD**2-IRID**2)*(VROD**2-VRID**2))
        *np.exp(-b2[i]*a*t/R**2))
    
    return Tmax*np.sum(Ax)*np.sum(Ar)

flash2DHL(0.139, 1.0, 0.001, 1.0, gamas, betas)
flash2DHL(10, 1.0, 0.001, 1.0, gamas, betas)
flash1DHL(0.139, 1.0, 0.001, 1.0, gamas)

from matplotlib import pyplot
N=100
noise=0.05
time = np.linspace(0.001,1.2,N)

#temperature=[]
#for i in range(0, len(time)):  
#    temperature.append(4.5*(flash2DHL(time[i], 1.0, Bio, 1.0)
#    + (np.random.random(1) - 0.5)*noise) - 3.0)

#pyplot.plot(time, temperature)

temp = list(map(lambda x: flash2DHL(x,1.0, Bio, 1.0, gamas, betas), time))
#add some noise
Tmax=3
baseline = -5
noisytemp = Tmax*(temp + (np.random.random(N) - 0.5)*noise) + baseline
pyplot.plot(time, noisytemp,'bd')

# ODR part
def f(B, x):
    gamas = roots(funA, B[2], 0.0001, 200)
    betas = roots(funR, B[2]*L/R, 0.0001, 200)
    temp = list(map(lambda y: flash2DHL(y, B[3], B[2], 1.0, gamas, betas), time))
    return B[0]+B[1]*temp
flash = odrpack.Model(f)

mydata = odrpack.RealData(time, noisytemp, sx=0.00001, sy=0.0001)

## get input ##
filename="results2DHL.txt"
 
## delete only if file exists ##
if os.path.exists(filename):
    os.remove(filename)
else:
    print("Sorry, I can not remove %s file." % filename)
    
myodr = odrpack.ODR(mydata, flash, beta0=[-4.,3, 0.5, 0.9], ifixb=[1,1,1,1], iprint=6616,rptfile=filename )
myoutput = myodr.run()
myoutput.pprint()

Bt = myoutput.beta
tempth = list(map(lambda x: flash2DHL(x,Bt[3], [Bt[2],Bt[2],Bt[2]],1.0), time))
theory = Bt[0]+Bt[1]*tempth
pyplot.plot(time, noisytemp, 'gd')
pyplot.plot(time, theory,'r')
pyplot.plot(time, noisytemp - theory, 'b+')
pyplot.xlabel("time")
pyplot.ylabel("temperature")
pyplot.show()