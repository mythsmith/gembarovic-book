# -*- coding: utf-8 -*-
"""
Created on Mon Dec 21 17:30:39 2015

@author: Jozef
"""

import numpy as np
import scipy.odr.odrpack as odrpack
np.random.seed(1)
#import matplotlib
from matplotlib import pyplot

N = 100
x = np.linspace(0,10,N)
y = 5*x - 1 + 10*np.random.random(N)
sx = np.random.random(N)
sy = np.random.random(N)

def f1D(t,a,L):
#    n = np.linspace(1,100,100)
#    return 1 + 2 * np.sum(((-1)**n) * (np.exp(-n**2 * t * np.pi^2 * a / L**2)))
    suma = 0
    for i in range (1,100):
        suma += (-1)**i * np.exp(-(i * np.pi)**2 * a * t / L**2)
#    print "factorial is ",result
    return 1 + 2 * suma



time = np.linspace(0.001,1,N)
temperature = f1D(time, 1.17, 1) + (np.random.random(N)-0.5)/10 -3.0
pyplot.plot(time, temperature)
pyplot.show()

# def f(B, x):
#     return B[0]*x + B[1]
# linear = odrpack.Model(f)
def f(B, x):
    return B[0]+B[1]*f1D(x,B[2],1)
flash = odrpack.Model(f)
# mydata = odrpack.Data(x, y, wd=1./np.power(sx,2), we=1./np.power(sy,2))

mydata = odrpack.RealData(time, temperature)

myodr = odrpack.ODR(mydata, flash, beta0=[-1.,0.7, 0.8])
myoutput = myodr.run()
myoutput.pprint()