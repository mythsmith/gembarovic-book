# -*- coding: utf-8 -*-
"""
Created on Tue Dec 22 10:51:51 2015

@author: Jozef
"""

import math
import scipy.special as scs
from scipy.optimize import brenth

def rootsearch(f,a,b,dx):
    x1 = a; f1 = f(a)
    x2 = a + dx; f2 = f(x2)
    while f1*f2 > 0.0:
        if x1 >= b:
            return None,None
        x1 = x2; f1 = f2
        x2 = x1 + dx; f2 = f(x2)
    return x1,x2


def roots(f, a, b, eps=1e-2):
    print ('The roots on the interval [%f, %f] are:' % (a,b))
    i=0
    myroots=[]
    while 1:
        x1,x2 = rootsearch(f,a,b,eps)
        if x1 != None:
            a = x2
            #root = bisect(f,x1,x2,1)
            root = brenth(f,x1,x2,xtol=1e-16)
            if root != None:
                pass
            print ('root',i, root, f(root))
            myroots.append(root)
            i = i+1
        else:
            print ('\nDone')
            return myroots
            
            break

#f=lambda x:x*math.cos(x-4)
fa = lambda x: (x*x-4)*np.sin(x) - (4)*x*np.cos(x)
fr = lambda x: (x*scs.j1(x) - 0.001*scs.j0(x))
Ra = roots(fa, 0, 80)
Rr = roots(fr,0,80)