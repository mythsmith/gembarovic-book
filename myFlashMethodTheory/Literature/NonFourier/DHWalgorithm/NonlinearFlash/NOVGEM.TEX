% Template article for preprint document class `elsart'
% SP 2000/09/06

\documentclass{elsart}
\usepackage{graphicx}% Include figure files

% if you use PostScript figures in your article
% use the graphics package for simple commands
% \usepackage{graphics}
% or use the graphicx package for more complicated commands
% \usepackage{graphicx}
% or use the epsfig package if you prefer to use the old commands
% \usepackage{epsfig}

% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}

\begin{document}

\begin{frontmatter}

% Title, authors and addresses

% use the thanksref command within \title, \author or \address for footnotes;
% use the corauthref command within \author for corresponding author footnotes;
% use the ead command for the email address,
% and the form \ead[url] for the home page:
\title{Nonlinear Effects in the Laser Flash Thermal Diffusivity Measurements}
 \author[Lafayette]{Jozef Gembarovic \corauthref {1}}
 \ead{gembar@tprl.com}
 \author[Lafayette]{ Jozef Gembarovic, Jr.}
 \corauth[1]{Corresponding author}
 \address[Lafayette]{TPRL, Inc., 3080 Kent Avenue, West Lafayette, IN 47906, USA}


\begin{abstract}
Numerical solution of nonlinear heat conduction equation is used
to analyze nonlinear effects in the laser flash experiment, when
the thermophysical parameters of the sample depend on the
temperature. Parameter estimation technique is proposed to
determinate the temperature dependence of the thermal diffusivity
from a response curve. Computer generated data as well as a real
experimental data were used to demonstrate the technique.
\end{abstract}

\begin{keyword}
% keywords here, in the form: keyword \sep keyword
Numerical algorithm  \sep nonlinear heat conduction \sep
temperature distribution

\end{keyword}
\end{frontmatter}

% main text
\section{Introduction}
In the laser flash method \cite{Parker61} one surface (at $x=0$)
of a small disc shaped sample of thickness $L$ is irradiated by a
laser pulse and resulting temperature rise at opposite surface
($x=L$) is used to calculate the thermal diffusivity $\alpha$ of
the sample material.
\par
Existing data reduction methods for calculation of thermal
diffusivity from the temperature rise of the sample are based on
assumption that the thermophysical parameters - heat capacity $c$
and thermal conductivity $\lambda$ (and also thermal diffusivity
$\alpha \equiv\lambda/c$) are constants independent of temperature
$T$ within the temperature range of the a flash experiment. One
(or two) dimensional linear heat conduction equation
\begin{equation}\label{e:HCE}
\frac{\partial T}{\partial t}=\alpha \frac{\partial^2 T}{\partial
x^2},
\end{equation}
with initial and boundary conditions relevant to the experiment is
solved and the thermal diffusivity is calculated by fitting the
experimental temperature rise to the appropriate analytical
solution of the Equation (\ref{e:HCE}). For most of materials,
temperature range and a final temperature rise $\lesssim$ 1 K, the
assumption of constant thermophysical properties is valid and the
results of the thermal diffusivity determination are usually
within a couple percent of claimed experimental uncertainty of the
flash method.
\par
Use of  short and powerful laser pulses to measure very thin
samples lead to a temperature rise much greater than than a couple
degrees K assumed for a perturbation type experiment. The
assumption that the temperature rise of the sample is not very
high is not longer valid. If the heat capacity $c(T)$ and the
thermal conductivity $\lambda(T)$ varies with temperature then
temperature distribution in sample is found by solving the
nonlinear heat conduction equation:
\begin{equation}\label{e:NLINHCE}
    c(T) \frac{\partial T}{\partial t}=\frac{\partial }{\partial x}
    \Biggl(\lambda(T) \frac{\partial T}{\partial x}\Biggr).
\end{equation}
Equation (\ref{e:NLINHCE}) will be solved numerically in this
paper for a constant heat capacity $c(T)=c_0$ and thermal
conductivity as a function of temperature
\begin{equation}\label{e:Flambda}
    \lambda(T)=\frac{a_0}{a_1 T + 1},
\end{equation}
where $a_0, a_1 >0$ are positive constant parameters. The effect
of temperature dependent $\lambda(T)$ on flash method diffusivity
measurement was analyzed in \cite{Degio85} where it was found that
nonlinearity can be neglected up to certain degree given by the
value of the parameter $a_1$. We will show that these parameters
can be determined from the response curve in the laser flash
experiment using a parameter estimation technique. Computer
generated, as well as real experimental data will be used to
demonstrate the usage of the proposed procedure.

\section{Numerical Solution}

Equation (\ref{e:NLINHCE}) has been solved numerically using an
implicit difference scheme \cite{Kreith93}. Sample thickness $L$
is divided into $N=21$ elements. The sample is initially in
equilibrium state at temperature $T_0$. Heat pulse is assumed to
be instantaneous (at $t=0$) and its energy is absorbed in the
first element, rising its temperature to $T_1$. Sample boundaries
are adiabatically insulated.
\par Temperature $T_{i,m+1}$ of the $i$-th element
($i=1,2,3,\ldots, N$) at the time $t_{m+1}=(m+1)\Delta t$,
$m=1,2,\ldots$ is given by a  system of equations:

\begin{eqnarray}\label{e:NumericSet}
  T_{1,m+1}=T_{1,m}+2Fo_r(T_{2,m+1}-T_{1,m+1}){}
                                \nonumber\\
  T_{i,m+1}=T_{i,m}+Fo_l(T_{i-1,m+1}-T_{i,m+1})+Fo_r(T_{i+1,m+1}-T_{i,m+1}){}
                                \nonumber\\
   \textrm{for } i=2,3,\ldots,N-1{}
                                \nonumber\\
  T_{N,m+1}=T_{N,m}+2Fo_l(T_{N-1,m+1}-T_{1N,m+1}){}
\end{eqnarray}
where
\setlength\arraycolsep{2pt}
\begin{eqnarray}\label{e:Fos}
Fo_l=\frac{\Delta t \lambda_l}{c \Delta x^2},\qquad
Fo_r=\frac{\Delta t \lambda_r}{c \Delta x^2}{}
\nonumber\\
{} \lambda_l=\frac{2 \lambda_{i-1}
\lambda_i}{\lambda_{i-1}+\lambda_i},\qquad \lambda_r=\frac{2
\lambda_{i+1} \lambda_i}{\lambda_{i+1}+\lambda_i}{}
\nonumber\\
\end{eqnarray}
and $\lambda_i$ is is the thermal conductivity of $i$-th element.
A standard iterative algorithm was used to solve Equations
(\ref{e:NumericSet}).
\par
\begin{figure}
\includegraphics[scale=0.8]{dift1n}
\caption{\label{fig:DifT1}}{Nonlinear temperature rise for
different initial temperature $T_1$ of the sample front surface.}
\end{figure}
\par
Nonlinear temperature rise $V(L,t)$ at $x=L$ was calculated for a
temperature dependence of $c(T)=c_0$, where $c_0$ is constant
value of heat capacity at $T_0$, and $\lambda(T)$ given by
Equation (\ref{e:Flambda}). Since the heat capacity is constant,
the temperature dependence $\alpha (T)$ will be similar to
$\lambda (T)$.
\par Figure \ref{fig:DifT1} shows the nonlinear temperature rise $V(L,t)$ as
a function of time for various initial temperatures $T_1$ of the
first element. The curves are normalized to a new steady
temperature after the pulse and time is normalized to halftime
value $t_{1/2}$. (Halftime is a time needed for the temperature at
$x=L$ to rise to half of its new steady state value after the
pulse.) Ideal curve for constant values of $c(T)=c_0$ and $\lambda
(T)=\lambda_0$ is also presented in Figure \ref{fig:DifT1}. Shape
of the nonlinear curves differs from the ideal curve. Generally,
the nonlinear curves lead the ideal one in the first half of their
rise and lag behind the ideal one in the second half. Curves for
higher $T_1$ rise more slowly, than those for lower $T_1$. The
shape distortion is more noticeable for the curves with higher
$T_1$.

\begin{figure}
\centering
\includegraphics[scale=0.9]{lndift1n}
\caption{\label{fig:LnVt}}{Function $(\ln(V)+\ln(t/t_{1/2})/2)$
versus $t_{1/2}/t$ for a different $T_1$.}
\end{figure}

\par
The differences between the ideal and nonlinear curves are more
visible in Figure \ref{fig:LnVt}, where a plot of
$(\ln(V)+\ln(t/t_{1/2})/2)$ versus $t_{1/2}/t$ is presented for a
different initial temperature $T_1$. Ideal curve, given by

\begin{equation}\label{IdealV}
    V_i(L,t)=\frac{L}{\sqrt{\pi \alpha
    t}}\sum_{n=0}^{\infty}{\exp\Biggl[-
    \frac{(2n+1)^2 L^2}{4\alpha t}\Biggr]},
\end{equation}

is a straight line with a slope $-L^2/4\alpha t_{1/2}\doteq 1.80$.
The deviations from the ideal curve shape increase with $T_1$.
\par The differences in shape between the ideal and nonlinear
curve make it impossible to match nonlinear curve with an ideal
one using a constant value of the thermal diffusivity. In
principle \cite{Phillips80} no single effective temperature $T_e$
can be found for $c(T_e)$ and $\lambda(T_e)$ to describe the
solution of nonlinear equation. Experimental nonlinear curves can
be normalized and apparent thermal diffusivity value can be
calculated from the halftime $t_{1/2}$ using Parker's formula
\begin{equation}\label{e:Parkers}
    \alpha = 0.139 \frac{L^2}{t_{1/2}},
\end{equation}
but the results will be a function of $T_1$ (laser energy), as it
was observed on graphite samples in \cite{Blumm02}.

\section{Parameter Estimation Technique}
Determination of temperature dependent thermophysical properties
from the measured temperature responses is a \textit{coefficient
inverse problem} and many numerical and analytical methods were
proposed to solve this problem (see e. g. \cite{Yang00}. In this
paper, we describe a new simple parameter estimation technique to
determinate unknown coefficients of the temperature dependent
thermal conductivity (diffusivity) given by Equation
(\ref{e:Flambda}) from a measured temperature response in the
flash method.
\par Sensitivity study of nonlinear response curve showed that
its sensitivity coefficients \cite{Beck77} (partial derivatives
with respect to $a_0, a_1, T_0$ and $T_1$ respectively) are
linearly independent, so the coefficients $\vec \beta \equiv (a_1,
a_2, T_0, T_1)$ can be found simultaneously.
\par Ordinary least square procedure (Fortran package ODRPACK \cite{ODRPACK}) was used to
find the unknown parameters $\vec \beta$ from

\begin{equation}\label{e:OLS}
    \min_{\vec \beta} \sum_{i=1}^n \big[ f_i(t_i;\vec \beta) - y_i \big]^2,
\end{equation}
where $f_i(t_i;\vec \beta)$ is the temperature point at time $t_i$
calculated using the numerical solution given by Equations
(\ref{e:NumericSet}) and $(t_i, y_i)$, $i=1,2,3,\ldots, n$ are the
points of the temperature response curve (observed data).

\section{Results and Discussion}

Five different sets of temperature rise were generated by a
computer in order to demonstrate proposed parameter estimation
technique. In this example the stability and accuracy of the
technique are tested. Set 1 was generated using: $L=0.002$ m,
$c_0=10^6$ J/(m$^3$K), $a_0=100$ W/(mK), $a_1=0.05$ K$^{-1}$,
$\Delta t=10^{-6}$ s, $T_0=0$ $^\circ$C and $T_1=500$ $^\circ$C.
The sets 2, 3, 4 and 5 were generated using Set 1 data with
different level of noise added to the temperature points.
Superimposed noise imitates experimental errors and was generated
using random number generator. The sets differ from each other by
noise to signal ratio.
\par The results of parameter estimations are listed in Table \ref{tab:Results}.
The reproducibility and accuracy of the calculated parameters is
relatively high, even for Set 5 with the highest noise to signal
ratio. The differences between estimated and exact value of the
parameter are $<$ 1\verb+%+ in all cases. Standard deviation
(SD) values of $a_0$ are $<$ 1\verb+%+ and SD values of $a_1$
are $<$ 2.5\verb+%+ of the estimated value.
\par Real experimental data has to be carefully examined before
the proposed parameter estimation technique is applied. Similar
distortion can be caused by e. g. finite pulse time effect when
the pulse duration is comparable with the halftime value or by a
nonlinearity of the temperature detector used in the experiment.
Repeated measurements using different laser energy, different
pulse duration, or using different sample thicknesses have to be
conducted to identify the presence of nonlinearity in response
curves.
\par A strong dependence of the apparent thermal diffusivity on
laser pulse energy for POCO ZXF-5Q graphite sample at room
temperature was reported in \cite{Blumm02}. The apparent values of
the diffusivity were lower for higher laser pulse energy. A
plausible explanation was found in a fact that the thermal
diffusivity of graphite decreases with temperature and the
dependence is stronger at room temperature than at elevated
temperatures.
\par Our laser flash experiments with a graphite foam samples at
temperatures around room temperature also showed temperature rise
curves distortions similar to the nonlinear curves plotted in
Figure \ref{fig:DifT1}. Typical temperature rise of graphite foam
sample ($L=2.01\cdot 10^{-3}$ m, $c_0=6.86\cdot 10^5$ J/(m$^3$K))
after the pulse was about 8 $^\circ$C. Apparent thermal
diffusivity was $\alpha=1.19\cdot10^{-4}$ m$^2$/s. After finite
pulse time correction \cite{Taylor74}, the thermal diffusivity
value was $\alpha=1.45 \cdot 10^{-4}$ m$^2$/s. The results of our
parameter estimation technique were: $T_0=99.03$ $^\circ$C,
$T_1=427$ $^\circ$C, $\alpha(T_0)=(2.16\pm 0.15)\cdot 10^{-4}$
m$^2$/s and $a_1=0.093$ K$^{-1}$. The value of the thermal
diffusivity at $T_0$ calculated using the parameter estimation
technique seems to be more realistic than the corrected apparent
value. On the other hand, the value of parameter $a_1$ indicates
that the thermal diffusivity decreases with temperature more
rapidly than was found in the experiment. The response curve was
distorted mainly due to the finite pulse time effect.
\par
\begin{table}
%  \centering
  \caption{Results of parameter estimation.}\label{tab:Results}
\begin{tabular}{cccccccc}
  \hline
  % after \\: \hline or \cline{col1-col2} \cline{col3-col4} ...
  Set & Ratio & $T_0$ & $T_1$ & $a_0$ & SD & $a_1$ & SD \\
  No. & N/S & $^\circ$C & $^\circ$C & W/(mK) & W/(mK) & 1/K & 1/K \\
  \hline
  1 & 0     & 0.00 & 500.00 & 100.00 & 0.00 & 0.05000 & 0.00000 \\
  2 & 0.008 & 0.00 & 499.96 & 99.87  & 0.20 & 0.04981 & 0.00025 \\
  3 & 0.017 & 0.00 & 499.93 & 99.74  & 0.43 & 0.04962 & 0.00057 \\
  4 & 0.025 & 0.00 & 499.89 & 99.60  & 0.64 & 0.04943 & 0.00086 \\
  5 & 0.042 & 0.00 & 499.85 & 99.47  & 0.86 & 0.04925 & 0.00114 \\
  \hline
\end{tabular}
\end{table}

%\section{}
%\label{}

\begin{thebibliography}{00}

% \bibitem{label}
% Text of bibliographic item

% notes:
% \bibitem{label} \note

% subbibitems:
% \begin{subbibitems}{label}
% \bibitem{label1}
% \bibitem{label2}
% If there is a note, it should come last:
% \bibitem{label3} \note
% \end{subbibitems}

%\bibitem{}


\bibitem{Parker61}W. J. Parker, R. H. Jenkins, C. P. Butler, and G. L. Abbott, {\em J. Appl. Phys. }{\bf 32 }(1961), 1679.

\bibitem{Degio85} A. Degiovanni, G. Sinicky and  M. Laurent, {\em in Thermal Conductivity 18,
Editors T. Ashworth and D. R. Smith } Plenum, NY, 1985 573.

\bibitem{Kreith93} F. Kreith and M. S. Bohn, {\em Principles of Heat Transfer, 5th Ed. } {\bf 117 }West, St. Paul, 1993
189.

\bibitem{Phillips80}W. A. Phillips, {\em J. Appl. Phys. }{\bf 51 }(1980), 3583.

\bibitem{Blumm02} J. Blumm and S. Lemarchand: "Influence of Test Conditions on
the Accuracy of Laser Flash Measurements", {\em in Proceedings of
ECTP 16}, London, 2002 (in print).

\bibitem{Yang00} Ching-yu Yang, {\em Int. J. Heat Mass Trans.  } {\bf 43}, (2000),
1261-1270.

\bibitem{Beck77} J. V. Beck and K. J. Arnold, {\em Parameter Estimation in Engineering and Science}, John Wiley and Sons,
NY 1997, 349.

\bibitem{ODRPACK} P. T. Boggs, R. H. Bird, J. E. Rogers and R. B.
Schnabel, {\em ODRPACK Ver. 2.01 User's Reference Guide} , US
Dept. of Commerce, Gaithersburg, MD, 1992.

\bibitem{Taylor74} R. E. Taylor, L. M. Clark III, {\em High Temper. High Press.  } {\bf 6}, (1974),
65.

\end{thebibliography}

\end{document}
