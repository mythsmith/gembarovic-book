% Template article for preprint document class `elsart'
% SP 2000/09/06

\documentclass{elsart}
\usepackage{graphicx}% Include figure files

% if you use PostScript figures in your article
% use the graphics package for simple commands
% \usepackage{graphics}
% or use the graphicx package for more complicated commands
% \usepackage{graphicx}
% or use the epsfig package if you prefer to use the old commands
% \usepackage{epsfig}

% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}

\begin{document}

\begin{frontmatter}

% Title, authors and addresses

% use the thanksref command within \title, \author or \address for footnotes;
% use the corauthref command within \author for corresponding author footnotes;
% use the ead command for the email address,
% and the form \ead[url] for the home page:
\title{Damped Heat Wave Algorithm for Temperature Distribution
Calculations}
 \author[Lafayette]{Jozef Gembarovic \corauthref {1}}
 \ead{gembar@tprl.com}
 \author [Lafayette]{Martin L\"{o}ffler}
 \author[Lafayette]{ Jozef Gembarovic, Jr.}
 \corauth[1]{Corresponding author}
 \address[Lafayette]{TPRL, Inc., 3080 Kent Avenue, West Lafayette, IN 47906, USA}


\begin{abstract}
% Text of abstract
Damped heat wave algorithm is used to approximately calculate
temperature distribution in a finite, one-dimensional medium and
the results are compared to known analytical solutions and finite
difference numerical algorithm. Proposed algorithm can serve as a
rapid, easy to understand and easy to implement alternative to
existing numerical and analytical procedures for calculating the
temperature distribution in solids.
\end{abstract}

\begin{keyword}
% keywords here, in the form: keyword \sep keyword
numerical algorithm  \sep heat conduction \sep temperature
distribution
% PACS codes here, in the form: \PACS code \sep code
\PACS 66.30.-h \sep 44.05.+e \sep 66.70.+f
\end{keyword}
\end{frontmatter}

% main text
\section{Introduction}
Heat flow in solids is generally regarded as a diffusion-like
process, which is proportional to the temperature gradient; the
constant of the proportionality for a given material is called
thermal conductivity $\lambda$. Temperature distribution is a
solution of the equation of linear flow of heat \cite{CJ15}:
\begin{equation}\label{e:HCE}
\frac{\partial T}{\partial t}=\alpha \frac {\partial^2 T}{\partial
x^2}
\end{equation}
where $T=T(x,t)$ is the temperature in space-time point $(x,t)$
and $\alpha $ is the thermal diffusivity. Equation (\ref{e:HCE})
represents a partial differential equation (PDE) of
\textit{parabolic} type and its analytical solutions show a
paradoxical behavior of infinite speed of propagation of the
thermal disturbance. Any local change in temperature causes an
instantaneous perturbation at each point of the medium, whatever
is its distance from the origin of the initial perturbation. It is
in contradiction with the theory of relativity and also with known
mechanisms of heat conduction.
\par Experiments with \textit{second sound} in solid helium
and in other crystalline solids \cite {AckerGuyer68} at very low
temperatures clearly showed that the heat flows as a damped wave.
If the crystal structure is almost defect free (perfect) and the
conditions for the second sound are met, then after a pulse
heating an observable humplike formation of increased temperature
(\textit {damped heat wave}) moves with a constant speed through
the medium. The wave bounces back and forth from the boundaries
while slowly dissipating its energy along the path. The damped
heat wave is described by PDE of \textit{hyperbolic} type first
derived by Maxwell \cite{Maxwell1867} and later postulated by
Vernotte \cite{Vernotte58} and Cattaneo \cite{Cattaneo58}:

\begin{equation}\label{e:HHCE}
\tau \frac{\partial^2 T}{\partial t^2}+\frac{\partial T}{\partial
t}=\alpha \frac {\partial^2 T}{\partial x^2}
\end{equation}
where $\tau$ is the thermal relaxation time. Speed of propagation
of the thermal wave is $\nu=\sqrt{\alpha/\tau}$. Amplitude of the
damped heat wave decays exponentially with time
\cite{Gembarovic87}. Equation (\ref{e:HCE}) is a limiting case of
Equation (\ref{e:HHCE}) for $\tau\rightarrow 0$.
\par

As it was first noted by Alifanov \cite{Alifanov1994}, explicit
finite difference methods for numerical solution of Equation
(\ref{e:HCE}) show a finite speed of heat propagation, so they can
be regarded as a solutions of Equation (\ref{e:HHCE}), rather than
Equation (\ref{e:HCE}).

\par This paper will show that the temperature distribution in one
dimensional finite medium can be approximately calculated using a
new, simple algorithm based on damped heat wave (DHW).

\section{Description of the Algorithm}

We will illustrate the algorithm on calculation of the temperature
distribution in an isotropic homogeneous finite medium ($0\leq
x\leq L$), with zero initial temperature, with one surface
instantaneously heated by an energy pulse at $t=0$. The heat flux
is one-dimensional. The medium is divided into $N$ equal slabs of
thickness $\Delta l=L/N$. These slabs are replaced by a perfect
conductor of the same heat capacity separated by thermal
resistance $\Delta l/\lambda$, so the  temperature within a slab
at any given time is constant.
\par Heat propagates from one slab to another due to
existence of a temperature difference between the slabs. The wave
takes certain portion (given by the \textit{inner transfer
coefficient} $\xi$) of excessive heat energy from one slab and
moves that amount to the next one (redistribution), lowering thus
the temperature difference  between the two neighbor slabs. The
wave marches in space from one pair of slabs to another and when
it reaches the boundary slab of the medium it bounces back and
moves in the opposite direction in a perpetual manner.
\par
Undamped heat wave ($\xi=1$) does not dissipate and all excessive
energy initially localized in the first slab is travelling
unchanged back and forth through the medium.
\par
If the wave is damped then only a portion ($\xi<1$) of the energy
difference between two neighboring slabs is moved by the wave in
the direction of the wave movement at a time, leaving the rest of
the energy behind. An example of the dissipative wave in
one-dimensional finite medium with $\xi$ = 0.95 is shown in Figure
\ref{fig:DampedHW}. The medium is divided into $N=6$ slabs. The
wave height is reduced from 10 to 3.61 units after 28 time steps.
The rest of the media is at about 1.2 units. The wave height
decays exponentially with time, similarly as in the solution of
Equation (\ref{e:HHCE}) for a finite medium discussed in details
in \cite{Gembarovic87}. Sum of all heights in the medium is always
equal to 10, in accordance with the total energy conservation law.


\begin{figure}
\begin{center}
\includegraphics[scale=1.4]{fig2m}
\caption{\label{fig:DampedHW}}{Damped heat wave in a finite
medium. Inner transfer coefficient $\xi = 0.95$.}
\end{center}
\end{figure}

Strongly damped heat wave ( with $\xi<0.5$) can be used for an
approximative solution of Equation (\ref{e:HCE}) in a finite
medium.
\par
Slab temperatures are $T_{i,m}\equiv T(x_i,t_m)$, where $x_i$,
($i=0,1,2,\ldots,N-1$) is a spatial point (middle of the $i$th
slab), and $t_m=m\Delta t $, ($ m=0,1,2,\ldots$) is discrete time
point. Temperature of the boundary slabs in our algorithm is
actually changing only after the heat wave finishes one whole
loop, therefore it is logical to have time step $\Delta t$ equal
to one loop time interval. Time step $\Delta t$ is divided to $2N$
sub-steps when the heat wave moves from one slab to another
redistributing thermal energy between two neighbor slabs.
Temperature of a particular slab is changed four times during one
time step - two times when the heat wave is marching over from
left to right, and two more times when the wave is heading in the
opposite direction.
\par Temperature distribution at time $t_{m+\frac{1}{2}}$ after
the heat wave finished marching from left to right is given by:

\begin{eqnarray}\label{e:leftwave0}
T_{0,m+\frac{1}{2}} = T_{0,m} -  \xi
(T_{0,m}-T_{1,m})=(1-\xi)T_{0,m} + \xi T_{1,m},
\end{eqnarray}

\begin{eqnarray}\label{e:leftwaven}
T_{n,m+\frac{1}{2}} = \xi^n (1-\xi)T_{0,m} +(1-\xi)^2
\sum_{j=1}^{n}\xi^{n-j}T_{j,m} + \xi T_{n+1,m} {}
                                \nonumber\\
 {} n=1,2,\ldots N-2 {},
\end{eqnarray}

\begin{eqnarray}\label{e:leftwaveN}
T_{N-1,m+\frac{1}{2}} = \xi^{N-1} T_{0,m} +
(1-\xi)\sum_{j=2}^{N}\xi^{N-j}T_{j,m}.
\end{eqnarray}

Similarly, the temperature distribution at time $t_{m+1}$ after
the heat wave finished marching from right to left is:

\begin{eqnarray}\label{e:rightwaveN}
T_{N-1,m+1} = (1-\xi)T_{N-1,m+\frac{1}{2}} + \xi
T_{N-2,m+\frac{1}{2}},
\end{eqnarray}

\setlength\arraycolsep{2pt}
\begin{eqnarray}\label{e:rightwaven}
T_{n,m+1} = \xi^{N-n-1} (1-\xi)T_{N-1,m+\frac{1}{2}} +{}
                                    \nonumber\\
{} +(1-\xi)^2 \sum_{j=1}^{N-n-1}\xi^{N-j-1}T_{N-j-1,m+\frac{1}{2}}
+ \xi T_{n-1,m+\frac{1}{2}}
\nonumber\\
 {} n=1,2,\ldots N-2 {},
\end{eqnarray}

\begin{eqnarray}\label{e:rightwave0}
T_{0,m+1} = \xi^{N-1} T_{N-1,m+\frac{1}{2}} +
(1-\xi)\sum_{j=2}^{N}\xi^{N-j}T_{N-j,m+\frac{1}{2}}.
\end{eqnarray}

Finally, the temperature distribution in medium at time $t_{m+1}$
as a function of the temperature distribution at $t_m$ is after a
lengthy but straightforward set of manipulation of Equations
(\ref{e:leftwave0}) - (\ref{e:rightwave0}) given by

\begin{eqnarray}\label{e:genfirst}
T_{0,m+1} = \biggl[ 2\frac{1+\xi^{2N-1}}{1+\xi}-1\biggr]T_{0,m} +
2\frac{1-\xi}{1+\xi} \sum_{j=1}^{N-1} \xi^j
\biggl(1+\xi^{2(N-j)-1}\biggr)T_{j,m},
\end{eqnarray}

\setlength\arraycolsep{2pt}
\begin{eqnarray}\label{e:genothers}
T_{i,m+1} =
2\frac{1-\xi}{1+\xi}\biggl(1+\xi^{2(N-i)-3}\biggl)\sum_{j=0}^{i-1}
\xi^j (1-\xi)^j T_{j,m}+ {}
                   \nonumber\\
 {}+\biggl[ (1-\xi)^2
 \biggl(2\frac{1+\xi^{2(N-i)-3}}{1+\xi}-1\biggr)+\xi^2\biggr]T_{i,m}+{}
                 \nonumber\\
+2\frac{(1-\xi)^2}{1+\xi} \sum_{j=0}^{N-i-1}\xi^j \biggr(1+
\xi^{2(N-i-j)+1}\biggr)T_{j+i+1,m} {}
                    \nonumber\\
                   {} i=1,2,\ldots,N-1. {}
\end{eqnarray}
The sum of all coefficients multiplying temperatures at right side
of the Equations (\ref{e:genfirst}) and (\ref{e:genothers}) is
equal to 1, as it should be due to validity of total thermal
energy conservation principle.
\par
As can be seen from Equations (\ref{e:genfirst}) and
(\ref{e:genothers}) the new temperature of the particular slab at
the time point $t_{m+1}$ depends not only on temperatures of its
neighbor slabs at time $t_m$, but on the temperatures of all slabs
in the medium. The influence of more distant slabs is diminishing
exponentially.

%\par The inner transfer coefficient $\xi$ is a dimensionless quantity defined as
%\begin{eqnarray}\label{e:TCoeff}
%\xi = \frac{1}{1 + 2Fo^{-1}},
%\end{eqnarray}
%where
%\begin{eqnarray}\label{e:FoDef}
%Fo = \frac{\Delta t\alpha}{\Delta l^2}
%\end{eqnarray}
%is the Fourier number for one slab.

\subsection{Inner Heat Transfer Coefficient}
In order to find physical meaning of the inner heat transfer
coefficient we will follow Fourier's explanation given in
\cite{Fourier-2} for the heat transfer between two discrete
bodies.
 \par Fourier considers two equal rectangular bodies mass $m$,
cross section area $A$, thickness $\Delta l$, of the same material
with specific heat $c$, density $\rho$, and perfect conductivity
at different temperatures $a$ and $b$. He imagines the
transmission of heat between the bodies by means of an ideal
shuttle mechanism consisting of infinitesimally small section
$\omega$ which moves to and fro in a fixed time $\Delta t$ between
the two masses. From the heat transfer point of view the situation
is identical with our DHW algorithm for $N=2$.
\par
 If these two bodies are  placed in
contact, the temperature in each would suddenly become equal to
the mean temperature $\frac{1}{2}(a+b)$. Two masses (see Figure
\ref{fig:2Bodies}) are separated by a very small interval, that a
thin layer $\delta$ of the first is detached so as to be joined to
the second, and that it returns to the first immediately after the
contact. Continuing thus to be transferred alternately, and at
equal small time intervals, the interchanged layer causes the heat
of the hotter body to pass gradually into that which is less
heated. There are no heat losses from the bodies to an ambient.
The quantity of heat contained in a thin layer is suddenly added
to that of the body with which it is in contact and the common
temperature results which is equal to the quotient of the sum of
the quantities of heat divided by the sum of the masses multiplied
by the specific heat. Let $\omega$ be the mass of small layer
which is separated from the hotter body, whose temperature is $a$;
let $\theta$ and $\vartheta$ be the variable temperatures which
correspond to the time $t$, and whose initial values are $a$ and
$b$. When the layer $\omega$ is separated from the mass $m$ which
becomes $m-\omega$, it has like this mass the temperature
$\theta$, and as soon as it touches the second body with
temperature $\vartheta$, it assumes at the same time with that
body a temperature equal to
\begin{equation}\label{e:T after 1st contact}
\frac{\vartheta mc+\theta \omega c}{mc+\omega c}=\frac{\vartheta
m+\theta \omega}{m+\omega}.
 \end{equation}
The layer $\omega$, retaining the last temperature, returns to the
first body, whose mass is $m-\omega$ and temperature $\theta$. The
temperature after the second contact is
\begin{equation}\label{e:T after 2nd contact}
    \frac{\theta(m-\omega)c+\biggl(\frac{\vartheta m+\theta\omega }
    {m+\omega}\biggr)\omega c}{mc}=\frac{\theta m+\vartheta
    \omega }{m+\omega}.
\end{equation}
\begin{figure}
\begin{center}
\includegraphics[scale=1.0]{graphic1}
\caption{\label{fig:2Bodies}}{Heat Transfer between two discrete
bodies.}
\end{center}
\end{figure}
The variable temperatures $\theta$ and $\vartheta$ become, after
the interval $\Delta t$,
 \begin{equation}\label{e:T after 2nd contact2}
 \theta -(\theta -
\vartheta)\frac{\omega}{m+\omega},\qquad and \qquad \vartheta +
(\theta - \vartheta)\frac{\omega}{m+\omega}.
\end{equation}
 For the differences we have
\begin{equation}\label{e:T differences}
\Delta\theta =-(\theta -
\vartheta)\frac{\omega}{m+\omega}\qquad\textrm{and}\qquad\Delta\vartheta
=(\theta - \vartheta)\frac{\omega}{m+\omega}.
\end{equation}
While $m=\Delta lA\rho$ and $\omega=\delta A\rho$, the masses $m$
and $\omega$ can be replaced by $\Delta l$ and $\delta$,
respectively. Equation (\ref{e:T differences}) is now
\begin{equation}\label{e:T differencesThickness}
\Delta\theta =-(\theta - \vartheta)\frac{\delta}{\Delta
l+\delta}\qquad\textrm{and}\qquad\Delta\vartheta =(\theta -
\vartheta)\frac{\delta}{\Delta l+\delta}.
\end{equation}
 Quantity of heat received in one instant by second mass is equal
to the quantity of heat lost by the first mass. The quantity of
heat is, if we assume that all other things being equal,
proportional to the actual difference of temperature of the two
bodies.
\par Term $\delta$ (or $\omega$) represents the velocity of
transmission, or the facility with which heat passes from one of
the bodies into the other. Fourier \cite{Fourier-2} calls it the
\textit{reciprocal conductivity}. In order to find its relation to
other thermophysical parameters, we have to express the actual
amount of heat transferred in one time step and to compare it to
Fourier law for heat flux. From Equation (\ref{e:T
differencesThickness}) we see, that the amount of heat transferred
by an infinitely small layer $\delta$ between the two bodies in
$\Delta t$ is $\Delta l A\rho c\Delta \theta=-(\theta -
\vartheta)\delta A \rho c$. From the comparison with Fourier law
\begin{equation}\label{e:Comparison}
-(\theta - \vartheta)\delta A \rho c=-\lambda\frac{(\theta -
\vartheta)}{\Delta l}A\Delta t,
\end{equation}
it follows, that $\delta$ is
\begin{equation}\label{e:Omega}
 \delta=\frac{\lambda\Delta
t}{\rho c \Delta l}=\frac{\alpha \Delta t}{\Delta l}.
\end{equation}
\par
The temperature difference $\Delta \theta$ in Equation (\ref{e:T
differencesThickness}) after these replacements is
\begin{equation}\label{e:T differenceTheta}
\Delta\theta =-(\theta - \vartheta)\frac{1}{1+Fo^{-1}},
\end{equation}
where
\begin{equation}\label{e:Fo}
 Fo=\frac{\alpha \Delta t}{\Delta l^2},
\end{equation}
is Fourier number for one layer.
\par
The time step in DHW algorithm is exactly 2 times longer than
$\Delta t$ from the Fourier model. Comparison of Equation
(\ref{e:T differenceTheta}) with Equation (\ref{e:leftwave0})
reveals that for the \textit{inner heat transfer coefficient}
$\xi$ in DHW algorithm we have
\begin{equation}\label{e:IHTCoefficient}
\xi =\frac{1}{1+2Fo^{-1}}.
\end{equation}
\par
Fourier also considered \cite{Fourier-N} the general case of $N$
separate equal masses arranged in a straight line and initially at
arbitrary temperatures $a$, $b$, $c$, $\ldots$ in which
transmission of heat takes place by the same shuttle mechanism
between the bodies as in the case of two bodies only.
Infinitesimally thin layers $\omega$ move to and fro between
successive bodies, all at once, so the situation for inner bodies
is not the same as for the two bodies at the boundaries. Fourier's
mechanism differs from DHW algorithm, in which a 'wave of
redistributions' is marching through the medium, consecutively
changing the temperatures of only two neighbor slices. Inner
transfer coefficient $\xi$ for $N>2$ is exactly the same as for
the case $N=2$. These two algorithms converge to an exact
analytical solution for $\xi \rightarrow 0$.
\par
\subsection{Examples}
\par When the wave imitates diffusion, the upper limit
for the inner transfer coefficient is $\xi<0.5$. It follows from
Equation (\ref{e:IHTCoefficient}) that the upper limit for the
time step $\Delta t$ is then given by:
\begin{eqnarray}\label{e:DeltatCond}
\Delta t < \frac{2\Delta l^2}{\alpha}.
\end{eqnarray}



\begin{figure}
\begin{center}
\includegraphics[scale=0.6]{adiab}
\caption{\label{fig:adiab} Temperature distributions for different
times in a real medium. Temperatures calculated using DHW
algorithm are compared with analytical solutions given by Eq.
(\ref{e:FlashEq}) (continuous lines). The profiles are for
$t_{60}=0.3$ s, $t_{120}=0.6$ s,$t_{180}=0.9$ s, $t_{240}=1.2$ s,
and $t_{480}=2.4$ s.}
\end{center}
\end{figure}
\begin{figure}
\begin{center}
\includegraphics[scale=0.6]{dadiab}
\caption{\label{fig:dadiab} Differences between the temperatures
calculated using DHW algorithm and the exact analytical solutions
from Fig. \ref{fig:adiab}.}
\end{center}
\end{figure}


This introduces a limit to the maximum size of time step that can
be chosen for a fixed $\Delta l$. In the case of strongly damped
waves, their actual position and speed are not that important. In
another words, the wave speed $v = 2L/\Delta t$ can be chosen
arbitrarily, but from the inequality (\ref{e:DeltatCond}) we get
$v>\alpha N^2/L$. Generally, the calculated distribution using
waves with a higher speed is more precise than those with the
lower ones. Condition (\ref{e:DeltatCond}) is similar to the
stability condition for the finite differences algorithms \cite
{Richt57}
\begin{eqnarray}\label{e:StabCondFD}
\Delta t < \frac{\Delta l^2}{2\alpha},
\end{eqnarray}
used for a numerical solution of one-dimensional heat conduction
equation. It is clear that for the same $\Delta l$ our algorithm
allows using longer time steps than the finite differences schemes
do.
\par Temperature distributions in a real medium where: $\alpha =
6.5\times 10^{-5}$ $\textrm{m}^2$ $\textrm{s}^{-1}$, $ l = 0.02$
m, with $\textit{N} = 20$ and $\Delta\textit{t} = 5.0\times
10^{-3}$ s, are shown in Figure \ref{fig:adiab}. The inner
transfer coefficient is $\xi = 0.139785$. The temperature is 20
units in the first slab (from left) at the beginning (\textit{t} =
0) and the rest of the sample is at zero temperature. There are no
heat losses at the boundaries. The temperature distribution
calculated by using DHW is compared with the temperature
distribution calculated using the exact analytical formula
\cite{CJ101}

\begin{eqnarray}\label{e:FlashEq}
T(x,t) = 1+2\sum_{n=1}^\infty\cos\left(n\pi\frac{x}{
L}\right)\exp\left[-n^2\pi^2\frac{\alpha t}{L^2}\right].
\end{eqnarray}
\par
Differences between the calculated temperatures and the analytical
solution are shown in Figure \ref{fig:dadiab}. Those differences
are not random, but show regular patterns. Generally, they are
smaller for longer times. Solution given by Eq. (\ref{e:FlashEq})
assumes that the heat is instantaneously absorbed in an infinite
thin surface layer of the solid, while the wave algorithm assumes
that the heat is equally distributed in the first slab at the time
zero. This explains the higher differences at small times.
\par
Temperature distributions in the real medium for $t_{60}= 0.3$ s
and $t_{120}= 0.6$ s calculated using DHW algorithm and explicit
finite difference (EFD) scheme are compared in Fig.
\ref{fig:comp}. The results of DHW algorithm at $t_{60}= 0.3$ s
are above the analytical solution values for the points in right
half of the medium, while the values of explicit finite difference
scheme are bellow the analytical ones. The differences between the
two numerical algorithms are negligible at higher times.


\begin{figure}
\begin{center}
\includegraphics[scale=0.6]{comp2}
\caption{\label{fig:comp} Temperature distributions for
$t_{60}=0.3$ s and $t_{120} = 0.6$ s in a real medium. The results
of DHW algorithm (+) are compared with those calculated using
explicit finite difference (EFD) scheme (diamonds). The analytical
solutions are shown as continuous lines.}
\end{center}
\end{figure}



\par
In the case of heat losses from the boundaries, the exact
analytical formula for the temperature within the finite medium is
given by

\begin{eqnarray}\label{e:HLFlashEq}
T(x,t) =\sum_{n=1}^\infty F(\beta_n)\left[\cos\left(\frac{\beta_n
x}{L}\right)+ \frac{B_1}{\beta_n}\sin\left(\frac{\beta_nx}{
L}\right)\right]\exp\left[-\frac{\beta_n^2at}{ L^2}\right],
\end{eqnarray}
where
\[
F(\beta_n)=\frac{2\beta_n^2(\beta_n^2+B_2^2)}
{(\beta_n^2+B_1^2)(\beta_n^2+B_2^2)+(B_1+B_2)(\beta_n^2+B_1B_2)},
\]
$\beta_n,n = 1,2,3, ...$ are positive roots of the equation:
\begin{eqnarray}\label{e:Betas}
(\beta^2-B_1B_2)\sin \beta - (B_1 + B_2)\beta\cos\beta = 0,
\end{eqnarray}
and $B_1, B_2$ are Biot numbers for the front surface ($x=0$), and
for the rear surface of the medium  ($x=L$), respectively. The
Biot numbers are defined as dimensionless quantities $B =
HL/\lambda$ , where $H$ is the coefficient of surface heat
transfer.
\par
If there are heat losses from the medium surfaces a part of the
excessive thermal energy leaves the medium, everytime the wave
reaches the boundary slabs. Temperatures of the boundary slabs are
furthermore changed due to the heat losses:
\begin{eqnarray}\label{e:Boundhl0}
T_{N-1,m+\frac{1}{2}} = T_{N-1,m}-\zeta_2 (T_{N-1,m}-T_A),
\end{eqnarray}
and
\begin{eqnarray}\label{e:BoundhlL}
T_{0,m+1} = T_{0,m\frac{1}{2}}-\zeta_1 (T_{0,m\frac{1}{2}}-T_A),
\end{eqnarray}

where $\zeta_1$ and  $\zeta_2$ are the \textit{surface transfer
coefficients} and $T_A$ is the ambient temperature. The surface
transfer coefficient is a dimensionless quantity defined as:
\begin{eqnarray}\label{e:STCoeff}
\zeta = Bi Fo ,
\end{eqnarray}
where $Bi$ is Biot number for one slab $Bi = B/N$. There are two
different surface transfer coefficients for a finite medium, one
for each surface. In order to fulfill the limitation $\zeta
\in<0,1>$, the time step $\Delta t$ has to be limited to
\begin{eqnarray}\label{e:BioLim}
\Delta t\leq\frac{\Delta l^2}{Bi \alpha}.
\end{eqnarray}
For $Bi < 0.5$ the condition given by inequality
(\ref{e:DeltatCond}) is stronger than (\ref{e:BioLim}), and is
actually limiting the time step value.

\par
Temperatures within the real medium from the previous example,
with heat losses given by Biot numbers $Bi_1=0.2$ and $Bi_2=0.1$
are in Figure \ref{fig:hl}. Values calculated using DHW algorithm
are compared with the the temperature distributions calculated
using the analytical formulae Eqs.
(\ref{e:HLFlashEq})-(\ref{e:Betas}) for the same times as those
from Fig. \ref{fig:adiab}. The differences between the
temperatures calculated using DHW and the analytical solutions are
shown in Figure \ref{fig:dhl}. For longer times the differences
are simple smooth curves which converge to almost
linear profile with the values ranging from 0.1\verb+%+ to 0.4\verb+%+.
This precision level is sufficient for many practical engineering
applications.



\begin{figure}
\begin{center}
\includegraphics[scale=0.6]{hl1}
\caption{\label{fig:hl} Temperature distributions for different
times in a real slab with heat losses. Analytical solutions
(continuous lines) were calculated using Eqs
(\ref{e:HLFlashEq})-(\ref{e:Betas}).}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=0.6]{dhl1}
\caption{\label{fig:dhl}  Differences between the temperatures
calculated by the damped heat wave algorithm and the exact
analytical formula from Fig. \ref{fig:hl}.}
\end{center}
\end{figure}

Various boundary conditions can be modelled by adjusting the
surface transfer coefficient $\zeta$. In the case $\zeta = 1$  the
constant temperature (equal to the ambient temperature) boundary
condition is being simulated. Adiabatically insulated surface is
given by $\zeta = 0$. Nonlinear conditions can be also modelled,
e.g. radiation from the surface, in which the rate of heat energy
leaving the surface is proportional to
\[
\sigma\epsilon(T_i^4-T_A^4),
\]
where $\sigma$ is the Stefan-Boltzmann constant and $\epsilon$ is
the emissivity of the surface. Temperature difference of the
fourth power of the temperatures will be used in Eqs.
(\ref{e:Boundhl0})-(\ref{e:BoundhlL}).

\section{Conclusion}
Temperature distribution in solids can be calculated from heat
conduction equation, which can be solved using a broad variety of
analytical or numerical methods. Existing numerical procedures are
generally simpler and more straightforward than analytical and
they can be used to solve more complex engineering problems. In
most of finite differences schemes \cite {Ozisik} and finite
elements methods \cite {Zienk71} systems of linear algebraic
equations have to be solved for every time step. In DHW algorithm
the temperature is calculated explicitly in one simple calculation
that is repeated 2$N$ times for each time step. The process of
calculation is sequential and only a one pair of neighboring slab
temperatures is taken into account. Temperature in the medium is
changing only at the front of the solitary wave of redistribution,
which is marching through the medium. Described algorithm is
similar to the explicit finite difference scheme, which utilize
both left and right neighbor points at the same time for the
temperature calculations.

\par DHW algorithm can be used by engineers and
designers in applications where temperature distribution in a
finite one-dimensional solid has to be calculated. It can serve as
a fast, easy to understand and easy to implement alternative to
existing numerical and analytical methods. It could simplify
hardware and software needs for temperature and heat flux
calculations, and open new possibilities for improving measurement
and non-destructive testing procedures used in this field. It can
also be used for solving mathematically similar boundary value
problems in mass diffusion and viscous flow.

%\section{}
%\label{}

\begin{thebibliography}{00}

% \bibitem{label}
% Text of bibliographic item

% notes:
% \bibitem{label} \note

% subbibitems:
% \begin{subbibitems}{label}
% \bibitem{label1}
% \bibitem{label2}
% If there is a note, it should come last:
% \bibitem{label3} \note
% \end{subbibitems}

%\bibitem{}


\bibitem{CJ15}H.~S. Carslaw and J.~C. Jaeger, {\em Conduction of Heat in Solids, 2nd ed., }
Oxford University Press, London 1959, p. 50.

\bibitem{AckerGuyer68} C.~C. Ackerman and R.~A. Guyer, {\em Annals of Physics }{\bf 50 }(1968)
128.

\bibitem{Maxwell1867}
J.~C. Maxwell, {\em Phil. Tran. R. Soc. } {\bf 157}, (1876), 49.

\bibitem{Vernotte58} J. Vernotte, {\em C. R. } {\bf 246}, (1958), 3154.

\bibitem{Cattaneo58} C. Cattaneo, {\em C. R. } {\bf 247}, (1958), 431.

\bibitem{Gembarovic87}J. Gembarovic and V. Majernik, {\em Int. J. Heat Mass Trans. }{\bf 31 }(1987) 1073.

\bibitem{Alifanov1994}O.~M. Alifanov, {\em Inverse Heat Transfer Problems }
Springer - Verlag, New York, 1994, 186.

\bibitem{Fourier-2} J. Fourier, {\em Analytical Theory of Heat,
translated with notes of A. Freeman, } (Dover Publications, New
York 1955) p. 225.

\bibitem{Fourier-N} J. Fourier, {\em Analytical Theory of Heat,
translated with notes of A. Freeman, } (Dover Publications, New
York 1955) p. 228.

\bibitem{Richt57}R.~D.
Richtmeyer, {\em Difference Methods for Initial Value Problems }
Interscience Publishers, New York, 1957.

\bibitem{CJ101}H.~S. Carslaw and J.~C. Jaeger, {\em Conduction of Heat in Solids, 2nd ed., }
Oxford University Press, London 1959, p. 101.

\bibitem{Ozisik}M.~N. Ozisik, {\em Finite Difference Methods in Heat Transfer}
CRC Press, Boca Raton, FL, 1994.

\bibitem{Zienk71} O.~C. Zienkiewicz, {\em The finite Element Method in Engineering
Science } McGraw-Hill, New York 1971.

\end{thebibliography}

\end{document}
