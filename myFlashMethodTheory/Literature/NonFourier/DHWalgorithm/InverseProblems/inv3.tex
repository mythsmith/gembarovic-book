%%%%%%%%%%%%%%%%%%%%%%% file template.tex %%%%%%%%%%%%%%%%%%%%%%%%%
%
% This is a template file for the global option of the SVJour class
%
% Copy it to a new file with a new name and use it as the basis
% for your article
%
%%%%%%%%%%%%%%%%%%%%%%%% Springer-Verlag %%%%%%%%%%%%%%%%%%%%%%%%%%
%
% First comes an example EPS file -- just ignore it and
% proceed on the \documentclass line

% Choose either the first of the next two \documentclass lines for one
% column journals or the second for two column journals.
\documentclass[referee]{svjour}
%\documentclass[global,twocolumn,referee]{svjour}
% Remove option referee for final version
%
% Remove any % below to load the required packages
%\usepackage{latexsym}
\usepackage{graphicx}
\usepackage{epstopdf}
% etc
%
% Insert the name of "your" journal with the command below:
\journalname{International Journal of Thermophysics}
%
\begin{document}
%
\titlerunning{An Application of DHW Algorithm}
%
\title{An Application of DHW Algorithm for the Solution of Inverse Heat Conduction
Problem\inst{1}}
%\subtitle{Do you have a subtitle?\\ If so, write it here}

\author{J. Gembarovic\inst{2,}\inst{3} and M. L\"{o}ffler\inst{2}
}                     % Do not remove
%
%\offprints{}          % Insert a name or remove this line
%
\institute{Paper presented at the Seventeenth European Conference on
Thermophysical Properties, September 5 - 8, 2005, Bratislava, Slovak
Republic. \and TPRL, Inc., 3080 Kent Ave, West Lafayette, Indiana
47906, U.S.A. \and To whom correspondence should be addressed.
E-mail: gembar@tprl.com}
%
\date{Received: date / Revised version: 4/19/2006}
%
\maketitle
%

\begin{abstract}
%Insert your abstract here.
\par
Damped heat wave (DHW) algorithm is applied for the temperature
distribution calculation in a solution of a linear inverse heat
conduction problem (IHCP). Nonlinear least squares algorithm is used
for calculation of the unknown boundary heat flux history in a
one-dimensional medium. The solution is based on the assumption that
the temperature measurements are available, at least, at one point
of the medium over the whole time domain. Sample calculations, for a
comparison between exact heat sources and estimated ones, are made
to confirm the validity of the proposed method. The close agreement
between the exact and estimated values calculated for both exact and
noisy data shows the potential of the proposed method for finding a
relatively accurate heat source distribution in a one-dimensional
homogeneous finite medium. The proposed method of solving inverse
heat conduction problems is very simple and easy to implement.
\par
\end{abstract}
\textbf{KEY WORDS :} inverse heat conduction problem; numerical
algorithm; temperature distribution

\newpage
% main text
\section{INTRODUCTION}

One of the important type of inverse heat conduction problems (IHCP)
deals with the determination of the boundary heat flux history from
the known transient temperature distribution in a solid.
\par
Existing methods for the calculation of the heat flux from the
temperature rise of the sample are based on an analytical or
numerical solution of the heat conduction equation:
\begin{equation}\label{e:HCE}
\rho c \frac{\partial T}{\partial t}=\nabla (\lambda \nabla T),
\end{equation}
where $\rho$ is the density, $c$ is the specific heat, $\lambda $ is
the thermal conductivity, $\nabla$ is the Hamilton operator, and
$T=T(\vec{r},t)$ is the temperature in a space-time point
$(\vec{r},t)$. Equation (\ref{e:HCE}) is solved  with certain
initial and boundary conditions, and the solution of this direct
heat conduction problem (DHCP) is then compared with the
experimental temperature rise. Although an IHCP is an ill-posed
problem, the heat flux function can usually be estimated from the
comparison. Fundamental concepts of inverse heat conduction, and an
extensive bibliography and survey on IHCP methods can be found in
Beck et al. \cite{Beck85} and in Alifanov \cite{Alifanov1994}.
\par
The present work addresses the unsteady linear IHCP in a finite
medium with a time-variable heat flux at the boundary. An numerical
solution of the DHCP is obtained using our damped heat wave (DHW)
algorithm described in Refs. \cite{Gembarovic04} and
\cite{Loffler05}. The test input data for IHCP were generated using
an explicit finite difference (EFD) algorithm. The temperature
points at discrete regular times were modified by adding random
errors produced by a random number generator. Three examples of the
temperature history for piece-wise heat flux functions are given in
this paper to illustrate the use of the algorithm even in cases of
very noisy temperature history signals.
\par
The DHW algorithm is very simple, universal, and easy to implement
in comparison with most analytical or numerical solutions of Eq.
(\ref{e:HCE}).


\section{DHW ALGORITHM DESCRIPTION}

In the DHW algorithm for the calculation of the temperature
distribution in one dimension, the medium \cite{Gembarovic04} is
divided into $N$ equal slabs of thickness $\Delta l=L/N$. These
slabs are replaced by a perfect conductor of the same heat capacity
separated by a thermal resistance $\Delta l/\lambda$ (where
$\lambda$ is the thermal conductivity of the medium), so the
temperature within a slab at any given time is constant. Heat
propagates from one slab to another due to the existence of a
temperature difference between the slabs. A certain portion (given
by the inner transfer coefficient $\xi$) of the excessive heat
energy moves from one slab to the next one, lowering thus the
temperature difference between the two neighbor slabs. This
redistribution process (called the damped heat wave) starts from the
left boundary slab and marches in space from one pair of slabs to
another. When the wave reaches the boundary of the medium, it
bounces back and moves in the opposite direction in a perpetual
manner.

\par The inner transfer coefficient $\xi$ is a dimensionless quantity
given by \cite{Loffler05}
\begin{equation}\label{e:TCoeff}
\xi = \frac{Fo^{*}}{Fo^* + 2},
\end{equation}
where
\begin{equation}\label{e:FoDef}
Fo^* = \frac{\alpha \Delta t}{\Delta l^2}
\end{equation}
is the Fourier number for one slab and $\alpha$ is the thermal
diffusivity of the slab material. The time step $\Delta t$ is equal
to one loop time interval of the heat wave.
\par When the wave imitates diffusion, the upper limit
for the inner transfer coefficient is $\xi<0.5$. It follows from Eqs
(\ref{e:TCoeff}) and (\ref{e:FoDef}) that the upper limit for the
time step $\Delta t$ is then given by
\begin{equation}\label{e:DeltatCond}
\Delta t < \frac{2\Delta l^2}{\alpha}.
\end{equation}
This introduces a limit to the maximum size of the time step that
can be chosen for a fixed $\Delta l$. The wave speed $v = 2L/\Delta
t$ can be chosen arbitrarily, but from Eq. (\ref{e:DeltatCond}) we
get $v>\alpha N^2/L$. Generally, the calculated distribution using
waves with a higher speed is more precise than that with the lower
ones.
\par
In case of heat losses a part of the excessive thermal energy leaves
the medium each time the wave reaches the boundary slabs. The
temperatures change $\Delta T$ of the boundary slab due to heat
losses is
\begin{equation}\label{e:BoundhlL}
\Delta T = -\zeta (T-T_a),
\end{equation}
where $\zeta$ is the surface transfer coefficient and $T_a$ is the
ambient temperature. The surface transfer coefficient is a
dimensionless quantity defined as
\begin{equation}\label{e:STCoeff}
\zeta = Bi Fo^* ,
\end{equation}
where $Bi$ is the Biot number for one slab ($Bi = hL/\lambda N$
where $h$ is the coefficient of surface heat transfer). Generally,
there are two different surface transfer coefficients for a finite
medium, one for each surface. In order to fulfill the limitation
$\zeta \in <0,1>$, the time step $\Delta t$ has to be limited to
\begin{equation}\label{e:BioLim}
\Delta t\leq\frac{\Delta l^2}{\alpha Bi }.
\end{equation}
For $Bi < 0.5$ the condition given in Eq. (\ref{e:DeltatCond}) is
more restrictive than in Eq. (\ref{e:BioLim}), and is actually
limiting the time step value.

\section{INVERSE PROBLEM FORMULATION}

Consider a medium of thickness $L$ and constant thermal properties,
originally at zero temperature. At a specific time, $t=0$, a heat
flux is applied to one surface at $x=0$. The temperature history is
measured on the opposite surface at $x=L$. We will use the
dimensionless time (the Fourier number) defined as $Fo = \alpha
t/L^2$ instead of the time $t$. The heat flux function $q$ is
calculated in certain discrete dimensionless time points
$Fo_j=\alpha t_j/L^2, j=0,1,2,3,\ldots, n$.
\par An ordinary least squares procedure
\cite{ODRPACK} was used to find the unknown parameters $q_j$ from

\begin{equation}\label{e:OLS}
    \min_{q_j} \sum_{i=1}^k \big[ T_i(Fo_i,q_j) - Y_i \big]^2,
\end{equation}
where $T_i(Fo_i,q_j)$ is the temperature point at time $Fo_i$
calculated using the DHW algorithm and $Y_i$, $i=1,2,3,\ldots, k$
are the points of the temperature response curve (observed data).
More than one set of temperature history points can be used to find
the heat flux components in this procedure. It means that the
temperature response can be at the same time measured in different
locations of the medium. Different weights can be also assigned to
different temperature points in order to improve the precision of
the calculation. A weighted orthogonal distance regression algorithm
\cite{ODRPACK} can be used for heat flux estimation in the case when
errors are expected in both temperature and time data.

\section{INVERSE PROBLEM SOLUTION}

\subsection{Sensitivity Analysis}
\par
In order to find conditions for an optimal experiment design, we
have to analyze sensitivity coefficients of the IHCP. Dimensionless
sensitivity coefficients $X_j(Fo)$ are defined \cite{Beck85_XI} as
the first partial derivative of temperature $T = T(Fo,q_j)$ with
respect to the heat flux component $q_j$
\begin{equation}\label{e:SCoeffsDef}
    X_j(Fo)\equiv\frac{\lambda}{L}\frac{\partial T}
    {\partial q_j}, j=0,1,2,3,\ldots , n.
\end{equation}
The heat flux values between the discrete time points $q_j=q(Fo_j)$,
where $Fo_j=j\Delta Fo, j=0,1,2,3,\ldots,n$, are linearly
interpolated. The component $q_0$ is

\begin{eqnarray}\label{e:heatfluxq0}
q_0(Fo)=\left\{ \begin{array}{ll}
 0, & \textrm{\qquad  $Fo<0$}\\
 q_0+(q_1-q_0)\frac{Fo}{Fo_1}, & \textrm{\qquad  $Fo\leq Fo_1$}\\
 0, & \textrm{\qquad  $Fo>Fo_{1}$}
 \end{array} \right.
\end{eqnarray}

For $q_j, j=1,2,3,\ldots,n-1$ we have

\begin{eqnarray}\label{e:heatfluxes}
q_j(Fo)=\left\{ \begin{array}{ll}
 0, & \textrm{\qquad  $Fo<Fo_{j-1}$}\\
 q_{j-1}+(q_j-q_{j-1})\frac{Fo-Fo_{j-1}}{Fo_j - Fo_{j-1}}, & \textrm{\qquad $Fo_{j-1}\leq Fo\leq Fo_j$}\\
 q_j+(q_{j+1}-q_j)\frac{Fo-Fo_j}{Fo_{j+1} - Fo_j}, & \textrm{\qquad  $Fo_j\leq Fo\leq Fo_{j+1}$}\\
 0, & \textrm{\qquad  $Fo>Fo_{j+1}$}
 \end{array} \right.
\end{eqnarray}
\par
 In order to identify an abruptly changing heat flux on the medium
surface, the number of heat flux points $n$ should be as high as
possible, with $\Delta Fo$ small. On the other hand, due to the
diffusion nature of heat propagation, this requirement is in
contradiction with the identifiability of the heat flux components.
As $\Delta Fo \rightarrow 0$, the sensitivity coefficients $X_j
\rightarrow 0$, and, furthermore, they become linearly dependent.
The problem of simultaneous heat flux component identification from
temperature history is difficult in this case and very sensitive to
measurement errors. Usually, an optimal value (or range) of $\Delta
Fo$ can be found, for which enough heat flux components can be
estimated with a reasonable precision to identify an unknown
piece-wise heat flux function.
\par The heat flux
sensitivity coefficients for a surface temperature in a homogeneous
finite medium for $0<Fo<2.5$, $\Delta Fo=0.5$, and $Bi=0.1$ are
plotted in Fig. \ref{fig:XiDFo05}. The first coefficient $X_0$ is
only half of the amplitude of the coefficients with higher $j$, and
its shape also differs from the others. Fortunately, the precision
of the first heat flux component determination can be enhanced using
additional information about the initial conditions of the problem
at $Fo=0$. The last two coefficients are affected by the fact that
it takes about $2\Delta Fo$ for the sensitivity coefficient to reach
its maximum value. It is therefore better to measure the temperature
long enough, to be able to make an additional assumption about the
value of the last heat flux component.
\par The first six sensitivity coefficients for the medium with $0<Fo<2.5$,
$\Delta Fo=0.25$, and $Bi=0.1$ are plotted in Fig.
\ref{fig:XiDFo025}. The coefficients are closer to each other and
rising steeper than those in Fig. \ref{fig:XiDFo05}. Their
magnitudes are directly proportional to $\Delta Fo$. The
identifiability of the coefficients, especially for a noisy signal,
is much smaller than in the case of $\Delta Fo = 0.5$.
\par For stable heat flux component
calculations, $\Delta Fo \geq 0.4$ should be chosen in the total
time domain $Fo\geq 4$. In this case, the first ten heat flux
components can be calculated with good accuracy.
\par It has to be noted that the optimal value of $\Delta Fo$ depends
also on the heat pulse shape and its duration. Sharp and abrupt
changes of the heat flux can lead to excessive oscillations in the
calculated components, which is characteristic for all ill-posed
problems.
\par The calculated heat flux components accuracy does not
depend on heat losses. The described IHCP procedure can be applied
to the temperature signal without prior knowledge of the heat loss
coefficient. It is assumed, that the the medium thickness $L$ and
the thermal diffusivity $\alpha$ are known parameters.

\subsection{Examples}

\subsubsection{Example 1}

An example of the triangular heat pulse is given in Fig.
\ref{fig:triangle}. The medium was divided into 20 parts. A total of
525 temperature versus time points were generated using the EFD
algorithm with a heat pulse which started at $Fo=0$, with a peak at
$Fo=1.8$, and ended at $Fo = 3$. The Biot number was $Bi = 0.1$ and
the noise level was set to 0.2 of the units on the temperature
scale. The temperature history data and the results of the heat flux
component calculation for $\Delta Fo = 0.5$ and $\Delta Fo = 0.25$
are plotted in Fig. \ref{fig:triangle} a, b respectively.
\par The agreement between theoretical and calculated values of the heat
flux components is very good, especially for the case of $\Delta Fo
= 0.5$. The last two heat flux components from the end of the time
interval were intentionally omitted, due to the reasons mentioned
above.

\subsubsection{Example 2}
The second example data were generated on the same manner as in
Example 1, but for a rectangular heat pulse, which started at $Fo =
0$ and ended at $Fo=3$. The noise level was set to 0.5. Data and
results of the heat flux component calculation are plotted in Fig.
\ref{fig:rectangle} a, b. An abrupt change in the heat flux at
$Fo=3$ caused significant oscillations in the calculated heat flux
components, both for $\Delta Fo =0.5$ and $\Delta Fo = 0.25$. The
first step change of the heat flux at $Fo=0$ was effectively damped
by the fact that the initial condition $T=0, Fo=0$ can be used as
additional information in the temperature calculations.

\subsubsection{Example 3}
The temperature history for a trapezoidal heat pulse, which started
at $Fo=0$, duration to $Fo=2.7$, was generated in the third example.
The noise level was set to 1. The temperature and the results of the
heat flux component calculations are plotted in Fig.
\ref{fig:trapezoid}. Even for such a noisy signal, the results for
heat flux components for $\Delta Fo=0.5$ are in very good agreement
with the theoretically expected values (see Figure
\ref{fig:trapezoid}a). On the contrary, the results for heat flux
components for $\Delta Fo = 0.25$, plotted in Fig.
\ref{fig:trapezoid}b, are obviously too scattered for a realistic
reconstruction of the heat pulse.

\section{CONCLUSION}
A one-dimensional IHCP in a finite medium can be solved using a
simple iterative DHW algorithm. In this algorithm, the temperature
is calculated explicitly in one simple calculation that is repeated
for each time step as the heat wave marches through the medium with
a constant speed.
\par The proposed algorithm is quite stable and the heat flux
function can be reconstructed even in the case of relatively noisy
temperature signals. The algorithm can be used as a fast,
easy-to-understand and easy-to-implement alternative to existing
analytical and numerical methods to solve inverse heat conduction
problems in a finite, one-dimensional, homogeneous medium.

\newpage

\begin{thebibliography}{00}

\bibitem{Beck85}J.~V. Beck, B. Blackwell, and C.~R. StClair, Jr.,
{\em Inverse Heat Conduction, Ill-Posed Problems, }
(Wiley-Interscience, New York 1985).

\bibitem{Alifanov1994}O.~M. Alifanov, {\em Inverse Heat Transfer Problems }
(Springer - Verlag, New York, 1994).

\bibitem{Gembarovic04} J Gembarovic, M L\"{o}ffler, and J Gembarovic Jr.,
{\em Appl. Math. Model. }{\bf 28}:173 (2004).

\bibitem{Loffler05} M L\"{o}ffler, J Gembarovic, and J
Gembarovic, Jr., "A New Way of Modelling Transport Processes", {\em
in Thermal Conductivity 26 Thermal Expansion 14,  R B Dinwiddie and
R Mannello, Eds} (DEStech Publications, Lancaster, Pennsylvania,
2005) pp. 123-133.

\bibitem{ODRPACK} P. T. Boggs, R. H. Bird, J. E. Rogers, and R. B.
Schnabel, {\em ODRPACK Ver. 2.01 User's Reference Guide} (U.S. Dept.
of Commerce, Gaithersburg, Maryland, 1992).

\bibitem{Beck85_XI}J.~V. Beck, B. Blackwell, and C.~R. StClair, Jr.,
{\em Inverse Heat Conduction, Ill-Posed Problems }
(Wiley-Interscience, New York, 1985), p. 30.

\end{thebibliography}
\newpage

\begin{figure}
\begin{center}
\includegraphics [scale=1.0]{sens05.eps}
\caption {\label{fig:XiDFo05} Sensitivity coefficients $X_i$ for
$\Delta Fo = 0.5$.}
\end{center}
\end{figure}


\begin{figure}
\begin{center}
\includegraphics [scale=1.0]{sens025.eps}
\caption {\label{fig:XiDFo025} Sensitivity coefficients $X_i$ for
$\Delta Fo = 0.25$.}
\end{center}
\end{figure}

\begin{figure}
%\begin{center}
\includegraphics [scale=1.6]{triangle.eps}
\caption {\label{fig:triangle} Temperature history and calculated
heat flux components for a triangular shape heat pulse: (a) $\Delta
Fo = 0.5$, and (b) $\Delta Fo = 0.25$. }
%\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics [scale=1.6] {rectangle.eps}
\caption {\label{fig:rectangle} Temperature history and calculated
heat flux components for a rectangular shape heat pulse: (a) $\Delta
Fo = 0.5$, and (b) $\Delta Fo = 0.25$. }
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics [scale=1.6] {Trapezoid.eps}
\caption {\label{fig:trapezoid} Temperature history and calculated
heat flux components for a trapezoid shape heat pulse: (a) $\Delta
Fo = 0.5$, and (b) $\Delta Fo = 0.25$. }
\end{center}
\end{figure}

\end{document}

% end of file inv.tex
