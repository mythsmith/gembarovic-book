% Template article for preprint document class `elsart'
% SP 2000/09/06

\documentclass[reviewcopy]{elsart}

%\documentclass{elsart}
\usepackage{graphicx}% Include figure files
%\linespread{1.6}
\usepackage{epstopdf}
\epstopdfsetup{update}

% if you use PostScript figures in your article
% use the graphics package for simple commands
% \usepackage{graphics}
% or use the graphicx package for more complicated commands
% \usepackage{graphicx}
% or use the epsfig package if you prefer to use the old commands
% \usepackage{epsfig}

% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}

\begin{document}

\begin{frontmatter}

% Title, authors and addresses

% use the thanksref command within \title, \author or \address for footnotes;
% use the corauthref command within \author for corresponding author footnotes;
% use the ead command for the email address,
% and the form \ead[url] for the home page:
\title{Use of Damped Heat Wave Algorithm in the Flash Method of Thermal Diffusivity Measurement\thanksref{X}}
 \author[Lafayette]{J. Gembarovic}
 \ead{gembar@tprl.com}
 \author[Lafayette]{ J. Gembarovic, Jr.}
 \address[Lafayette]{TPRL, Inc., 3080 Kent Avenue, West Lafayette, IN 47906, U.S.A.}
 \thanks[X]{Paper for IJHMT}

\end{frontmatter}
%\newpage

% main text
\textbf{ABSTRACT}
\par
A novel, simple iterative algorithm based on Damped Heat Wave
(DHW) is used to calculate temperature distribution in sample in
the flash method of thermal diffusivity measurement. Parameter
estimation technique is then used to calculate unknown thermal
diffusivity of the sample by fitting experimental temperature
versus time data with the calculated temperature distribution.
Examples of using the algorithm in case of heat losses from the
sample, finite pulse time duration, non-uniform heating of the
sample, in one and two-dimensional cases are given and the results
are compared with data reduction methods based on the exact
analytical solutions.
\par
KEY WORDS: Thermal diffusivity; Flash method; Numerical algorithm;
Temperature distribution
%\newpage
% main text
\section{Introduction}

 In the laser flash method \cite{Parker61} one
surface (at $x=0$) of a small disc shaped sample of thickness $L$
is irradiated by a laser pulse and resulting temperature rise at
opposite surface ($x=L$) is used to calculate the thermal
diffusivity $\alpha$ of the sample material.
\par
Existing data reduction methods for calculation of thermal
diffusivity from the temperature rise of the sample are based on
analytical solution of one
%assumption that the thermophysical parameters - heat capacity $c$
%and thermal conductivity $\lambda$ (and also thermal diffusivity
%$\alpha \equiv\lambda/c$) are constants independent of temperature
%$T$ within the temperature range of the a flash experiment. One
(or two) dimensional linear heat conduction equation
\begin{equation}\label{e:HCE}
\frac{\partial T}{\partial t}=\alpha \Delta T,
\end{equation}
where $\alpha $ is the thermal diffusivity, $\Delta$ is Laplace
operator and $T=T(\vec{r},t)$ is the temperature in space-time
point $(\vec{r},t)$. Equation (\ref{e:HCE}) is solved with initial
and boundary conditions relevant to the experiment. Thermal
diffusivity is calculated by fitting the experimental temperature
rise to the appropriate analytical solution.
\par
Analytical solutions of Equation (\ref{e:HCE}) have been found
only for a limited set of initial and boundary conditions and its
form is very complicated especially in case when heat losses are
combined with finite duration of the pulse, or with nonuniform
heating of the front surface of the sample. Standard software for
calculations of temperature distribution, based on these
analytical solutions is therefore very complicated and the results
are often inconclusive due to a slow convergence of the infinite
series involved in the analytical formulas. It is hard to model
real experimental situations especially for extremal cases when
simplified models are useless.
\par
Recently, we have developed a novel, simple iterative algorithm
based on damped heat wave for approximate calculation of
temperature distribution in finite medium \cite{Gembarovic2004}.
This paper will show that this algorithm can be used to calculate
temperature distribution in the flash method of measuring thermal
diffusivity.

\section{Analytical Solution}

General formulae for axially symmetrical pulse heating of a right
circular cylindrical solid directly applicable to flash method
have been derived by Watt \cite{Watt66}. Temperature distribution
in point given by axial and radial coordinate $x$, $r$, at time
$t$ for an extended heat pulse is given
\begin{eqnarray}\label{e:General}
    w(x,r,t)=\frac{Q}{\rho c}\sum_{n=1}^{\infty}{Y_n(x)\int_{0}^{L}f(x')Y_n(x')}dx'
    {}\nonumber\\
     {}\times \sum_{i=1}^{\infty}{\frac{2}{R^2}}\frac{{Z_i}^2 J_0(Z_i
     r/R)}{({Z_i}^2+{L_r}^2){J_0}^2(Z_i)}
     \int_{0}^{R}r'g(r')J_0\Biggl( {Z_i \frac{r'}{R} \Biggr)}dr'
     {}\nonumber\\
     {}\times \int_{t_1}^{t_2}{\exp\Biggl[ - \alpha \Biggl(\frac{{Z_i}^2}{R^2}+
     \frac{{\beta_n}^2}{L^2}\Biggr)(t-t')\Biggr]\psi (t')dt'},
\end{eqnarray}
where $Q$ is amount of heat in the pulse, $\rho$ is the density,
$c$ is the specific heat, $\alpha$ is the thermal diffusivity, $L$
is the sample thickness and $R$ is the sample radius.
Dimensionless functions $f(x)$, $g(r)$ and $\psi(t)$ are
distribution functions for the heat pulse in axial, radial
direction and in time, respectively, from $t_1$ to $t_2$
($t>t_2$). Function $Y_n(x)$ is given
\begin{equation}\label{e:Y(x)}
Y_n(x)=\frac{2[{\beta_n}^2+{B_2}^2]^{1/2}[\beta_n \cos
\beta_n(x/L)+B_1 \sin \beta_n(x/L)]}{L^{1/2}[({\beta_n}^2+{B_1}^2)
({\beta_n}^2+{B_2}^2+B_2)+B_1({\beta_n}^2+{B_2}^2)]^{1/2}},
\end{equation}
where $B_1$ and $B_2$ are Biot numbers for $x=0$ and $x=L$
surfaces, the $\beta_n$ ($n=1,2,3\ldots$) are positive roots of
\begin{equation}\label{e:betas}
    \tan \beta=\frac{\beta (B_1+B_2)}{\beta^2 - B_1B_2}.
\end{equation}
Numbers $Z_i$ are positive roots of
\begin{equation}\label{e:Zs}
    Z_i J_1(Z_i)-B_r J_0(Z_i)=0,
\end{equation}
where $B_r$ is Biot number for the surface $r=R$. Functions
$J_0(z)$ and $J_1(z)$ are Bessel function of the first kind, zero
and first order, respectively.
\par
For a uniform plane pulse of square wave shape ($\psi
(t')=H(t')-H(t'-\tau)$, where $H(t)$ is Heaviside unit step
function,) at the front surface of the sample ($x'=0$, $g(r')=1$,
$f(x')=\delta (x')$), the dimensionless temperature rise at the
rear surface ($x=L$) for $t>\tau$ is, from Equation
(\ref{e:General}) with radiation boundary conditions,
\begin{eqnarray}\label{e:SquarePulse}
 V(L,r,t)=\frac{w(L,r,t)}{Q\tau/\rho c L}=\sum_{n=1}^{\infty}{L
 Y_n(L)Y_n(0)}\sum_{i=1}^{\infty}{\frac{{Z_i}^2 J_0(Z_i
     r/R)}{({Z_i}^2+{B_r}^2){J_0}^2(Z_i)}}
     {}\nonumber\\
     {}\times \exp\Biggl[ - \alpha t\Biggl(\frac{{Z_i}^2}{R^2}+
     \frac{{\beta_n}^2}{L^2}\Biggr)\Biggr]\frac{\exp[\alpha
     \tau({Z_i}^2/R^2+{\beta_n}^2/L^2)]-1}{\alpha
     \tau({Z_i}^2/R^2+{\beta_n}^2/L^2)}.
\end{eqnarray}
For the case of zero heat losses ($B_1=B_2=B_r=0$), there will be
no radial heat flow and the temperature rise at the rear surface
after square shape heat pulse will be
\begin{eqnarray}\label{e:SquarePulseNoHL}
V(L,t)=\frac{w(L,t)}{Q\tau/\rho c L}=
   1+\sum_{i=1}^{\infty}{(-1)^n \exp{\Biggl({-\frac{n^2 \pi^2
    \alpha t}{L^2}}\Biggr)}}
     {}\nonumber\\
     {}\times \Biggl[ \exp{\Biggl({\frac{n^2 \pi^2
    \alpha t}{L^2}}\Biggr)-1}\Biggr]\frac{L^2}{n^2 \pi^2 \tau}
\end{eqnarray}
\par
Temperature rise in case when the pulse is instantaneous, evenly
distributed over the front surface, absorbed in an infinite thin
surface layer and there are no radial heat losses is given by

\begin{eqnarray}\label{e:HLFlashEq}
V(L,t) =\sum_{n=1}^\infty \frac{2{\beta_n}^2({\beta_n}^2+{B_2}^2)}
{({\beta_n}^2+{B_1}^2)({\beta_n}^2+{B_2}^2)+(B_1+B_2)({\beta_n}^2+B_1B_2)}
{}\nonumber\\
     {}\times \left[\cos\beta_n + \frac{B_1}{\beta_n}\sin
\beta_n\right]\exp\left[-\frac{{\beta_n}^2\alpha t}{ L^2}\right].
\end{eqnarray}
\par
If the surfaces are adiabatically insulated then the temperature
rise (so called \textit{ideal} rise) is
\begin{equation}\label{e:IdealVClassic}
    V_i(L,t)=1+2\sum_{n=1}^{\infty}{(-1)^n \exp\Biggl[-
    \frac{n^2 \pi^2 \alpha t}{L^2}\Biggr]},
\end{equation}
or in the form suitable for small times,
\begin{equation}\label{e:IdealV}
    V_i(L,t)=\frac{L}{\sqrt{\pi \alpha
    t}}\sum_{n=0}^{\infty}{\exp\Biggl[-
    \frac{(2n+1)^2 L^2}{4\alpha t}\Biggr]}.
\end{equation}
\par

\section{Algorithm Description}

\subsection{Heat Transfer Coefficient}
Similarly to J. Fourier explanation given in \cite{Fourier-2} the
heat transfer between two discrete bodies can be described as
follows:  Two equal bodies mass $m$ of the same material with
perfect conductivity at different temperatures $a$ and $b$.
Transmission of heat between the bodies is imagined by means of an
ideal shuttle mechanism consisting of infinitesimally small
section $\omega$ which moves to and fro in a fixed time $\Delta t$
between the two masses \cite{Herivel}. From the point of view of
heat transfer this situation is identical with our DHW algorithm
for $N=2$.
\par
 If we place these the two bodies in
contact, the temperature in each would suddenly become equal to
the mean temperature $\frac{1}{2}(a+b)$. Two masses are separated
by a very small interval, that a thin layer of the first is
detached so as to be joined to the second, and that it returns to
the first immediately after the contact. Continuing thus to be
transferred alternately, and at equal small time intervals, the
interchanged layer causes the heat of the hotter body to pass
gradually into that which is less heated. There are no heat losses
from the bodies. The quantity of heat contained in a thin layer is
suddenly added to that of the body with which it is in contact and
the common temperature results which is equal to the quotient of
the sum of the quantities of heat divided by the sum of the
masses. Let $\omega$ be the mass of small layer which is separated
from the hotter body, whose temperature is $a$; let $\theta$ and
$\vartheta$ be the variable temperatures which correspond to the
time $t$, and whose initial values are $a$ and $b$. When the layer
$\omega$ is separated from the mass $m$ which becomes $m-\omega$,
it has like this mass the temperature $\theta$, and as soon as it
touches the second body with temperature $\vartheta$, it assumes
at the same time with that body a temperature equal to
$\frac{\vartheta m+\theta \omega}{m+\omega}$. The layer $\omega$,
retaining the last temperature, returns to the first body, whose
mass is $m-\omega$ and temperature $\theta$. The temperature after
the second contact is
\begin{equation}\label{e:T after 2nd contact}
    \frac{\theta(m-\omega)+\biggl(\frac{m\vartheta+\theta\omega}
    {m+\omega}\biggr)\omega}{m}\equiv\frac{\theta m+\vartheta
    \omega}{m+\omega}.
\end{equation}
The variable temperatures $\theta$ and $\vartheta$ become, after
the interval $\Delta t$, $\theta -(\theta -
\vartheta)\frac{\omega}{m+\omega}$, and $\vartheta + (\theta -
\vartheta)\frac{\omega}{m+\omega}$. For the differences we have
\begin{equation}\label{e:T differences}
\Delta\theta =-(\theta -
\vartheta)\frac{\omega}{m+\omega}\qquad\textrm{and}\qquad\Delta\vartheta
=(\theta - \vartheta)\frac{\omega}{m+\omega}.
\end{equation}
Quantity of heat received in one instant by second mass is equal
to the quantity of heat lost by the first mass. We see, that the
quantity of heat is, all other things being equal, proportional to
the actual difference of temperature of the two bodies.
\par The amount of heat transferred by an infinitely small $\omega$
between the two bodies in $\Delta t$ is $m\Delta \theta=-(\theta -
\vartheta)\omega$.  If we compare this amount to Fourier law
\begin{equation}\label{e:Comparison}
-(\theta - \vartheta)\omega=-\frac{\lambda}{\rho c}\frac{(\theta -
\vartheta)}{\Delta x}\Delta t
\end{equation}
we see, that the quantity $\omega$ is
\begin{equation}\label{e:Omega}
 \omega=\frac{\lambda\Delta
t}{\rho c \Delta x}=\frac{\alpha \Delta t}{\Delta x}.
\end{equation}
\par After replacing  the term $m$ with $\Delta x$, and $\omega$ with
$\alpha \Delta t/\Delta x$,  the temperature difference
$\Delta \theta$ in Equation (\ref{e:T differences}) is
\begin{equation}\label{e:T differenceTheta}
\Delta\theta =-(\theta - \vartheta)\frac{1}{1+Fo^{-1}},
\end{equation}
where $Fo=\alpha \Delta t/\Delta x^2$ is Fourier number for one
layer. The time step in DHW algorithm is exactly 2 times longer
than $\Delta t$ from the Fourier model. For the \textit{inner heat
transfer coefficient} $\xi$ in DHW algorithm  we have
\begin{equation}\label{e:IHTCoefficient}
\xi =\frac{1}{1+2Fo^{-1}}.
\end{equation}
\par
Fourier also considered \cite{Fourier-N} the general case of $N$
separate equal masses arranged in a strait line and initially at
arbitrary temperatures $a$, $b$, $c$, $\ldots$ in which
transmission of heat takes place by the same shuttle mechanism
between successive bodies as in the case of two bodies only. This
mechanism differs from our DHW algorithm, in which a 'wave of
redistributions' is marching through the medium, consecutively
changing the temperatures of two neighbor slices. These two
algorithms converge to an exact analytical solution for $\xi
\rightarrow 0$.
\par
In DHW algorithm for calculation of temperature distribution in one dimension the
medium is divided into $N$ equal slabs of thickness $\Delta
l=L/N$. These slabs are replaced by a perfect conductor of the
same heat capacity separated by thermal resistance $\Delta
l/\lambda$, so the temperature within a slab at any given time is
constant. Heat propagates from one slab to another due to
existence of a temperature difference between the slabs. The wave
takes certain portion (given by the \textit{inner transfer
coefficient} $0<\xi<1$) of excessive heat energy from one slab and
moves that amount to the next one (redistribution), thus lowering
the temperature difference between the two neighbor slabs. The
wave starts from the left boundary slabs and marches in space from
one pair of slabs to another, redistributing the thermal energy
between the slabs. When it reaches the boundary of the medium, the
wave bounces back and moves in the opposite direction in a
perpetual manner.
\par Slab temperatures are $T_{i,m}\equiv T(x_i,t_m)$,
where $x_i$, ($i=0,1,2,\ldots,N-1$) is a spatial point (middle of
the $i$th slab), and $t_m=m\Delta t $, ($ m=0,1,2,\ldots$) is
discrete time point. Temperature distribution at time $t_{m+1}$
when the heat wave is marching from left to right is given by:

\begin{eqnarray}\label{e:leftwaveHHCE}
T_{n,m+1} = T_{n,m} - \xi( T_{n,m}-T_{n+1,m})\delta_{n,m},{}
                                \nonumber\\
T_{n+1,m+1} = T_{n+1,m} + \xi( T_{n,m}-T_{n+1,m})\delta_{n,m},{}
                                \nonumber\\
{} \verb"for " n=0, 1, 2, \ldots, N-1,
\end{eqnarray}
where $\delta_{n,m}$ is Kronecker delta. Temperature of each slab
changes twice as the wave passes the slab.
\par
Similarly, the temperature distribution at time $t_{m+1}$ when the
heat wave is marching in opposite direction from right to left:

\begin{eqnarray}\label{e:rightwaveHHCE}
T_{2N-n,m+1} = T_{2N-n,m} - \xi
(T_{2N-n,m}-T_{2N-n-1,m})\delta_{n,m},{}
                                \nonumber\\
T_{2N-n-1,m+1} = T_{2N-n-1,m} + \xi
(T_{2N-n,m}-T_{2N-n-1,m})\delta_{n,m},{}
                                \nonumber\\
{} \verb"for " n=N,N+1, N+2, \ldots, 2N-1.
\end{eqnarray}
\par
An example of the heat wave in one-dimensional finite medium with
$\xi$ = 0.95 is shown in Figure \ref{fig:DampedHW}. The medium is
divided into $N=6$ slabs. The wave height is reduced from 10 to
3.61 units after 28 time steps. The rest of the media is at about
1.2 units. The wave height decays exponentially with time,
similarly as in the solution given by Equation
(\ref{e:AnalSolHHCE}). Sum of all heights in the medium is always
equal to 10, in accordance with the total energy conservation law.
\par
When the heat wave imitates diffusion (parabolic heat transfer)
then the wave is strongly damped ($\xi<0.5$) and its actual
position is not important. The time step $\Delta t$ is therefore
chosen to be equal to one loop time interval. On the contrary, the
wave position is essential in the case of hyperbolic heat
transfer, when the heat wave is much less damped ($\xi\rightarrow
1$) and moves across the medium with a constant speed. The time
step in non-Fourier heat transfer has to be equal to the heat
pulse duration ($\Delta t=t_1$). Time origin is also set to $t_1$
when heat pulse already entered the medium. Slab thickness $\Delta
l $ is then given by
\begin{equation}\label{Deltal}
 \Delta l = vt_1=\sqrt{\frac{\alpha}{\tau}}t_1.
\end{equation}

Fraction $L/\Delta l$ defines the number of slabs $N$ which should
be an integer number, equal or bigger than 5. In other words, the
heat pulse duration $t_1$ should be at least 5 times less than the
time $t^{*}=L \sqrt{\tau/\alpha}$, needed for heat wave to reach
the opposite end of the medium. These conditions limit the use of
our algorithm, especially for long pulses, or very thin layers.
\par
The inner transfer coefficient $\xi$ for hyperbolic heat transfer
is defined as
\begin{eqnarray}\label{e:TCoeffHHCE}
\xi = \biggl(1 + \frac {\Delta t}{2\tau}\biggr)^{-1}.
\end{eqnarray}
\par When the wave imitates non-Fourier heat transfer, the inner transfer
coefficient is $1>\xi\gtrsim 0.9$. It follows from Equation
(\ref{e:TCoeffHHCE}) that the upper limit for the time step
$\Delta t$ is given by:
\begin{eqnarray}\label{e:DeltatCond}
\Delta t \lesssim \frac{2}{9}\tau.
\end{eqnarray}

This introduces a limit to the pulse duration in comparison with
the relaxation time that can be modelled by our algorithm.
\par
Figure \ref{fig:exampleHHCE}(a)(b)(c) show the temperature
distributions in a finite medium for the case with $L = 1$ cm,
$\alpha = 0.025$ cm$^2$/s, $\tau = 10$ s, $t_1=2$ s. The inner
transfer coefficient is $\xi = 0.90909091$. Initial temperature
(at $t=2$ s) is 10 units for the first slab (from left) and the
rest of the medium is at zero temperature. There are no heat
losses at the boundaries. The temperature distribution calculated
using our algorithm is compared with the exact analytical solution
given by Equation (\ref{e:AnalSolHHCE}). In non-Fourier heat
transfer it is important to know the temperature distribution in
early stages after the heat pulse, so the profiles are calculated
for: (a) $t=4$ s; (b) $t=18$ s; and (c) $t=32$ s. It can be seen,
that the calculated temperatures are in a good agreement with the
analytical ones.

\par
If there are heat losses from the medium surfaces a part of the
excessive thermal energy leaves the medium, when the wave reaches
the boundary slabs. Temperatures of the boundary slabs are
furthermore changed due to the heat losses:
\begin{equation}\label{e:Boundhl0}
T_{N-1,N} = T_{N-1,N-1}-\zeta (T_{N-1,N-1}-T_A),
\end{equation}
and
\begin{equation}\label{e:BoundhlL}
T_{0,2N+1} = T_{0,2N}-\zeta (T_{0,2N}-T_A),
\end{equation}
where $\zeta$ is the \textit{surface transfer coefficient} and
$T_A$ is the ambient temperature.
\par
Various boundary conditions can be modelled by adjusting the
surface transfer coefficient. In the case $\zeta = 1$  the
constant temperature (equal to the ambient temperature) boundary
condition is being simulated. Adiabatically insulated surface is
given by $\zeta = 0$. Nonlinear conditions can be also modelled,
e.g. radiation from the surface, in which the rate of heat energy
leaving the surface is proportional to
\[
\sigma\epsilon(T_i^4-T_A^4),
\]
where $\sigma$ is the Stefan-Boltzmann constant and $\epsilon$ is
the emissivity of the surface. Temperature difference of the
fourth power of the temperatures will be used in Eqs.
(\ref{e:Boundhl0})-(\ref{e:BoundhlL}).

\section{Conclusion}
Temperature distribution in a finite medium in case of non-Fourier
heat conduction can be calculated using a simple iterative
algorithm based on damped heat wave. In this algorithm the
temperature is calculated explicitly in one simple calculation
that is repeated for each time step as the heat wave marches
through the medium with a constant speed.
\par Proposed algorithm can be used by engineers and
designers as a fast, easy to understand and easy to implement
alternative to existing numerical and analytical methods. It could
simplify hardware and software needs for temperature and heat flux
calculations in real applications and open new possibilities for
improving measurement and non-destructive testing procedures used
in this field.
\newpage

\begin{thebibliography}{00}

\bibitem{Parker61}W. J. Parker, R. H. Jenkins, C. P. Butler, and G. L. Abbott, {\em J. Appl. Phys. }{\bf 32: }1679 (1961).

\bibitem{Gembarovic2004}  J. Gembarovic, M. L\"{o}ffler, and J. Gembarovic Jr.,
{\em Applied Mathematical Modelling}, {\bf 28/2}:173-182 (2004).

\bibitem{Watt66}D. A. Watt, {\em Brit. J. Appl. Phys. }{\bf
17: }231 (1966).

\bibitem{Fourier-2} J. Fourier, {\em Analytical Theory of Heat,
translated with notes of A. Freeman, } (Dover Publications, New
York 1955) p. 225.

\bibitem{Fourier-N} J. Fourier, {\em Analytical Theory of Heat,
translated with notes of A. Freeman, } (Dover Publications, New
York 1955) p. 228.

\bibitem{Herivel} J. Herivel, {\em Joseph Fourier - The Man And The Physicist,}
 (Claredon Press, Oxford 1975) p. 192.

\bibitem{CJ15}H.~S. Carslaw and J.~C. Jaeger, {\em Conduction of Heat in Solids, 2nd ed., }
(Oxford University Press, London 1959) p. 9.

\bibitem{AckerGuyer68} C.~C. Ackerman and R.~A. Guyer, {\em Annals of Physics }{\bf 50}:128 (1968).

\bibitem{Mitra95} K. Mitra, et. al., {\em J. of Heat Transfer, Trans. ASME } {\bf 117}:568 (1995).

\bibitem{Yun-Sheng95} Yun-Sheng Xu and Zheng-Yuan Guo, {\em Int. J. Heat Mass Trans. }{\bf  38}:2919 (1995).

\bibitem{Maxwell1867}
J.~C. Maxwell, {\em Phil. Tran. R. Soc. } {\bf 157}:49 (1876).

\bibitem{Vernotte58} J. Vernotte, {\em C. R. } {\bf 246}:3154 (1958).

\bibitem{Cattaneo58} C. Cattaneo, {\em C. R. } {\bf 247}:431 (1958).

\bibitem{TZOUbook}D. Y. Tzou, {\em Macro- to Microscale Heat Transfer, The Lagging
Behavior} (Taylor and Francis, Washington DC 1997).

\bibitem{Loffler} M. L\"{o}ffler, J. Gembarovic and J. Gembarovic Jr.,
{\em in Proceedings of 26 ITCC}, Cambridge, 2001 (in print).

\bibitem{Gembarovic87}J. Gembarovic and V. Majernik, {\em Int. J. Heat
Mass Trans. }{\bf 31}:1073 (1987).



\newpage
\textbf{Figure Captions}
\par
\textbf{Fig. 1} Temperature distributions in a finite medium
calculated using the analytical solution given by Equation
(\ref{e:AnalSolHHCE}).
\par
\textbf{Fig. 2} Damped heat wave in a finite medium. Inner
transfer coefficient $\xi = 0.95$.
\par
\textbf{Fig. 3} Temperature distributions in a finite medium.
Temperatures calculated using our algorithm (solid lines) are
compared with exact analytical solutions (dotted lines) given by
Eq. (\ref{e:AnalSolHHCE}). The profiles are: (a) at $t=4$ s; (b)
at $t=18$ s; and (c) at $t=32$ s.
\newpage
\end{thebibliography}

\begin{figure}
\centering
\includegraphics[scale=1.0]{analyt}
\caption{\label{fig:AnalytHHCE}}{}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=1.9]{fig2m}
\caption{\label{fig:DampedHW}}{}
\end{figure}

\begin{figure}
\centering
\includegraphics[scale=1.0]{example}
\caption{\label{fig:exampleHHCE}} {}
\end{figure}


\end{document}
