#require(rootSolve)
####################################################
## NonFourierFiguresRear.R
## reads RearV.prn and creates plots
##    6/6/2016 
## data were created using 
##    MathCAD file HHCEandHCE_RearSide.xmcd
####################################################
library(ggplot2)
setwd("~/repos/latexy/myFlashMethodTheory/Literature/NonFourier/DHWalgorithm/NonFourierDHW")
D <- read.csv("RearV.prn",header = F, sep="")
imax <- length(D[,1])

# curves calculated using MathCAD file HHCEandHCE_RearSide.xmcd
# Parameters
Par <- D[1:13,1]
alpha <- Par[1] # thermal diffusivity
L <- Par[2]     # thickness
t1 <- Par[3]    #  heat pulse duration
tau <- Par[4]   #  relaxation time
iVe <- Par[5:10] # fourier numbers (dimensionless times)
Ve <- sqrt(alpha*tau)/L # Vernotte number
1/Ve
L1 <- t1*sqrt(alpha/tau) # the peak thickness
v <- sqrt(alpha/tau) # speed of propagation
omega1 <- alpha*t1/L^2 # dimensionless pulse duration time

# dimensionless position X[1:1000] 
t <- D[1:imax,2]
# Classical Heat Conduction Equation (Fourier) solutions
HCE <- D[1:imax,3]

# Hyperbolic Heat Conduction Equation Solutions
HHCE1 <- D[1:imax,4]
HHCE2 <- D[1:imax,5]
HHCE3 <- D[1:imax,6]
HHCE4 <- D[1:imax,7]
HHCE5 <- D[1:imax,8]
HHCE6 <- D[1:imax,9]

## Figure NonFourierRear.eps #############
df <- data.frame(t,HCE, HHCE1,HHCE2,HHCE3,HHCE4,HHCE5,HHCE6)

ggplot(df,aes(x=t)) +xlab(expression(omega)) +
  ylab("V") +
  geom_line(aes(y=HCE,colour = ">25", linetype="HCE")) +
  geom_line(aes(y=HHCE1,colour = "15.0",linetype="HHCE"))+
  geom_line(aes(y=HHCE2,colour = "6.0",linetype="HHCE"))+
  geom_line(aes(y=HHCE3,colour = "4.0",linetype="HHCE"))+
  geom_line(aes(y=HHCE4,colour = "3.0",linetype="HHCE"))+
  geom_line(aes(y=HHCE5,colour = "1.5",linetype="HHCE"))+
  geom_line(aes(y=HHCE6,colour = "0.8",linetype="HHCE"))+
  scale_colour_manual(expression(1/Ve), 
                      breaks = c(">25", "15.0", "6.0", "4.0", "3.0","1.5","0.8"),
                      values = c("firebrick3", "darkseagreen", "lightpink", "lightgoldenrod3",
                                 "orange3", "lightsteelblue3","goldenrod2"))+
  scale_linetype_manual("Model", breaks=c("HCE", "HHCE"), values = c(1,6)) +
  annotate("text",label='omega[1] == 10^{-5}', parse=T, x=2.5, y=1.1)# +
  #annotate("text",label="Pulse Duration = 1.0e-5", parse=F, x=3, y=0.8+0.2)# +
  #annotate("text",label="Pulse Width = 0.05", parse=F, x=0.6, y=11.4+2) 
