% Template article for preprint document class `elsart'
% SP 2000/09/06

\documentclass{elsart}
\usepackage{setspace}
\usepackage{graphicx}% Include figure files
\usepackage{rotating}


\newcommand{\beq}{\begin{equation}}
\newcommand{\eeq}{\end{equation}}
\newcommand{\bea}{\begin{eqnarray}}
\newcommand{\eea}{\end{eqnarray}}

% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}


\begin{document}

\begin{frontmatter}

% Title, authors and addresses

\title{A Comparison of DHW Algorithm for Temperature Distribution Calculation
with Fourier's Algorithm for Transmission of Heat Between Discrete
Bodies}

 \author[]{J. Gembarovic\thanksref{1} \thanksref{2}}
 \author[]{ J. Gembarovic, Jr.\thanksref{3}}

 \thanks[1]{TPRL, Inc., 3080 Kent Avenue, West Lafayette, Indiana 47906, U.S.A.}
 \thanks[2]{To whom correspondence should be addressed. Email: gembar@tprl.com}
 \thanks[3]{College of Technology, Purdue University, West Lafayette, IN 47906, U.S.A.}

\end{frontmatter}

\newpage
\textbf{ABSTRACT}
\par
% Text of abstract
 \setstretch{1.5}
In this paper, our damped heat wave (DHW) algorithm for the
calculation of temperature distribution in a homogeneous finite
medium is compared with earlier Fourier's algorithm for transmission
of heat between discrete bodies.
\par
\textbf{KEY WORDS}:  heat conduction; numerical algorithm;
temperature distribution

\newpage

% main text
\section{INTRODUCTION}
%\onehalfspacing
 \setstretch{1.5}
 Joseph Fourier's work \cite{Fourier-original-book}
in formulating the heat conduction in terms of a partial
differential equation and developing the methods for solving the
equation is well known and recognized as one of the biggest
scientific achievements of mankind. Less known is his first
attempt to solve the problem of heat transmission between discrete
bodies.
\par Fourier started work on heat conduction sometime between 1802 and
1804 \cite{Herivel-beginning}. Inspired by the Laplacian philosophy of
action at a distance, he followed an eighteenth-century technique of
developing a discrete model of the continuous phenomenon and
initially formulated the heat conduction as an n-body problem. He
studied and found the solutions only to the two examples - straight
line and circular arrangement of $N$ discrete bodies - and then he
stopped. Although he never even mentioned that there was a
difficulty, he probably reached the point from which only the
investigation of special cases was possible
\cite{Grattan-Guinness-stop}. Fourier abandoned the n-body approach
around 1804, and probably inspired by Biot's work \cite{Biot1804}, he
started to work on a theory of heat conduction in continuous bodies.
\par
Although his first attempt to describe heat transfer using discrete
bodies is regarded as a dead end or a blind alley, Fourier never
failed to describe it in detail in his 1805 Draft Paper, through his
1807 Essay \cite{Fourier1807}, to the Prize Paper of 1811, and the
book {\em Th\'eorie Analytique de la Chaleur} \cite{Fourier1822}.
As a result of Fourier's lively historical sense, the "Communication of
Heat Between Discrete Bodies" is the second largest section of
Fourier's book \cite{Fourier-ChIVsect2} as a monument to his
earliest research in the heat conduction problem
\cite{Herivel-monument}.
\par
Recently, we have described a very simple algorithm for calculation of
the temperature distribution in finite one dimensional bodies
\cite{Gembarovic04} \cite{Loffler05}. We named it the damped heat wave
(DHW) algorithm, and it is similar (but not the same) to the above
mentioned Fourier's algorithm for transmission of heat between
discrete bodies. In Section 2 of this article, we will describe both
the Fourier's and the DHW algorithms. In case of the earlier, we
tried to follow an original description found in Fourier's book
\cite{Fourier-ChIVsect2}, adding only our remarks and formulae for a
length representation of the temperatures. In Section 3, the two
algorithms will be compared using a simple (but practical) boundary
value problem.

\section{DESCRIPTION OF ALGORITHMS}


\subsection{Fourier's Algorithm}

 \par
Fourier first considered  \cite{Fourier-2} two rectangular bodies of
equal mass $m$, cross-section area $A$, and thickness $\Delta l$ of the
same material with the same specific heat $c$, density $\rho$, and
perfect thermal conductivity at different temperatures $a$ and $b$. He
imagined the transmission of heat between the bodies by means of an
ideal shuttle mechanism consisting of an infinitesimally small
section of thickness $\delta$ and mass $\omega$ which moves to and
fro in a fixed time $\Delta t$ between the two masses.

\par
If these two bodies are placed in contact, the temperature in each
would suddenly become equal to the mean temperature
$\frac{1}{2}(a+b)$. Two masses (see Fig. \ref{fig:2Bodies}) are
separated by a very small interval. A thin layer $\omega$ of the
first is detached so as to be joined to the second, and then it
returns to the first immediately after the contact. Continuing thus
to be transferred alternately, and at equal small time intervals,
the interchanged layer causes the heat of the hotter body to pass
gradually into that which is less heated. There are no heat losses
from the bodies to ambient. The quantity of heat contained in the
thin layer is suddenly added to that of the body with which it is in
contact, and a common temperature results which is equal to the
quotient of the sum of the quantities of heat divided by the sum of
the masses multiplied by the specific heat. Let $\omega$ be the mass
of the small layer which is separated from the hotter body, whose
temperature is $a$; let $\theta$ and $\vartheta$ be the variable
temperatures which correspond to the time $t$, and whose initial
values are $a$ and $b$. When the layer $\omega$ is separated from
the mass $m$ which becomes $m-\omega$, it has the
temperature $\theta$, and as soon as it touches the second body with
the temperature $\vartheta$, it assumes at the same time with that
body a temperature equal to
 \beq \label{e:T after 1st contact}
\frac{\vartheta mc+\theta \omega c}{mc+\omega c}=\frac{\vartheta
m+\theta \omega}{m+\omega}.
 \eeq
%
The layer $\omega$, retaining the last temperature, returns to the
first body, whose mass is $m-\omega$ and temperature is $\theta$.
The temperature after the second contact is
 \beq \label{e:T after_2ndcontact}
    \frac{\theta(m-\omega)c+\biggl(\frac{\vartheta m+\theta\omega }
    {m+\omega}\biggr)\omega c}{mc}=\frac{\theta m+\vartheta
    \omega }{m+\omega}.
\eeq
The variable temperatures $\theta$ and $\vartheta$ become,
after the interval $\Delta t$,
 \beq \label{e:T after 2nd contact2}
 \theta -(\theta -
 \vartheta)\frac{\omega}{m}\qquad \textrm{and} \qquad \vartheta +
 (\theta - \vartheta)\frac{\omega}{m}.
\eeq
\par
 For the differences we have
 \beq \label{e:T differences}
  \Delta\theta =-(\theta - \vartheta)\frac{\omega}{m}\qquad\textrm{and}\qquad
 \Delta\vartheta=(\theta - \vartheta)\frac{\omega}{m}.
 \eeq
The quantity of heat received in one instant by the second mass is equal
to the quantity of the heat lost by the first mass. The quantity of
the heat is, if we assume that all other things being equal,
proportional to the actual difference of temperature of the two
bodies.
\par
While $m=\Delta lA\rho$ and $\omega=\delta A\rho$, the masses $m$
and $\omega$ can be replaced by $\Delta l$ and $\delta$,
respectively. (We will call this substitution a length
representation.) Equation (\ref{e:T differences}) is now
 \beq
\label{e:T differencesThickness} \Delta\theta =-(\theta -
\vartheta)\frac{\delta}{\Delta
l}\qquad\textrm{and}\qquad\Delta\vartheta =(\theta -
\vartheta)\frac{\delta}{\Delta l}.
 \eeq
The term $\omega$ (or $\delta$) represents the velocity of
transmission, or the facility with which the heat passes from one of
the bodies into the other. Fourier \cite{Fourier-rcondut} called it
the \textit{reciprocal conducibility}. He integrated the
temperatures of Eq. (\ref{e:T differences}) and found a formula for
the transient temperature of the system.
\par
In a general case of $N$ separate equal masses arranged in a
straight line, Fourier considered \cite{Fourier-N} transmission of
heat by the same shuttle mechanism as in the case of two bodies
only. Infinitesimally thin layers $\omega$ move to and fro between
successive bodies, all at once, so the situation for inner bodies is
not the same as for the two bodies at the boundaries.
\par Let $\alpha,\beta,\gamma,\ldots,\psi$, be the variable
temperatures which correspond to the same time $t$, and which have
succeeded to the initial values $a$, $b$, $c$, $\ldots$. When the
layers $\omega $ have been separated from the first masses and put
in contact with the neighboring masses, the temperatures become \beq
   \frac{\alpha (m - \omega)}{m-\omega},
   \quad \frac{\beta(m-\omega) + \alpha \omega}{m},\quad
   \ldots,\quad \frac{m\psi + \chi \omega}{m+\omega};
\eeq
or
\beq
\label{e:N-Bodies1step}
    \alpha,\quad \beta+(\alpha-\beta)\frac{\omega}{m},\quad \ldots, \quad
    \psi+(\chi-\psi)\frac{\omega}{m+\omega}.
\eeq
\par When the layers $\omega$ have returned to their former
places, the new temperatures (after the instant $dt$) are \beq
\label{e:N-Bodies T M}
    \alpha +(\beta-\alpha)\frac{\omega}{m},\;
    \beta+(\alpha-2\beta+\gamma)\frac{\omega}{m},\;
    \ldots,\; \psi+(\chi-\psi)\frac{\omega}{m},
\eeq
 where the terms with $\omega^2$ are neglected.
\par
If the masses $\omega$ and $m$ in Eq. (\ref{e:N-Bodies T M})
are replaced with the layer's thicknesses $\delta$ and $\Delta l$,
respectively, the temperatures finally become

\beq  \label{e:N-Bodies T L}
    \alpha +(\beta-\alpha)\frac{\delta}{\Delta l},\;
    \beta+(\alpha-2\beta+\gamma)\frac{\delta}{\Delta l},\;
    \ldots,\; \psi+(\chi-\psi)\frac{\delta}{\Delta l}.
\eeq
%N bodies in a circle
\par
Finally, Fourier considered $N$ separate equal masses to be placed
at equal distances on the circumference of a circle
\cite{Fourier-N-circle}. Heat is transferred by the same shuttle
mechanism between the bodies as in the case of $N$ separate equal
masses arranged in a line, but the masses are now all considered to
be inner. Transient temperatures $\alpha, \beta, \ldots, \psi$ can
now be expressed in symmetrical forms,
 \bea \label{e:N-Bodies-Circle T M}
    \alpha +(\psi-2\alpha+\beta)\frac{\omega}{m},\;
    \beta+(\alpha-2\beta+\gamma)\frac{\omega}{m},\;
    \ldots,\;
    \nonumber\\
     \psi+(\chi-2\psi+\alpha)\frac{\omega}{m},
\eea
 or in the length representation,
 \bea \label{e:N-Bodies-Circle T
L}
    \alpha +(\psi-2\alpha+\beta)\frac{\delta}{\Delta l},\;
    \beta+(\alpha-2\beta+\gamma)\frac{\delta}{\Delta l},\;
    \ldots,\;
    \nonumber\\
    \psi+(\chi-2\psi+\alpha)\frac{\delta}{\Delta l}.
\eea

\par
After an ingenious and original set of manipulations, Fourier found
an analytical solution for the temperature of the system and showed
that the formula for the discrete bodies arranged in a circle is equal
for $N \rightarrow \infty$ to the one he found solving the partial
differential equation of heat conduction. Then he remarked
\cite{Fourier-circle-remark}: "It is not necessary to resort to
analysis of partial differential equations in order to obtain the
general equation which expresses the movement of heat in a ring. The
problem may be solved for a definite number of bodies, and that
number may be then supposed infinite. This method has a clearness
peculiar to itself, and guided our first researches."


\subsection{DHW Algorithm}

In the DHW algorithm for calculation of a temperature distribution
\cite{Gembarovic04}, a finite homogeneous medium of thickness
$L$ is divided into $N$ equal slabs of thickness $\Delta l=L/N$.
These slabs are replaced by a perfect conductor of the same heat
capacity separated by the thermal resistance $\Delta l/\lambda$,
(where $\lambda$ is the thermal conductivity of the medium), so the
temperature within a slab at any given time is constant. Heat
propagates through the medium due to a temperature difference
between the slabs. A certain portion (given by the inner heat transfer
coefficient $\xi$) of the excessive heat energy moves from one slab
to the next one, lowering thus the temperature difference between
the two neighbor slabs. This redistribution process (the damped heat
wave) starts from the left boundary slab and marches in space from
one pair of slabs to another. When the wave reaches the boundary of
the medium, it bounces back and moves in the opposite direction in a
perpetual manner.
\par
 Slab temperatures are $T_{i,m}\equiv T(x_i,t_m)$, where $x_i$,
($i=0,1,2,\ldots,N-1$) is a spatial point (middle of the $i$th
slab), and $t_m=m\Delta t $ ($ m=0,1,2,\ldots$) is a discrete time
point. The temperature of the boundary slabs is actually changing
only after the heat wave finishes one whole loop, therefore, the time
step $\Delta t$ is equal to one loop time interval. The time step
$\Delta t$ is thus divided into $2N$ sub-steps. Despite almost
trivial simplicity of this algorithm, the temperature distribution
in the medium at time $t_{m+1}$ as a function of the temperature
distribution at $t_m$ can be expressed by rather lengthy and
complicated formulae \cite{Gembarovic04}:


\setlength\arraycolsep{2pt} \bea \label{e:DHW} T_{0,m+1} = \biggl[
2\frac{1+\xi^{2N-1}}{1+\xi}-1\biggr]T_{0,m}
\nonumber\\
+2\frac{1-\xi}{1+\xi} \sum_{j=1}^{N-1} \xi^j
\biggl(1+\xi^{2(N-j)-1}\biggr)T_{j,m},
                   \nonumber\\
T_{i,m+1} =
2\frac{1-\xi}{1+\xi}\biggl(1+\xi^{2(N-i)-3}\biggl)\sum_{j=0}^{i-1}
\xi^j (1-\xi)^j T_{j,m} {}
                   \nonumber\\
 {}+\biggl[ (1-\xi)^2
 \biggl(2\frac{1+\xi^{2(N-i)-3}}{1+\xi}-1\biggr)+\xi^2\biggr]T_{i,m}{}
                 \nonumber\\
+2\frac{(1-\xi)^2}{1+\xi} \sum_{j=0}^{N-i-1}\xi^j \biggr(1+
\xi^{2(N-i-j)+1}\biggr)T_{j+i+1,m}, {}
                    \nonumber\\
                   {} i=1,2,\ldots,N-1. {}
\eea
\par The inner heat transfer coefficient $\xi$ is a dimensionless quantity
given by \cite{Loffler05}
%
\beq \label{e:TCoeff} \xi = \frac{\mu}{\mu+2}, \eeq
%
where
%
\beq \label{e:FoDef} \mu = \frac{\alpha \Delta t}{(\Delta l)^2},
\eeq
%
is a dimensionless quantity called the Fourier number for one slab, or
simply the mesh ratio, and $\alpha$ is the thermal diffusivity of the
slab material. As can be seen from Eq. (\ref{e:DHW}), the new
temperature of a particular slab at the time $t_{m+1}$ depends not
only on temperatures of its neighboring slabs at time $t_m$, but also
on the temperatures of all slabs in the medium. The influence of
more distant slabs is diminishing exponentially.

\section{COMPARISON}

\par
For $N>2$ bodies arranged in a straight line, the Fourier's algorithm
principally differs from the DHW algorithm. In the DHW algorithm,
the wave of redistribution is marching through the medium,
consecutively changing the temperatures of neighboring slices, two at
the time, while in the Fourier's algorithm, $(N-1)$ thin layers
$\omega$ move to and fro between the successive bodies, all at once.
Nevertheless, from the heat transfer point of view, the two
algorithms are identical for $N=2$. From Eq. (\ref{e:T
differencesThickness}) it follows that the amount of heat
transferred by an infinitely small layer $\delta$ between the two
bodies in $\Delta t$ is $\Delta l A\rho c\Delta \theta=-(\theta -
\vartheta)\delta A \rho c$. From the comparison with the Fourier law,

 \beq  \label{e:Comparison} -(\theta - \vartheta)\delta A \rho
c=-\lambda\frac{(\theta - \vartheta)}{\Delta l}A\Delta t,
 \eeq
%
it follows that
%
\beq  \label{e:Omega}
 \frac{\delta}{\Delta l}=\frac{\lambda\Delta
t}{\rho c \Delta l^2}=\frac{\alpha \Delta t}{\Delta l^2}=\mu.
\eeq
%
If the terms ($\delta/\Delta l$) in Eq. (\ref{e:N-Bodies T L}) are
replaced with the mesh ratio $\mu$, the temperatures of $N$ bodies
arranged in a straight line become finally
 \beq \label{e:N-Bodies T Fo}
    \alpha +(\beta-\alpha)\mu,\;
    \beta+(\alpha-2\beta+\gamma)\mu,\;
    \ldots,\; \psi+(\chi-\psi)\mu,
\eeq
%
or, in the notation similar to the DHW algorithm,
 \bea \label{e:N-Bodies T Tij}
    T_{0,m+1}=T_{0,m} +\mu(T_{1,m}-T_{0,m}),\; \nonumber\\
    T_{i,m+1}=T_{i,m}+\mu(T_{i-1,m}-2T_{i,m}+T_{i+1,m}),\;
    i=1,2,3,\ldots,N-2, \nonumber\\
    T_{N-1,m+1}=T_{N-1,m}+\mu(T_{N-2,m}-T_{N-1,m}).
\eea

% mathematical formulation of the boundary value problem
\par
We consider a simple model problem for the heat flow in a finite
homogeneous unchanging medium of thickness $L$, with no heat
source. The medium is adiabatically insulated and the initial
temperature distribution is given by the Dirac delta function
$\delta(x,t)$. The problem for $x\in[0,L]$ and $t\geq 0$ is
 \bea
 u_t=\alpha u_{xx}, \qquad t\geq 0, 0\leq x \leq L,\nonumber\\
 u_x(0,t)=u_x(L,t)=0,\qquad t\geq 0 \nonumber\\
 u(x,0)=\delta(x,t),\qquad 0\leq x \leq L.
 \label{e:problem_formulation}
 \eea
The analytical solution of the problem, Eq. (\ref{e:problem_formulation})
is given by
 \bea \label{e:analytical_solution}
 u(X,Fo)=\frac{1}{\sqrt{\pi Fo}} \sum_{n=0}^{\infty} {\biggl(\exp \biggl[ \frac
 {-(2n+X)^2}{4Fo}\biggr]}\nonumber\\
 +{\exp \biggl[ \frac{-(2n+2-X)^2}{4Fo}\biggr]\biggr)},
 \eea
 where  $X$ and $Fo$ are the dimensionless $x$-coordinate and the Fourier number, respectively, defined as
 \beq \label{e:X Fo_definition}
 X=\frac{x}{L}, \quad Fo = \frac{\alpha t}{L^2}.
 \eeq

\par
To approximate the model, Eq. (\ref{e:problem_formulation}), by both the DHW
and Fourier algorithms, the medium will be divided into $N$
equal slabs of thickness $\Delta l=L/N$, with nodal points in
the middle of the slabs.
% \beq \label{e:nodal points}
% x_i = (i+0.5)\Delta l, \quad i=0,1,2,\ldots,N-1.
% \eeq
The dimensionless nodal point positions,
 \beq \label {e:X_i}
 X_i=\frac{x_i}{L}, \quad i=0,1,2,\ldots,N-1,
 \eeq
and the dimensionless time,
 \beq \label {e:Fo_m}
 Fo_m=m\Delta Fo=m\frac{\alpha \Delta t}{L^2},\quad m=0,1,2,\ldots \quad,
 \eeq
will be used in graphs. The initial temperature of the first slab
will be numerically equal to $N$, while the rest of the slabs will
be at zero temperature at $Fo=0$.
 \par
The temperature distributions for five different $Fo$ calculated using the
DHW algorithm for $N=20, \mu = 0.2$, and $\Delta Fo=5.0\times10^{-4}$ are
shown in Fig. \ref{fig:DHW&ThN20dFo02} along with the exact solution in Eq.
(\ref{e:analytical_solution}). Errors, $E_{i,m}$, defined as the
difference between the approximative and exact temperatures
$E_{i,m} = T_{i,m}-u(X_i,Fo_m)$, for $N=20, \mu=0.2$, and $ \Delta
Fo=5.0\times10^{-4}$ at $Fo=0.025, 0.05, 0.075, 0.15, 2.0$, are
shown in Fig. \ref{fig:N20dFo02}. Both algorithms clearly give quite
accurate results. The DHW algorithm is more precise than the
Fourier algorithm, especially for slabs close to the front of
the medium.
\par
If the same calculations are carried out with a refined time step,
then the Fourier algorithm results are closer to those of the DHW.
The errors for $N=20,\mu=0.01$, and $\Delta Fo=2.5\times10^{-5}$ are shown
in Fig. \ref{fig:N20DFo001}. The time step refinement has no
significant effect on the DHW algorithm.
\par
On the contrary, an increase in the number of divisions $N$ has a
profound effect on the precision of both algorithms. The error
distributions for both algorithms are shown in Fig.
\ref{fig:N40dFo005} for $N=40, \mu = 0.05$, and $\Delta
Fo=3.125\times10^{-5}$ at $Fo=0.025, 0.05, 0.075, 0.15, 2.0$. The
errors are now about four times less than those from Fig.
\ref{fig:N20DFo001}.
\par
In modern numerical analysis, the system of Eq. (\ref{e:N-Bodies T Tij})
represents an explicit finite difference (EFD) scheme, although a
pattern of grid points used in today EFD schemes differs from the
one used in Fourier's algorithm. It is well known fact
\cite{Morton&Mayers2005_stability} that the EFD scheme is not stable
for the mesh ratio $\mu\geq \frac{1}{2}$. From Fig.
\ref{fig:DHW&F&ThN20dFo05}, where two sets of temperature results
calculated with the DHW and Fourier algorithms are shown (both
for $N=20, \mu = 0.5$, and $\Delta Fo=1.25\times10^{-3}$ at $Fo=0.025,
0.05, 0.075, 0.15, 2.0$), it is clearly visible that Fourier's
algorithm is becoming unstable and the errors $E$ are oscillating.
The DHW algorithm's $E$ values for $N=20, \mu=0.5$, and $\Delta
Fo=1.25\times10^{-3}$, shown in Fig. \ref{fig:DHWResN20DFo05}, are
on the contrary still quite small.
\par When the DHW imitates diffusion, the upper limit
for the inner transfer coefficient is $\xi<0.5$. It follows from Eqs.
(\ref{e:TCoeff}) and (\ref{e:FoDef}) that the upper limit for the
mesh ratio in DHW is $\mu\leq 2$. This 'stability' criterion is not
as strong as in the case of the EFD scheme, because the time step in the DHW
is actually subdivided (discretized) to $2N$ sub-steps and
oscillations are effectively dumped. This is illustrated in Figs
\ref{fig:DHW+ThN20DFo25} and \ref{fig:DHWResN20DFo25}, where the
results of approximative temperature calculations using the DHW
algorithm and the error distribution, respectively, are shown for
$N=20, \mu=2.5$, and $\Delta Fo=6.25\times10^{-3}$. With the exception of the
first curve for $Fo=0.025$, all error values are less than 0.005.

\section{CONCLUSION}
From the comparison of the DHW algorithm and the Fourier algorithm
for transmission of heat between discrete bodies we have found:
\begin{itemize}
  \item For the same number of divisions $N$ and the same mesh ratio $\mu$,
   the DHW algorithm is generally more precise than the Fourier
  algorithm.
  \item For the same number of divisions $N$, the Fourier algorithm
  results converge to the DHW algorithm
  results for the mesh ratio $\mu \rightarrow 0$.
  \item The Fourier algorithm represents the explicit finite difference
  scheme with nodal points in the middle of equal
  thickness slabs.
  \item The Fourier algorithm is stable for the mesh ratio
  $\mu<\frac{1}{2}$, while the DHW algorithm is stable for $\mu < 2$.
  Even for $\mu > 2$, the DHW algorithm  results are not showing signs of
  oscillations.
  \item Both algorithms converge to the exact solution for $N \rightarrow \infty$ and $\mu\rightarrow 0$.
\end{itemize}

%\section{}
%\label{}
\newpage
\begin{thebibliography}{00}

\bibitem{Fourier-original-book} J. Fourier, {\em Analytical Theory
of Heat, translated with notes of A. Freeman} (Dover Pubs,
New York, 1955)

\bibitem{Herivel-beginning} J. Herivel, {\em Joseph Fourier The Man and The Physicist
} (Claredon Press, Oxford, 1975), p. 149.

\bibitem{Grattan-Guinness-stop} I. Grattan-Guinness and J. R. Ravetz, {\em Joseph Fourier 1768-1830
} (MIT Press, Cambridge, 1972), p. 38.

\bibitem{Biot1804}J. B. Biot, {\em Biblioth\'eque Britannique, }{\bf 37}:310 (1804).

\bibitem{Fourier1807} J. Fourier, {\em Th\'eorie de la Propagation de la
Chaleur, } printed with comments in I. Grattan-Guinness, {\em
Joseph Fourier 1768 - 1830} (MIT Press, Cambridge, Massachusetts,
1972)

\bibitem{Fourier1822} J. Fourier, {\em Th\'eorie Analytique de la Chaleur
} (Firmin Didot, Paris, 1822).

\bibitem{Fourier-ChIVsect2} J. Fourier, {\em Analytical Theory
of Heat, translated with notes of A. Freeman  } (Dover Pubs,
New York, 1955), Chap. IV, Sect. 2

\bibitem{Herivel-monument} J. Herivel, {\em Joseph Fourier The Man and The Physicist
} (Claredon Press, Oxford, 1975), p. 149.

\bibitem{Gembarovic04} J. Gembarovic, M. L\"offler, and J.
Gembarovic, Jr., {\em  Appl. Math. Model. }{\bf 28}:173 (2004).

\bibitem{Loffler05} M. L\"offler, J. Gembarovic, and
J. Gembarovic, Jr., {\em A New Way of Modelling Transport Processes,
in Thermal Conductivity 26 Thermal Expansion 14,}  R. B. Dinwiddie
and R. Mannello, eds, (DEStech Pubs, Lancaster,
Pennsylvania, 2005) pp. 123 - 133.


\bibitem{Fourier-2} J. Fourier, {\em Analytical Theory of Heat,
translated with notes of A. Freeman} (Dover Pubs, New
York, 1955), p. 225.

\bibitem{Fourier-rcondut} J. Fourier, {\em Analytical Theory of Heat,
translated with notes of A. Freeman  } (Dover Pubs, New York,
1955), p. 227.

\bibitem{Fourier-N} J. Fourier, {\em Analytical Theory of Heat,
translated with notes of A. Freeman  } (Dover Pubs, New
York, 1955), p. 228.

\bibitem{Fourier-N-circle} J. Fourier, {\em Analytical Theory of Heat,
translated with notes of A. Freeman  } (Dover Pubs, New York,
1955), p. 238.

\bibitem{Fourier-circle-remark} J. Fourier, {\em Analytical Theory of Heat,
translated with notes of A. Freeman  } (Dover Pubs, New York,
1955), p. 261.

%\bibitem{Herivel1822} J. Herivel, {\em Joseph Fourier The Man and The Physicist,
%} (Claredon Press, Oxford 1975) p. 192.

\bibitem{Morton&Mayers2005_stability} K. W. Morton and F. D. Mayers, {\em Numerical Solution
of Partial Differential Equations} (Cambridge University Press,
Cambridge, 2005), p. 14.

\end{thebibliography}

\newpage
\textbf{Figure Captions}
\par
\textbf{Fig. 1.} Heat transfer between two discrete bodies.
\par
\textbf{Fig. 2.} Temperature distribution in a finite body
calculated using the DHW algorithm for $N=20, \mu=0.2$, and $\Delta Fo = 5
\times 10^{-4}$ at different times $Fo=0.025, 0.05, 0.075, 0.15,
2.0$. Exact solutions calculated using Eq.
(\ref{e:analytical_solution}) are depicted as solid lines.
\par
\textbf{Fig. 3.} Error distribution for the DHW and Fourier's
algorithm (both for $N=20, \mu=0.2$, and $\Delta Fo = 5 \times 10^{-4}$)
at different times Fo.
\par
\textbf{Fig. 4.} Error distribution for the DHW and Fourier's
algorithm (both for $N=20, \mu=0.01$, and $\Delta Fo = 2.5 \times
10^{-5}$) at different times $Fo=0.025, 0.05, 0.075, 0.15, 2.0$.
\par
\textbf{Fig. 5.} Error distribution for the DHW and Fourier's
algorithm (both for $N=40, \mu=0.05$, and $\Delta Fo = 3.125 \times
10^{-5}$) at different times $Fo=0.025, 0.05, 0.075, 0.15, 2.0$.
\par
\textbf{Fig. 6.} Temperature distribution in a finite body
calculated using the DHW and Fourier's algorithm for $N=20,
\mu=0.5$, and $\Delta Fo = 1.25 \times 10^{-3}$ at five different times
$Fo=0.025, 0.05, 0.075, 0.15, 2.0$. Exact solutions calculated
using Eq. (\ref{e:analytical_solution}) are depicted as solid lines.
\par
\textbf{Fig. 7.} Error distribution for the DHW algorithm ($N=20,
\mu=0.5$, and $\Delta Fo = 1.25 \times 10^{-3}$) at different times
$Fo=0.025, 0.05, 0.075, 0.15, 2.0$.
\par
\textbf{Fig. 8.} Temperature distribution in a finite body
calculated using the DHW algorithm for $N=20, \mu=2.5$, and $\Delta Fo =
6.25 \times 10^{-3}$ at five different times $Fo=0.025, 0.05, 0.075,
0.15, 2.0$. Exact solutions calculated using Eq.
(\ref{e:analytical_solution}) are depicted as solid lines.
\par
\textbf{Fig. 9.} Error distribution for the DHW algorithm for $N=20,
\mu=2.5$, and $\Delta Fo = 6.25 \times 10^{-3}$ at five different times
$Fo=0.025, 0.05, 0.075, 0.15, 2.0$.


% Figures

\newpage

\begin{figure}
\begin{center}
\includegraphics[scale=1.3]{TwoBodies.eps}
\caption{\label{fig:2Bodies}}{}
\end{center}
\end{figure}


\begin{figure}
\begin{center}
\includegraphics[scale=1.5]{DHW&ThN20dFo02.eps}
\caption{\label{fig:DHW&ThN20dFo02}}{}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=1.3]{N20dFo02.eps}
\caption{\label{fig:N20dFo02}}{}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=1.3]{N20DFo001.eps}
\caption{\label{fig:N20DFo001}}{}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=1.3]{N40dFo005.eps}
\caption{\label{fig:N40dFo005}}{}
\end{center}
\end{figure}


\begin{figure}
\begin{center}
\includegraphics[scale=1.3]{DHW+F+ThN20dFo05.eps}
\caption{\label{fig:DHW&F&ThN20dFo05}}{}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=1.3]{DHWResN20DFo05.eps}
\caption{\label{fig:DHWResN20DFo05}}{}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=1.3]{DHW+ThN20DFo25.eps}
\caption{\label{fig:DHW+ThN20DFo25}}{}
\end{center}
\end{figure}


\begin{figure}
\begin{center}
\includegraphics[scale=1.3]{DHWResN20DFo25.eps}
\caption{\label{fig:DHWResN20DFo25}}{}
\end{center}
\end{figure}


\end{document}
