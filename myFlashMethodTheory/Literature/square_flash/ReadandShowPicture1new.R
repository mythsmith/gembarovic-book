#############################################################
# Square Sample as multiplication three flash1DHL functions
#############################################################
# Jozef Gembarovic (c) 10/2/2016


require(myFlash2)
require(ggplot2)

# citaj txt subory vytvorene mathcadovskym suborom 
# Parallelepiped_Sample_in_the_Flash_Method0.xmcd
setwd("C:/Users/Jozef/Documents/mybook/myFlashMethodTheory/Literature/square_flash")
s1 <-  read.table(file="Sh101010d41010_a0210.txt",  skip=8, col.names = c("time", "P210","P22"))
s2 <-  read.table(file="Sh101010d41010_a0220.txt",  skip=8, col.names = c("time", "P220","P22"))
s3 <-  read.table(file="Sh101010d41010_a0205.txt",  skip=8, col.names = c("time", "P25","P22"))
s4 <-  read.table(file="Sh101010d41010_a02002.txt",  skip=8, col.names = c("time", "P202","P22"))


s5 <-  read.table(file="Ch1010_d410_a0210.txt",  skip=8, col.names = c("time", "C210","C22"))
s6 <-  read.table(file="Ch1010_d410_a0220.txt",  skip=8, col.names = c("time", "C220","C22"))
s7 <-  read.table(file="Ch1010_d410_a0205.txt",  skip=8, col.names = c("time", "C25","C22"))
s8 <-  read.table(file="Ch1010_d410_a02002.txt",  skip=8, col.names = c("time", "C202","C22"))


shape <- rep("Cylinder", 500)
conductivities <- rep("2 0.2", 500)
dfCylanis4 <- data.frame(shape, conductivities, s8$time, s8$C202/max(s8$C202))
colnames(dfCylanis4) <- c("shape","conductivities","time", "T")

conductivities <- rep("2 2", 500)
dfCyliso <- data.frame(shape, conductivities, s5$time,s5$C22/max(s5$C22))
colnames(dfCyliso) <- c("shape","conductivities","time", "T")
dfAll <- rbind(dfCylanis4, dfCyliso)

conductivities <- rep("2 5", 500)
dfCylanis3 <- data.frame(shape, conductivities, s7$time, s7$C25/max(s7$C25))
colnames(dfCylanis3) <- c("shape","conductivities","time", "T")
dfAll <- rbind(dfAll,dfCylanis3)

conductivities <- rep("2 10", 500)
dfCylanis1 <- data.frame(shape, conductivities, s5$time,s5$C210/max(s5$C210))
colnames(dfCylanis1) <- c("shape","conductivities","time", "T")
dfAll <- rbind(dfAll, dfCylanis1)

conductivities <- rep("2 20", 500)
dfCylanis2 <- data.frame(shape, conductivities, s6$time, s6$C220/max(s6$C220))
colnames(dfCylanis2) <- c("shape","conductivities","time", "T")
dfAll <- rbind(dfAll,dfCylanis2)



ggplot (data = dfAll, aes(x=time, y=T, color=conductivities))+geom_line()+
  theme_bw() +
  theme(legend.position=c(0.5,0.2)) 

shape <- rep("Parallelepiped", 500)
conductivities <- rep("2 0.2", 500)
dfParanis4 <- data.frame(shape, conductivities, s4$time, s4$P202/max(s4$P202))
colnames(dfParanis4) <- c("shape","conductivities","time", "T")
dfAll <- rbind(dfAll,dfParanis4)

conductivities <- rep("2 2", 500)
dfPariso <- data.frame(shape, conductivities, s1$time,s1$P22/max(s1$P22))
colnames(dfPariso) <- c("shape","conductivities","time", "T")
dfAll <- rbind(dfAll,dfPariso)

conductivities <- rep("2 5", 500)
dfParanis3 <- data.frame(shape, conductivities, s3$time, s3$P25/max(s3$P25))
colnames(dfParanis3) <- c("shape","conductivities","time", "T")
dfAll <- rbind(dfAll,dfParanis3)

conductivities <- rep("2 10", 500)
dfParanis1 <- data.frame(shape, conductivities, s1$time,s1$P210/max(s1$P210))
colnames(dfParanis1) <- c("shape","conductivities","time", "T")
dfAll <- rbind(dfAll,dfParanis1)

conductivities <- rep("2 20", 500)
dfParanis2 <- data.frame(shape, conductivities, s2$time, s2$P220/max(s2$P220))
colnames(dfParanis2) <- c("shape","conductivities","time", "T")
dfAll <- rbind(dfAll,dfParanis2)

d <- expression(~~italic(lambda[L])~~italic(lambda[r]))
ggplot (data = dfAll, aes(x=time, y=T, color=conductivities, linetype=shape))+geom_line() +
  labs(x = "time [s]", y = "V", color = d) # +
  #theme_bw() +
  #ggtitle("Normalized Temperature Responses \n for Different Conductivities")+
  #theme(legend.position=c(0.5,0.4))
#labs(title = "Mormalized Temperature Rise\n", x = "time [s]", y = "V", color = d)



colnames(dfCylanis) <- c("shape","conductivities","time", "C210","C220","C25","C202")

# merge two data frames by ID and Country
#total <- merge(data frameA,data frameB,by=c("ID","Country")) 
conductivities <- rep("Isotropic", 500),rep("Anisotropic", 500))
dfCyl <- data.frame(conductivities,s5$time,s5$C22)

shape <- rep("Parallelepiped", 500)
conductivities <- rep("Isotropic", 500)
dfPariso <- data.frame(shape, conductivities, s1$time,s1$P22)
colnames(dfPariso) <- c("shape","conductivities","time", "P22")
conductivities <- rep("Anisotropic", 500)
dfParanis <- data.frame(shape, conductivities, s2$time,s1$P210,s2$P220,s3$P25, s4$P202)
colnames(dfParanis) <- c("shape","conductivities","time", "P210","P220","P25","P202")

# plot for a different values of the Biot number H_1
d <- expression(~~italic(lambda[L])~italic(lambda[r]))
ggplot(data=dfPariso) +xlab("time") +
  ylab("V") +
  geom_line(aes(x=time, y=P22/max(P22),  colour = "2 2"),linetype=1, size=1)+
  geom_line(data= dfParanis, aes(x=time, y=P210/max(P210),  colour = "2 10"), linetype=1)+
  geom_line(data= dfParanis, aes(x=time, y=P202/max(P202),  colour = "2 0.2"), linetype=1)+
  geom_line(data= dfParanis, aes(x=time, y=P220/max(P220),  colour = "2 20"), linetype=1)+
  geom_line(data= dfParanis, aes(x=time, y=P25/max(P25),  colour = "2 5"), linetype=1)+
  geom_line(data= dfCyliso, aes(x=time, y=C22/max(C22),  colour = "2 2"), linetype=2, size=1)+
  geom_line(data= dfCylanis, aes(x=time, y=C202/max(C202),  colour = "2 0.2"), linetype=2)+
  geom_line(data= dfCylanis, aes(x=time, y=C210/max(C210),  colour = "2 10"), linetype=2)+
  geom_line(data= dfCylanis, aes(x=time, y=C220/max(C220),  colour = "2 20"), linetype=2)+
  geom_line(data= dfCylanis, aes(x=time, y=C25/max(C25),  colour = "2 5"), linetype=2)+
  scale_x_continuous(expand=c(0,0), limits=range(dfPariso$time/1.5)) +
  annotate("text",label=paste("italic(L)==4*~m*m~~~italic(b)==~italic(c)==10*~m*m"), 
           parse=T, x=0.12, y=0.22, size=4) +
  annotate("text",label=paste("italic(L)==4*~m*m~~~italic(R)==5*~m*m"), 
           parse=T, x=0.12, y=0.15, size=4) +
  annotate("text",label=paste("italic(h)==10*~W/m^2*K"), 
           parse=T, x=0.12, y=0.08, size=4) +
  scale_color_manual(d, 
                      breaks = c( "2 0.2","2 2","2 5","2 10", "2 20"),
                      values = c("lightgoldenrod3","lightsalmon","lightskyblue","darkseagreen3","darkslategray1")) 

# comparison of temperature responses from a paralllepiped and a cylinder samples

d <- expression(~~italic(H))
ggplot(data=dfpar) +xlab("time") +
  ylab("V") +
  geom_line(data=dfcyl, aes(x=s1.time, y=s1$Tcyl2001,  colour = "0.01c"), linetype=4)+
  geom_line(aes(x=s1.time, y=s1$Tpar144001,  colour = "0.01p"),linetype=1)+
  geom_line(data=dfcyl, aes(x=s1.time, y=s5$Tcyl101,  colour = "0.1c"), linetype=4)+
  geom_line(aes(x=s1.time, y=s5$Tpar12201,  colour = "0.1p"), linetype=1)+
  geom_line(data=dfcyl, aes(x=s1.time, y=s7$Tcyl0502,  colour = "0.2c"), linetype=4)+
  geom_line(aes(x=s1.time, y=s7$Tpar11102,  colour = "0.2p"), linetype=1)+
  
  scale_x_continuous(expand=c(0,0), limits=range(dfpar$s1.time)) +
  #  text(0.01, 5, expression(paste(frac(1, sigma*sqrt(2*pi)), " ",
  #                              plain(e)^{frac(-(x-mu)^2, 2*sigma^2)})),
  #       cex = 1.2)
  #annotate("text",label=paste("psi(t) == over(t,t[p]^2)*~e^{-t/t[p]}"), parse=T, x=1.3, y=0.70, size=4)+
  #annotate("text",label=paste("italic(L)==1*~~italic(b)==4*~~italic(c)==4"), parse=T, x=1.2, y=0.89, size=4)+
  guides(color=guide_legend(override.aes=list(shape=c(NA,NA,NA,NA, NA, NA),
                                              linetype=c(4,1,4,1,4,1))))+
  scale_colour_manual(d, 
                      breaks = c("0.01c","0.01p","0.1c","0.1p","0.2c","0.2p"),
                      values = c("lightgoldenrod3", "darkseagreen3", 
                                 "lightskyblue","lightsalmon",
                                 "tomato", "darkseagreen"))