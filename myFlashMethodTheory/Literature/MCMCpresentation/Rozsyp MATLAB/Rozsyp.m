% Program pre Matov Rozsyp Monte Carlo metodou
% 7/19/2011 Copyright Jozef Gembarovic
% k = muk-randn(1)*sigmak - funguje !! 9/12/2011
global P; global Ks; global S;
t = [];
Temper=[];
N=30  %pocet deleni
D=0;  %boundaries are adiabatically insulated
% D=1;  % fixed boundary temperatures
Tmax=100000
Burnin=0*N
% ***   pociatocna teplota  ***
for i=1:N;
   t(i) = 0;%10*sin((i-1)/(N-1)*pi);
end
t(1)=N;%; t1=t(1);
%t(N)=-N/2; tN=t(N);
t1=t;
%pociatocna_suma=sum(t); %test na zachovanie celkovej energie v telese

clf(); %vymaz stary obrazok z GNUPLOTu

for i=1:N
    X(i)=(i-0.5)/N; % normalizovana poloha x/L (0,1)
end
 

%plot((1:N)-0.5,t(1:N),'B'); 
%axis([1,N]);
title ('Initial Temperature Distribution');
xlabel ('x');
ylabel ('Temperature');
%drawnow;

for i=1:100
    k=0.0001; %koeficient prenosu
    
    TM=MRozsyp(t,N,Tmax,k,D);
    t=TM;
    %plot (1:Tmax, S); % zobraz priebeh Entropie S
    %pause
    
    TMC=MCrozsypGk(t1,N,Tmax,k,k/50,D);
    t1=TMC;
    %hist(Ks);
    %xlabel '\xi'; ylabel 'f'; 
    %title(['\fontsize{14}', ' Histogram \xi ~ Normal(0.001, 0.002)'])
    %pause
    
%hist(P);
%pause
if mod(i,10)==0
    cas=2*k*Tmax*i/(2*N*N^2*(1-k)); %realny cas 
    TTh=Flash(cas,X,1);
    
    % 
    %figure (1)% Temperature Distribution
    subplot (2,1,1)
    plot((1:N)-0.5,TTh(1:N),'R',(1:N)-0.5,TMC(1:N),'oB',...
        (1:N)-0.5,TM(1:N),'+G');
    
    legend('Analytical', 'MCMC', 'DHW');
    
    title (['Temperature for k = ', num2str(k), ' at time = ', num2str(cas)]);
    %title (['Residuals at k = ', num2str(k), ' cas = ', num2str(cas)]);
    xlabel ('x');
    grid on;
    ylabel ('Temperature');
    axis([0 30 0 10]);
    drawnow
 
    %figure(2) %Residuals
    subplot (2,1,2)
    plot((1:N)-0.5,(TTh(1:N)-TMC(1:N))*1, ...
    'oB',(1:N)-0.5,TTh(1:N)-TM(1:N),'+G'); 
    
    legend('MCMC', 'DHW');
    
    title (['Residuals for k = ', num2str(k), ' at time = ', num2str(cas)]);
    xlabel ('x');
    grid on;
    ylabel ('\Delta Temperature');
    axis([0 30 -0.2 0.2]);
    drawnow
    pause
    
    %Mv(:,i) = getframe;
end
    %input('dobre? Enter!')

end
% Show movie
%movie(Mv,2)
 %bar(1:N,t(1:N)); drawnow;