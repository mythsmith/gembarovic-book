%% -*- texinfo -*-
%% @deftypefn {Function File} {} MRozsyp (@var{t}, @var{N}, @var{Tmax}, @var{k})
%% The function calculates temperature distribution in a 1 dimensional
%% finite medium with initial distribution given in vector 
%%t(N) if D>0 then boundary temperatures are fixed 
%%
%% @example
%% @group
%% MRozsyp ([1,0,0,0] 4, 300, 0.1,1)
%%      @result{} [ 0.1, 0.1, 0.1, 0.1 ]
%% @end group
%% @end example
%%
%% @seealso{Flash}
%% @end deftypefn

%% Author: Jozef Gembarovic

function resc = MRozsyp3Pc (t, N, Tmax, k, sigma)
global cas;
global tf;
global tr;
% cas=2*k*Tmax/(2*N*N^2*(1-k)) %realny koncovy cas 
%tr0=0; tf0=0; zaciatok = 100;

%tf=[]; tr=[]; res=[];
%i=1:Tmax;
%cas = 2*k*i/(2*N*N^2*(1-k));

%for i=1:Tmax  
%    if (i<zaciatok)
%        tf(i)=tf0;
%        tr(i)=tr0;
%    else
%        tf(i)=tf0+2e4*cas(i-zaciatok+1)*exp(-cas(i-zaciatok+1)*2E3);
%        tr(i)=tr0+cas(i-zaciatok+1)/0.1;
%    end 
%end
count = 1;    pocet=1;
while (count <= Tmax)
        %rozsypuj napravo
        t(1)=tf(pocet);
        for j=1:N-1  
            dt=k*(t(j)-t(j+1)); %vypocitaj rozdiel teplot dvoch susedov
            t(j)= t(j)-dt;  % odober z vacsieho
            t(j+1)= t(j+1)+dt; %pridaj mensiemu
        end
        % rozsyp nalavo
        t(N)=tr(pocet);
        for j=N:2
            dt=k*(t(j)-t(j-1));
            t(j)= t(j)+dt;
            t(j-1)= t(j-1)-dt;
        end
        casr = 2*k*count/(2*N*N^2*(1-k));
        if (casr >= cas(pocet))      
        %casr = 2*k*count/(2*N*N^2*(1-k));
        % add Gaussian noise
        %tf(count)=tf(count)+randn(1)*sigma;
        %tr(count)=tr(count)+randn(1)*sigma;
        %t(N/2)=t(N/2)+randn(1)*sigma;
        % zaznamenaj vysledok
        resc(pocet,:)=[pocet,casr, tf(pocet),t(N/2),tr(pocet)]; % zaznamenaj vysledok do res
        pocet=pocet+1
    end
    count = count + 1;
end

