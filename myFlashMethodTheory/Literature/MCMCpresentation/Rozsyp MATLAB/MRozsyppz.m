%% -*- texinfo -*-
%% @deftypefn {Function File} {} MRozsyp (@var{t}, @var{N}, @var{Tmax}, @var{k})
%% The function calculates temperature distribution in a 1 dimensional
%% finite medium with initial distribution given in vector 
%%t(N) if D>0 then boundary temperatures are fixed 
%%
%% @example
%% @group
%% MRozsyp ([1,0,0,0] 4, 300, 0.1,1)
%%      @result{} [ 0.1, 0.1, 0.1, 0.1 ]
%% @end group
%% @end example
%%
%% @seealso{Flash}
%% @end deftypefn

%% Author: Jozef Gembarovic
% poloha teplotnych bodov je teraz urcena tak ako v standartnych FD 
%  |*-|-*-|-*-| ... |-*-|-*|
% prvy a posledny dielik maju iba 1/2 dlzky 
% ostatnych vnutornych dielikov

function Temperature = MRozsyppz (t, N, Tmax,k,D)
global S;

t1=t(1);tN=t(N); %fixed for Dirichlet boundary conditions   
count = 1;    
B=[];
K1=1; K2=2;
while (count < Tmax)
    %rozsypuj napravo
    j=1;
    dt=2*k*(t(j)-t(j+1))/3; %vypocitaj rozdiel teplot dvoch susedov
        t(j)= t(j)-K1*dt;  % odober z vacsieho
        t(j+1)= t(j+1)+K2*dt; %pridaj mensiemu
        count = count + 1;
        if (count>=Tmax)
            break;
        end    
    for j=2:N-2 
%   K=300;
        dt=k*(t(j)-t(j+1)); %vypocitaj rozdiel teplot dvoch susedov
        %S(count,:)=log(1-dt/(t(j)+K))+log(1+dt/(t(j+1)+K)) ;    
        %pause
        % Entropy change in this step
        t(j)= t(j)-dt;  % odober z vacsieho
        t(j+1)= t(j+1)+dt; %pridaj mensiemu
        count = count + 1;
        if (count>=Tmax)
            break;
        end    
        %input("Enter");
    end
    j=N-1;
        dt=2*k*(t(j)-t(j+1))/3; %vypocitaj rozdiel teplot dvoch susedov
        t(j)= t(j)-K1*dt;  % odober z vacsieho
        t(j-1)= t(j-1)+K2*dt; %pridaj mensiemu
        count = count + 1;
        if (count>=Tmax)
            break;
        end  
    % rozsypuj nalavo
    j=N;
    dt=k*(t(j)-t(j-1))/3; %vypocitaj rozdiel teplot dvoch susedov
        t(j)= t(j)-K2*dt;  % odober z vacsieho
        t(j-1)= t(j-1)+K1*dt; %pridaj mensiemu
        count = count + 1;
        if (count>=Tmax)
            break;
        end
    for j=N-1:3
        dt=k*(t(j)-t(j-1));
        %S(count,:)=log(1-dt/(t(j)+K))+log(1+dt/(t(j-1)+K)) ; 
        % Entropy change in this step
        t(j)= t(j)-dt;
        t(j-1)= t(j-1)+dt;
        
        
        count = count + 1;
        if (count>=Tmax)
            break;
        end  
    end
    j=2;
    dt=2*k*(t(j)-t(j-1))/3; %vypocitaj rozdiel teplot dvoch susedov
        t(j)= t(j)-K2*dt;  % odober z vacsieho
        t(j-1)= t(j-1)+K1*dt; %pridaj mensiemu
        count = count + 1;
        if (count>=Tmax)
            break;
        end
    if (D==1) 
            t(1)=t1;
            t(N)=tN;
    end    
        %input(" pockaj na graf po prvom rozsype tam a spat");
        %bar(1:N,t(1:N)); drawnow   
end
% vrat konecny vektor teploty
    Temperature=t;
   % EntropyChange=sum(S); % ma vyjst 
   %30*log(301)-log(330)-29log(300)=4.5235e-3 OK.
    %pause
end
