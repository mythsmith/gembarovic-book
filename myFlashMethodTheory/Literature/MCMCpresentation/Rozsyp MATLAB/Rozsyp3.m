% Program pre Matov Rozsyp Monte Carlo metodou
% 7/19/2011 Copyright Jozef Gembarovic
%
global t1; global tN;
global cas;
global tf;
global tr;
t = []; res=[];
Temper=[];
N=40  %pocet deleni
k=0.2 %koeficient prenosu
D=1;  %boundaries are adiabatically insulated
% D=1;  % fixed boundary temperatures
Tmax=1000;
Burnin=0*N;
sigma=0.05;
% ***   pociatocna teplota  ***
for i=1:N;
   t(i) = 0;
end
%t(1)=N; t1=t(1);
%t(N)=N/2; tN=t(N);

%pociatocna_suma=sum(t); %test na zachovanie celkovej energie v telese


%input('cakaj na vysledok MRozsyp!')

clf(); %vymaz stary obrazok z GNUPLOTu

%cas=res(:,2); tf=res(:,3);tr=res(:,4);tm=res(:,5);
%L=length(cas); tf=tf+randn(L,1)*sigma;tr=tr+randn(L,1)*sigma;
%tm=tm+randn(L,1)*sigma;
%save TriPdata res -ascii;
%clear cas tf tm tr;
load TriPdata -ascii;
M=TriPdata;
cas=M(:,2); tf=M(:,3);tr=M(:,5);tm=M(:,4);
plot(cas,tf,'+R',cas,tr,'xB', cas, tm,'-G'); 
%axis([1,N]);
title ('Experimental Data');
xlabel ('Time');
ylabel ('Temperatures');
drawnow;

res=MRozsyp3Pc(t,N,Tmax,k,sigma);
cas=res(:,2); tf=res(:,3);tr=res(:,5);tm=res(:,4);
plot(cas,tf,'+R',cas,tr,'xB', cas, tm,'-M',cas,tm,'+G'); 
%axis([1,N]);
title ('Experimental Data');
xlabel ('Time');
ylabel ('Temperatures');
drawnow;