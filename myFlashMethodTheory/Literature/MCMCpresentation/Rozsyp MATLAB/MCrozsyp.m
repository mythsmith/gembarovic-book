%% -*- texinfo -*-
%% @deftypefn {Function File} {} MCRozsyp (@var{t}, @var{N}, @var{Tmax}, @var{k})
%% The function calculates temperature distribution in a 1 dimensional
%% finite medium with initial distribution given in vector 
%% t(N) using Markov Chain Monte Carlo algorithm
%% Rozsyp sa deje tak, ze t_i = -k*(t_i -t_j), t_(j)= -k*(t_j-t_i)  
%% kde i je nahodne generovane cislo (poloha) od 1 do N a j=i + (-1)^m 
%% m je nahodne generovane cele cislo 0 alebo 1
%% if D=0 then boundaries are adiabatically insulated 
%% if D=1 then boundary temperatures are fixed 
%%
%% @example
%% @group
%% MRozsyp ([1,0,0,0] 4, 300, 0.1,1)
%%      @result{} [ 0.1, 0.1, 0.1, 0.1 ]
%% @end group
%% @end example
%%
%% @seealso{Flash}
%% @end deftypefn

%% Author: Jozef Gembarovic

function Temperature = MCRozsyp (t, N, Tmax,k,D)
global t1; global tN;


for j=1:Tmax
 poloha=round(rand(1)*N);
 %znamienko=round(rand(1));
 if ((poloha <= (N-1))&&(poloha >= 2));
    s=sign(rand(1)-0.5); % vyber znamienko (sign), ci pre pohy vpravo (0) alebo vlavo(1)
	%s=(-1)^N;
    dt=-k*(t(poloha)-t(poloha+s)); %vypocitaj rozdiel teplot dvoch susedov
	t(poloha)= t(poloha)+dt;  % odober z vacsieho
    t(poloha+s)= t(poloha+s)-dt; %pridaj mensiemu
	%input("");
 end   
 if (poloha == 1);
	%s=1;
    s=sign(rand(1)-0.5);
    if s>=0
        dt=-k*(t(poloha)-t(poloha+s));
        t(poloha)= t(poloha)+dt;
        t(poloha+s)= t(poloha+s)-dt;
    end    
 end
 if (poloha == N);
	%s=-1;
    s=sign(rand(1)-0.5);
    if s<=0
        dt=-k*(t(poloha)-t(poloha+s));
        t(poloha)= t(poloha)+dt;
        t(poloha+s)= t(poloha+s)-dt;
    end    
 end
 if (D==1) 
            t(1)=t1;
            t(N)=tN;
 end    
 %input(" pockaj");
 %bar(1:N,t(1:N)); drawnow
Temperature = t;
end
