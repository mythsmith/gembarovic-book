% Program pre Matov Rozsyp Monte Carlo metodou
% 7/19/2011 Copyright Jozef Gembarovic
%
% Rozsyp sa deje tak, ze t_i = -k*(t_i -t_j), t_(j)= -k*(t_j-t_i)  
% kde i je nahodne generovane cislo (poloha) od 1 do N a j=i + (-1)^m 
% m je nahodne generovane cele cislo 0 alebo 1

%
%
t = [];
N=50; k=0.05;
for i=1:N;
   t(i) = 0; #10*sin((i-1)/(N-1)*pi);
endfor
t(1)=N;

pociatocna_suma=sum(t)

clf();
plot(1:N,t(1:N),"-2"); drawnow
axis([1,N]);
for j=1:400000
 poloha=round(rand(1)*N);
 znamienko=round(rand(1));
 if ((poloha <= (N-1))&(poloha >= 2));
    s=sign(rand(1)-0.5); # vyber znamienko (sign), ci pre pohy vpravo (0) alebo vlavo(1)
	s=(-1)^N;
    dt=-k*(t(poloha)-t(poloha+s)); #vypocitaj rozdiel teplot dvoch susedov
	t(poloha)= t(poloha)+dt;  # odober z vacsieho
    t(poloha+s)= t(poloha+s)-dt; #pridaj mensiemu
	#input("");
 endif   
 if (poloha == 1);
	s=1;
    dt=-k*(t(poloha)-t(poloha+s));
	t(poloha)= t(poloha)+dt;
    t(poloha+s)= t(poloha+s)-dt;
 endif
 if (poloha == N);
	s=-1;
    dt=-k*(t(poloha)-t(poloha+s));
	t(poloha)= t(poloha)+dt;
    t(poloha+s)= t(poloha+s)-dt;
 endif
 #input(" pockaj");
 #bar(1:N,t(1:N)); drawnow
endfor
input(" po 1000 krokoch. Stisni Enter pre konecny graf!");
konecna_suma = sum(t);
cas=0.020*8;
for i=1:N
    teplota(i)=Flash(cas,i/N,1);
endfor

plot(1:N,t(1:N),"-2",1:N,teplota(1:N),"+4"); drawnow
 #bar(1:N,t(1:N)); drawnow;