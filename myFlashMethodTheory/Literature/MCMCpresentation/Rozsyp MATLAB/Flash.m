%% -*- texinfo -*-
%% @deftypefn {Function File} {} Flash (@var{t}, @var{x}, @var{a})
%% The function Flash calculates temperature distribution in time
%% t and position x in a medium with the thermal diffusivity
%% a
%%
%% @example
%% @group
%% T=Flash(1,0.5,0.005)
%%      @result{} T= 0.89
%% @end group
%% @end example
%%
%% @seealso{}
%% @end deftypefn

%% Author: Jozef Gembarovic

function temper = Flash (t, x, a)
suma=1;
imax=50;
for n=1:imax
    suma=suma+2*cos(n*pi*x)*exp(-(n*pi)^2*t*a);
end
temper=suma;
end
