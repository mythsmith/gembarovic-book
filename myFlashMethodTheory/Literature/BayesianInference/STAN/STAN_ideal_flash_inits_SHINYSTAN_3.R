# clears workspace: 
rm(list=ls()) 

library(rstan)
library(coda)
library(myFlash2)

####################################
# Ideal Flash regression in stan 
# STAN_ideal_flash_inits_SHINYSTAN.R
# (c) Jozef Gembarovic 11/2/2016
####################################

# Ideal (no heat losses) model in Flash method (R code)
L <- c(0.5)
f1D <- function (a,b,x){
  temper <- b*(1+2*sum({{-1}^(1:80)}*{exp(-(1:80)^2*x*pi^2*a/L^2)}))
}
Fun0 <- function(a,b,x)
{  
  f1Dm <- sapply(x,f1D, b, a)
}

##################################################
model <- "
functions {
//... function declarations and definitions ... 
real  FlashIdeal(real time, real L, real a, real T0, real Tm)
{
real Fo ;
real exponential; 
real pi;
pi = 3.1415926535897;
Fo = L^2/(4*a*time);
  exponential  = exp(-Fo)+exp(-9*Fo)+exp(-25*Fo);
  return (T0 + 2*Tm*L/sqrt(pi*a*time)*exponential);
}

}
data {
int<lower=0> N;
vector[N] x;
vector[N] y;
}
parameters {
real <lower=0> a;
real <lower=0> Tm;
real T0;
real<lower=0> sigma;
}
model {
a ~ normal(0.8,1);
Tm ~ normal(1,1);
T0 ~ normal(-1,1);
 for(i in 1:N){
 y[i] ~ normal(FlashIdeal(x[i],0.5,a,T0,Tm), sigma);
 }
}"
set.seed(123)
x <- seq(0.001,0.15,length.out = 100)
N <- length(x)

a <- c(1.17)
Tm <- c(0.8)
T0 <- c(-0.5)

c <- c(0.01)


yy <- Fun0(a,Tm,x)
y <- T0 + yy+rnorm(length(x), sd=c)
plot(x,y, col="skyblue", xlab="t", ylab="T")
grid()

# generate a list of lists to specify initial values
# Copied from ?stan


initf2 <- function(chain_id = 1) {
  # cat("chain_id =", chain_id, "\n")
  list(a=list(1.0,0.5,0.3,0.2), Tm=list(0.4,0.4,0.4,0.4),  T0=list(-0.2,-0.3,-0.4,-0.6), sigma=list(0.01,0.01,0.01,0.01))
}

n_chains <- 4
init_ll <- lapply(1:n_chains, function(id) initf2(chain_id = id))

data <- list(x=x, y=y, N=N) # to be passed on to Stan
myinits <- lapply(1:n_chains, function(id) initf2(chain_id = id))

# parameters to be monitored: 
parameters <- c("a", "Tm", "T0", "sigma")

# The following command calls Stan with specific options.
# For a detailed description type "?rstan".
samples <- stan(model_code=model,   
                data=data, 
                init=myinits,  # If not specified, gives random inits
                pars=parameters,
                iter=10000, 
                chains=4, 
                cores=4,
                # warmup = 100,  # Stands for burn-in; Default = iter/2
                seed = 123  # Setting seed; Default is random seed
                )
# Now the values for the monitored parameters are in the "samples" object, 
# ready for inspection.

print(samples, digits = 5)

# calculate means
means <- samples@.MISC$summary$msd[1:4]
means

# plot experimental data and a newly calculated theoretical curve
plot(x,y, col="skyblue")
lines(x,Fun0(means[1], means[2], x)+means[3], col='goldenrod')
grid()

library(shinystan)
#launch_shinystan(samples)
#save(file="stan_ideal_flash_Shinystan.dat", samples)
pairs(samples)

# function to convert stan to coda
library(coda)
stan2coda <- function(fit) {
  mcmc.list(lapply(1:ncol(fit), function(x) mcmc(as.array(fit)[,x,])))
}

# Thus, you can then get standard rjags/coda-style summary tables and plots.
s <- stan2coda(samples)
summary(s)
plot(s)

require(ggmcmc)
#ggmcmc(ggs(s))  # Directly from a coda object
ggs_density(ggs(s))
autocorr.plot(s)
crosscorr.plot(s)
ggs_traceplot(ggs(s))
ggs_caterpillar(ggs(s))
cumuplot(s)
acfplot(s)
densplot(s)
#geweke.plot(s)  nefunguje

########################################################################
# skusam tieto grafy
library("bayesplot")
library("rstanarm")
library("ggplot2")

#fit <- stan_glm(mpg ~ ., data = mtcars)
fit1 <- samples
posterior <- as.matrix(fit1)

plot_title <- ggtitle("Posterior distributions",
                      "with medians and 80% intervals")
mcmc_areas(posterior, 
           pars= c("a"),# "T0"),#"Tm", "sigma"),
           #           pars = c("cyl", "drat", "am", "wt"), 
           prob = 0.8) + plot_title
color_scheme_set("red")

# with rstan demo model
library("rstan")
#fit2 <- stan_demo("eight_schools", warmup = 300, iter = 700)

posterior2 <- as.matrix(fit1)

color_scheme_set("mix-blue-pink")
p <- mcmc_trace(posterior2,  pars = c("a", "Tm", "T0", "sigma"), n_warmup = 5000,
                facet_args = list(nrow = 2, labeller = label_parsed))
p + facet_text(size = 15)

color_scheme_set("blue")
np <- nuts_params(fit1)
mcmc_nuts_energy(np, merge_chains = F) + ggtitle("NUTS Energy Diagnostic")

############### END #########################################################


