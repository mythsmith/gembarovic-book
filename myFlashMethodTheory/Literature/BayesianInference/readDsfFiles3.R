# Nacitaj multiple subory DFS

library(rlist)
library(ggplot2)
wd <- "D:/Backups/Vsetky2010/Reports" # lenovo
wd <- "C:/users/jgembarovic/Downloads/vsetky" # dell
setwd(wd)
dfsfiles <- list.files(pattern=".DFS", include.dirs=T, 
                     full.names= T, recursive=T)
dfsfiles

# define empty lists

alldfslist <- list()

for(k in 1:(length(dfsfiles))){
  
  f <- dfsfiles[k]

  alllines <- readLines(f)
  nlines <- length(alllines)

#  alldfslist <- list()

  ncount <- 0 
  i <- 1
  while(ncount < nlines){
    dfslist <- list()
    if(ncount == nlines){exit}
    header <- alllines[(ncount+1):(ncount+4)]
    ncount <- ncount+4
    sampleID <- header[1]
    xCol <- header[3]
    yCol <- header[4]
  
    # get number of rows in the table
    l <- header[2]
    s <- strsplit(l, "[^[:digit:]]")
    ## convert strings to numeric ("" become NA)
    solution <- as.numeric(unlist(s))
  
    ## remove NA and duplicates
    solution <- unique(solution[!is.na(solution)])
    solution
    # the number of lines in data table
    tableLength <- solution[1]
  
  
    tabledfs <- read.table(f, skip = ncount, nrows = tableLength)
    ncount <- ncount+tableLength
    etabledfs <- data.frame()
  
    for(j in 1:tableLength){
      # replace D with e
      etabledfs[j,1] <- as.numeric(as.character(gsub( "D","e",tabledfs[j,1])))
      etabledfs[j,2] <- as.numeric(as.character(gsub( "D","e",tabledfs[j,2])))
      #  as.numeric(as.character(etabledfs))
    }
  
    dfslist  <- list( file = f, 
                    sample = sampleID,
                    temperature = etabledfs[,1],
                    diffusivity = etabledfs[,2])
    i <- i+1
    alldfslist <- append(alldfslist, list(dfslist))
  } # the end of while cycle

 # alllist <- append(alldfslist, list(alldfslist))
} # the end of for cycle

a <- alldfslist

# search a list using rlist
library(rlist)
list.search(a, .[grepl("DSS",.)], unlist = F)


#al <- toJSON(alllist)
#al1 <- fromJSON(al)
#al1 # nefunguje

str(list.filter(a))
list.which(a, "DSS fake" %in% sample)

# filter a list
library(pipeR)
a %>>%
  list.filter(sample=="DSS fake") %>>%
  list.mapv(file)

# convert the a list do json format
ajson <- toJSON(a,pretty = TRUE)
ajson
# plot the set 2
plot(a[[4]]$temperature,a[[4]]$diffusivity,xlim=c(0,1300), ylim=c(0,0.9))
points(a[[3]]$temperature,a[[3]]$diffusivity, col="green")
points(a[[2]]$temperature,a[[2]]$diffusivity, col="red")
points(a[[1]]$temperature,a[[1]]$diffusivity, col="blue")
grid()

# rename the list 

alldfslist

names(alldfslist) <- dfsfiles

library(pipeR)

a %>>%
  list.filter(diffusivity[1] >= 0.4) %>>% 
  list.mapv(sample)

#people %>>%
#  list.filter(Expertise$R >= 1 & Expertise$Python >= 1) %>>%
#  list.class(Interests) %>>%
#  list.sort(-length(.)) %>>%
#  list.take(3) %>>%
#  list.map(. %>>% list.table(Age))

