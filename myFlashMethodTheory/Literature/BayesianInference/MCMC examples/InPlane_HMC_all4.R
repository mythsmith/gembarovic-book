############################################################################
# In Plane Flash Response with Heat Losses  
#   Hamiltonian Monte Carlo Algorithm
#        File:InPlane_HMC_all4.R
#      all four parameters are optimized
# (c)  Jozef Gembarovic 11/4/2016 (with priors added 11/18/2016)    
############################################################################

require(rootSolve)
require(numDeriv) # for the gradient
set.seed(12345)
## define values of parameters (CGS units)
a <- 1 # thermal diffusivity (as for a Copper sample)
L <- 0.01 # sample thickness
R <- 6.35 # sample radius
IRI <- 5.0 # irradiated radius inner
IRO <- 6.0 # irradiated radius outer
VRI <- 0.0 # viewed radius inner
VRO <- 1.0 # viewed radius outer

#dimensionless radii
IRID <- IRI/R
IROD <- IRO/R
VRID <- VRI/R
VROD <- VRO/R

# Theoretical values
a.theor <- 1.25 # thermal diffusivity
m.theor  <- 2 # dimensionless parameter m
Tm.theor <- 3.5  # Tmax (adiabatic)
T0.theor <- -1.5  # Tmax (adiabatic)

funR <- function (x) (besselJ(x,1)) 

## find about 64 Radial (betas) roots (for once only)

betas <- uniroot.all(funR, c(0, 200), n=50000)
betas <- betas[betas > 0]

# calculate constant terms which depend only on geometry
b2 <- betas^2   
Ar <- b2*besselJ(betas,0)^2*(IROD^2-IRID^2)*(VROD^2-VRID^2)
termr <- 4*(VRID*besselJ(betas*VRID,1)-VROD*besselJ(betas*VROD,1))*
  (IRID*besselJ(betas*IRID,1)-IROD*besselJ(betas*IROD,1))/Ar

Temper <- function(t, a, m) {
  Fo <- a*t/R^2
  exp(-m^2*Fo)+sum(termr*exp(-(m^2+b2)*Fo))
}
#    t <- seq(0.01, 20, length= 100) 
#    Te <- sapply(t,Temper, a=a.theor, m=m.theor)
#     plot(t,Te) # funguje to!! OK.   

fIPv <- function(a,m,Tm,T0,t) 
{
  Te <- sapply(t,Temper, a=a, m=m)
  TIPv <- Te*Tm + T0
}

like    = function(a,m,Tm,T0){prod(dnorm(y,fIPv(a,m,Tm,T0,t),sqrt(tau2)))}
loglike = function(a,m,Tm,T0){sum(dnorm(y, fIPv(a,m,Tm,T0,t),sqrt(tau2),log=TRUE))}
logpost = function(a,m,Tm,T0){dunif(a,theta0,Ctheta,log=TRUE)+
#    dexp(m, mlambda, log=TRUE)+
#    dunif(Tm,Tm0,CTm,log=TRUE)+
#    dunif(T0,T00,CT0,log=TRUE)+
    loglike(a,m,Tm,T0)}


gradE   <- function(a,m,Tm,T0){
  
  logpth  <- function(t)logpost( t, m, Tm,T0)
  logpm   <- function(t)logpost(a, t, Tm, T0)
  logpTm  <- function(t)logpost(a, m, t , T0)
  
  gradE <- c(-grad(logpth, a),
             -grad(logpm,  m ),
             -grad(logpTm, Tm),
             -1.0)
}

# generate experimental points
t <- seq(0.01, 8, length= 200)

y <- rep(0,length(t))
y <- fIPv(a.theor,m.theor,Tm.theor,T0.theor,t)

# add some normally distributed noise
tau2    <- 0.01  # error distribution variance 
y <- y+rnorm(length(y), 0, tau2)
par(mfrow=c(1,1))
plot(t,y,type="p", col="red", xlab="time", ylab="T")
grid()

# prior for the thermal diffusivity is uniform(0,10)
theta0  = 0.001 # minimum
Ctheta  = 10 # maximum 

# prior for the m is exponential distribution dexp(m, mlambda=1)
mlambda  = 1.0 # density for m = 0 

# prior for the Tm is uniform(0,20)
Tm0  = 0.0 # minimum
CTm  = 20 # maximum 

# prior for T0 is uniform(-10,10)
T00  = -10 # minimum
CT0  = 10 # maximum 

##############################################################################
# Hamiltonian Monte Carlo Algorithm
##############################################################################
#set.seed(192796)

Taumax  <- 10
epsilon <- 0.0075

theta  = 0.8

em  = 1.8

Tm = 2.4

T0 = -1.0

burnin = 500
M      = 1000

x <- c(theta,em,Tm, T0)
dfx <- as.data.frame(x) #  data.frame x of all points
dfa <- as.data.frame(x) #  data.frame x of accepted points

g <- gradE(x[1], x[2], x[3], x[4]) # OK, tested numerically
E <- logpost(x[1], x[2], x[3], x[4])

accepts <- 0
rejects <- 0

strt<-Sys.time()
ind    = 0
for (iter in 1:(burnin+M-1)){
  p <- rnorm(length(x))
  H <- t(p)%*%p/2 + E
  Tau <- round(runif(1,0,2*Taumax))
  xnew <- x
  gnew <- g
  for (tau in 1:Tau){
    p <- p - epsilon * gnew/2 # make half step in p
    xnew <- xnew + epsilon*p # make a step in x
    gnew <- gradE(xnew[1], xnew[2], xnew[3], xnew[4])
    p <- p - epsilon * gnew/2 # make half step in p
  }
  Enew <- logpost(xnew[1], xnew[2], xnew[3], xnew[4])
  Hnew <- t(p)%*%p/2 + Enew
  dH <- (H - Hnew)
  dH
  if (dH < 0) {
    accepts <- accepts + 1
    g <- gnew
    x <- xnew
    E <- Enew
    dfa <- cbind(dfa,x)
  } 
  else  {
    if (runif(1)<exp(-dH)){
      accepts <-accepts +1
      g <- gnew
      x <- xnew
      E <- Enew
    }
    else{ rejects <- rejects + 1}    
    
  }
  
  dfx <- cbind(dfx,x)
}
print(Sys.time()-strt)
# Time difference of 8.716555 mins
tdfx <- as.data.frame(t(dfx)) # transformed data.frame x
tdfa <- as.data.frame(t(dfa)) # transformed data.frame x
colnames(tdfx)  <- c("a", "m", "Tm","T0")
colnames(tdfa)  <- c("a", "m", "Tm","T0")
s <- summary(tdfx[burnin:nrow(tdfx),])
s
#a               m               Tm              T0        
#Min.   :1.157   Min.   :1.813   Min.   :3.109   Min.   :-1.605  
#1st Qu.:1.230   1st Qu.:1.967   1st Qu.:3.438   1st Qu.:-1.518  
#Median :1.250   Median :2.003   Median :3.525   Median :-1.499  
#Mean   :1.251   Mean   :2.003   Mean   :3.514   Mean   :-1.500  
#3rd Qu.:1.269   3rd Qu.:2.049   3rd Qu.:3.596   3rd Qu.:-1.481  
#Max.   :1.331   Max.   :2.187   Max.   :3.841   Max.   :-1.387 

ra <- c(burnin:nrow(tdfx))
par(mfrow=c(4,1))
# hist(tdfx$a[ra],20)
# hist(tdfx$m[ra],20)
# hist(tdfx$Tm[ra],20)
# hist(tdfx$T0[ra],20)

iteration <- c(1:nrow(tdfx))
nr <- c(nrow(tdfx))

par(mfrow=c(4,1))
plot(iteration, tdfx$a,type="l",xlab=" ", ylab="a")
lines(iteration, rep(a.theor, nr),type="l", col="red")
grid()
plot(iteration, tdfx$m,type="l",xlab=" ", ylab="m")
lines(iteration,rep(m.theor, nr),type="l", col="red")
grid()
plot(iteration, tdfx$Tm,type="l",xlab=" ", ylab="Tm")
lines(iteration, rep(Tm.theor, nr),type="l", col="red")
grid()
plot(iteration, tdfx$T0,type="l",xlab="iteration", ylab="T0")
lines(iteration, rep(T0.theor, nr),type="l", col="red")
grid()
xs <- seq(0.01, max(t), length= 500)
calcT <-  rep(0,length(500))
calcT <- fIPv(mean(tdfx$a[ra]), mean(tdfx$m[ra]),mean(tdfx$Tm[ra]),mean(tdfx$T0[ra]), xs)
calcTh <-  rep(0,length(500))
calcTh <- fIPv(a.theor, m.theor,Tm.theor,T0.theor, xs)

# errors

means <- c(mean(tdfx$a[ra]), mean(tdfx$m[ra]),mean(tdfx$Tm[ra]),mean(tdfx$T0[ra]))
theors <- c(a.theor, m.theor,Tm.theor, T0.theor)
errors <- (means-theors)/theors
# diffusivity, m, Tm, T0
errors*100
# [1]  0.05405490  0.17033629  0.41389092 -0.03103938

# reconstruction
ma <- means[1]
mm <- means[2]
mTm <- means[3]
mT0 <- means[4]

mastr <- as.character(round(ma,3))
mmstr <- as.character(round(mm, 3))
mTmstr <- as.character(round(mTm, 3))
mT0str <- as.character(round (mT0, 3))

par(mfrow=c(1,1))
plot(t,y, xlab="time (s)",col = "skyblue",ylab="T")
for (ra1 in 1200:1450) {
  lines(xs,fIPv(tdfa$a[ra1], tdfa$m[ra1], tdfa$Tm[ra1], tdfa$T0[ra1], xs), col="pink")
}
#lines(xs,calcTh,col="yellow") # thoeretical
points(t,y,col="steelblue")
lines(xs,calcT,col="tomato", lwd=2) # calculated from the HMC
grid()

#  iteration curves 
starti <- c(1) #start with iteration 1000 and create 50 curves
curves <-  fIPv(ma, mm, mTm, mT0, xs)
for (j in starti:(starti+100)) {
 curves <- rbind(curves, fIPv(tdfa$a[j], tdfa$m[j], tdfa$Tm[j], tdfa$T0[j], xs))
}
curvest <- t(curves)
# create data frames for ggplot
exp.dat <- data.frame(t,y)
all.dat <- data.frame(xs,curvest)

library(reshape2)
iters <- melt(all.dat, id.vars = "xs")

x0 <- 5

# ggplots
library(ggplot2)
ggplot()+
  geom_line(data=iters, aes(x=xs, y=value,# color=variable,
                            color=variable), alpha=0.2)+
  geom_point(data=exp.dat, aes(x=t, y=y), colour="steelblue", shape=1.0)+
  geom_line(data=all.dat, aes(x=xs, y=curves), colour="tomato", size=0.7)+
  labs(x="time", y="Temperature")+
  xlim(0,max(t))+ theme(legend.position = "none")+
  # ylab("V/Vmax")+
  #ggtitle("Normalized Flash Responses") +
  #geom_hline(yintercept=T0, col="steelblue", linetype="dashed", size=0.1)+
  #geom_hline(yintercept=T0+Tmax, col="steelblue",linetype="dashed", size=0.1)+
  #geom_point(x=0.139,y=0.5, col="green", size = 3)
  
  #scale_colour_manual("", 
   #                   breaks = c("experiment","theory"),
  #                    values = c("skyblue", "indianred1"))+ 
  annotate("text",label="Mean values:", parse=F, x=x0, y=-0.45) +
  annotate("text",label=paste("italic(T)[0] == ",mT0str), parse=T, x=x0, y=-0.6) +
  annotate("text",label=paste("italic(T)[m] == ",mTmstr), parse=T, x=x0, y=-0.7) +
  annotate("text",label=paste("italic(a) == ",mastr), parse=T, x=x0, y=-0.8) +
    annotate("text",label=paste("italic(m) == ",mmstr), parse=T, x=x0, y=-0.9) # +
#  guides(color=guide_legend(override.aes=list(shape=c(1,NA),
#                                              linetype=c(0,1))))+
#  theme(legend.position = "top",legend.background = element_rect(colour = "dimgray"))
alldatmatrix <- as.matrix(tdfx) #[200:1500,])

library(mcmc)
library(coda)
library(ggthemes)
library(ggmcmc)

mcmmmy <- as.mcmc(alldatmatrix)
densplot(mcmmmy)
mcmmmy.gg <- ggs(mcmmmy)
S <- mcmmmy.gg
ggs_density(S,rug = T, greek = T)+theme_solarized(light=T)
ggs_pairs(S,diag=list(continuous="density", color="blue"),
          lower = list(continuous = "density"))+theme_solarized(light=T)# theme_tufte()
ggs_rocplot(S, outcome=S)
ggs_autocorrelation(S, nLags = 200)
ggs_crosscorrelation(S)
ggs_geweke(S)
ggs_running(S)
ggs_traceplot(S)+theme_solarized(light=T)

ggs_traceplot(S)+theme_solarized(light=T)#theme_tufte() #+

accepts/rejects
