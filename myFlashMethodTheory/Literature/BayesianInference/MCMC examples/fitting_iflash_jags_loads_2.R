# fitting_iflash_jags_loads2.R 
# (c) Jozef Gembarovic (11/24/2016)
#
# fit a ideasl flash model,  a continuous outcome variable y (temperature response) 
# as a function of predictor t (time)  and a
# disturbance term e. 

# library for plots
library(ggplot2)

# load simulated data set 
# ideal flash model
# T0 = -2; Tmax = 2.5; a = 0.75; normal noise distribution: mean=0, sd = 0.04;
#
load("df_simdat.dat") # load the data frame sim.dat

#plot the simulated data
ggplot(sim.dat)+
  geom_point(aes(x=t, y=y),colour="skyblue", shape=1)+
  labs(x="time", y="Temperature")

# define a model for JAGS
bayes.mod <- function() {
  for(i in 1:N){
  y[i] ~ dnorm(mu[i], tau)
  mu[i] <- T0+Tmax*(2/sqrt(3.141593*abs(a)*t[i])*(exp(-1/(4*abs(a)*t[i]))+exp(-9/(4*abs(a)*t[i]))))
  }
  T0 ~ dnorm(-2, 0.000001)
  a ~ dnorm(0.50,0.3)
  Tmax ~ dnorm(2, 0.000003)
  tau ~ dgamma(.01, .01)
}

 y <- sim.dat$y
 t <- sim.dat$t
 N <- nrow(sim.dat) # number of data points = number of rows in sim.dat

 # Read in the data frame for JAGS
 sim.dat.jags <- list("y", "t", "N")

 # define parameters
 bayes.mod.params <- c("T0","Tmax", "a") #(baseline, Tmax, diffusivity)
 
 #specific separate starting values for each chain:
 inits1 <- list("T0"=-1.9,"Tmax" = 2.4, "a" = 0.7)
 inits2 <- list("T0"=1.9,"Tmax" = 2.0, "a" = 1.9)
 inits3 <- list("T0"=-2.9,"Tmax" = 1.4, "a" = 0.1)
 bayes.mod.inits <- list(inits1, inits2, inits3)

 # Before using R2jags the first time, you need
 library(R2jags)
 
 #Fit the model in JAGS:
 bayes.mod.fit <- jags(data = sim.dat.jags, inits = bayes.mod.inits,
     parameters.to.save = bayes.mod.params, n.chains = 3, n.iter = 9000,
     n.burnin = 1000, model=bayes.mod)
 
# Update your model if necessary - e.g. if there is no/little convergence:
   bayes.mod.fit.upd <- update(bayes.mod.fit, n.iter=1000)
  bayes.mod.fit.upd <- autojags(bayes.mod.fit)
 #This function will auto-update until convergence.

  #  4.3 Diagnostics
 print(bayes.mod.fit)
 #Inference for Bugs model at "C:/Users/JGEMBA~1/AppData/Local/Temp/RtmpcTy6K4/model1fb8420766cf.txt", fit using jags,
 #3 chains, each with 9000 iterations (first 1000 discarded), n.thin = 8
 #n.sims = 3000 iterations saved
 #mu.vect sd.vect     2.5%      25%      50%      75%    97.5%  Rhat n.eff
 #T0         -2.011   0.015   -2.041   -2.021   -2.011   -2.002   -1.980 1.004   670
 #Tmax        2.518   0.015    2.488    2.509    2.519    2.528    2.547 1.003   760
 #a           0.752   0.007    0.738    0.747    0.752    0.756    0.765 1.003   850
 #deviance -359.791   3.689 -364.387 -362.465 -360.677 -358.007 -350.799 1.001  3000
 
 #For each parameter, n.eff is a crude measure of effective sample size,
 #and Rhat is the potential scale reduction factor (at convergence, Rhat=1).
 
 #DIC info (using the rule, pD = var(deviance)/2)
 #pD = 6.8 and DIC = -353.0
 #DIC is an estimate of expected predictive error (lower deviance is better).
 
 library(coda) 
 library(mcmcplots)
 
 bayes.mod.fit.mcmc <- as.mcmc(bayes.mod.fit)
 s <- summary(bayes.mod.fit.mcmc)
 #plot(bayes.mod.fit)
 #traceplot(bayes.mod.fit)
 
 # More diagnostics are available when you convert your model output into an MCMC object:
 
 #geweke.diag(bayes.mod.fit.mcmc)
 
#densplot(bayes.mod.fit.mcmc)
 
#cumuplot(bayes.mod.fit.mcmc)
#gelman.plot(bayes.mod.fit.mcmc)

#crosscorr.plot(bayes.mod.fit.mcmc)



 denplot(bayes.mod.fit.mcmc, parms = c("T0","Tmax", "a"))
 traplot(bayes.mod.fit.mcmc, parms = c("T0","Tmax", "a"))
# caterplot(bayes.mod.fit.mcmc, parms =  c( "T0","Tmax", "a"),
#  labels = c( "T0","Tmax", "a"))
 
library(ggmcmc)

  bayes.mod.fit.gg <- ggs(bayes.mod.fit.mcmc)
  ggs_density(bayes.mod.fit.gg,rug = T, greek = T)
# ggs_density(bayes.mod.fit.gg, family = c("T0"), rug = T, greek = T)
# ggs_density(bayes.mod.fit.gg, family = c("a"), rug = T, greek = T)
# ggs_density(bayes.mod.fit.gg, family = c("Tmax"), rug = T, greek = T)
 # or sa everything into a pdf file
#ggmcmc(bayes.mod.fit.gg, 
#  file = "jags_fit_iflash_final.pdf")
##

# reconstruction
mT0 <- s$statistics[1]
ma <- s$statistics[3]
mTmax <- s$statistics[2]
mT0str <- as.character(round (mT0, 3))
mastr <- as.character(round(ma,3))
mTmaxstr <- as.character(round(mTmax, 3))

theory <- mT0+iflash(t,ma,1.0,mTmax)

all.dat <- data.frame(sim.dat$y, t, theory)
#plot(t,y, col="skyblue")
#lines(t,theory, col="sienna2")
#grid()

ggplot(all.dat)+
  geom_point(aes(x=t, y=sim.dat.y, colour="experiment"), shape=1)+
  geom_line(aes(x=t, y=theory, colour="theory"))+
  labs(x="time",
       y="Temperature")+
  xlim(0,1)+
  # ylab("V/Vmax")+
  #ggtitle("Normalized Flash Responses") +
  #geom_hline(yintercept=T0, col="steelblue", linetype="dashed", size=0.1)+
  #geom_hline(yintercept=T0+Tmax, col="steelblue",linetype="dashed", size=0.1)+
  #geom_point(x=0.139,y=0.5, col="green", size = 3)
  
  scale_colour_manual("", 
                      breaks = c("experiment","theory"),
                      values = c("skyblue", "indianred1"))+ 
  annotate("text",label="Calculated Means:", parse=F, x=0.75, y=-0.5) +
  annotate("text",label=paste("T[0] == ",mT0str), parse=T, x=0.75, y=-0.8) +
  annotate("text",label=paste("T[max] == ",mTmaxstr), parse=T, x=0.75, y=-1.1) +
  annotate("text",label=paste("a == ",mastr), parse=T, x=0.75, y=-1.4) +
  guides(color=guide_legend(override.aes=list(shape=c(1,NA),
                                            linetype=c(0,1))))+
  theme(legend.position = "top",legend.background = element_rect(colour = "dimgray"))

# graph of iterations in a phase space
# diffusivity vs Tmax

diffIndex0 <- which(bayes.mod.fit.gg$Parameter=="a")[1]
diffus <- as.numeric(inits1[3])
for (i in diffIndex0:(diffIndex0+9000)) {
  diffus <- c(diffus, 
              as.numeric(bayes.mod.fit.gg[i,4]), 
              as.numeric(bayes.mod.fit.gg[i,4]))
}

TmaIndex0 <- which(bayes.mod.fit.gg$Parameter=="Tmax")[1] 
Tma=c(as.numeric(inits1[2]))#,
  #    as.numeric(inits1[2]))
for (i in TmaIndex0:(TmaIndex0+9000)) {
  Tma <- c(Tma, 
           as.numeric(bayes.mod.fit.gg[i,4]),
           as.numeric(bayes.mod.fit.gg[i,4]))
}

basIndex0 <- which(bayes.mod.fit.gg$Parameter=="T0")[1] 
bas=c(as.numeric(inits1[1]),
      as.numeric(inits1[1]))
for (i in basIndex0:(basIndex0+9000)) {
  bas <- c(bas, 
           as.numeric(bayes.mod.fit.gg[i,4]),
           as.numeric(bayes.mod.fit.gg[i,4]))
}

# contour plot and many points

# big dataframe containg points for the density calculation
df.space <- c(0)
df.space <- data.frame(bas[1000:5000], Tma[1000:5000], 
                       diffus[1000:5000])
names(df.space) <- c("T0","Tmax", "a")

# calculate bivariate distribution parameters for elipses
center <- apply(df.space[,1:2], 2, mean)
sigma <- cov(df.space[,1:2])

#The formula requires inversion of the variance-covariance matrix:

sigma.inv = solve(sigma, matrix(c(1,0,0,1),2,2))

#The ellipse "height" function is the negative of the logarithm of the bivariate normal density:

ellipse <- function(s,t) {u<-c(s,t)-center; u %*% sigma.inv %*% u / 2}

#(I have ignored an additive constant equal to log(2pideterminant(sum).)

#To test this, let's draw some of its contours. That requires generating a grid of points in the x and y directions:

n <- 50
y <- seq(min(df.space$Tmax),max(df.space$Tmax),length.out=n)
#y <- seq(0.75-0.007,0.75+0.007,length.out=n) #for the diffusivity
x <- seq(min(df.space$T0),max(df.space$T0),length.out=n)

#n <- 50
#x <- (0:(n-1)) * (500000/(n-1))
#y <- (0:(n-1)) * (50000/(n-1))

#Compute the height function at this grid and plot it:

z <- mapply(ellipse, as.vector(rep(x,n)), as.vector(outer(rep(0,n), y, `+`)))

library(stringr)
library(reshape)

R = expand.grid(x, y)
dim(R)
names(R) = c("x", "y")
R$z = z 

#setwd("~/mybook/myFlashMethodTheory/Literature/BayesianInference/MCMC examples")
#save(R, file="df_R.dat") # save R to a file
# rm(R)                       # remove the data.frame R from memory
#load("df_R.dat")           # load under the same name again

head(R)

ggplot(R, aes(x = x, y = y, z = z)) + stat_contour()
# small data framne for individual points
imin <- 3
imax <- 10
imaxstr <- as.character(imax+imin)
iminstr <- as.character(imin)

df.spaceSmall <- c(0)
df.spaceSmall <- data.frame(bas[imin:(imax+imin+1)], 
                       Tma[imin:(imax+imin+1)])
names(df.spaceSmall) <- c("TmaxSmall", "DiffusivitySmall")

#center <- c(-2.0,2.5)
ggplot(R)+ 
  geom_point(data=df.spaceSmall, aes(x=TmaxSmall, y=DiffusivitySmall), colour="skyblue")+
  geom_path(data=df.spaceSmall, aes(x=TmaxSmall, y=DiffusivitySmall), colour="skyblue")+
  stat_contour(aes(x = x, y = y, z = z),  col="darkolivegreen3", bins=15)+
  geom_hline(yintercept=center[2], col="skyblue3", linetype="dashed", size=0.1)+
  geom_vline(xintercept=center[1], col="skyblue3",linetype="dashed", size=0.1)+
  geom_point(x=center[1],y=center[2], col="gray34", size = 3)+
  labs(y=expression(T[max]),x=expression(T[0]))+
  annotate("text",label=paste("x[",iminstr,"]"), parse=T,
           x=df.spaceSmall$TmaxSmall[1],y=df.spaceSmall$DiffusivitySmall[1]+0.006, size=5) +
  annotate("text",label=paste("x[",imaxstr,"]"), parse=T, 
           x=df.spaceSmall$TmaxSmall[length(df.spaceSmall$TmaxSmall)],
           y=df.spaceSmall$DiffusivitySmall[length(df.spaceSmall$TmaxSmall)]-0.005, size=5)

# small data framne for 500 individual points
imin <- 3
imax <- 500
imaxstr <- as.character(imax+imin)
iminstr <- as.character(imin)

df.spaceSmall <- c(0)
df.spaceSmall <- data.frame(bas[imin:(imax+imin+1)], 
                            Tma[imin:(imax+imin+1)])
names(df.spaceSmall) <- c("TmaxSmall", "DiffusivitySmall")
ggplot(R)+
  geom_point(data=df.spaceSmall, aes(x=TmaxSmall, y=DiffusivitySmall), colour="skyblue", size=1)+
  geom_path(data=df.spaceSmall, aes(x=TmaxSmall, y=DiffusivitySmall), colour="skyblue")+
  stat_contour(aes(x = x, y = y, z = z),  col="darkolivegreen3", bins=15)+
  geom_hline(yintercept=center[2], col="skyblue3", linetype="dashed", size=0.1)+
  geom_vline(xintercept=center[1], col="skyblue3",linetype="dashed", size=0.1)+
  geom_point(x=center[1],y=center[2], col="gray34", size = 3)+
  labs(y=expression(T[max]),x=expression(T[0]))
