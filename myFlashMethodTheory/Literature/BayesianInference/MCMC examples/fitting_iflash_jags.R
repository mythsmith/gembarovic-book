# fitting_iflash_jags.R 
# (c) Jozef Gembarovic (11/19/2016)
#
# fit a ideasl flash model,  a continuous outcome variable y (temperature response) 
# as a function of predictor t (time)  and a
# disturbance term e. We simulate a dataset with 100 observations.
set.seed(123)
library(myFlash2)
library(ggplot2)
# generate experimental points
# dimless time
t <- seq(0.01, 1, length= 100)
# simulated parameters
baselines <- -2.0 # baseline
Tmaxs <- 2.50 # adiabatic rise
diffusivitys <- 0.75 # thermal diffusivity

y <- rep(0,length(t), ncol=2)
y <- baselines+iflash(t,diffusivitys,1.0,Tmaxs)  

# add some normally distributed noise
tau2    <- 0.06  # error distribution variance 
n.sim <- 100 
e <- rnorm(n.sim, mean = 0, sd = tau2)
y <- y+e

# Combine the variables into one dataframe:
sim.dat <- data.frame(y, t)

#plot(t,y,type="p", col="skyblue")
#grid()
ggplot(sim.dat)+
  geom_point(aes(x=t, y=y),colour="skyblue", shape=1)+
  labs(x="time",
       y="Temperature")

# define a model for JAGS
bayes.mod <- function() {
  for(i in 1:N){
  y[i] ~ dnorm(mu[i], tau)
  mu[i] <- baseline + Tmax*(2/sqrt(3.141593*diffusivity*t[i])*(exp(-1/(4*diffusivity*t[i]))+exp(-9/(4*diffusivity*t[i]))))
  }
  baseline ~ dunif(-5, 5)
  diffusivity ~ dunif(0, 10)
  Tmax ~ dunif(0, 20)
  tau ~ dgamma(.01, .01)
}

 y <- sim.dat$y
 t <- sim.dat$t
 N <- nrow(sim.dat)
# Read in the data frame for JAGS
 sim.dat.jags <- list("y", "t", "N")

 # define parameters
 bayes.mod.params <- c("baseline","Tmax", "diffusivity")
 
 #specific separate starting values for each chain:
 inits1 <- list("baseline"=0,"Tmax" = 0.5, "diffusivity" = 0.7)
 inits2 <- list("baseline"=-3,"Tmax" = 1, "diffusivity" = 2)
 inits3 <- list("baseline"=1,"Tmax" = 2, "diffusivity" = 0.2)
 bayes.mod.inits <- list(inits1, inits2, inits3)

 # Before using R2jags the first time, you need
 library(R2jags)
 set.seed(123)
 
 #Fit the model in JAGS:
 bayes.mod.fit <- jags(data = sim.dat.jags, inits = bayes.mod.inits,
     parameters.to.save = bayes.mod.params, n.chains = 3, n.iter = 9000,
     n.burnin = 1000,
    model=bayes.mod)
 
# Update your model if necessary - e.g. if there is no/little convergence:
  # bayes.mod.fit.upd <- update(bayes.mod.fit, n.iter=1000)
  #bayes.mod.fit.upd <- autojags(bayes.mod.fit)
 #This function will auto-update until convergence.

  #  4.3 Diagnostics
 print(bayes.mod.fit)
 #Inference for Bugs model at "C:/Users/Jozef/AppData/Local/Temp/RtmpG4PpYy/model203c3bb976ae.txt", fit using jags,
 #3 chains, each with 9000 iterations (first 1000 discarded), n.thin = 8
 #n.sims = 3000 iterations saved
 #mu.vect sd.vect     2.5%      25%      50%      75%    97.5%  Rhat n.eff
 #Tmax           2.493   0.020    2.454    2.480    2.493    2.506    2.532 1.008   290
 #baseline      -1.981   0.020   -2.021   -1.994   -1.981   -1.968   -1.942 1.007   310
 #diffusivity    0.739   0.008    0.723    0.734    0.739    0.745    0.756 1.004   640
 #deviance    -295.373   3.064 -299.288 -297.670 -296.074 -293.800 -287.668 1.001  3000
 
 
 #For each parameter, n.eff is a crude measure of effective sample size,
 #and Rhat is the potential scale reduction factor (at convergence, Rhat=1).
 
# DIC info (using the rule, pD = var(deviance)/2)
# pD = 4.8 and DIC = -290.6
# DIC is an estimate of expected predictive error (lower deviance is better).
 
 library(coda) 
 library(mcmcplots)
 
 bayes.mod.fit.mcmc <- as.mcmc(bayes.mod.fit)
 s <- summary(bayes.mod.fit.mcmc)
 plot(bayes.mod.fit)
 traceplot(bayes.mod.fit)
 
 # More diagnostics are available when you convert your model output into an MCMC object:
 
 geweke.diag(bayes.mod.fit.mcmc)
 
densplot(bayes.mod.fit.mcmc)
 
cumuplot(bayes.mod.fit.mcmc)
gelman.plot(bayes.mod.fit.mcmc)

crosscorr.plot(bayes.mod.fit.mcmc)



 denplot(bayes.mod.fit.mcmc, parms = c("baseline","Tmax", "diffusivity"))
 traplot(bayes.mod.fit.mcmc, parms = c("baseline","Tmax", "diffusivity"))
 caterplot(bayes.mod.fit.mcmc, parms =  c( "baseline","Tmax", "diffusivity"),
  labels = c( "baseline","Tmax", "diffusivity"))
 
library(ggmcmc)

  bayes.mod.fit.gg <- ggs(bayes.mod.fit.mcmc)
  ggs_density(bayes.mod.fit.gg,rug = T, greek = T)
 ggs_density(bayes.mod.fit.gg, family = c("baseline"), rug = T, greek = T)
 ggs_density(bayes.mod.fit.gg, family = c("diffusivity"), rug = T, greek = T)
 ggs_density(bayes.mod.fit.gg, family = c("Tmax"), rug = T, greek = T)
 # or sa everything into a pdf file
#ggmcmc(bayes.mod.fit.gg, 
#  file = "jags_fit_iflash_final.pdf")
##

# reconstruction
T0 <- s$statistics[2]
diffusivity <- s$statistics[4]
Tmax <- s$statistics[1]
theory <- T0+iflash(t,diffusivity,1.0,Tmax)

all.dat <- data.frame(y, t, theory)
#plot(t,y, col="skyblue")
#lines(t,theory, col="sienna2")
#grid()

ggplot(all.dat)+
  geom_point(aes(x=t, y=y, colour="experiment"), shape=1)+
  geom_line(aes(x=t, y=theory, colour="theory"))+
  labs(x="time",
       y="Temperature")+
  xlim(0,1)+
  # ylab("V/Vmax")+
  #ggtitle("Normalized Flash Responses") +
  #geom_hline(yintercept=T0, col="steelblue", linetype="dashed", size=0.1)+
  #geom_hline(yintercept=T0+Tmax, col="steelblue",linetype="dashed", size=0.1)+
  #geom_point(x=0.139,y=0.5, col="green", size = 3)
  
  scale_colour_manual("", 
                      breaks = c("experiment","theory"),
                      values = c("skyblue", "indianred1"))+ 
  annotate("text",label="Calculated Means:", parse=F, x=0.75, y=-0.5) +
  annotate("text",label="T[0] == -1.981", parse=T, x=0.75, y=-0.8) +
  annotate("text",label="T[max] == 2.493", parse=T, x=0.75, y=-1.1) +
  annotate("text",label="a == 0.739", parse=T, x=0.75, y=-1.4) +
  guides(color=guide_legend(override.aes=list(shape=c(1,NA),
                                            linetype=c(0,1))))+
  theme(legend.position = "top",legend.background = element_rect(colour = "dimgray"))

# graph of iterations in a phase space
# diffusivity vs Tmax
imax <- 30
diffus <- as.numeric(bayes.mod.fit.gg[6001,4])
for (i in 6002:(6002+imax)) {
  diffus <- c(diffus, 
              as.numeric(bayes.mod.fit.gg[i,4]), 
              as.numeric(bayes.mod.fit.gg[i,4]))
}

Tma=c(as.numeric(bayes.mod.fit.gg[9001,4]),
      as.numeric(bayes.mod.fit.gg[9001,4]))
for (i in 9002:(9002+imax)) {
  Tma <- c(Tma, 
           as.numeric(bayes.mod.fit.gg[i,4]),
           as.numeric(bayes.mod.fit.gg[i,4]))
}

plot(Tma[1:(2*imax-1)], diffus[1:(2*imax-1)], type="l")
points(Tma[1:(2*imax-1)], diffus[1:(2*imax-1)], col="green")
grid()

df.space <- c(0)
df.space <- data.frame(Tma[1:(2*imax-1)], 
                       diffus[1:(2*imax-1)])
names(df.space) <- c("Tmax", "Diffusivity")

ggplot(df.space)+
  geom_path(aes(x=Tmax, y=Diffusivity), colour="skyblue")+
  geom_point(aes(x=Tmax, y=Diffusivity), colour="skyblue")+
  labs(x="Tmax",
       y="Diffusivity")+
  geom_hline(yintercept=0.75, col="lightblue", linetype="dashed", size=0.1)+
  geom_vline(xintercept=2.5, col="lightblue",linetype="dashed", size=0.1)+
  geom_point(x=df.space$Tmax[1],y=df.space$Diffusivity[1], col="chartreuse", size = 2)+
  geom_point(x=df.space$Tmax[2*imax-1],y=df.space$Diffusivity[2*imax-1], col="orchid1", size = 2)+
  geom_point(x=2.5,y=0.75, col="black", size = 3)+
 
  annotate("text",label="P[1]", parse=T,x=df.space$Tmax[1]+0.0007,y=df.space$Diffusivity[1]-0.002) +
  annotate("text",label="P[99]", parse=T, x=df.space$Tmax[2*imax-1]+0.0015,y=df.space$Diffusivity[2*imax-1]-0.002)+
  annotate("text",label="X", parse=T, x=2.5,y=0.75-0.002)

