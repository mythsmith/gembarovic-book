
# fit a linear model, so we create a continuous outcome variable y 
# as a function of two predictors x1 and x2 and a
# disturbance term e. We simulate a dataset with 100 observations.

# first we create predictors
n.sim <- 100; set.seed(123)
 x1 <- rnorm(n.sim, mean = 5, sd = 2)
 x2 <- rbinom(n.sim, size = 1, prob = 0.3)
 e <- rnorm(n.sim, mean = 0, sd = 1)
 
 #Next, we create the outcome y based on coefficients b1 and b2 for the respective predictors
 #and an intercept a:
 b1 <- 1.2
 b2 <- -3.1
 a <- 1.5
 y <- a + b1 * x1 + b2 * x2 + e
 
 # Now, we combine the variables into one dataframe for processing later:

 sim.dat <- data.frame(y, x1, x2)
 
 # And we create and summarize a (frequentist) linear model fit on these data:
 freq.mod <- lm(y ~ x1 + x2, data = sim.dat)
 summary(freq.mod)

#Call:
#   lm(formula = y ~ x1 + x2, data = sim.dat)
 
# Residuals:
#   Min      1Q  Median      3Q     Max 
# -1.3432 -0.6797 -0.1112  0.5367  3.2304 
 
# Coefficients:
#   Estimate Std. Error t value Pr(>|t|)    
# (Intercept)  1.84949    0.28810    6.42 5.04e-09 ***
#   x1           1.13511    0.05158   22.00  < 2e-16 ***
#   x2          -3.09361    0.20650  -14.98  < 2e-16 ***
#   ---
#   Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1
 
# Residual standard error: 0.9367 on 97 degrees of freedom
# Multiple R-squared:  0.8772,	Adjusted R-squared:  0.8747 
# F-statistic: 346.5 on 2 and 97 DF,  p-value: < 2.2e-16
 
# Now, we write a model for JAGS and save it as 

# the text file "bayes.mod" in your working directory:
 model1.string <-"
 model{
  for(i in 1:N){
  y[i] ~ dnorm(mu[i], tau)
  mu[i] <- alpha + beta1 * x1[i] + beta2 * x2[i]
 }
  alpha ~ dnorm(0, .01)
  beta1 ~ dunif(-100, 100)
  beta2 ~ dunif(-100, 100)
  tau ~ dgamma(.01, .01)
}
"
bayes.mod<-textConnection(model1.string) 
 y <- sim.dat$y
 x1 <- sim.dat$x1
 x2 <- sim.dat$x2
 N <- nrow(sim.dat)
# Read in the data frame for JAGS
 sim.dat.jags <- list("y", "x1", "x2", "N")

 # define parameters
 bayes.mod.params <- c("alpha", "beta1", "beta2")
 
 #specific separate starting values for each chain:
 inits1 <- list("alpha" = 0, "beta1" = 0, "beta2" = 0)
 inits2 <- list("alpha" = 1, "beta1" = 1, "beta2" = 1)
 inits3 <- list("alpha" = -1, "beta1" = -1, "beta2" = -1)
 bayes.mod.inits <- list(inits1, inits2, inits3)

 # Before using R2jags the first time, you need
 library(R2jags)
 set.seed(123)
 
 #Fit the model in JAGS:
 bayes.mod.fit <- jags(data = sim.dat.jags, inits = bayes.mod.inits,
     parameters.to.save = bayes.mod.params, n.chains = 3, n.iter = 9000,
     n.burnin = 1000,
    model=bayes.mod)
 
# Update your model if necessary - e.g. if there is no/little convergence:
   bayes.mod.fit.upd <- update(bayes.mod.fit, n.iter=1000)
  bayes.mod.fit.upd <- autojags(bayes.mod.fit)
 #This function will auto-update until convergence.

  #  4.3 Diagnostics
 print(bayes.mod.fit)
 
# Inference for Bugs model at "3", fit using jags,
# 3 chains, each with 9000 iterations (first 1000 discarded), n.thin = 8
# n.sims = 3000 iterations saved
# mu.vect sd.vect    2.5%     25%     50%     75%   97.5%  Rhat n.eff
# alpha      1.849   0.294   1.259   1.649   1.856   2.042   2.427 1.001  2900
# beta1      1.134   0.052   1.032   1.100   1.133   1.169   1.237 1.001  3000
# beta2     -3.086   0.212  -3.494  -3.232  -3.090  -2.943  -2.667 1.001  3000
# deviance 271.794   2.866 268.155 269.746 271.131 273.231 278.901 1.001  2600
 
# For each parameter, n.eff is a crude measure of effective sample size,
# and Rhat is the potential scale reduction factor (at convergence, Rhat=1).
 
# DIC info (using the rule, pD = var(deviance)/2)
# pD = 4.1 and DIC = 275.9
# DIC is an estimate of expected predictive error (lower deviance is better).
library(coda) 
 plot(bayes.mod.fit)
 traceplot(bayes.mod.fit)
 # More diagnostics are available when you convert your model output into an MCMC object:

 bayes.mod.fit.mcmc <- as.mcmc(bayes.mod.fit)
 summary(bayes.mod.fit.mcmc)
 
 geweke.diag(bayes.mod.fit.mcmc)
 

densplot(bayes.mod.fit.mcmc)
 
cumuplot(bayes.mod.fit.mcmc)
gelman.plot(bayes.mod.fit.mcmc)

crosscorr.plot(bayes.mod.fit.mcmc)

library(mcmcplots)

 denplot(bayes.mod.fit.mcmc, parms = c("alpha", "beta1", "beta2"))
 traplot(bayes.mod.fit.mcmc, parms = c("alpha", "beta1", "beta2"))
 caterplot(bayes.mod.fit.mcmc, parms =  c("alpha", "beta1", "beta2"),
  labels = c("alpha", "beta1", "beta2"))
 
library(ggmcmc)

  bayes.mod.fit.gg <- ggs(bayes.mod.fit.mcmc)
 ggs_density(bayes.mod.fit.gg)
 # or sa everything into a pdf file
ggmcmc(bayes.mod.fit.gg, 
  file = "C:/Users/jgembarovic/Documents/mybook/myFlashMethodTheory/Literature/BayesianInference/MCMC examples/bayes_fit_ggmcmc.pdf")
##