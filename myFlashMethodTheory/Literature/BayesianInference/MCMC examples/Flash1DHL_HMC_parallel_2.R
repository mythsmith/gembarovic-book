############################################################################
#                  1D Flash Response with Heat Losses  
#                   Hamiltonian Monte Carlo Algorithm
#                    File:Flash1DHL_HMC_parallel_2.R
#               (c)  Jozef Gembarovic 12/2/2014 (9/17/2017)
#
#         verzia pre generovanie obrazkov do mybooku
############################################################################
set.seed(19279)

require(rootSolve)
require(numDeriv) # for the gradient
library(ggplot2)

library(mcmc)
library(coda)
library(ggthemes)
library(ggmcmc)

## define values of parameters (CGS units)
L <- 0.5 # sample thickness

# Theoretical values
Biot.theor  <- 0.4 # dimensionless parameter m
td.theor <- 0.5 # thermal diffusivity
Tmax.theor <- 1.5  # Tmax (adiabatic)

biot1 <- Biot.theor
biot2 <- biot1 # Biot number rear axial

funA <- function (x, b1, b2) (x*x-b1*b2)*sin(x) - (b1+b2)*x*cos(x)

## find about 64 Axial (gamas)  and Radial (betas) roots

#curve(funA(x,b1=biot1,b2=biot1), 0, 200, n=100000, ylim=c(-0.002,0.0020))
#abline(h = 0, lty = 3)
gamas <- uniroot.all(funA, b1=biot1, b2=biot1, c(0, 200), n=50000)
gamas <- gamas[gamas > 0]
#points(gamas, funA(gamas,biot1,biot2), pch=16, cex=0.5, col="red")
b3 <- biot3
#curve(funR(x,b3), 0, 200, n=100000, ylim=c(-0.000002,0.000002))
#abline(h = 0, lty = 3)
betas <- uniroot.all(funR, b3=biot3, c(0, 200), n=50000)
betas <- betas[betas > 0]
#points(betas, funR(betas,biot3), pch=16, cex=0.5, col="green")


flash1D <- function(t, a, Tmax) 
{
  term <- ((-1)^(1:40)*exp(-(1:40)^2*pi^2*a*t/L^2))
  T1D <- Tmax*(1+2*sum(term))
}

flash1DHL<- function(t, a, Bi, Tmax) 
{
  Bi1=Bi
  Bi2=Bi
    g2 <- gamas^2
    Ax <- 2*g2*(g2+Bi2^2)/((g2+Bi1^2)*(g2+Bi2^2)+(Bi1+Bi2)*(g2+Bi1*Bi2))
    term <- Ax*(cos(gamas*1.0)+Bi1/gamas*sin(gamas*1.0))*exp(-g2*a*t/L^2)
  T1DHL1 <- Tmax*sum(term)
}


fv <- function(td,Bi,Tmax,t) 
{
  Tv <- sapply(t,flash1DHL, a=td, Bi=Bi, Tmax=Tmax)
}

like    = function(td,Bi,Tmax){prod(dnorm(y,fv(td,Bi,Tmax,t),sqrt(tau2)))}
loglike = function(td,Bi,Tmax){sum(dnorm(y, fv(td,Bi,Tmax,t),sqrt(tau2),log=TRUE))}
logpost = function(td,Bi,Tmax){dunif(td,theta0,Ctheta,log=TRUE)+
                              loglike(td,Bi,Tmax)}


gradE   <- function(td,Bi,Tmax){
  
  logpth  <- function(t)logpost( t, Bi, Tmax)
  logpm   <- function(t)logpost(td, t, Tmax)
  logpTm  <- function(t)logpost(td, Bi, t )
  
  gradE <- c(-grad(logpth, td),
             -grad(logpm,  Bi ),
             -grad(logpTm, Tmax))
}


# generate experimental points
t <- seq(0.001, 0.5, length= 200)

y <- rep(0,length(t))
y <- fv(td.theor,Biot.theor,Tmax.theor,t)

# add some normally distributed noise
tau2    <- 0.01  # error distribution variance 
y <- y+rnorm(length(y), 0, tau2)
par(mfrow=c(1,1))
plot(t,y,type="p", col="red")


# prior for the thermal diffusivity is uniform(0,10)
theta0  = 0.001 # minimum
Ctheta  = 1 # maximum 

##############################################################################
# Hamiltonian Monte Carlo Algorithm
##############################################################################
set.seed(192796)
library(doParallel)
# Find out how many cores are available (if you don't already know)
detectCores()
## [1] 4 for jgem
# Create cluster with desired number of cores
cl <- makeCluster(4)
# Register cluster
registerDoParallel(cl)
# Find out how many cores are being used
getDoParWorkers()
## [1] 4
#stopCluster(cl)

strt <- Sys.time()
Taumax  <- 8
epsilon <- 0.001

Vtheta = 0.01
theta  = 0.8

Vm = 0.02
Biot  = 0.1

Tmax = 1.4

burnin = 400
M      = 5000


x <- c(theta,Biot,Tmax)
dfx <- as.data.frame(x) #  data.frame x

gamas <- uniroot.all(funA, b1=Biot, b2=Biot, c(0, 200), n=50000)
gamas <- gamas[gamas > 0]

g <- gradE(x[1], x[2], x[3]) # OK, tested numerically
E <- logpost(x[1], x[2],x[3])

dfx <- foreach(i = 1:(M+burnin-1), .combine=cbind) %dopar% {
  require(numDeriv) # for the gradient
  require(rootSolve)
  p <- rnorm(length(x))
  H <- t(p)%*%p/2 + E
  Tau <- round(runif(1,0,2*Taumax))
  xnew <- x
  gnew <- g
 
  for (tau in 1:Tau){
    p <- p - epsilon * gnew/2 # make half step in p
    xnew <- abs(xnew + epsilon*p) # make a step in x
    gamas <- uniroot.all(funA, b1=xnew[2], b2=xnew[2], c(0, 200), n=50000)
    gamas <- gamas[gamas > 0]
    gnew <- gradE(xnew[1], xnew[2], xnew[3])
    p <- p - epsilon * gnew/2 # make half step in p
  }
  Enew <- logpost(xnew[1], xnew[2], xnew[3])
  Hnew <- t(p)%*%p/2 + Enew
  dH <- (H - Hnew)
  if (dH < 0) {
    g <- gnew
    x <- xnew
    E <- Enew
  } 
  else  {
    if (runif(1)<exp(-dH)){
      g <- gnew
      x <- xnew
      E <- Enew
    }
    else{ }    
  }
   dfx <- x 
}

stopCluster(cl)

print(Sys.time()-strt)
# Time difference of 23.43668 mins

tdfx <- as.data.frame(t(dfx)) # transformed data.frame x
colnames(tdfx)  <- c("a", "Biot", "Tmax")
save(tdfx,file = "C:/Users/Jozef/Documents/mybook/myFlashMethodTheory/Literature/BayesianInference/MCMC examples/tdfx1.dat")

##lazy start ######################################################################
burnin <- 2000
load("C:/Users/Jozef/Documents/mybook/myFlashMethodTheory/Literature/BayesianInference/MCMC examples/tdfx1.dat")

summary(tdfx[burnin:nrow(tdfx),])

ra <- c(burnin:nrow(tdfx))
par(mfrow=c(3,1))
 hist(tdfx$a[ra],20)
 hist(tdfx$Biot[ra],20)
 hist(tdfx$Tmax[ra],20)

ra1 <- c(1:nrow(tdfx))
nr <- c(nrow(tdfx))

par(mfrow=c(3,1))
plot(ra1, tdfx$a,type="l")
lines(ra1, rep(td.theor, nr),type="l", col="red")
plot(ra1, tdfx$Biot,type="l")
lines(ra1,rep(Biot.theor, nr),type="l", col="red")
plot(ra1, tdfx$Tmax,type="l")
lines(ra1, rep(Tmax.theor, nr),type="l", col="red")

xs <- seq(0.001, 0.5, length= 200)
gamas <- uniroot.all(funA, b1=mean(tdfx$Biot[ra]), 
                     b2=mean(tdfx$Biot[ra]), c(0, 200), n=50000)
gamas <- gamas[gamas > 0]
calcT <-  rep(0,length(500))
calcT <- fv(mean(tdfx$a[ra]), mean(tdfx$Biot[ra]),mean(tdfx$Tmax[ra]), xs)
gamas <- uniroot.all(funA, b1=Biot.theor, 
                     b2=Biot.theor, c(0, 200), n=50000)
gamas <- gamas[gamas > 0]
calcTh <-  rep(0,length(500))
calcTh <- fv(td.theor, Biot.theor,Tmax.theor, xs)

par(mfrow=c(1,1))
plot(t,y, xlab="time (s)", pch=23, cex=0.66,col = "blue",ylab="T (a.u.)")
lines(xs,calcT,col="red") # calculated from the HMC
lines(xs,calcTh,col="yellow") # thoeretical
residuals <- calcT-calcTh
hist(residuals,50)

summary(tdfx[burnin:nrow(tdfx),])
##################################################################
# Time difference of 24.13016 mins on
# PC Jozef 9/17/2017

#> summary(tdfx[burnin:nrow(tdfx),])
# td              Biot             Tmax      
#Min.   :0.4725   Min.   :0.3432   Min.   :1.409  
#1st Qu.:0.4925   1st Qu.:0.3885   1st Qu.:1.480  
#Median :0.4994   Median :0.4013   Median :1.502  
#Mean   :0.4989   Mean   :0.4029   Mean   :1.504  
#3rd Qu.:0.5049   3rd Qu.:0.4168   3rd Qu.:1.527  
#Max.   :0.5303   Max.   :0.4540   Max.   :1.584 
##################################################################
#save(tdfa,tdfx, file="tdfx")
# errors
means <- c(mean(tdfx$a[ra]), mean(tdfx$Biot[ra]),mean(tdfx$Tmax[ra]))
theors <- c(td.theor, Biot.theor,Tmax.theor)
errors <- (means-theors)/theors
# diffusivity, Biot, Tm
errors*100
# [1]  0.05405490  0.17033629  0.41389092 -0.03103938

# reconstruction
ma <- means[1]
mm <- means[2]
mTm <- means[3]

mastr <- as.character(round(ma,4))
mmstr <- as.character(round(mm, 4))
mTmstr <- as.character(round(mTm, 3))

par(mfrow=c(1,1))
plot(t,y, xlab="time (s)",col = "skyblue",ylab="T")
for (ra1 in 3000:3100) {
  gamas <- uniroot.all(funA, b1= tdfx$Biot[ra1], 
                       b2=tdfx$Biot[ra1], c(0, 200), n=50000)
  gamas <- gamas[gamas > 0]
  lines(t,fv(tdfx$a[ra1], tdfx$Biot[ra1], tdfx$Tmax[ra1],t), col="pink")
}
#lines(xs,calcTh,col="yellow") # thoeretical
points(t,y,col="steelblue")
lines(xs,calcT,col="tomato", lwd=2) # calculated from the HMC
grid()

#  iteration curves 
starti <- c(2000) #start with iteration1 000 and create 50 curves
rm(curves)
gamas <- uniroot.all(funA, b1= mm, 
                     b2=mm, c(0, 200), n=50000)
gamas <- gamas[gamas > 0]
curves <-  fv(ma, mm, mTm, t)
for (ra1 in starti:(starti+100)) {
  gamas <- uniroot.all(funA, b1= tdfx$Biot[ra1], 
                       b2=tdfx$Biot[ra1], c(0, 200), n=50000)
  gamas <- gamas[gamas > 0]
  curves <- rbind(curves, fv(tdfx$a[ra1], tdfx$Biot[ra1], tdfx$Tmax[ra1],t))
}
curvest <- t(curves)
# create data frames for ggplot
exp.dat <- data.frame(t,y)
all.dat <- data.frame(t,curvest)

library(reshape2)
iters <- melt(all.dat, id.vars = "t")

x0 <- 0.2

# ggplots
ggplot()+
  geom_line(data=iters, aes(x=t, y=value,# color=variable,
                            color=variable), alpha=0.1)+
  geom_point(data=exp.dat, aes(x=t, y=y), colour="steelblue", shape=20, size=1.4)+
  geom_line(data=all.dat, aes(x=t, y=curves), colour="red", size=0.9)+
  labs(x="time", y="Temperature")+
  xlim(0,max(t))+ theme(legend.position = "none")+
  # ylab("V/Vmax")+
  #ggtitle("Normalized Flash Responses") +
  #geom_hline(yintercept=T0, col="steelblue", linetype="dashed", size=0.1)+
  #geom_hline(yintercept=T0+Tmax, col="steelblue",linetype="dashed", size=0.1)+
  #geom_point(x=0.139,y=0.5, col="green", size = 3)
  
  #scale_colour_manual("", 
  #                   breaks = c("experiment","theory"),
  #                    values = c("skyblue", "indianred1"))+ 
  annotate("text",label="Mean values:", parse=F, x=x0, y=0.6) +
 # annotate("text",label=paste("italic(T)[0] == ",mT0str), parse=T, x=x0, y=-0.6) +
  annotate("text",label=paste("italic(T)[max] == ",mTmstr), parse=T, x=x0, y=0.4) +
  annotate("text",label=paste("italic(a) == ",mastr, "~c*m^2/s"), parse=T, x=x0, y=0.5) +
  annotate("text",label=paste("italic(H) == ",mmstr), parse=T, x=x0, y=0.3) 
#  guides(color=guide_legend(override.aes=list(shape=c(1,NA),
#                                              linetype=c(0,1))))+
#  theme(legend.position = "top",legend.background = element_rect(colour = "dimgray"))
alldatmatrix <- as.matrix(tdfx) #[200:1500,])

## mcmc plots
mcmmmy <- as.mcmc(alldatmatrix)
densplot(mcmmmy)
mcmmmy.gg <- ggs(mcmmmy)
S <- mcmmmy.gg
ggs_density(S,rug = T, greek = T)+theme_solarized(light=T)
ggs_pairs(S,diag=list(continuous="density", color="blue"),
          lower = list(continuous = "density"))+theme_solarized(light=T)# theme_tufte()
ggs_rocplot(S, outcome=S)
ggs_autocorrelation(S, nLags = 200)
ggs_crosscorrelation(S)
ggs_geweke(S)
ggs_running(S)
ggs_traceplot(S)+theme_solarized(light=T)

ggs_traceplot(S)+theme_tufte() #+

accepts/rejects # zadefinuj!
