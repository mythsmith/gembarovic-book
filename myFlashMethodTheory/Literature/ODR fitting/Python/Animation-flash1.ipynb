{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Embedding Matplotlib Animations in IPython Notebooks"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*This notebook first appeared as a*\n",
    "[*blog post*](http://jakevdp.github.io/blog/2013/05/12/embedding-matplotlib-animations/)\n",
    "*on*\n",
    "[*Pythonic Perambulations*](http://jakevdp.github.io).\n",
    "\n",
    "*License:* [*BSD*](http://opensource.org/licenses/BSD-3-Clause)\n",
    "*(C) 2013, Jake Vanderplas.*\n",
    "*Feel free to use, distribute, and modify with the above attribution.*"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<!-- PELICAN_BEGIN_SUMMARY -->\n",
    "I've spent a lot of time on this blog working with matplotlib animations\n",
    "(see the basic tutorial\n",
    "[here](http://jakevdp.github.io/blog/2012/08/18/matplotlib-animation-tutorial/),\n",
    "as well as my examples of animating\n",
    "[a quantum system](http://jakevdp.github.io/blog/2012/09/05/quantum-python/),\n",
    "[an optical illusion](http://jakevdp.github.io/blog/2012/09/26/optical-illusions-in-matplotlib/),\n",
    "[the Lorenz system in 3D](http://jakevdp.github.io/blog/2013/02/16/animating-the-lorentz-system-in-3d/),\n",
    "and [recreating Super Mario](http://jakevdp.github.io/blog/2013/01/13/hacking-super-mario-bros-with-python/)).\n",
    "Up until now, I've not have not combined the animations with IPython notebooks.\n",
    "The problem is that so far the integration of IPython with matplotlib is\n",
    "entirely static, while animations are by their nature dynamic.  There are some\n",
    "efforts in the IPython and matplotlib development communities to remedy this,\n",
    "but it's still not an ideal setup.\n",
    "\n",
    "I had an idea the other day about how one might get around this limitation\n",
    "in the case of animations.  By creating a function which saves an animation\n",
    "and embeds the binary data into an HTML string, you can fairly easily create\n",
    "automatically-embedded animations within a notebook.\n",
    "<!-- PELICAN_END_SUMMARY -->"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## The Animation Display Function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As usual, we'll start by enabling the pylab inline mode to make the\n",
    "notebook play well with matplotlib."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Populating the interactive namespace from numpy and matplotlib\n"
     ]
    }
   ],
   "source": [
    "%pylab inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we'll create a function that will save an animation and embed it in\n",
    "an html string.  Note that this will require ffmpeg or mencoder to be\n",
    "installed on your system.  For reasons entirely beyond my limited understanding\n",
    "of video encoding details, this also requires using the libx264 encoding\n",
    "for the resulting mp4 to be properly embedded into HTML5. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from tempfile import NamedTemporaryFile\n",
    "\n",
    "VIDEO_TAG = \"\"\"<video controls>\n",
    " <source src=\"data:video/x-m4v;base64,{0}\" type=\"video/mp4\">\n",
    " Your browser does not support the video tag.\n",
    "</video>\"\"\"\n",
    "\n",
    "def anim_to_html(anim):\n",
    "    if not hasattr(anim, '_encoded_video'):\n",
    "        anim.save(\"test.mp4\", fps=20, extra_args=['-vcodec', 'libx264'])\n",
    "\n",
    "        video = open(\"test.mp4\",\"rb\").read()\n",
    "\n",
    "    anim._encoded_video = video.encode(\"base64\")\n",
    "    return VIDEO_TAG.format(anim._encoded_video)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With this HTML function in place, we can use IPython's HTML display tools\n",
    "to create a function which will show the video inline:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from IPython.display import HTML\n",
    "\n",
    "def display_animation(anim):\n",
    "    plt.close(anim._fig)\n",
    "    return HTML(anim_to_html(anim))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### An ideal response (one dimensional heat flow, an instantaneous pulse, no heat losses)\n",
    "Analytical formula for the temperature response at the rear side of a sample (at $x=L$) to an instantaneous heat pulse at the front side ($x=0$) for $t>0$ is\n",
    "\n",
    "$$ T(t,a)=T_0+T_{max}\\big[1+2\\sum_{n=1}^\\infty (-1)^n e^{-n^2\\pi^2\\frac{at}{L^2}}\\big],$$\n",
    "\n",
    "where $a$ is the thermal diffusivity and $L$ is the sample thickness. $T_0$ is the initial (baseline) temperature level and $T_{max}$ represents the maximum temperature rise of adiabatically insulated sample after the flash, which depends on the amount of heat deposited on the sample and the sample heat capacity. \n",
    "\n",
    "Analytical formulae for the temperature rise are usually given in a simplified, relative form, where $T_0=0$ and $T_{max}=1$.\n",
    "\n",
    "If the dimensionless time \\omega is defined as\n",
    " $$\\omega = \\frac{at}{L^2}$$\n",
    "then for a dimensionless, normalized temperature response we have a simple formula\n",
    "### $$ \\frac{T(t,a)}{T_{max}}= 1+2\\sum_{n=1}^\\infty (-1)^n e^{-n^2\\pi^2\\omega}.$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "ename": "AttributeError",
     "evalue": "'bytes' object has no attribute 'encode'",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mAttributeError\u001b[0m                            Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-4-3aba20b536a1>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m()\u001b[0m\n\u001b[1;32m     47\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m     48\u001b[0m \u001b[1;31m# call our new function to display the animation\u001b[0m\u001b[1;33m\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m---> 49\u001b[0;31m \u001b[0mdisplay_animation\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0manim\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m<ipython-input-3-63a8cd04f4be>\u001b[0m in \u001b[0;36mdisplay_animation\u001b[0;34m(anim)\u001b[0m\n\u001b[1;32m      3\u001b[0m \u001b[1;32mdef\u001b[0m \u001b[0mdisplay_animation\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0manim\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m:\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m      4\u001b[0m     \u001b[0mplt\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mclose\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0manim\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m_fig\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m----> 5\u001b[0;31m     \u001b[1;32mreturn\u001b[0m \u001b[0mHTML\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0manim_to_html\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0manim\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;32m<ipython-input-2-7abc6bb12f6f>\u001b[0m in \u001b[0;36manim_to_html\u001b[0;34m(anim)\u001b[0m\n\u001b[1;32m     12\u001b[0m         \u001b[0mvideo\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mopen\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;34m\"test.mp4\"\u001b[0m\u001b[1;33m,\u001b[0m\u001b[1;34m\"rb\"\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mread\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[1;32m     13\u001b[0m \u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0;32m---> 14\u001b[0;31m     \u001b[0manim\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m_encoded_video\u001b[0m \u001b[1;33m=\u001b[0m \u001b[0mvideo\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mencode\u001b[0m\u001b[1;33m(\u001b[0m\u001b[1;34m\"base64\"\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n\u001b[0m\u001b[1;32m     15\u001b[0m     \u001b[1;32mreturn\u001b[0m \u001b[0mVIDEO_TAG\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0mformat\u001b[0m\u001b[1;33m(\u001b[0m\u001b[0manim\u001b[0m\u001b[1;33m.\u001b[0m\u001b[0m_encoded_video\u001b[0m\u001b[1;33m)\u001b[0m\u001b[1;33m\u001b[0m\u001b[0m\n",
      "\u001b[0;31mAttributeError\u001b[0m: 'bytes' object has no attribute 'encode'"
     ]
    },
    {
     "data": {
      "text/plain": [
       "<matplotlib.figure.Figure at 0x2bbef8d14e0>"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "from matplotlib import animation\n",
    "\n",
    "# First set up the figure, the axis, and the plot element we want to animate\n",
    "\n",
    "fig = plt.figure()\n",
    "fig = plt.figure(figsize=(12, 10)) \n",
    "major_ticks = np.arange(0, 1.1, 0.1)\n",
    "ax = plt.axes(xlim=(0, 1/0.1388), ylim=(0, 1.1))\n",
    "ax.set_yticks(major_ticks)      \n",
    "plt.ylabel(r'$\\frac{T}{T_{max}}$', size=30)\n",
    "plt.xlabel(r'$\\omega / \\omega_{0.5}$',size=20)\n",
    " \n",
    "ax.grid(which='major', alpha=0.7)\n",
    "\n",
    "plt.title('Flash Method Ideal Response Curve', size=25)\n",
    "#plt.text(0.139, .5, r'$Fo=0.1388,\\ \\Delta T=0.5*T_{max}$')\n",
    "#plt.annotate('halftime', xy=(0.1388, 0.5), xytext=(0.2, 0.5),\n",
    "#            arrowprops=dict(facecolor='black', shrink=0.05),\n",
    "#            )\n",
    "\n",
    "line, = ax.plot([], [], lw=2)\n",
    "\n",
    "# initialization function: plot the background of each frame\n",
    "def init():\n",
    "    line.set_data([], [])\n",
    "    return line,\n",
    "\n",
    "def flash1D(t, a): # One dimensional ideal model (an instantaneous heat pulse, no heat losses)\n",
    "    suma =0.0\n",
    "    L = 1.0\n",
    "    for n in range(1, 50):\n",
    "        suma = suma + (-1)**n*np.exp(-((n*np.pi)**2*a*t/L**2))\n",
    "    return (1+2.0*suma)\n",
    "\n",
    "# animation function.  This is called sequentially\n",
    "def animateFlash1D(i):\n",
    "    #x = np.linspace(0, 2, 1000)\n",
    "    x = np.linspace(0.001,i*0.005,200)\n",
    "    #y = np.sin(2 * np.pi * (x - 0.01 * i))\n",
    "    y = flash1D(x,1.0)\n",
    "    line.set_data(x/0.1388, y)\n",
    "    return line,\n",
    "\n",
    "# call the animator.  blit=True means only re-draw the parts that have changed.\n",
    "anim = animation.FuncAnimation(fig, animateFlash1D, init_func=init,\n",
    "                               frames=200, interval=1, blit=True);\n",
    "\n",
    "# call our new function to display the animation\n",
    "display_animation(anim)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Dimensionless time (Fourier number)\n",
    "### $$\\omega = \\frac{at}{L^2}$$\n",
    "For an ideal response curve in the Flash method:\n",
    "### $$\\omega_{0.5} = \\frac{at_{0.5}}{L^2}=0.1388$$\n",
    "From this equation we get for the thermal diffusivity $a$ the Parker's formula\n",
    "### $$a = 0.1388\\frac{L^2}{t_{0.5}}$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Making the Embedding Automatic"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can go a step further and use IPython's display hooks to automatically\n",
    "represent animation objects with the correct HTML.  We'll simply set the\n",
    "``_repr_html_`` member of the animation base class to our HTML converter\n",
    "function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "animation.Animation._repr_html_ = anim_to_html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now simply creating an animation will lead to it being automatically embedded\n",
    "in the notebook, without any further function calls:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "<matplotlib.animation.FuncAnimation at 0x2bbf00210b8>"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "animation.FuncAnimation(fig, animateFlash1D, init_func=init,\n",
    "                        frames=200, interval=10, blit=False)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So simple!  I hope you'll find this little hack useful!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "*This post was created entirely in IPython notebook.  Download the raw notebook*\n",
    "[*here*](http://jakevdp.github.io/downloads/notebooks/AnimationEmbedding.ipynb), *or see a static view on*\n",
    "[*nbviewer*](http://nbviewer.ipython.org/url/jakevdp.github.io/downloads/notebooks/AnimationEmbedding.ipynb)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.0"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
