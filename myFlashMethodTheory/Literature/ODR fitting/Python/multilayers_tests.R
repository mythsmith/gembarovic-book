# multilayer Example
require(multilayer)

# dimensions are in CGS units
time <- seq(-0.0, 0.025, length = 100)
td_theor=1.17
alpha     <- c(td_theor/10,td_theor/2,td_theor) # thermal diffusivities 
thickness <- c(0.05,0.04,0.02)
rho <- c(8,7,5)
cp <- c(0.7, 0.4, 0.5)
baseline <- c(-5)
deltaT <- c(3)
slope <- c(-0.0)

strt <- Sys.time()
# layers 123 -  irradiate the first layer
ys1 <- c(0)
ys1 <- ThreeLayer( time, thickness,cp,alpha,rho,baseline, deltaT,slope)

# reverse the sample layers 321 - irradiate the third layer
alpha2 <- c(td_theor,td_theor/2,td_theor/10) # thermal diffusivities 
thickness2 <- c(0.02,0.04,0.05)
rho2 <- c(5,7,8)
cp2 <- c(0.5, 0.4, 0.7)
ys2 <- c(0)
ys2 <- ThreeLayer( time, thickness2,cp2,alpha2,rho2,baseline, deltaT,slope)

print(Sys.time()-strt)
plot(time,ys1, col="blue", type="l",
     ylab = c("Temperature"), xlab=c("time (s)"))
lines(time, ys2, col="red")
grid()

# End 

# multilayer Example
require(multilayer)

# dimensions are in CGS units
time <- seq(-0.01, 0.25, length = 5000) # times in s
alpha     <- c(0.02,1.17) # thermal diffusivities in cm^2/s 
thickness <- c(0.02,0.1) # thicknesses (front, rear) layer in cm
rho <- c(1,1) # densities in g/cm^3
cp <- c(1,1) #  specific heats in J/g K
baseline <- c(0) # initial temperature level in C
deltaT <- c(1) # the adiabatic temperature rise in K
slope <- c(0.0) # baseline slope in K/s
hl <- c(0.00001,0.00001) # heat loss coeficients in W/(m^2 K)
tcr <- c(1.5) # thermal contact resistance in cm^2 K/W

strt <- Sys.time()
ys <- c(0)
ys <- TwoLayer( time, thickness,cp,alpha,rho,hl, tcr,baseline, deltaT,slope)
print(Sys.time()-strt)
plot(time,ys, col="blue", type="l",
     ylab = c("Temperature"), xlab=c("time (s)"))
grid()

# End 
