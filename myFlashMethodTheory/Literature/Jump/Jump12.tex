\documentclass{elsart}
%\documentclass{article}
\usepackage{graphicx}% Include figure files
\usepackage{subfig}
\usepackage{amsmath}

\usepackage{epstopdf}
% if you use PostScript figures in your article
% use the graphics package for simple commands
% \usepackage{graphics}
% or use the graphicx package for more complicated commands
% \usepackage{graphicx}
% or use the epsfig package if you prefer to use the old commands
% \usepackage{epsfig}

% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}
%\captionsetup{singlelinecheck=on}

\begin{document}

{\textbf{Notes on Negative Jumps in the Flash Method\\ (A Working Theory)}
\par
\textit{Jozef Gembarovic,(\today)}

\section{Introduction}

If the temperature response of a sample tested using the laser flash method [Parker] is measured on an opposite (rear) face of a disc shape sample using an IR detector, then the experimental temperature response signal during and immediately after the heat pulse is usually slightly distorted from a theoretically expected shape. Usually, there is a short positive spike during the pulse, followed by a slow recovery back to the baseline level before the pulse. There are many theories particular to the instrument and sample quality, used to explain the origins of such a disturbances, ranging from a direct electromagnetic interactions between the laser and detector electrical circuits, to a scattered laser radiation hitting the sample rear surface, the laser beam penetration, or mechanical effects. The positive spikes were studied in the laser flash literature for translucent samples with conduction and radiation heat transfer [*]. Due to its complexity and great variances with different instruments, the problem was rarely mentioned for opaque samples. Despite the deficiency, the experimenters involved in the flash method and the instrument makers paid a great deal of attention to this problem and they found that the effect of this type of disturbances can be relatively simply mitigated or almost eliminated by the design of the flash instrument (shielding the electronic circuits and preventing scattered light reaching the rear sample surface and IR detector) and also the experiment design by using thicker, better prepared samples, with highly emissive surfaces, for which the signal will have more time to recover from the disturbances. Commercial analytical software used for the thermal diffusivity calculations, will usually jump over the distorted region and use only portions of the experimental signal undisturbed by the heat pulse, with the baseline level prior the pulse.    

In this article, we will describe another type of disturbance, which may occur in a response signal from IR detector if the rear sample surface is of a low emissivity and also highly reflective. In this case, instead of a positive spike, the signal will during and after the pulse suddenly decrease to a level lower than the original baseline before the pulse. The signal will then only gradually recover to a pre-flash level and in the process will be overlapped with the actual rise due the sample temperature increase. If the positive disturbances can be relatively easily accounted for in the thermal diffusivity calculations, the negative ones are extremely difficult to deal with, and total error of simply ignoring them and use the baseline level before the flash for the calculation, is usually leading to errors in the thermal diffusivity determination, which are often multiples of the accepted levels.

Obvious solutions, like increasing the sample surface roughness and emissivity, by using a sandpaper, sand-blasting them, or spraying them with black paints, are not always applicable. In a case of testing shiny, highly polished samples, or molten metals, we can not alter the sample surfaces, so we have to deal with the negative jumps in the response signals, especially at higher temperatures. This problem, although witnessed by many flash method practitioners, was never described, nor discussed in existing literature dealing with the laser flash method.

\section{Instrument Description}
\label{sec:Instrument}

TA Instrument DLF2-EM1600 laser flash instrument was used in our experiments. ... description, see brochure .... Furnace, measurement chamber and IR detector schematics, is shown in Figure \ref{fig:Furnace}. A detail view of the sample holder is in Figure \ref{fig:Holder}. The holder is made from a translucent Alumina material.   

\begin{figure}
\begin{center}
\includegraphics[scale=0.5]{FurnaceInside.eps}
\caption{\label{fig:Furnace} Furnace and Detector Schematics.}
\end{center}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[scale=0.3]{SampleHolderLaser2.eps}
\caption{\label{fig:Holder} Sample and Holder Detail View.}
\end{center}
\end{figure}


\section{Experimental Evidence}
\label{sec:ExperEvid}

\begin{figure}[htb]
\
\centering
  \subfloat[]{%
    \includegraphics[width=.6\textwidth]{4max.eps}}\\
  \subfloat[]{%
    \includegraphics[width=.6\textwidth]{4min.eps}}
  \caption{Signals from Samples with Different Rear Surfaces }\label{fig:JumpEvol4}
\end{figure}
    
%\begin{figure}
%\begin{center}
%\includegraphics[scale=0.3]{4max.eps}
%\caption{\label{fig:4curves} Temperature rise for different samples.}
%\end{center}
%\end{figure}
%\begin{figure}
%\begin{center}
%\includegraphics[scale=0.4]{4min.eps}
%\caption{\label{fig:4curves} Temperature rise Time Detail.}
%\end{center}
%\end{figure}


\begin{figure}[htb]
\
\centering
  \subfloat[Coarse]{%
    \includegraphics[width=.33\textwidth]{sp220.eps}}\hfill
  \subfloat[Fine]{%
    \includegraphics[width=.33\textwidth]{sp1000.eps}}\hfill
  \subfloat[Lapped]{%
    \includegraphics[width=.33\textwidth]{l.eps}}\\

  \subfloat[]{%
    \includegraphics[width=.33\textwidth]{sp220N.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.33\textwidth]{sp1000N.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.33\textwidth]{lN.eps}}\\
  
  \caption{A Comparison of IR Signals from Samples with Different Rear Surfaces }\label{fig:JumpEvol}
\end{figure}
An evolution of negative jumps in IR signals of samples with different rear surface finish is in Figure \ref{fig:JumpEvol} (Job 1285MOht, 2nd level). Four molybdenum samples (diameter 1.27 cm,, thickness $L \approx$ 3 mm) were tested using the flash method at temperature close to 390~$^{\circ}$C, in DLF2-EM1600 instrument. A liquid nitrogen cooled, InSb IR detector (InfraRed Associates, Inc.), with the spectral detectivity ranging from 1~$\mu$m to 6~$\mu$m, maximum at 5~$\mu$m wavelength, was used to generate the response curves. The detector signal of sample \textbf{MOsb}, with a sand-blasted surface (the roughest one), is compared with signals from: (a) sample \textbf{MOsp220}, treated with a (US CAMI) grade 220 sand paper, (b) a finer, grade 1000 sandpaper \textbf{MOsp1000}, and finally (c) with a signal from sample \textbf{MOl}, with a mirror-like, lapped surface. Time Detail graphs reveal time development of the signal during the laser pulse which is generated using Nd:glass laser, with wavelength 1.06 $\mu$m, and duration 400 $\mu$s. Immediately after the laser pulse started ($t=0$~s), the signals from smoother surfaces are first rising, but not as high as for the sand-blasted surface, and then falling down, reaching a minimum in around $t=$1~ms, which is lower than the baseline level before the pulse.

Relative shapes of the signals are compared in Figure \ref{fig:JumpEvol}(c)-(d), where correspondent normalized signals are plotted against dimensionless time, the Fourier number. The normalized signal $V_N$ is calculated from the signal $V$ as $V_N=(V-V_0)/(V_{max}-V_0)$, where $V_0$ is the signal (baseline) level before the flash, and $V_{max}$ is the signal maximum after the flash. The dimensionless time is calculated as $Fo=at/L^2$, where $a$ is the thermal diffusivity (set for a simplicity to 1 cm$^2$/s), $t$ is the time in s, and $L$ is the sample thickness in cm. It can be seen from Figure \ref{fig:JumpEvol}, the negative jump magnitude depends on the roughness of the sample rear surface and for shiny mirror-like surfaces the jump magnitude is comparable with the signal maximum rise, $\Delta V_{max}(=V_{max}-V_0)$, after the flash. 

\begin{figure}[htb]
\
\centering
 \captionsetup[subfigure]{labelformat=empty}
  \subfloat[]{%
    \includegraphics[width=.32\textwidth]{MOsb_a_0.eps}} \\
  \subfloat[]{%
    \includegraphics[width=.32\textwidth]{MOsp220_a_0.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.32\textwidth]{MOsp1000_a_0.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.32\textwidth]{MOl_a_0.eps}}\\
    
  \subfloat[]{%
    \includegraphics[width=.32\textwidth]{MOsp220_a_min.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.32\textwidth]{MOsp1000_a_min.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.32\textwidth]{MOl_a_min.eps}}\\
   \hfill
  \subfloat[]{%
    \includegraphics[width=.32\textwidth]{MOsp1000_a_mid.eps}}\enspace
  \subfloat[]{%
    \includegraphics[width=.32\textwidth]{MOl_a_mid.eps}}\\
  
  \caption{A Comparison of Thermal Diffusivity Calculations }\label{fig:AlphaAnalysis}
\end{figure}

 Signals from four samples with different surfaces, shown in Figure \ref{fig:JumpEvol}, are analyzed in Figure \ref{fig:AlphaAnalysis} and the thermal diffusivities are calculated. As we can see, for the samples with smooth, shiny rear surfaces, the calculated thermal diffusivities are close to the expected value  only if the baseline is 'moved' down to a level between the original baseline value before the pulse and the minimum value after the pulse. Theoretical curves (Theory) were calculated using the Two Dimensional model (Equation (\ref{e:2Dintegral}) from Appendix II). 
 
\begin{figure}[htb]
\
\centering
  \subfloat[190 $^{\circ}$C]{%
    \includegraphics[width=.33\textwidth]{1295SSFeShiny18.eps}}\hfill
  \subfloat[290 $^{\circ}$C]{%
    \includegraphics[width=.33\textwidth]{1295SSFeShiny20.eps}}\hfill
  \subfloat[392 $^{\circ}$C]{%
    \includegraphics[width=.33\textwidth]{1295SSFeShiny22.eps}}\\

  \subfloat[]{%
    \includegraphics[width=.33\textwidth]{1295SSFeShiny17N.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.33\textwidth]{1295SSFeShiny21N.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.33\textwidth]{1295SSFeShiny23N.eps}}\\
  
  \caption{An Evolution of Negative Jumps with Temperature }\label{fig:JumpEvolTemp}
\end{figure}

An evolution of negative jumps with the ambient temperature is given in Figure~\ref{fig:JumpEvolTemp}. Two stainless steel samples, (\textbf{SS94l} and \textbf{SS91lg}, diameter 1.27 cm, thickness 0.1556 cm and 0.1526 cm, mass 1.5328 g and 1.5064 g, respectively) were tested at temperatures 190 $^{\circ}$C, 290 $^{\circ}$C and 392 $^{\circ}$C. Both samples have the same high emissivity front surfaces (sand-blasted and sprayed with a thin layer of a Graphite lubricant), while rear surfaces of both samples were highly polished (lapped). The rear surface of sample \textbf{SS91lg} was additionally sputtered with a very thin layer of gold. 
The experiments were conducted in an inert gas (pure N$_2$), at normal pressure and the sample surface emissivities were not changed within this temperature range. 

As can be seen from Figure~\ref{fig:JumpEvolTemp}(a), the signal maximum rise, $\Delta V_{max}$, for sample \textbf{SS94l}, is 5 times bigger than for sample \textbf{SS91lg}, due to a difference in the emissivity of the rear surfaces. The emissivity of gold sputtered surface of sample \textbf{SS91lg} is lower than the emissivity of sample \textbf{SS94l}. As can be seen from the Time Detail graph, despite of differences in the initial phase, the signal minimum after the flash $\Delta V_{min} (=V_{min} - V_0)$ is about the same for both samples. It indicates that the rear surface roughness, not the emissivity, is affecting the jump's magnitude.
 
The normalized signals (Figure \ref{fig:JumpEvolTemp}(d)) for these two samples, are very much different. The curve of gold sputtered sample \textbf{SS91lg} is concave for time $t >$~1~ms, without an inflection point, characteristic for a response curve due to a diffusion of heat through the sample in the flash method. The curve resembles a response curve of two capacitance layers with a thermal contact resistance between them (see Figure \ref{fig:T2vsTime} in Appendix).

Although the $\Delta V_{max}$ values are changing at higher temperatures, as the amplification in the detector circuit is switched from a high to a low gain, the signal minimum $\Delta V_{min}$ values remain the same for both samples. The initial signal rise during the heat pulse is less pronounced and both signals are converging at higher temperatures (see Time Detail graphs in Figure \ref{fig:JumpEvolTemp}(a)-(c)). The normalized signals (Figure \ref{fig:JumpEvolTemp}(d)-(f)) also remain approximately the same for all tested temperature levels.

\begin{figure}[htb]
\centering
  \subfloat[]{%
    \includegraphics[width=.45\textwidth]{1282MOht26.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.45\textwidth]{1282MOht28.eps}}\\
  \caption{Gas Influence on Negative Jumps}\label{fig:JumpGas}
\end{figure}

Purge gas influence on negative jumps is illustrated in Figure \ref{fig:JumpGas}, where molybdenum sample \textbf{MOl}, with a shiny, mirror-like (lapped) rear surface, was first tested at temperature  995.6~$^{\circ}$C, while purged with nitrogen gas, then at 1011.6~$^{\circ}$C, in vacuum $p=50$ mtorr, and finally at 1019.5~$^{\circ}$C, while purged with argon gas. As we can see from Figure \ref{fig:JumpGas}(a), where the signal from the nitrogen purged sample is compared with the signal acquired in vacuum, the amplitude of the negative jump, $\Delta V_{min}$, is practically independent of the ambient gas pressure. The only difference is in the cooling portions of the curves, where the sample purged with the gas is exhibiting relatively bigger heat losses. 

The negative jumps are the same also for different purge gases, as shown in Figure \ref{fig:JumpGas}(b), where signals from the nitrogen purged sample, at 995.6 $^{\circ}$C,  is compared with the signal from the argon purged sample tested at 1019.5 $^{\circ}$C. The are no significant differences, neither in the amplitude ($\Delta V_{min}$), nor the shape of the negative jumps. The argon purge seems to create bigger signal fluctuations, best visible in the cooling portions of the response curves. 

\begin{figure}[htb]
\
\centering
  \subfloat[193 $^{\circ}$C]{%
    \includegraphics[width=.33\textwidth]{13050.eps}}\hfill
  \subfloat[293 $^{\circ}$C]{%
    \includegraphics[width=.33\textwidth]{13052.eps}}\hfill
  \subfloat[394 $^{\circ}$C]{%
    \includegraphics[width=.33\textwidth]{13054.eps}}\\

  \subfloat[]{%
    \includegraphics[width=.33\textwidth]{13051N.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.33\textwidth]{13053N.eps}}\hfill
  \subfloat[]{%
    \includegraphics[width=.33\textwidth]{13055N.eps}}\\
  
  \caption{An Evolution of Negative Jumps with Temperature for Two Different Sample Thicknesses }\label{fig:JumpEvolTempThickness}
\end{figure}

IR detector response signals from two molybdenum samples, \textbf{MOl5} and \textbf{Thin-MO1l5}, with dimensions and masses given in Table \ref{tab:thick_thin}, are compared in Figure \ref{fig:JumpEvolTempThickness}. (A lower gain amplification is used at temperature 394 $^{\circ}$C.)As we can see from Table \ref{tab:thick_thin},  while $\Delta V_{max}$ ratio is (as expected) closely following mass ratio of the two samples ($m_1/m_2$ = 1.2038) for all three temperature levels,  ratio  $\Delta V_{max}$ remains constant at around 2.05. It means, that normalized curves for sample 
\textbf{Thin-MO1l5} are showing more pronounced negative jump levels as the temperature increases. Ratio $|\Delta V_{min}/\Delta V_{max}|$ for both samples is rising 27\% for every 100~$^{\circ}$C, but thinner sample \textbf{Thin-MO1l5} started from a higher ratio value (0.6676) at 193~$^{\circ}$C. Practically it means that the error in the thermal diffusivity calculation due to the negative jump is increasing with the temperature, if the surface quality remains the same. In higher temperatures ($T > 500~^\circ $C) sample surface quality is usually changed due to oxidation and other chemical reactions. These changes can lead to a different relationship between the extremes of IR signal. 

Reflection pattern for the laser light differs for different sample thicknesses due to sample holder construction (see Figure \ref{fig:Holder}). The amount the heat accumulated at the sample front surface is the same for both thin and thick samples, but the reflected laser light can reach the region below the holder with a higher probability if reflected from a thinner sample than from a thicker one.

\begin{table}[b]
%\centering % used for centering table
\caption{Thick and Thin Sample Comparison} % title of Table
\begin{tabular}{c c c c c c c c} % centered columns (7 columns)
\hline\hline %inserts double horizontal lines
Sample ID & $L$ [cm] & Mass [g] & $T$ [$^{\circ}$C] & $\Delta V_{max}$ [V] & $\Delta V_{min}$ [V] & $|\Delta V_{min}/\Delta V_{max}|$ \\[0.5ex] 
\hline % inserts single horizontal line
MOl5 		& 0.2985  & 3.8582 & 192.9 & 0.1838	& -0.0709 & 0.3857\\ % inserting body of the table
	 		&	 	  &        & 293.1 & 0.4969	& -0.2449 & 0.4929\\ 
	 		&	 	  &        & 394.0 & 0.1927	& -0.1211 & 0.6284\\ [0.5ex]
Thin-MO1l5 	& 0.2456  &	3.2050 & 193.6 & 0.2136	& -0.1426 & 0.6676\\ 	
	 		&	 	  &        & 293.3 & 0.5929	& -0.5060 & 0.8534\\ 
	 		&	 	  &        & 394.0 & 0.2355	& -0.2493 & 1.0586\\[1ex] % [1ex] adds vertical space
\hline %inserts single line
\end{tabular}
\label{tab:thick_thin} % is used to refer this table in the text
\end{table}

If the front sample surface is blackened with Graphite lubricant spray, than both positive and negative jumps are bigger than for those for samples with not blackened front surface. (see eps files from 1307MOlt job from EM1600 No.1, 11/18/2015).

\clearpage

\section*{APPENDIX - Two Capacitance Layers}
\label{CC}

If the total thermal conductance of two layers is much higher than the thermal contact conductance between them, then a simplified (Two Capacitance Layers) model can be used to describe the temperature distribution in a flash method experiment, when one side of the two layer sample is suddenly heated with an instantaneous, homogeneous heat pulse, and the temperature response is measured at the opposite side of the sample.

 In general, if the two layers have the densities
 $\rho_1$ and $\rho_2$, the thicknesses $L_1$ and $L_2$, the specific heat $c_1$ and
 $c_2$, and the same cross section area, then the amount of heat flowing in time $dt$ through a unit area of the contact can be expressed as

\begin{equation}\label{e:dq1}
L_1\rho_1c_1dT_1=h(T_2-T_1)dt,
\end{equation}

\begin{equation}\label{e:dq2}
L_2\rho_2c_2dT_2=-h(T_2-T_1)dt,
\end{equation}

 where $T_1=T_1(t)$ and $T_2=T_2(t)$ are the temperatures of the first and second layer at time $t$, and $h$ is the thermal contact conductance.
 \par
From Eqs.(\ref{e:dq1}) and (\ref{e:dq2}), for the temperature
difference we have
\begin{equation}\label{e:dq1-2}
dT_2-dT_1=-h(T_2-T_1)\left[\frac{1}{L_1\rho_1c_1}+\frac{1}{L_2\rho_2c_2}\right]dt.
\end{equation}
After the substitution
\begin{equation}\label{e:substy}
    y=T_2-T_1,      dy=dT_2-dT_1,
\end{equation}
Eq.(\ref{e:dq1-2}) became
\begin{equation}\label{e:dy}
    \frac{dy}{dt}=-\vartheta y,
\end{equation}
where
\begin{equation}\label{e:k}
    \vartheta=h\left[\frac{1}{L_1\rho_1c_1}+\frac{1}{L_2\rho_2c_2}\right].
\end{equation}
A general solution of Eq.(\ref{e:dy}) is
\begin{equation}\label{e:gensoldy}
    y=c_1e^{-\vartheta t}+c_0.
\end{equation}
The temperature difference between two layers is
\begin{equation}\label{e:gensolT21}
    T_2(t)-T_1(t)=c_1e^{-\vartheta t}+c_0,
\end{equation}
where $c_0$ and $c_1$ are constants independent of time.
\par
For the individual temperatures $T_1$ and $T_2$ in a thermally
insulated system we have
\begin{equation}\label{e:gensolT1}
    T_1(t)=k_1e^{-\vartheta t}+T_s
\end{equation}
and
\begin{equation}\label{e:gensolT2}
    T_2(t)=k_2e^{-\vartheta t}+T_s,
\end{equation}
where $k_1$ and $k_2$ are time-independent constants, determined
from the boundary and initial conditions, and $T_s$ is the steady
state temperature for $t\rightarrow \infty$.

From the condition $T_2(0)=0$ it follows that $k_2=-T_s$. Equation
(\ref{e:gensolT2}) is then
\begin{equation}\label{e:T2}
    T_2(t)=T_s-T_s e^{-h\left(\frac{1}{L_1\rho_1c_1}+\frac{1}{L_2\rho_2c_2} \right) t}
\end{equation}
For a normalized temperature rise of the second layer we have:
\begin{equation}\label{e:T2N}
    \frac{T_2(t)}{T_s}= 1 - e^{-h\left(\frac{1}{L_1\rho_1c_1}+\frac{1}{L_2\rho_2c_2} \right)
    t}.
\end{equation}
If $L_1\rho_1c_1=L_2\rho_2c_2=L\rho c$, then Eq. (\ref{e:T2N}) is
simply
\begin{equation}\label{e:T2Ns}
    \frac{T_2(t)}{T_s}= 1 - e^{-\frac{2ht}{L\rho c}}.
\end{equation}
Temperature rise of the rear layer as a function of time is plotted in
Figure \ref{fig:T2vsTime}.
\par

\begin{figure}
\begin{center}
\includegraphics[scale=0.6]{LumpBodies.eps}
\caption{\label{fig:T2vsTime} Temperature rise of the second layer in the Two Capacitance Layers model.}
\end{center}
\end{figure}

\clearpage




\clearpage

\section*{APPENDIX II - Two Dimensional Model of the Flash Method}
\label{ATheory}

Temperature in a two-dimensional model in the flash method [Watt], when sample is a circular cylinder with radius $R$, thickness $L$, and thermal diffusivity $\alpha$, with the heat source distribution is described as a composite of axial, radial and time distribution functions,
\begin{equation}
\label{e:sourceDistr}
q(z',r',t')=\frac{Q}{\rho c_pL}f(z')g(r')\psi(t'),
\end{equation}
then the temperature of the sample is 
\begin{multline}
\label{e:2DTemperature}
T(z,r,t)= \frac{Q}{\rho c_pL} \sum_{n=1}^{\infty}Y_n(z) \int_{0}^{L}f(z')Y_n(z')dz'
\frac{2}{R^2}\sum_{i=1}^{\infty} \frac{\gamma_i^2 \mathrm{J}_0(\gamma_ir/R)}{[\gamma_i^2 + B_r^2]\mathrm{J}_0^2(\gamma_i)} \times\\
 \times \int_{0}^{R}r'g(r')\mathrm{J}_0(\gamma_ir'/R)dr' \int_{0}^{t}\psi(t')\exp{\big[-\alpha \big(\frac{\gamma_i^2}{R^2} + \frac{\eta_n^2}{L^2}\big)(t-t')\big]}dt',
\end{multline}
where
\begin{equation}
\label{e:Ycomponent}
Y_n(z)=\frac{\sqrt{2(\eta_n^2+B_L^2)}[\eta_n \cos(\eta_nz/L)+B_0\sin(\eta_nz/L)]}{\sqrt{L[(\eta_n^2 +B_0)(\eta_n^2+ B_L^2+B_L)+B_0(\eta_n^2+B_L^2)]}},
\end{equation}
and $B_0$, $B_L$, $B_r$, are the Biot numbers for front, rear, and lateral surfaces, resp.

Eigenvalues $\eta$ and $\gamma$ are positive roots of 
\begin{equation}
\label{e:etas}
\tan(\eta)-\frac{\eta (B_0 + B_L)}{\eta^2 - B_0 B_L} = 0,
\end{equation} 
\begin{equation}
\label{e:gamas}
\mathrm{J}_1(\gamma)-B_r \mathrm{J}_0(\gamma) = 0.
\end{equation} 
In our analysis, the heat source distribution was approximated as
 \begin{equation}
\label{e:actualsourceDistr}
q(z',r',t')=\frac{Q}{\rho c_pL} \delta (z')\bigg[\frac{H(r') - H(R_b - r')}{R_b}\bigg] \bigg[ \frac{H(t')-H(\tau-t')}{\tau }\bigg],
\end{equation}
where $\delta(x)$ is the Dirac delta function, $H(x)$ is the Heaviside step function, $R_b$ is the laser beam radius, and $\tau$ is the laser pulse duration.

The actual rear face ($z=L$) temperature response was recorded by an IR detector over a concentric circular area of radius $R_v$, therefore the signal is 
 
\begin{equation}
\label{e:2Dintegral}
T_d(L,t)=2\int_{0}^{R_v}r'T(L,r',t)dr'/R_v^2.
\end{equation}


\end{document}
