########################################################
##
##               inplanePics. R 
##          Figures for In-Plane Chapter
##
##      Jozef Gembarovic (c)  9/29/2016 
##
########################################################

# myFlash2 In Plane 
require(myFlash2)

# dimensions are in CGS units
time <- seq(-0.1, 2, length = 500)
Nmax <- c(80)
alpha     <- c(1.17) # thermal diffusivity of Copper 
m <- c(1.0)
R <- c(1.27)
IRI <- c(R*0.8)
IRO <- c(R*0.9)
VRI <- c(R*0.0)
VRO <- c(R*0.4)
baseline <- c(0)
deltaT <- c(1)
slope <- c(0.0)


# multiple responses for different m values
m <- c(0.01, 0.4, 0.7, 1.0)     

yipm <- sapply(m, InPlane , time=time, Nmax=Nmax, alpha=alpha, 
               R=R, IRI=IRI, IRO=IRO, 
               VRI=VRI,VRO=VRO, T0=baseline, 
               Tmax=deltaT)


dfoz <- data.frame(time,yipm)
names(dfoz) <- c("time", "yipm1","yipm2","yipm3","yipm4")

# responses for different tp
d <- expression(~~italic(m))
x0 <- c(1.7)
ggplot(dfoz) +xlab("Fo") + ylab("V") +
  geom_line(aes(x=time, y=yipm1,colour = "0.01"),linetype=1) + 
  geom_line(aes(x=time, y=yipm2,colour = "0.4"),linetype=1) +
  geom_line(aes(x=time, y=yipm3,colour = "0.7"),linetype=1)+ 
  geom_line(aes(x=time, y=yipm4,colour = "1.0"),linetype=1)+
  scale_x_continuous(expand=c(0,0), limits=range(dfoz$time)) +
  scale_colour_manual(d, 
                      breaks = c("0.01","0.4","0.7","1.0"),
                      values = c("tomato","lightgoldenrod3", "darkseagreen", 
                                 "lightskyblue")) +
  guides(color=guide_legend(override.aes=list(shape=c(NA,NA,NA,NA),
                                              linetype=c(1,1,1,1))))+
  annotate("text",label="italic(R)[o] == 0.9", parse=TRUE, x=x0, y=0.9, size=4)+
  annotate("text",label="italic(R)[i] == 0.8", parse=TRUE, x=x0, y=0.8, size=4)+ 
  annotate("text",label="italic(R)[v] == 0.4", parse=TRUE, x=x0, y=0.6, size=4)
  
# responses for different VRO
# multiple responses for different Viewed Radii
m <- c(0.7)
time2 <- seq(-0.1, 0.61, length = 300)
VRO <- c(R*0.001, R*0.3, R*0.4, R*0.5, R*0.6)         
# exponetial pulse pd/2
yipVRO <- sapply(VRO, InPlane , time=time2, Nmax=Nmax, m=m, alpha=alpha, 
                 R=R, IRI=IRI, IRO=IRO, 
                 VRI=VRI, T0=baseline, 
                 Tmax=deltaT)

dfozVRO <- data.frame(time2,yipVRO)
names(dfozVRO) <- c("time", "yipVRO1","yipVRO2","yipVRO3","yipVRO4","yipVRO5")

d <- expression(~~italic(R)[v])
x0 <- c(0.5)
ggplot(dfozVRO) +xlab("Fo") + ylab("V") +
  geom_line(aes(x=time, y=yipVRO1,colour = "0.001"),linetype=1) + 
  geom_line(aes(x=time, y=yipVRO2,colour = "0.3"),linetype=1) +
  geom_line(aes(x=time, y=yipVRO3,colour = "0.4"),linetype=1)+ 
  geom_line(aes(x=time, y=yipVRO4,colour = "0.5"),linetype=1)+
  geom_line(aes(x=time, y=yipVRO5,colour = "0.6"),linetype=1)+
  scale_x_continuous(expand=c(0,0), limits=range(dfozVRO$time)) +
  scale_colour_manual(d, 
                      breaks = c("0.001","0.3","0.4","0.5","0.6"),
                      values = c("tomato","lightgoldenrod3", "darkseagreen", 
                                 "lightskyblue","lightsalmon")) + 
  guides(color=guide_legend(override.aes=list(shape=c(NA,NA,NA,NA, NA),
                                              linetype=c(1,1,1,1,1))))+
  annotate("text",label="italic(R)[o] == 0.9", parse=TRUE, x=x0, y=0.5, size=4)+
  annotate("text",label="italic(R)[i] == 0.8", parse=TRUE, x=x0, y=0.4, size=4)+ 
  annotate("text",label="italic(m) == 0.7", parse=TRUE, x=x0, y=0.3, size=4)
