############################################################################
# Flash Response with Heat Losses  Hot Tea Problem.R              
# (c)  Jozef Gembarovic  7/7/2016
# 
# graphs for heat loss correction in the flash method
#
############################################################################

library(ggplot2)
require(myFlash2)

## define values of parameters (CGS units)
# dimensions are in CGS units
time <- seq(-0.0001, 0.3, length = 5000)
Nmax <- c(80) # how many terms are taken into the calculation in Fourier series
# cp*rho remains constant
thickness <- c(0.2)
pd <- c(0.000) # zero pulse duration
b1 <- c(0.1) # Biot number
R <- c(0.635)
IRI <- c(0)
IRO <- c(R*1)
VRI <- c(0)
VRO <- c(R*0.5)
baseline <- c(0)
deltaT <- c(1)
slope <- c(0.0)


# multiple responses for different thermal diffusivities and Biot numbers 
alpha <- c(1.0,0.5,0.25,0.125,0.06125)   
b1 <- b1/alpha

alpha # the thermal diffusivity
b1 # the Biot number 

ally <- flash2DHL(time=time, Nmax=Nmax,alpha=alpha[1], 
                  thickness=thickness,
                  pd=pd, b1=b1[1], R=R, IRI=IRI, IRO=IRO, 
                  VRI=VRI,VRO=VRO, baseline=baseline,
                  deltaT=deltaT, slope=slope)

for (i in 2:length(alpha)) {
  y <- flash2DHL(time=time, Nmax=Nmax,alpha=alpha[i], 
                  thickness=thickness,
                  pd=pd, b1=b1[i], R=R, IRI=IRI, IRO=IRO, 
                  VRI=VRI,VRO=VRO, baseline=baseline,
                  deltaT=deltaT, slope=slope)
  ally <- cbind(ally,y)
}



plot(time,ally[,1], type = "l", col="red", ylab = "T")
lines(time,ally[,2], col="green")
lines(time,ally[,3], col="blue")
lines(time,ally[,4], col="orange")
lines(time,ally[,5], col="plum1")
legend("topright",                       # x-y coordinates for location of the legend  
       legend=c(alpha),      # Legend labels  
       col=c("red", "green", "blue", "orange","plum1"),   # Color of points or lines  
       #       pch=c(21,19,19),                 # Point type  
      # lty=c(1,2,4),                    # Line type  
       lwd=c(1,1,1),#,                    # Line width  
      title="alpha")             # Legend title  
grid()

# calculate max rise
mx <- max(ally[,1])
for (i in 2:length(alpha)) {
  nmx <-  max(ally[,i])
  mx <- cbind(mx,nmx)
}
names(mx) <- b1
mx

