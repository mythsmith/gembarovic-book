{ MAINREAD.PRC    (FOURIER)         }

procedure Initialize(var NumPoints : integer;
                     var XReal     : TNvectorPtr;
                     var XImag     : TNvectorPtr;
                     var YReal     : TNvectorPtr;
                     var HReal     : TNvectorPtr;
                     var HImag     : TNvectorPtr;
                     var Error     : byte);

{----------------------------------------------------------}
{- Output: NumPoints, XReal, XImag, HReal, HImag, Error   -}
{-                                                        -}
{- This procedure initializes the above variables to zero -}
{----------------------------------------------------------}

begin
  NumPoints := 0;
  New(XReal);
  New(XImag);
  New(YReal);
  New(HReal);
  New(HImag);
  New(SampName);
  New(SampThick);
  FillChar(XReal^, SizeOf(XReal^), 0);
  FillChar(XImag^, SizeOf(XImag^), 0);
  FillChar(YReal^, SizeOf(YReal^), 0);
  FillChar(HReal^, SizeOf(HReal^), 0);
  FillChar(HImag^, SizeOf(HImag^), 0);
  FillChar(SampName^, SizeOf(SampName^), ' ');
   FillChar(SampThick^, SizeOf(SampThick^), 0);
  Error := 0;
end; { procedure Initialize }

procedure IOCheck;
var
  IOcode : integer;

procedure Error(Msg : Lines);
begin
  Writeln;
 { Beep;}
  Writeln(Msg);
  Writeln;
end; { procedure Error }

begin { procedure IOCheck }
  IOcode := IOresult;
  IOerr := IOcode <> 0;
  if IOerr then
    case IOcode of
      2   : Error('File not found.');
      3   : Error('Path not found.');
      4   : Error('Too many open files.');
      5   : Error('File access denied.');
      6   : Error('Invalid file handle.');
      12  : Error('Invalid file access code.');
      15  : Error('Invalid drive number.');
      16  : Error('Cannot remove current directory.');
      17  : Error('Cannot rename across drives.');
      100 : Error('Disk read error.');
      101 : Error('Disk write error.');
      102 : Error('File not assigned.');
      103 : Error('File not open.');
      104 : Error('File not open for input.');
      105 : Error('File not open for output.');
      106 : Error('Invalid numeric format.');
      150 : Error('Disk is write-protected.');
      151 : Error('Unknown unit.');
      152 : Error('Drive not ready.');
      153 : Error('Unknown command.');
      154 : Error('CRC error in data.');
      155 : Error('Bad drive request structure length.');
      156 : Error('Disk seek error.');
      157 : Error('Unknown media type.');
      158 : Error('Sector not found.');
      159 : Error('Printer out of paper.');
      160 : Error('Device write fault.');
      161 : Error('Device read fault.');
      162 : Error('Hardware failure.');
    else
      begin
        Writeln;
{        Writeln(Bell);}
        Writeln('Unidentified error message = ', IOcode, '. See manual.');
        Writeln;
      end;
    end; { case }
end; { procedure IOCheck }


procedure GetRealVectorFromFile(var NumPoints : integer;
                                var SampName  : StrPtr;
                                var SampThick : SinglePtr;
                                var XReal     : TNvectorPtr;
                                var YReal     : TNvectorPtr
                                );

{-----------------------------------------------}
{- Output: NumPoints, X                        -}
{-                                             -}
{- This procedure reads in a real vector of    -}
{- data points from a data file.               -}
{-----------------------------------------------}

var
{  FileName : string[255];}
  InFile : text;
  DirInfo: SearchRec;

begin
  Writeln;
  repeat
 {   FileName:='s03.dat';}
 
    begin
      FindFirst('*.dat', Archive, DirInfo);
      writeln;
      writeln(' Data files in the current directory :');
       while DosError = 0 do
        begin
          WriteLn('                                          '+DirInfo.Name);
          FindNext(DirInfo);
        end;
    end;

    Write('File name? ');
    Readln(FileName);
    Assign(InFile, FileName);
{$I-}    Reset(InFile); {$I+}
    IOCheck;
  until not IOerr ;
  NumPoints := 0;
  Readln(InFile, SampName^);
  Readln(InFile, SampThick^);
     SampThick^:=SampThick^*0.01;
  while not EOF(InFile)  do
  begin
    Readln(InFile, XReal^[NumPoints], YReal^[NumPoints]);
    NumPoints := Succ(NumPoints);
    IOCheck;
  end;
  Close(InFile);
end; { procedure GetRealVectorFromFile }

