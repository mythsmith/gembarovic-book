###########################
##
## 3P_CJ_read_analyze_real_data.R 
## analyze response curves in 3 P method 
##
##
## (c) Jozef Gembarovic 4/24/2017
##
###########################
library(ggplot2)

# read data from a file
setwd("C:/Users/Jozef/Documents/mybook/myFlashMethodTheory/Literature/3P method")
inFile <- "Data/Saffil_32_N2_760torr_22.lvm"

f0 <- inFile

Z <- matrix(scan(f0, skip=23), ncol=4, byrow=T)

startRow <- 1
time <- Z[,1]
btime <- time[time<10]

T_F <- Z[,2]
T_M <- Z[,3]
T_R <- Z[,4]

T_Fb <- T_F[1:(which.max(btime))]
T_Mb <- T_M[1:(which.max(btime))]
T_Rb <- T_R[1:(which.max(btime))]

# baseline at T_F initial level
#T_F0 <- T_F
#T_M0 <- T_M-(-mean(T_Fb)+mean(T_Mb))
#T_R0 <- T_R-(-mean(T_Fb)+mean(T_Rb))

# baseline level at Zero
imax <- 100
T_F0 <- T_F[1:imax] - mean(T_Fb)
T_M0 <- T_M[1:imax] - mean(T_Mb)
T_R0 <- T_R[1:imax] - mean(T_Rb)

plot(time[1:imax], T_F0, "l", col="red")
lines(time[1:imax], T_M0, col="green")
lines(time[1:imax],T_R0, col="blue")
grid()

datF <- data.frame(
  x = time[1:imax],
  y = T_F0
)

datR <- data.frame(
  x = time[1:imax],
  y = T_R0
)

#loessDataF <- data.frame(
#  x = time[1:imax],
#  y = predict(loess(y~x, datF, span=0.1)),
#  method = "loess()"
#)

#loessDataR <- data.frame(
#  x = time[1:imax],
#  y = predict(loess(y~x, datR, span=0.1)),
#  method = "loess()"
#)

#splineDataF <- data.frame(
#  with(datF, 
#      spline(x, y, xout = seq(time[1],time[2], length.out = 100))
#  ),
# method = "spline()"
#)
#plot(time[1:imax], T_F0, "p",pch=5, cex=0.2, col="red")
#lines(time[1:imax], loessDataF$y, col="green")
#lines(splineData$x, splineData$y, col="tomato")
#points(time[1:imax], T_R0, pch=5, cex=0.2, col="blue")
#lines(time[1:imax], loessDataR$y, col="skyblue")

a <-  0.002  # thermal diffusivity
L <- 0.6968  # thickness

### boundary functions ############################################
f0 <- function(t){
  splineDataF <- data.frame(
    with(datF, 
         spline(x, y, xout = seq(0,max(t), length.out  = length(t)))
    ),
    method = "spline()"
  )
  return(splineDataF$y[length(t)])
}

f1 <- function(t){
  splineDataR <- data.frame(
    with(datR, 
         spline(x, y, xout = seq(0,max(t), length.out  = length(t)))
    ),
    method = "spline()"
  )
  return(splineDataR$y[length(t)])
}
####################################################################

# Analytical Formula CJ p. 103
####################################################################
integrand <- function(x,n,t) {
  exp(-n^2*pi^2*a/L^2*(t - x))*(f1(x) - f0(x)*(-1)^n)
}

w <- function(x, L, cas, Nmax){
  integr <- 0
  for(j in 1:Nmax){
    temp <- integrate(integrand,lower=0, upper=cas, n=j, t=cas, abs.tol = 0.001, 
                       stop.on.error=F)
    mtemp <- temp$value*j*sin(j*pi*x/L)
    integr <- integr+mtemp
  }
  return(integr*2*a*pi/L^2)
}

wv <- Vectorize(w, vectorize.args="cas") 
####################################################################

cas <- time[1:imax]
L <- 0.6968
y <- 0.3216

# calculate vector of response values for different time
V <- wv(y, L, cas, Nmax=40)

plot(cas,T_F0)
lines(cas, V, col="blue")
lines(cas, T_R0)
lines(cas, T_M0, col="red")

k <- rep("CJ",imax)
dfA <- data.frame(cas,V, k)
colnames(dfA) <- c("time","Temperature","k")
dfAll <- rbind(dfAll, dfA)



# plot All CJ = Carslaw Jaeger p. 103 analytical formula
# 1 and 40 are boundary temperatures
#########################################################################
ggplot (data = dfAll, aes(x=time, y=Temperature, colour=k))+geom_line()+
  theme_bw() +
  scale_x_continuous(expand=c(0,0), limits=range(0,max(cas))) +
  labs( x ="t (s)", y = "T")
############### The End #################################################

