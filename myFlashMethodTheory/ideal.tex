\section{Ideal Response}
\label{ch:ideal-derivation}

\subsection{Problem Formulation}
First we derive a solution for temperature in sample for the simplest initial and boundary conditions, which can b formulated as:
\begin{itemize}
\item sample temperature before the pulse is constant and zero
\item an instantaneous heat pulse is absorbed at the from surface of the sample at zero time
\item sample is thermally insulated from ambient
\item sample thermal parameters are constant, independent of position, time and temperature
\item heat pulse is uniformly distributed over whole sample front surface
\item sample material is homogeneous and isotropical
\end{itemize}

\begin{marginfigure}
%  \includegraphics[width=\linewidth]{MO5l_100C.eps}%
  \includegraphics[scale=0.5]{graphics/sample_ideal_side.eps}%
  \caption{Sample and Coordinate System.}%
  \label{fig:idealSample}%
\end{marginfigure}

If the sample is a small cylinder with thickness $L$, as in Figure~\ref{fig:idealSample}, the heat flow after the heat pulse is one dimensional and temperature $T$ of the sample is a function of one spatial coordinate $x$ and time $t$. Temperature distribution in the sample after the heat pulse is governed by one dimensional HCE
\begin{equation}
\label{eq:1DHCE}
	\frac{\partial T(x,t)}{\partial t}=a\frac{\partial^2 T(x,t)}{\partial x^2} , \quad 0<x<L, \quad t>0
\end{equation}
where $a$ is the thermal diffusivity of the sample material. In order to find a unique solution of HCE \ref{eq:1DHCE}, initial and boundary conditions have to be specified. The initial condition is

\begin{equation}
\label{eq:initIdeal}
 T(x,0) = f(x) = \begin{cases}  \frac{Q}{\rho c g} &\mbox{if } 0<x<g \\ 
0 & \mbox{if }  g<x<L, \end{cases}
\end{equation}
where $Q$ is the amount of heat generated by the heat pulse per unit area of the sample surface, $\rho$ is the sample material density, $c$ is the sample specific heat, and $g$ is the thickness of the surface layer, where the pulse is absorbed.

If the sample is thermally insulated then the boundary conditions are formulated as for a zero heat flux at the front ($x=0$) and rear ($x=L$) sample surfaces

\begin{equation}
\label{eq:Neuman}
\frac{\partial T(0,t)}{\partial x}=0,  \quad \frac{\partial T(L,t)}{\partial x}=0,
\end{equation}

\subsection{Separation of Variables}
The method of separation variables is the oldest systematic method of solving PDEs, having been used by D'Alembert, Daniel Bernoulli, and Euler in about 1750 in their investigations of waves and vibrations. It has been considerably refined and generalized in the meantime, and remains a method of great importance and frequent use today\cite{Boyce1997}. We will use the method to solve heat conduction equation for a sample in the flash method. 

We have to find function $T(x,t)$, which satisfies the HCE \ref{eq:1DHCE}, the initial condition \ref{eq:initIdeal} and the boundary conditions \ref{eq:Neuman}. The problem described by equations \ref{eq:1DHCE} - \ref{eq:Neuman} is called \textit{boundary value problem} and is \textit{linear}, since  $T(x,t)$ appears only to the first power. The HCE and the boundary conditions are also \textit{homogeneous}, which allows us to seek solution as a product of a function of $x$ only and a function of $t$ only. We assume

\begin{equation}
\label{eq:Tseparated}
 T(x,t)= X(x)Y(t).
\end{equation}
Substituting this form of $T(x,t)$ into the HCE \ref{eq:1DHCE} yields

\begin{equation}
\label{eq:YprimeYprime}
 aX''Y=XY',
\end{equation}
where primes refer to ordinary differentiation with respect to the independent variable, whether $x$ or $t$.

If we separate the variables, the we have

\begin{equation}
\label{eq:XYseparated}
 \frac{X''}{X} =\frac{1}{a} \frac{Y'}{Y}
\end{equation}
where the left side is depends only on $x$ and the right side only on $t$. For Equation \ref{eq:XYseparated} to be valid for $0<x<L$ and $t>0$, it is necessary that both sides of the equation be equal to the same, so called separation constant $-\lambda$:

\begin{equation}
\label{eq:XYseparatedConst}
 \frac{X''}{X} =\frac{1}{a} \frac{Y'}{Y}= -\lambda .
\end{equation}

The HCE \ref{eq:1DHCE} just can be broken into the following two ordinary differential equations

\begin{equation}
\label{eq:XYseparatedTwo}
  X'' + \lambda X = 0,\qquad Y' +a\lambda Y = 0.
\end{equation}
From the boundary conditions (\ref{eq:Neuman}) it follows 

\begin{equation}
\label{eq:NeumanX}
X'(0)T(t)=0,  \quad X'(L)Y(t)=0.
\end{equation}
If $Y(t)$ is zero for all $t$, then the temperature in the sample is always zero. This is indeed a solution if there is no heat pulse. Otherwise we must assume that $Y(t)$ is nonzero for $t>0$ and conclude that

\begin{equation}
\label{eq:NeumanX}
X'(0)=0,  \quad X'(L)=0.
\end{equation}

Now we have to find solutions for $X$ and $Y$ from

\begin{equation}
\label{eq:Xseparated}
\begin{split}
  X'' + \lambda X = 0,\\
  X'(0)=0, \quad X'(L)=0,
\end{split}
\end{equation}
and
\begin{equation}
\label{eq:Yseparated}
  Y' + \lambda a Y = 0.
\end{equation}

A value for $\lambda$ for which the problem in (\ref{eq:Xseparated}) has a nontrivial  solution (nonzero in some points) is called an \textit{eigenvalue} of this problem. For such a $\lambda$, any nontrivial solution for $X$ is called an \textit{eigenfunction}. 

Consider cases for $\lambda$.
\begin{description}
  \item [\textbf{Case 1}]  $\lambda = 0$.
  
  Then $X''=0$, so the general solution is $X(x) = ax+b$, where $a$ and $b$ are constants independent of $x$. Now $X'(0)=a=0$ and $X'(L)=a=0$. It means that $\lambda = 0$ is an eigenvalue and $X(x)=const \neq 0$ is an eigenfunction of our problem.
  \item [\textbf{Case 2}]  $\lambda < 0$
  
  Let denote $\lambda=-\zeta^2$, with $\zeta>0$. Then $X''-\zeta^2 X = 0$, with general solution
\begin{equation}
\label{eq:X_sol_exp}
  X(x)= ae^{\zeta x} + be^{-\zeta x}.
\end{equation}  
Now

\begin{equation}
\label{eq:X_sol_exp_der_0}
  X'(0)= a\zeta - b\zeta = 0,
\end{equation}  
so $a=b$, and 

\begin{equation}
\label{eq:X_sol_exp_der_0_eigenf}
  X(x)= a(e^{\zeta x} + e^{-\zeta x}).
\end{equation} 

Next,
\begin{equation} 
\label{eq:X_sol_exp_der_L}
  X'(L)= a\zeta (e^{\zeta L} - e^{-\zeta L}) = 0.
\end{equation}  
Now, $e^{\zeta L} - e^{-\zeta L} \neq 0$ because $\zeta L>0$, so $a=0$. Therefore, there are no nontrivial solutions for $\lambda <0$, and this problem has no negative eigenvalues. 

  \item [\textbf{Case 3}]  $\lambda > 0$
  
  Now we write $\lambda=\zeta^2$, with $\zeta>0$. The general solution of $X''+\zeta^2 X = 0$ is
  \begin{equation} 
	\label{eq:X_sol_cos_sin}
  		X(x)= a \cos(\zeta x) + b\sin(\zeta x)
  \end{equation} 
  Since $X' = -a \zeta\sin(\zeta x) + b\zeta\cos(\zeta x)$, zero heat flux condition at $x=0$ is		  $X'(0)= b\zeta = 0$. It means that $b=0$,  so eigenfunction $X(x)=  a \cos(\zeta x)$. To have a         	nontrivial solution we must be able to choose $a \neq 0$ which will fulfill the zero heat flow condition at $x=L$. This requires $sin(\zeta L)=0$ which occurs if $\zeta L$ is a positive integer multiple of $\pi$, say $\zeta L = n\pi$. Thus the values of $\zeta$ are 
   	\begin{equation} 
	\label{eq:zeta_sin}
  		\zeta=\frac{n\pi}{L},\quad n=1,2,3,\ldots .
  	\end{equation}  	
 	For each such $n$, we can choose an eigenfuction 
 	\begin{equation} 
	\label{eq:eigenf_sin_case3}
  		X_n(x)=\cos\bigg( \frac{n\pi x}{L}\bigg),
  	\end{equation}
 	corresponding to the eigenvalue $\lambda=\zeta^2=n^2\pi^2/L^2$.
\end{description}

If we summarize our results from \textit{Case 1} and \textit{Case 3}, the eigenfuctions of the problem (\ref{eq:XYseparated}) are 
\begin{equation} 
	\label{eq:eigenf_sin_case3}
  		X_n(x)=\cos\bigg(\frac{n\pi x}{L}\bigg),
\end{equation}
and the eigenvalues
\begin{equation} 
	\label{eq:eigenv_sin}
  		\lambda=\frac{n^2\pi^2}{L^2},\quad n=0,1,2,3,\ldots .
\end{equation}
Since all these differential equations are linear, the solution can be expressed as a sum of particular solutions - the eigenfunctions  
\begin{equation}
\label{eq:Xcomponent}
 X(x) = \sum_{n=0}^\infty \cos\bigg( \frac{n\pi x}{L}\bigg).
\end{equation}

Back to a problem of finding time dependent function $Y(t)$ of HCE. With  $\lambda=n^2\pi^2/L^2$ , the differential equation (\ref{eq:Yseparated}) is
\begin{equation}
\label{eq:Yseplambda}
  Y' + \frac{n^2\pi^2a}{L^2}Y = 0,
\end{equation}
with general solution
\begin{equation}
\label{eq:Ycomponent}
 Y_n(x) = c_n \exp\bigg[{-\frac{n^2\pi^2 a}{L^2}t}\bigg].
\end{equation}
Combining solutions for $X(x)$ and $Y(x)$ for each $n=0,1,2,\ldots$ we have

\begin{equation}
\label{eq:T_XY}
 T(x,t) = \sum_{n=0}^\infty c_n \cos\bigg(\frac{n\pi x}{L}\bigg) \exp\bigg[{-\frac{n^2\pi^2 a}{L^2}t}\bigg].
\end{equation}
This function satisfies the HCE and the boundary conditions. To satisfy the initial condition (\ref{eq:initIdeal}), we to find the values for constant $c_n$ from
\begin{equation}
\label{eq:T_XY_0}
 T(x,0) = f(x)= \sum_{n=0}^\infty c_n \cos\bigg(\frac{n\pi x}{L}\bigg).
\end{equation}

So far all mathematical operations described in this chapter were simple and straightforward, and all steps taken were quite easy to understand with basic knowledge of the theory of ordinary differential equations. The task, to find the coefficients $c_0, c_1, c_2, \ldots$, is actually much more difficult and we have to be explain in details what has to be done.

\subsection*{Fourier Series - Historical Notes}
\label{sch:Fourier_Series}

(Excerpts from Carslaw's book\cite{Carslaw1906introduction}).


In the middle of eighteen century there was prolong controversy as to the possibility of an expansion of an arbitrary function of a real variable into a series of sines and cosines of multiples of the variable. The question arose in connection with the problem of the Vibrations of Strings. The theory of these vibrations reduces to the solution of the PDE
\begin{equation}
\label{eq:Vibrations}
	\frac{\partial^2 y(x,t)}{\partial t^2}=a^2\frac{\partial^2 y(x,t)}{\partial x^2} 
\end{equation}
and the earliest attempts at its solutions were ade by d'Alembert \cite{dAlembert1747}, Euler\cite{Euler1748} and D. Bernoulli\cite{Bernoulli1753}. Both D'Alembert and Euler obtained the solution in the functional form
\begin{equation}
\label{eq:EulerSolution}
y=\psi(x+at) + \phi(x-at)
\end{equation}
The principal difference between them lay in the fact that D'Alembert supposed the initial form of the spring to be given as a single analytical expression while Euler regarded them as laying along a any arbitrary continuous curve, different parts of which might be given by by different analytical expressions. Bernoulli, on the other hand gave the solution, when the string starts from rest, in the form of trigonometric series

\begin{equation}
\label{eq:BernoulliSolution}
y=A_1\sin(x)\cos(at) +A_2\sin(2x)\cos(2at)+ \ldots,
\end{equation}
and confidently declared that this solution, being perfectly general, must contain those given by D'Alembert and Euler. The importance of his discovery was immediately recognized, and Euler pointed out that if this statement of the solution was correct, an arbitrary function of a single variable must be expandable into an infinite series of sines of multiple of the variable. This he held to be obviously impossible, since the series of sines are both periodic and odd, and he argued that if the arbitrary function has not the both of these properties it could not be expanded in such a series.

In 1759 Lagrange\cite{Lagrange1759} published a memoir in which he showed that when the initial displacement of the string of unit length is given by $f(x)$, and the initial velocity by $F(x)$, the displacement at time $t$ is given by

\begin{equation}
\label{eq:LagrangeSolution}
\begin{split}
 y = 2\int_{0}^{1} \sum_{n=1}^{\infty}\sin(n\pi x') \sin(n\pi x) \cos(n\pi at)f(x') dx' + \\
 \frac{2}{a\pi}\int_{0}^{1} \sum_{n=1}^{\infty}\frac{1}{n}\sin(n\pi x') \sin(n\pi x) \sin(n\pi at)F(x') dx'.
 \end{split}
\end{equation}
This result, and the discussion of the problem with Lagrange gave in this and other memoires, have prompted some mathematicians to deny the importance of Fourier's discoveries, and to attribute to Lagrange the priority in the proof of the expanding of an arbitrary function in trigonometric series. It is true that in the formula quoted above it is only necessary to change the order of summation and integration,and to put $t=0$, in order that we may obtain the expansion of the function $f(x)$ in a series od sines and that the coefficients shall take the definite integral forms which we are now familiar. Still Lagrange did not take the step, because his purpose was to demonstrate the truth of Euler's solution and to defend its general conclusions against D'Alembert's attacks.

Euler\cite{Euler1793} in a paper from 1777 and published in 1793, actually employed the method of multiplying both sides of the equation
\begin{equation}
\label{eq:Euler_fx}
 f(x)=a_0+2a_1 \cos x +2a_2 \cos 2x+\ldots+2a_n \cos nx+\ldots
\end{equation}
by $\cos nx$ and integrating the series term by term between limits $0$ and $\pi$. In this way he found that
\begin{equation}
\label{eq:EulerCoeffs}
 a_n=\frac{1}{\pi} \int_{0}^{\pi}f((x)\cos(nx)dx
\end{equation}
  
It is curious that these papers seem to have had no effect upon the discussion of the problem of the Vibrations of Strings, in which as we have seen, D'Alembert, Euler, Bernoulli and Lagrange were about the same time engaged. The explanation is probably to be found in the fact that these results were not accepted with confidence and that they were used in determining the coefficients of expansions whose existence could be demonstrated by other means. It was left to Fourier to place our knowledge a firmer foundations, and owing to the material advance made by him in this subject the most important of these expansions are now generally associated with his name and called Fourier's series.

His treatment was suggested by the problems he met in the Mathematical Theory of Heat. It is to be found in various memoirs contained in his book \textit{Th\'eorie Mathematique de la Chaleur} (1822). In his discussion Fourier followed the line of argument which is now customary in dealing with the infinite series. He proved that when the values 

\begin{equation}
\label{eq:FourierCoeffs}
\begin{split}
 a_0=\frac{1}{2\pi} \int_{-\pi}^{\pi}f((x')dx', \\
  a_n=\frac{1}{\pi} \int_{-\pi}^{\pi}f((x')\cos(nx')dx'   & \quad \text{for} \quad n \leq 1\\
  b_n=\frac{1}{\pi} \int_{-\pi}^{\pi}f((x')\sin(nx')dx'   & \quad \text{for} \quad n\leq 1
\end{split}
\end{equation}
are inserted in the terms of the series

\begin{equation}
\label{eq:Fourier_fx}
 a_0+(a_1 \cos x +b_1 \sin x)+(a_2 \cos 2x +b_2 \sin 2x)+\ldots
\end{equation}
the sum of the terms up to $\cos nx$ and $\sin nx$ is
\begin{equation}
 \frac{1}{\pi} \int_{-\pi}^{\pi}f((x)\frac{\sin \frac{1}{2}(2n+1)(x'-x)}{\sin \frac{1}{2}(x'-x)}dx'.
\end{equation}

Then he discussed the limiting value of this sum as $n$ becomes infinite, and thus obtained the sum of the series now called Fourier's Series.

Fourier made no claim to the discovery of the values of the coefficients $a_0, a_1, b_1,\ldots$. We have already seen that they were employed by others this time. Still there is an important difference between Fourier's interpretation of these integrals and that which was current between the mathematicians of the eighteen century. The earlier writers by whom they were employed applied them to determination of the coefficients of series whose \textit{ existence had been demonstrated by different means}. Fourier was the the first to apply them to the representation of an \textit{ entirely arbitrary function}. In this he made a distinct advance upon his predecessors.

Indeed Riemann\cite{Riemann1867} asserts that when Fourier, in his first paper to the Paris Academy in 1807, stated that a completely arbitrary function could be expressed in a such a series, his statement so surprised Lagrange that he denied the possibility in the most definitive terms. Fourier's \textit{ Memoire sur la Propagation de la Chaleur} won the Academy grand prix deu Mathematiques for 1812, but referees Laplace, Lagrange, Legendre, and others openly criticized rigor of his analysis and the methods and the paper was not published in the Memoires de l'Academie des Sciences. 

Fourier always resented the treatment he had received. When finally publishing his treatise in 1822, he incorporated in it practically without change, the first part of his memoire, and two years later, having become Secretary of the Academy on the death of Delambre, he caused his original paper in the form it had been communicated in 1811, to be published in the Memoires.(to secure to himself the priority or to show the injustice of the criticism which he had been passed upon his work??)

After the publication of his treatise, when the results of his memoires had been become known, it was recognized that real advance had been made by him in the treatment of the subject and the substantial accuracy of his reasoning was admitted.

\textbf{The End of the Historical Notes on Fourier Series}


This equation represents now well known Fourier expansion of $f(x)$ on $[0,L]$ and the coefficients $c_n$ are calculated from

\begin{equation}
\label{eq:Coeff_C_0}
 c_0 = \frac{1}{L} \int_{0}^{L} f(\xi) d\xi,
\end{equation}

\begin{equation}
\label{eq:Coeff_C_n}
 c_n = \frac{2}{L} \int_{0}^{L} f(\xi) \cos\bigg(\frac{n\pi \xi}{L}\bigg ) d \xi.
\end{equation}
With a certain physically reasonable conditions on $f(x)$ this Fourier series converges to $f(x)$ for $0<x<L$ and the formal solution of our boundary value problem is

\begin{equation}
\label{eq:Txtfx}
\begin{split}
 T(x,t) = \frac{1}{L} \int_{0}^{L} f(\xi) d\xi + \frac{2}{L} \sum_{n=1}^{\infty} \cos\bigg(\frac{n\pi x}{L}\bigg)\exp\bigg[{-\frac{n^2\pi^2 a}{L^2}t}\bigg] \\
 \times \int_{0}^{L} f(\xi) \cos\bigg(\frac{n\pi \xi}{L}\bigg ) d \xi.
\end{split}
\end{equation}
With regards to our particular initial condition the solution will be
\begin{equation}
\label{eq:Txtfxintegral}
\begin{split}
 T(x,t) = \frac{Q}{\rho cL}\Bigg\{  1 + 2\sum_{n=1}^{\infty} \cos\bigg(\frac{n\pi x}{L}\bigg)\exp\bigg[{-\frac{n^2\pi^2 a}{L^2}t}\bigg] \\
 \times \int_{0}^{g}\cos\bigg(\frac{n\pi \xi}{L}\bigg ) d \xi \Bigg\}.
\end{split}
\end{equation}

\begin{equation}
\label{eq:TxtfxSinx_over_x}
 T(x,t) = \frac{Q}{\rho cL}\Bigg\{  1 + 2\sum_{n=1}^{\infty} \cos\bigg(\frac{n\pi x}{L}\bigg) \exp\bigg[-\frac{n^2\pi^2 a}{L^2}t \bigg]\frac{\sin\bigg(\frac{n\pi g}{L}\bigg )}{\frac{n\pi g}{L}} \Bigg\}.
\end{equation} 
For a small $g \ll L$, $\sin(n\pi g/L) \approx n\pi g/L$, and we finally have 
\begin{equation}
\label{eq:Txt}
 T(x,t) = \frac{Q}{\rho cL}\Bigg\{ 1 + 2\sum_{n=1}^{\infty} \cos\bigg(\frac{n\pi x}{L}\bigg) \exp\bigg[-\frac{n^2\pi^2 a}{L^2}t \bigg] \Bigg\}.
\end{equation} 
This is a solution of HCE for a instantaneous heat pulse absorbed uniformly in an infinitely thin surface layer of thermally insulated sample initially at zero temperature. 
If we define $T_m=Q/(\rho cL)$ and dimensionless time $\omega=at/L^2$ then the solution \ref{eq:Txt} can be written in a dimensionless form
\begin{equation}
\label{eq:Txtdimless}
 V(x,\omega)=\frac{T(L,t)}{T_m} =  1 + 2\sum_{n=1}^{\infty} \cos\bigg(\frac{n\pi x}{L}\bigg) \exp\bigg[-n^2\pi^2\omega\bigg].
\end{equation} 
\begin{marginfigure}
  \includegraphics[scale=0.5]{FourierExpFlash.eps}%
  \caption{Fourier components.}%
  \label{fig:FourierExpFlash}%
\end{marginfigure}
The temperature $V(x,\omega)$, in \ref{eq:Txtdimless}, is an infinite sum of components of the Fourier expansion of an instantaneous heat pulse at time zero multiplied with an exponential term $\exp(-n^2\pi^2\omega)$. Components
\begin{equation}
\begin{split}
1, \quad n=0,\\
2\cos\bigg(\frac{n\pi x}{L}\bigg) \exp\bigg[-n^2\pi^2\omega \bigg], \quad n=1,2,\ldots,6,
\end{split}
\end{equation}
for $\omega=0.01$, are shown in Figure \ref{fig:FourierExpFlash}, along with the sum of all seven. For small times the convergence of \ref{eq:Txtdimless} is very poor and many more terms of the expansion have to be added in order to make a precise temperature distribution calculation. The convergence improves with increasing time and only few terms have to be taken into account for a long times ($\omega>0.6$).

In the flash method experiment, our interest is usually focused on temperature on rear side of the sample. The solution for $x=L$ is 
\begin{equation}
\label{eq:Tidealdimless}
 V(\omega) = 1 + 2\sum_{n=1}^{\infty} (-1)^n \exp\big[-n^2\pi^2\omega \big].
\end{equation} 
Relative temperature rise depends only on the dimensionless time $\omega$. Parker choose 
$V=1/2$, and from \ref{eq:Tidealdimless} he calculated the correspondent dimensionless time
\begin{equation}
\label{eq:halftime}
\omega_{1/2} = \frac{at_{1/2}}{L^2}=\frac{1.37}{\pi^2} = 0.1388,
\end{equation}
where $t_{1/2}$ is experimentally obtained time when the response curve rises to 50$\%$ of its final temperature rise.
The unknown thermal diffusivity can then be calculated from \ref{eq:halftime} as
\begin{equation}
\label{eq:ParkerFormula}
 a=0.1388\frac{L^2}{t_{1/2}},
 \end{equation} 
which is the famous Parker's formula.   


 
