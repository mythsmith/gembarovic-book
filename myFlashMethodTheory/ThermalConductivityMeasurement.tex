% !TEX root = mybook.tex
\chapter{Thermal Conductivity}

\section{Historical Notes}

In a broader, qualitative sense, the concept of the thermal conductivity, as an ability of material to conduct heat, is as old as the mankind itself. It started with a prehistoric man when he first used animal hides to protect his body against elements. He most certainly realized, while cooking his meat over an open fire that certain sticks are transferring heat from a fire and burning his hands more readily than the others.
\begin{marginfigure}
  \includegraphics[scale=0.5]{graphics/caveman-cooking.jpg}%
  \caption{The first experimenter.}%
  \label{fig:caveman-cooking}%
\end{marginfigure} 

%% Narasimhan's article from Physics Today, August 2010, p. 36
The quantitative study of heat can be dated back to 18th century when Polish born German physicist and glass blower Daniel Gabriel Fahrenheit (1686--1736) invented a mercury-in-glass thermometer capable of reproducible measurements and introduced the first temperature scale now called after him.
%\begin{marginfigure}
%  \includegraphics[scale= 0.01]{graphics/Fahrenheit.png}%
%  \caption{Computer generated image of Fahrenheit as 25 years old.}%
%  \label{fig:Fahrenheit}%
%\end{marginfigure}  
In 1761 Joseph Black introduced the concepts of latent heat and specific heat. Heat was quantified in 1780 by Antoine Lavoisier and Pierre Simon Laplace, when they invented the calorimeter. 

In 1776 Swiss polymath Johann Heinrich Lambert conducted a series of experiments with a long metal rod heated in one side while heat was allowed to dissipate to the atmosphere. He found that the temperature profile along the rod decreases logarithmically and he realized the importance of the rod's geometry.

 At about the same time, Benjamin Franklin conceived a scheme to evaluate relative heat-conducting abilities of different metals by coating them with wax, heating them, and observing the distance to which the wax would melt in different cases. Following his suggestions, Dutch physiologist, biologist and chemist, Jan Ingen-Housz\cite{ingen1785nouvelles}, (best known for the discovery of photosynthesis) coated a number of wires with beeswax; all had the same length and diameter, but each was of a different material. The wires were tightened in parallel between blocks of wood. One side of the wires was in the same time and to the same length dipped into a hot oil. He observed that the wax coat melted along all wires, but the final definitive distances varied among the materials. He assumed that the  distance is related the material ability to conduct heat.\sidenote{The thermal conductivities are in the ratio of the squares of these distances.} He thus found the following order of decreasing conductivity of metals: silver, copper, gold, tin, iron, steel, and lead. It was the first published thermal conductivity experiment.
 
 Count Rumford (Sir Benjamin Thompson), motivated by the need to more efficiently clothe the Bavarian army, studied relative insulating properties of cotton silk, wool, fur, down, and other materials. His apparatus consisted of a thermometer enclosed and hermetically sealed in a carefully blown glass bulb\cite{rumford1968collected}. The bulb was evacuated or filled  with the material of interest. He either plunged the apparatus first into a hot bath and then into a freezing water to observe the gradual decline in temperature as a function of time, or he reversed the process plunging the apparatus into freezing water and then into a hot bath. The rate of heat loss or gain as revealed by the temperature-time relation was considered as a measure of thermal conductivity of the tested material. While investigating gases, he described on a completely new mode of heat transfer, convection. In 1799 Count Rumford unfortunately concluded that "a gas was unable to conduct heat!". He further postulated that " $\dots$ liquids conduct heat, although far more slowly than the worst conductors among solid bodies, the fact, that air and gases are absolute nonconductors, is established beyond all possibility of cavil" and because of his reputation, this conclusion was not challenged for many years. 
 
  All these experiments were conducted before the end of 18th century, and they have a significant influence on development of analytical theory of heat in the beginning of 19th century. 
    
  While carefully avoiding discussion about the actual nature of heat, Fourier defined heat flux as an amount of heat flowing through a unit of area in unit time and defined that it is proportional to the temperature gradient. The proportionality coefficient was the thermal conductivity. He made a distinction between internal conduction, characterized with the thermal conductivity of material, and external conduction(heat exchange with ambient), governed by the Newton's Law of cooling. In Fourier's final formulation, the flow process in the interior is distinct from that at the surface. 
  
  In order to demonstrate that his mathematical theory was physically credible, Fourier had to devise suitable experiments and methods to measure the thermal conductivity. It is not widely recognized that in his unpublished 1807 manuscript and also in the prize essay he submitted to the Institut de France in 1811, Fourier provided results from transient and steady state experiments and outlined methods to invert experimental data to estimate thermal conductivity. The first experimental determination of a thermal conductivity of in a solid was made by Fourier. From his experiments on annulus and sphere he arrived at the value $3/2$ for the thermal conductivity of steel. The unit of heat was the quantity of heat required to convert 1 kilogram of ice at the freezing point to an equal mass of water at the same temperature, the other relevant units were meter, minute, degree Fahrenheit, and kilogram.
  
  Fourier set the stage for the investigations that would take place during the rest of the 19th century. Experimenters attempting to determine thermal conductivity using the Fourier Law pursued research along two broad lines. The first was to quantitatively establish the relative conductivities of different materials, and the second was to estimate absolute conductivity of specific materials by analyzing experimental data. 
  
  French phisicist and chemist C{\'e}sar-Mansu{\'e}te Despretz\cite{Despretz1821} studied the regularity of recently discovered laws of heat conduction of solids. For this purpose, Despretz shaped each of the substances he wanted to study, as a square bar, 21 mm side. Cavities, 14 mm deep and 5 mm long, separated 10 cm from their centers; were carved to carry very sensitive thermometers. The center of the thermometer reservoir was located on the axis of the bar and the little space left between the reservoir and the cavity was filled with oil. All bars were covered with a layer of the same varnish, in order to present the same radiating power. Each bar was heated on one end with the aid of an oil lamp. Experiences lasted 5-6 h; it required at least 2-3 h for each thermometer to reach a constant reading, that is, for the bar to reach an equilibrium state. Interpretation of the results indicated that
  \begin{enumerate}
  	\item good conducting substances, such as copper, silver,
gold, platinum, iron and tin, gave the same geometrical
series as predicted by calculations,
	\item for mediocre conducting
metals, such as lead, the measurements were
not exactly as predicted,
	\item with poor conducting substances,
such as marble, porcelain, earths, etc., the results
were not concurrent, unless the thermometers were
located more far apart. 
  \end{enumerate}  
The relative conductivities were
as follows\cite{wisniak2009cesar}: gold, 1.000 (reference); platinum, 0.981; silver,
0.973; copper, 0.8982; iron, 0.3743; zinc, 0.3630; tin,
0.3039; lead, 0.1796; marble, 0.0236; porcelain, 0.0122;
and earths, 0.0114. 

James David Forbes, a Scottish physicist and glaciologist, determined the absolute thermal conductivity of an 8-foot-long bar of wrought iron with a 1.25 inch-square cross section, using the two step experimental design similar to the Fourier's. In his analysis, he did not relay on solutions to the heat conduction equation, rather he used a graphical procedure, where he plotted temperature in excess of atmospheric temperature as a function of position from the steady state experiment and fitted a smooth curve to estimate temperature gradient at any point. He also plotted the decline of temperature with time from the second experiment and fitted the curve that enabled him to estimate the rate of of the decline in any time for the segment under consideration. Since, in a steady state, the quantity of heat crossing the bar at point $x$ must be equal to the of heat lost to the atmosphere from the bar beyond $x$, he was able to show that thermal conductivity of wrought iron at 200 $^\circ$C is only about 77 $\%$ of its value at 100 $^\circ$C. Forbes established, for the first time, that the absolute thermal conductivity of wrought iron depends on temperature.\sidenote{This results could not have be obtained with using Fourier's mathematical approach because the heat equation assumes that the thermal conductivity is independent of temperature.}   

French mineralogist Henri Hureau de S{\'e}narmont conducted experiments\cite{Senarmont1847} with thin plates of different crystals, covered with wax, and found that thermal conductivity, just like refractive index, is directionally dependent, with characteristic directions related to crystallographic axes.\sidenote{It was Stokes who provided a mathematical theory of heat conduction in solids, where the thermal conductivity as a tensor was introduced for the first time. }  

The progress in the thermal conductivity measurement was greatly accelerated by the discovery made by James Prescott Joule in 1840  when he calculated the amount of heat generated by the electrical current in a conductor. Joule's Law provided the way of calculating quantity of heat generated per unit length per unit time is proportional to the product of the electrical resistance of the conductor and the square of the current. Among the earliest to introduce electrical heaters as controlled sources of heat in thermal conductivity measurements was Charles Lees\cite{lees1898thermal}. 

A steady state method for metals was described in 1900 by Kohlrausch\cite{kohlrausch1900stationaren}. In this method, a bar is heated by passing a direct electrical current and steady state temperature is measured in three different locations along the specimen. The specimen's electrical conductivity is actually measured and then the thermal conductivity is calculated using the Wiedemann-Franz Law, which states that the ratio of the electrical and thermal conductivities is proportional to the absolute temperature. The proportionality constant is called Lorentz number and its value remains fairly constant for a wide range of metals in a wide range of temperatures. 

{\AA}ngstr{\"o}m was the first to to measure thermal conductivity using a method based on transient boundary conditions. He employed long bars of a small cross section, with one end subjected to periodic changes of temperature. In the experiments, one end of the specimen was alternatively heated by a current of steam and cooled by a current of cold water. When this was gone on for some time, the temperature in the bar ultimately settled down to a periodic state, independent of the initial conditions. Temperatures at two points different points were measured and from amplitudes, frequencies and phase angles of the temperature signals, the thermal conductivity could be estimated\cite{angstrom1861new}.

Later Weber described a series of experiments\cite{weber1872} where both ends of the rod were exposed to periodical changes in temperature, extending thus the {\AA}ngstr{\"o}m method to short specimens.

Before the end of 19th century, the hot wire technique was developed as a technique for measuring the thermal conductivity of gases. In this method a wire, immersed in a tested medium, is heated by passing a direct electric current. The wire's temperature rise, which is measured, is a function of medium's conductivity.  The first transient hot-wire instrument capable of measuring conductivity of solids, powders and fluids was proposed by Bertil St{\^a}lhane and Sven Pyk in 1931\cite{stalhane1931new}. The hot wire was wound around a thin tube with a thermometer inside. The tube was immersed into the tested medium, and the wire was subjected to a step change in the heat input. Thermal conductivity of the medium was calculated from the transient temperature rise. The hot wire technique then quickly developed to a one of the most versatile and used methods for measurement of gases and fluids\cite{assael2010historical}.

Kondratiev, in 1954, introduced methods based on so called "regular regimes", when temperature at any point of the body changes exponentially in time. He, and his coworkers, used those methods to determine thermal diffusivities and thermal conductivity of variuos materials\cite{kondratiev1954regular}.


 Progress in using transient methods was in the first half of 20th century impeded by a lack of reliable recording devices for temperature as a function of time. It was practically impossible to record and analyze analog signals from thermocouples and other sensors before oscilloscopes,  plotters, and other similar recording devices were readily available. Thermal conductivity research was therefore focused on a development steady state methods based on the Fourier Law. Boundary temperatures for specimens in a shape of rod, plates, cylinders and spheres, were maintained at constant values and temperatures were measured at two or more different points along the specimen. Heat flux values were estimated inversely from a known material properties of standard materials and thermal conductivity values were calculated using the Fourier Law. These methods were time consuming, temperature range limited to room temperature or slightly higher. The biggest challenges were in the estimation of heat flux values and accounting for parasitic heat flows. Despite of all problems, the steady state methods are today well standardized and often used for thermally very low conductive insulation materials.

   
\section{Fourier Analytical Theory of Heat Conduction}
%(copied from \verb|Thermal_Conductivity_Measurement11.doc|, 5/20/2016)
  Joseph Fourier started work on heat conduction sometime between 1802 and 1804\cite{herivel1975man}. Inspired by the Laplacian philosophy of action at a distance, he followed an 18th-century technique of developing a discrete model of the continuous phenomenon and initially formulated the heat conduction as an n-body problem. He studied and found the solutions only to the two examples -- straight line and circular arrangement of N discrete bodies --and then he stopped. Although he never even mentioned that there was a difficulty, he probably reached the point from which only the investigation of special cases was possible\cite{grattan1972joseph}. Fourier abandoned the n-body approach around 1804, and probably inspired by Biot's work\cite{biot1804memoire}, he started to work on a theory of heat conduction in continuous bodies. 

Fourier first defined the heat flux, $\vec q$, as a vector pointing to the heat flow direction with a magnitude equal to the amount of heat flowing through unit area of medium in unit time. Then he postulated relationship between the heat flux and the temperature gradient, $\text{grad} T$, now called the Fourier Law
\begin{equation}
\label{eq:Fourier Law}
\vec{q}=-k\text{grad} T,
\end{equation}
where $\text{grad}T$ is defined in Cartesian coordinate system as 
\begin{equation}
\label{eq:Operator nabla}
 \text{grad}T=\frac{\partial T}{\partial x}\vec{i}+\frac{\partial T}{\partial y}\vec{j}+\frac{\partial T}{\partial z}\vec{k},
\end{equation}
and $\vec{i}$, $\vec{j}$, and $\vec{k}$, are unit vectors in $x$, $y$, and $z$ axis directions.

The proportionality coefficient $k$ is the thermal conductivity of the medium. It characterizes ability of a solid homogeneous material to conduct heat and numerically equals to the amount of heat, flowing in steady state through unit surface area of material of unit thickness in unit time with unit temperature difference on its faces.

Together with the continuity equation\begin{equation}
\label{eq:Continuity Equation}
\text{div}\vec{q}+c_p\rho \frac{\partial T}{\partial t}=0,
\end{equation}
where $c_p$ is the specific heat, and $\rho$ is the bulk density, the Fourier Law is a base for the heat conduction equation (HCE) in solids 
\begin{equation}
\label{eq:HCE div grad}
 c_p\rho \frac{\partial T}{\partial t}=\text{div}\big(k\text{grad}T
\big) 
\end{equation}
If the thermal conductivity $k$ is a constant, independent of time and a position, then the HCE can be expressed as
\begin{equation}
 \label{eq:HCE Delta}
 c_p\rho \frac{\partial T}{\partial t}= k\Delta T, 
 \end{equation}
where $\Delta$ is the Laplace operator, which in Cartesian coordinates is
\begin{equation}
\label{eq:Operator Delta}
 \Delta =\frac{\partial^2}{\partial x^2}+\frac{\partial^2}{\partial y^2}+\frac{\partial^2}{\partial z^2}.
\end{equation}
  
HCE is also written in a form
\begin{equation}
 \label{eq:HCE Delta}
 \frac{\partial T}{\partial t}= a\Delta T, 
 \end{equation}
where $a$ is the thermal diffusivity, defined as
\begin{equation}
 \label{eq:diffusivity definition}
 a=\frac{k}{c_p\rho}. 
\end{equation}

\subsection{Dimensions of the Thermal Conductivity} 

From the Fourier Law in Equation \ref{eq:Fourier Law} it follows that the dimensions of the thermal conductivity $[k]$ are
 \begin{equation}
 \label{eq:k dimensions}
 [k]=\frac{[Q]}{\text{tLT}},
 \end{equation}
 where $[Q]$  is the dimension of heat (energy), $\text{t}$ is the unit of time, $\text{L}$ is the unit of length, and $\text{T}$ is the unit of temperature. 
 
As it was first noted by James Clark Maxwell\cite{Maxwell2001}, further discussion of the dimensions of the thermal conductivity will depend on a mode of expressing the heat and the temperature.

\begin{enumerate}[(1)]
\item If the heat is measured as energy, its dimensions are
 \begin{equation}
 \label{eq:1 Q dimensions}
 [Q]=\frac{\text{L}^2\text{M}}{\text{t}^2},
 \end{equation}
or J (Joule), and those of $k$ become 
 \begin{equation}
 \label{eq:1 k dimensions}
 [k]=\frac{\text{L}\text{M}}{\text{t}^3 \text{T} },
 \end{equation}
which in SI units is J/(s m K) or W/(m K).

Maxwell called this the \textit{dynamical} measure of the conductivity, and in today terms, it represents the thermal conductivity.

\item If heat is measured in thermal units, such that each thermal unit is capable of raising unit of mass of a standard substance through one degree of temperature, the dimensions of the heat are $[Q]=\text{MT}$, and those of $k$ will be
 \begin{equation}
 \label{eq:2 k dimensions}
 [k]=\frac{\text{M}}{\text{LT}}.
 \end{equation}
 
This he called the \textit{calorimetric} measure of the conductivity. We do not have an equivalent for this measure in today used terminology.

\item If the unit of heat is taken as of heat that which will raise unit of volume of the substance \textit{itself} one degree, the dimensions of heat term are $[Q]=\text{L}^3\text{T}$, and those of $k$ are 
 \begin{equation}
 \label{eq:3 k dimensions}
 [k]=\frac{\text{L}^2}{\text{t}}.
 \end{equation}
 
 
This Maxwell called the \textit{thermometric} measure of the conductivity. It was later renamed by Kelvin to the thermal diffusivity, and this name is used today.
\end{enumerate}
 
Maxwell's simple and elegant interpretation of the thermal diffusivity, as the thermal conductivity expressed in heat units based on the substance itself, was long time forgotten and the thermal diffusivity is one of the least understood parameter in thermophysics. There is a surprising amount of confusion about the interpretation and meaning of the concept of the thermal diffusivity\cite{salazar2003thermal}. Simple rules, based on electrical analogy, which can be intuitively applied for a calculation of the thermal conductivity of composite bodies, are not applicable for the thermal diffusivity. Despite of apparent mathematical simplicity of the thermal diffusivity definition (Equation \ref{eq:diffusivity definition}), there is no simple relationship between the thermal diffusivity and the thermal conductivity of substances. Very low conductive materials, e.g. air, or pure gases at normal pressure, usually do have high thermal diffusivity, comparable with metals.  

As Maxwell\cite{Maxwell2001} pointed out: 
\begin{quote}As long as we are solving problems relating to the diffusion of heat and the distribution of temperature in a single substance, the phenomena depend on the thermometric conductivity (the thermal diffusivity) expressed in terms of the substance itself; but whenever we have to do with the effect of the flow of heat upon different bodies, we must use a definite thermal unit, and express the conductivity in terms of it. 
\end{quote}

\subsection{Two Diffusivities}
The amount of heat that will raise unit of mass of a substance one degree is called the specific heat capacity (simply just specific heat). The specific heat has to be multiplied by the density $\rho$,  in order to get so called volumetric heat capacity $C$,  which is the heat which will raise unit of volume of the substance one degree.  Substance temperature can change either at a constant pressure ($p=\text{const}$, or at a constant volume ($V=\text{const}$, therefore, there are two specific heats: the specific heat at constant pressure $c_p$, and the specific heat at constant volume $c_V$. It means there are also two thermal diffusivities\cite{Lykov1978}, at constant pressure $a_p$, and at constant volume $c_V$.   
\subsection{Thermal Diffusivity at Constant Volume}
The Fourier Law can be expressed as
 \begin{equation}
 \label{eq:FourierLaw aV}
 \vec{q}=-k\text{grad} T = -k\bigg(\frac{\partial T}{\partial u}\bigg)_V\text{grad} u_V=-a_V\text{grad} u_V,
 \end{equation}
where $u_V$ is the volumetric concentration of internal energy and $a_V$ represents the thermal diffusivity at constant volume, 
\begin{equation}
a_V=\frac{k}{c_V \rho},
\end{equation}
since
\begin{equation}
\bigg(\frac{\partial u}{\partial T}\bigg)_V= c_V\rho.
\end{equation}
 
	From Equation \ref{eq:FourierLaw aV} it implies that the heat flux is directly proportional to the gradient of the internal energy concentration of the material with the thermal diffusivity at constant volume as the proportionality coefficient.
	
\subsection{Thermal Diffusivity at Constant Pressure}

The thermal diffusivity at constant pressure is similarly defined as
\begin{equation}
a_p=\frac{k}{c_p \rho},
\end{equation}
where $c_p$ is the specific heat at constant pressure, defined as:
\begin{equation}
c_p = \frac{1}{\rho}\bigg(\frac{\partial H}{\partial T}\bigg)_V,
\end{equation}
and  $H_V$ is the enthalpy concentration.

	The Fourier Law at a constant pressure can be written in a form 
 \begin{equation}
 \label{eq:FourierLaw aP}
 \vec{q}=-k\text{grad} T = -k\bigg(\frac{\partial T}{\partial H}\bigg)_V\text{grad} H_V=-a_p\text{grad} H_V,
 \end{equation}
where $a_P$ is the coefficient of proportionality between the heat flux and the gradient of the enthalpy concentration. The thermal diffusivity now represents the diffusion coefficient of the enthalpy concentration. 

	The thermal diffusivity therefore represents a coefficient of diffusion of either the internal energy $u_V$, or the enthalpy $H_V$, depends on external conditions of the substance (if $V=\text{const}$,  or  $P=\text{const}$).

The specific heat at constant pressure is theoretically higher than the specific heat at constant volume, because an additional amount of energy is used for the substance expansion. In solids this difference is so minute that can be neglected, and the specific heat at constant pressure is equal to the specific heat at constant volume $c_p = c_V$. Similarly the thermal diffusivity at constant pressure is equal to the thermal diffusivity at constant volume $a_p = a_V = a$. 
 
