library(devtools)
install_github("h2oai/h2o-3/h2o-r/ensemble/h2oEnsemble-package")

library(rsparkling)
spark_install(version = "2.0.0",hadoop_version = "2.7")

# check installed versions
# >spark_installed_versions()


library(sparklyr)
options(rsparkling.sparklingwater.version = "2.0.3")

library(h2o)
library(dplyr)

sc <- spark_connect("local", version = "2.0.0")

mtcars_tbl <- copy_to(sc, mtcars, "mtcars")

#Now, let's perform some simple transformations - we'll

# Remove all cars with horsepower less than 100,
# Produce a column encoding whether a car has 8 cylinders or not,
# Partition the data into separate training and test data sets,
# Fit a model to our training data set,
# Evaluate our predictive performance on our test dataset.

# transform our data set, and then partition into 'training', 'test'
partitions <- mtcars_tbl %>%
  filter(hp >= 100) %>%
  mutate(cyl8 = cyl == 8) %>%
  sdf_partition(training = 0.5, test = 0.5, seed = 1099)
partitions

#Now, we convert our training and test sets into H2O Frames 
# using rsparkling conversion functions.
#We have already split the data into training and test frames using dplyr.

training <- as_h2o_frame(sc, partitions$training, strict_version_check = FALSE)
test <- as_h2o_frame(sc, partitions$test, strict_version_check = FALSE)

# fit a linear model to the training dataset
glm_model <- h2o.glm(x = c("wt", "cyl"), 
                     y = "mpg", 
                     training_frame = training,
                     lambda_search = TRUE)

library(ggplot2)

# compute predicted values on our test dataset
pred <- h2o.predict(glm_model, newdata = test)
# convert from H2O Frame to Spark DataFrame
predicted <- as_spark_dataframe(sc, pred, strict_version_check = FALSE)

# extract the true 'mpg' values from our test dataset
actual <- partitions$test %>%
  select(mpg) %>%
  collect() %>%
  `[[`("mpg")

# produce a data.frame housing our predicted + actual 'mpg' values
data <- data.frame(
  predicted = predicted,
  actual    = actual
)
# a bug in data.frame does not set colnames properly; reset here 
names(data) <- c("predicted", "actual")

# plot predicted vs. actual values
ggplot(data, aes(x = actual, y = predicted)) +
  geom_abline(lty = "dashed", col = "red") +
  geom_point() +
  theme(plot.title = element_text(hjust = 0.5)) +
  coord_fixed(ratio = 1) +
  labs(
    x = "Actual Fuel Consumption",
    y = "Predicted Fuel Consumption",
    title = "Predicted vs. Actual Fuel Consumption"
  )



# finally disconnect SPARK
spark_disconnect(sc)




