x0 <- 5
# ggplots
library(ggplot2)
# plot of simulated data points
ggplot()+
geom_point(data=exp.dat, aes(x=t, y=y), colour="steelblue", shape=1.0)+
labs(x="time", y="Temperature")+
xlim(0,max(t))+ theme(legend.position = "none")+
annotate("text",label="Parameter values:", parse=F, x=x0, y=-0.45) +
annotate("text",label=paste("italic(T)[0] == ",T0str), parse=T, x=x0, y=-0.6) +
annotate("text",label=paste("italic(T)[m] == ",Tmstr), parse=T, x=x0, y=-0.7) +
annotate("text",label=paste("italic(a) == ",astr), parse=T, x=x0, y=-0.8) +
annotate("text",label=paste("italic(m) == ",mstr), parse=T, x=x0, y=-0.9)
ggplot()+
geom_line(data=iters, aes(x=xs, y=value,# color=variable,
color=variable), alpha=0.2)+
geom_point(data=exp.dat, aes(x=t, y=y), colour="steelblue", shape=1.0)+
geom_line(data=all.dat, aes(x=xs, y=curves), colour="tomato", size=0.7)+
labs(x="time", y="Temperature")+
xlim(0,max(t))+ theme(legend.position = "none")+
# ylab("V/Vmax")+
#ggtitle("Normalized Flash Responses") +
#geom_hline(yintercept=T0, col="steelblue", linetype="dashed", size=0.1)+
#geom_hline(yintercept=T0+Tmax, col="steelblue",linetype="dashed", size=0.1)+
#geom_point(x=0.139,y=0.5, col="green", size = 3)
#scale_colour_manual("",
#                   breaks = c("experiment","theory"),
#                    values = c("skyblue", "indianred1"))+
annotate("text",label="Mean values:", parse=F, x=x0, y=-0.45) +
annotate("text",label=paste("italic(T)[0] == ",mT0str), parse=T, x=x0, y=-0.6) +
annotate("text",label=paste("italic(T)[m] == ",mTmstr), parse=T, x=x0, y=-0.7) +
annotate("text",label=paste("italic(a) == ",mastr), parse=T, x=x0, y=-0.8) +
annotate("text",label=paste("italic(m) == ",mmstr), parse=T, x=x0, y=-0.9) # +
#  guides(color=guide_legend(override.aes=list(shape=c(1,NA),
#                                              linetype=c(0,1))))+
#
mcmmmy <- as.mcmc(alldatmatrix)
mcmmmy <- as.mcmc(accdatmatrix)
#densplot(mcmmmy)
mcmmmy.gg <- ggs(mcmmmy)
S <- mcmmmy.gg
ggs_density(S,rug = T, greek = T)+theme_solarized(light=T)
ggs_pairs(S,diag=list(continuous="density", color="blue"),
lower = list(continuous = "density"))+theme_solarized(light=T)# theme_tufte()
ggs_autocorrelation(S, nLags = 300)
ggs_crosscorrelation(S)
ggs_geweke(S)
ggs_running(S)
ggs_traceplot(S)+theme_solarized(light=T)
# ratio
accepts/rejects
diffIndex0 <- which(S$Parameter=="a")[1]
diffus <- theta
for (i in diffIndex0:(diffIndex0+400)) {
diffus <- c(diffus, as.numeric(S[i,4]))
}
TmaIndex0 <- which(S$Parameter=="Tm")[1]
Tma=Tm
#    as.numeric(inits1[2]))
for (i in TmaIndex0:(TmaIndex0+400)) {
Tma <- c(Tma, as.numeric(S[i,4]))
}
basIndex0 <- which(S$Parameter=="T0")[1]
bas=T0
for (i in basIndex0:(basIndex0+400)) {
bas <- c(bas, as.numeric(S[i,4]))
}
mIndex0 <- which(S$Parameter=="m")[1]
ms=em
for (i in mIndex0:(mIndex0+400)) {
ms <- c(ms, as.numeric(S[i,4]))
}
# contour plot and many points
# big dataframe containg points for the density calculation
df.space <- c(0)
lindex <- 200
uindex <- 900
df.space <- data.frame(bas[lindex:uindex], Tma[lindex:uindex],
diffus[lindex:uindex], ms[lindex:uindex])
names(df.space) <- c("T0","Tmax", "a", "m")
# calculate bivariate distribution parameters for elipses
center <- apply(df.space[,3:4], 2, mean)
sigma <- cov(df.space[,3:4])
#The formula requires inversion of the variance-covariance matrix:
sigma.inv = solve(sigma, matrix(c(1,0,0,1),2,2))
#The ellipse "height" function is the negative of the logarithm of the bivariate normal density:
ellipse <- function(s,t) {u<-c(s,t)-center; u %*% sigma.inv %*% u / 2}
n <- 50
y <- seq(min(df.space$m),max(df.space$m),length.out=n)
#y <- seq(0.75-0.007,0.75+0.007,length.out=n) #for the diffusivity
x <- seq(min(df.space$a),max(df.space$a),length.out=n)
#n <- 50
#x <- (0:(n-1)) * (500000/(n-1))
#y <- (0:(n-1)) * (50000/(n-1))
#Compute the height function at this grid and plot it:
z <- mapply(ellipse, as.vector(rep(x,n)), as.vector(outer(rep(0,n), y, `+`)))
library(stringr)
library(reshape)
R = expand.grid(x, y)
dim(R)
names(R) = c("x", "y")
R$z = z
ggplot(R, aes(x = x, y = y, z = z)) + stat_contour()
ggplot(R, aes(x = x, y = y, z = z)) + stat_contour()
lindex <- 200
uindex <- 800
df.space <- data.frame(bas[lindex:uindex], Tma[lindex:uindex],
diffus[lindex:uindex], ms[lindex:uindex])
names(df.space) <- c("T0","Tmax", "a", "m")
View(df.space)
center <- apply(df.space[,3:4], 2, mean)
sigma <- cov(df.space[,3:4])
#The formula requires inversion of the variance-covariance matrix:
sigma.inv = solve(sigma, matrix(c(1,0,0,1),2,2))
#The ellipse "height" function is the negative of the logarithm of the bivariate normal density:
ellipse <- function(s,t) {u<-c(s,t)-center; u %*% sigma.inv %*% u / 2}
#(I have ignored an additive constant equal to log(2pideterminant(sum).)
#To test this, let's draw some of its contours. That requires generating a grid of points in the x and y directions:
n <- 50
y <- seq(min(df.space$m),max(df.space$m),length.out=n)
#y <- seq(0.75-0.007,0.75+0.007,length.out=n) #for the diffusivity
x <- seq(min(df.space$a),max(df.space$a),length.out=n)
min(df.space$m)
df.space$m
df.space <- c(0)
lindex <- 200
uindex <- 400
df.space <- data.frame(bas[lindex:uindex], Tma[lindex:uindex],
diffus[lindex:uindex], ms[lindex:uindex])
names(df.space) <- c("T0","Tmax", "a", "m")
# calculate bivariate distribution parameters for elipses
center <- apply(df.space[,3:4], 2, mean)
sigma <- cov(df.space[,3:4])
#The formula requires inversion of the variance-covariance matrix:
sigma.inv = solve(sigma, matrix(c(1,0,0,1),2,2))
#The ellipse "height" function is the negative of the logarithm of the bivariate normal density:
ellipse <- function(s,t) {u<-c(s,t)-center; u %*% sigma.inv %*% u / 2}
n <- 50
y <- seq(min(df.space$m),max(df.space$m),length.out=n)
#y <- seq(0.75-0.007,0.75+0.007,length.out=n) #for the diffusivity
x <- seq(min(df.space$a),max(df.space$a),length.out=n)
#n <- 50
#x <- (0:(n-1)) * (500000/(n-1))
#y <- (0:(n-1)) * (50000/(n-1))
#Compute the height function at this grid and plot it:
z <- mapply(ellipse, as.vector(rep(x,n)), as.vector(outer(rep(0,n), y, `+`)))
library(stringr)
library(reshape)
R = expand.grid(x, y)
dim(R)
names(R) = c("x", "y")
R$z = z
#setwd("~/mybook/myFlashMethodTheory/Literature/BayesianInference/MCMC examples")
#save(R, file="df_R.dat") # save R to a file
# rm(R)                       # remove the data.frame R from memory
#load("df_R.dat")           # load under the same name again
head(R)
ggplot(R, aes(x = x, y = y, z = z)) + stat_contour()
# small data framne for individual points
imin <- 2
imax <- 10
imaxstr <- as.character(imax+imin)
iminstr <- as.character(imin)
df.spaceSmall <- c(0)
df.spaceSmall <- data.frame(df.space$a[imin:(imax+imin+1)],
df.space$m[imin:(imax+imin+1)])
names(df.spaceSmall) <- c("aSmall", "mSmall")
#center <- c(-2.0,2.5)
ggplot(R)+
geom_point(data=df.spaceSmall, aes(x=aSmall, y=mSmall), colour="skyblue")+
geom_path(data=df.spaceSmall, aes(x=aSmall, y=mSmall), colour="skyblue")+
stat_contour(aes(x = x, y = y, z = z),  col="darkolivegreen3", bins=15)+
geom_hline(yintercept=center[2], col="skyblue3", linetype="dashed", size=0.1)+
geom_vline(xintercept=center[1], col="skyblue3",linetype="dashed", size=0.1)+
# geom_point(x=center[1],y=center[2], col="gray34", size = 2)+
labs(y=expression(m),x=expression(a))+
annotate("text",label=paste("x[",iminstr,"]"), parse=T,
x=df.spaceSmall$aSmall[1], y=df.spaceSmall$mSmall[1]-0.006, size=5) +
annotate("text",label=paste("x[",imaxstr,"]"), parse=T,
x=df.spaceSmall$aSmall[length(df.spaceSmall$aSmall)],
y=df.spaceSmall$mSmall[length(df.spaceSmall$mSmall)]-0.005, size=5)
imin <- 1
imax <- 10
imaxstr <- as.character(imax+imin)
iminstr <- as.character(imin)
df.spaceSmall <- c(0)
df.spaceSmall <- data.frame(df.space$a[imin:(imax+imin+1)],
df.space$m[imin:(imax+imin+1)])
names(df.spaceSmall) <- c("aSmall", "mSmall")
#center <- c(-2.0,2.5)
ggplot(R)+
geom_point(data=df.spaceSmall, aes(x=aSmall, y=mSmall), colour="skyblue")+
geom_path(data=df.spaceSmall, aes(x=aSmall, y=mSmall), colour="skyblue")+
stat_contour(aes(x = x, y = y, z = z),  col="darkolivegreen3", bins=15)+
geom_hline(yintercept=center[2], col="skyblue3", linetype="dashed", size=0.1)+
geom_vline(xintercept=center[1], col="skyblue3",linetype="dashed", size=0.1)+
# geom_point(x=center[1],y=center[2], col="gray34", size = 2)+
labs(y=expression(m),x=expression(a))+
annotate("text",label=paste("x[",iminstr,"]"), parse=T,
x=df.spaceSmall$aSmall[1], y=df.spaceSmall$mSmall[1]-0.006, size=5) +
annotate("text",label=paste("x[",imaxstr,"]"), parse=T,
x=df.spaceSmall$aSmall[length(df.spaceSmall$aSmall)],
y=df.spaceSmall$mSmall[length(df.spaceSmall$mSmall)]-0.005, size=5)
# small data framne for 500 individual points
imin <- 1
imax <- 200
imaxstr <- as.character(imax+imin)
iminstr <- as.character(imin)
df.spaceSmall <- c(0)
df.spaceSmall <- data.frame(df.space$a[imin:(imax+imin+1)],
df.space$m[imin:(imax+imin+1)])
names(df.spaceSmall) <- c("aSmall", "mSmall")
ggplot(R)+
geom_point(data=df.spaceSmall, aes(x=aSmall, y=mSmall), colour="skyblue", size=1)+
geom_path(data=df.spaceSmall, aes(x=aSmall, y=mSmall), colour="skyblue")+
stat_contour(aes(x = x, y = y, z = z),  col="darkolivegreen3", bins=15)+
geom_hline(yintercept=center[2], col="skyblue3", linetype="dashed", size=0.1)+
geom_vline(xintercept=center[1], col="skyblue3",linetype="dashed", size=0.1)+
geom_point(x=center[1],y=center[2], col="gray34", size = 3)+
labs(y=expression(m),x=expression(a))
ggplot(R)+
geom_point(data=df.spaceSmall, aes(x=aSmall, y=mSmall), colour="skyblue", size=1)+
geom_path(data=df.spaceSmall, aes(x=aSmall, y=mSmall), colour="skyblue")+
stat_contour(aes(x = x, y = y, z = z),  col="darkolivegreen3", bins=15)+
geom_hline(yintercept=mm, col="skyblue3", linetype="dashed", size=0.1)+
geom_vline(xintercept=ma, col="skyblue3",linetype="dashed", size=0.1)+
geom_point(x=ma,y=mm, col="gray34", size = 3)+
labs(y=expression(m),x=expression(a))
ma
mm
ggplot(R)+
geom_point(data=df.spaceSmall, aes(x=aSmall, y=mSmall), colour="skyblue", size=1)+
geom_path(data=df.spaceSmall, aes(x=aSmall, y=mSmall), colour="skyblue")+
stat_contour(aes(x = x, y = y, z = z),  col="darkolivegreen3", bins=15)+
geom_hline(yintercept=mm, col="skyblue3", linetype="dashed", size=0.1)+
geom_vline(xintercept=ma, col="skyblue3",linetype="dashed", size=0.1)+
geom_point(x=ma,y=mm, col="gray34", size = 3)+
labs(y=expression(m),x=expression(a))
source("DBDA2E-utilities.R")
diagMCMC( codaObject=bayes.mod.fit.mcmc , parName="a" )
diagMCMC( codaObject=bayes.mod.fit.mcmc , parName="T0" )
diagMCMC( codaObject=bayes.mod.fit.mcmc , parName="Tmax" )
diagMCMC( codaObject=bayes.mod.fit.mcmc , parName="deviance" )
effectiveSize(bayes.mod.fit.mcmc)
install.packages("Rcpp")
install.packages("ggplot2")
install.packages("~/repos/myflashapp/myFlash2_2.0.zip", repos = NULL, type = "win.binary")
install.packages("shiny")
if(!require(installr)) {
install.packages("installr"); require(installr)} #load / install+load installr
updateR() # this will start the updating process of your R installation.  It will check for newer versions, and if one is available, will guide you through the decisions you'd need to make.
require(myFlash2)
time <- seq(-0.0001, 0.03, length = 5000)
Nmax <- c(80)
alpha     <- c(1.17)
thickness <- c(0.2)
pd <- c(0.000)
b1 <- c(0.5)
baseline <- c(0)
deltaT <- c(1)
slope <- c(0.0)
y1DHL <- flash1DHLsq(time, alpha,thickness,deltaT,pd=0.001,b1)
require(myFlash2)
time <- seq(-0.01, 0.3, length = 100)
Nmax <- c(80)
alpha     <- c(1.17) # thermal diffusivity of Copper
thickness <- c(0.2608)
pd <- c(0.025)
b1 <- c(0.01)
R <- c(0.635)
IRI <- c(0)
IRO <- c(R*0.5)
VRI <- c(0)
VRO <- c(R*0.95)
baseline <- c(0)
deltaT <- c(1)
slope <- c(0.0)
strt <- Sys.time()
y1 <- iflash(time, alpha, thickness, deltaT)
y1DHL <- flash1DHL(time, alpha,thickness,deltaT,pd*3, b1=b1)
VRO <- c(R*0.5, R*0.75, R*0.9999)
ye <- sapply(VRO, flash2DHLe, time=time, Nmax=Nmax, alpha=alpha,
thickness=thickness,
pd=pd,b1=b1, R=R, IRI=IRI, IRO=IRO,
VRI=VRI,baseline=baseline,
deltaT=deltaT, slope=slope)
ys <- sapply(VRO, flash2DHLsq, time=time, Nmax=Nmax, alpha=alpha,
thickness=thickness,
pd=pd*3,b1=b1, R=R, IRI=IRI, IRO=IRO,
VRI=VRI,baseline=baseline,
deltaT=deltaT, slope=slope)
detach("package:myFlash2", unload=TRUE)
remove.packages("myFlash2", lib="~/R/win-library/3.3")
require(multilayer)
time <- seq(-0.01, 0.11, length = 5000) # times in s
alpha     <- c(0.02,1.17) # thermal diffusivities in cm^2/s
thickness <- c(0.02,0.1) # thicknesses (front, rear) layer in cm
rho <- c(1,1) # densities in g/cm^3
cp <- c(1,1) #  specific heats in J/g K
baseline <- c(0) # initial temperature level in C
deltaT <- c(1) # the adiabatic temperature rise in K
slope <- c(0.0) # baseline slope in K/s
hl <- c(0.1,0.1) # heat loss coeficients in W/(m^2 K)
tcr <- c(1.5) # thermal contact resistance in cm^2 K/W
strt <- Sys.time()
ys <- c(0)
ys <- TwoLayer( time, thickness,cp,alpha,rho,hl, tcr,baseline, deltaT,slope)
print(Sys.time()-strt)
plot(time,ys, col="blue", type="l",
ylab = c("Temperature"), xlab=c("time (s)"))
install.packages("~/repos/myflashapp/myFlash3/myFlash2_2.0.zip", repos = NULL, type = "win.binary")
require(myFlash2)
time <- seq(-0.01, 0.3, length = 100)
Nmax <- c(80)
alpha     <- c(1.17) # thermal diffusivity of Copper
thickness <- c(0.2608)
pd <- c(0.025)
b1 <- c(0.01)
R <- c(0.635)
IRI <- c(0)
IRO <- c(R*0.5)
VRI <- c(0)
VRO <- c(R*0.95)
baseline <- c(0)
deltaT <- c(1)
slope <- c(0.0)
strt <- Sys.time()
y1 <- iflash(time, alpha, thickness, deltaT)
y1DHL <- flash1DHL(time, alpha,thickness,deltaT,pd*3, b1=b1)
VRO <- c(R*0.5, R*0.75, R*0.9999)
ye <- sapply(VRO, flash2DHLe, time=time, Nmax=Nmax, alpha=alpha,
thickness=thickness,
pd=pd,b1=b1, R=R, IRI=IRI, IRO=IRO,
VRI=VRI,baseline=baseline,
deltaT=deltaT, slope=slope)
detach("package:myFlash2", unload=TRUE)
remove.packages("myFlash2", lib="~/R/win-library/3.3")
require(myFlash2)
time <- seq(-0.01, 0.3, length = 100)
Nmax <- c(80)
alpha     <- c(1.17) # thermal diffusivity of Copper
thickness <- c(0.2608)
pd <- c(0.025)
b1 <- c(0.01)
R <- c(0.635)
IRI <- c(0)
IRO <- c(R*0.5)
VRI <- c(0)
VRO <- c(R*0.95)
baseline <- c(0)
deltaT <- c(1)
slope <- c(0.0)
strt <- Sys.time()
y1 <- iflash(time, alpha, thickness, deltaT)
y1DHL <- flash1DHL(time, alpha,thickness,deltaT,pd*3, b1=b1)
VRO <- c(R*0.5, R*0.75, R*0.9999)
ye <- sapply(VRO, flash2DHLe, time=time, Nmax=Nmax, alpha=alpha,
thickness=thickness,
pd=pd,b1=b1, R=R, IRI=IRI, IRO=IRO,
VRI=VRI,baseline=baseline,
deltaT=deltaT, slope=slope)
ys <- sapply(VRO, flash2DHLsq, time=time, Nmax=Nmax, alpha=alpha,
thickness=thickness,
pd=pd*3,b1=b1, R=R, IRI=IRI, IRO=IRO,
VRI=VRI,baseline=baseline,
deltaT=deltaT, slope=slope)
print(Sys.time()-strt)
plot(time,ys[,1], col="tomato", type="l", lty="dotted",
ylab = c("Temperature"), xlab=c("time (s)"),
ylim=c(0,max(ys))) # dottes are square pulse responses
lines(time,ys[,2], col="green",lty="dotted")
lines(time,ys[,3], col="magenta",lty="dotted")
lines(time,ye[,1], col="tomato")
lines(time,ye[,2], col="green")
lines(time,ye[,3], col="magenta")
lines(time,y1DHL, col="blue", lty="solid")
lines(time,y1, col="lightblue")
grid()
source('~/.active-rstudio-document', echo=TRUE)
source('~/.active-rstudio-document', echo=TRUE)
source('~/mybook/myFlashMethodTheory/Literature/Multilayers/3layers_Pics.R', echo=TRUE)
source('~/mybook/myFlashMethodTheory/Literature/Multilayers/3LreversePics.R', echo=TRUE)
source('~/mybook/myFlashMethodTheory/Literature/Multilayers/3LreversePics.R', echo=TRUE)
install.packages("sparc")
install.packages("sparkR")
install.packages("spark")
install.packages("sparklyr")
spark_install(version = "1.6.2")
library(sparklyr)
spark_install(version = "1.6.2")
devtools::install_github("rstudio/sparklyr")
sc <- spark_connect(master = "local")
source('~/R/win-library/3.3/sparklyr/tests/testthat/test-install-spark.R', echo=TRUE)
sc <- spark_connect(master = "local")
install.packages(c("nycflights13", "Lahman"))
library(dplyr)
iris_tbl <- copy_to(sc, iris)
flights_tbl <- copy_to(sc, nycflights13::flights, "flights")
batting_tbl <- copy_to(sc, Lahman::Batting, "batting")
src_tbls(sc)
testthat_spark_connection <- function(version = NULL) {
# generate connection if none yet exists
connected <- FALSE
if (exists(".testthat_spark_connection", envir = .GlobalEnv)) {
sc <- get(".testthat_spark_connection", envir = .GlobalEnv)
connected <- sparklyr::connection_is_open(sc)
}
if (!connected) {
version <- version %||% Sys.getenv("SPARK_VERSION", unset = "2.1.0")
setwd(tempdir())
sc <- spark_connect(master = "local", version = version)
assign(".testthat_spark_connection", sc, envir = .GlobalEnv)
}
# retrieve spark connection
get(".testthat_spark_connection", envir = .GlobalEnv)
}
testthat_tbl <- function(name) {
sc <- testthat_spark_connection()
tbl <- tryCatch(dplyr::tbl(sc, name), error = identity)
if (inherits(tbl, "error")) {
data <- eval(as.name(name), envir = parent.frame())
tbl <- dplyr::copy_to(sc, data, name = name)
}
tbl
}
skip_unless_verbose <- function(message = NULL) {
message <- message %||% "Verbose test skipped"
verbose <- Sys.getenv("SPARKLYR_TESTS_VERBOSE", unset = NA)
if (is.na(verbose)) skip(message)
invisible(TRUE)
}
test_requires <- function(...) {
for (pkg in list(...)) {
if (!require(pkg, character.only = TRUE, quietly = TRUE)) {
fmt <- "test requires '%s' but '%s' is not installed"
skip(sprintf(fmt, pkg, pkg))
}
}
invisible(TRUE)
}
context("install")
sc <- spark_connect(master = "local")
library(sparklyr)
sc <- spark_connect(master = "local")
iris_tbl <- copy_to(sc, iris)
library(sparklyr)
spark_install(version = "1.6.2")
devtools::install_github("rstudio/sparklyr")
library(sparklyr)
sc <- spark_connect(master = "local")
install.packages(c("nycflights13", "Lahman"))
library(dplyr)
iris_tbl <- copy_to(sc, iris)
flights_tbl <- copy_to(sc, nycflights13::flights, "flights")
batting_tbl <- copy_to(sc, Lahman::Batting, "batting")
src_tbls(sc)
flights_tbl %>% filter(dep_delay == 2)
delay <- flights_tbl %>%
group_by(tailnum) %>%
summarise(count = n(), dist = mean(distance), delay = mean(arr_delay)) %>%
filter(count > 20, dist < 2000, !is.na(delay)) %>%
collect
# plot delays
library(ggplot2)
ggplot(delay, aes(dist, delay)) +
geom_point(aes(size = count), alpha = 1/2) +
geom_smooth() +
scale_size_area(max_size = 2)
library(DBI)
iris_preview <- dbGetQuery(sc, "SELECT * FROM iris LIMIT 10")
iris_preview
mtcars_tbl <- copy_to(sc, mtcars)
partitions <- mtcars_tbl %>%
filter(hp >= 100) %>%
mutate(cyl8 = cyl == 8) %>%
sdf_partition(training = 0.5, test = 0.5, seed = 1099)
fit <- partitions$training %>%
ml_linear_regression(response = "mpg", features = c("wt", "cyl"))
fit
plot(mtcars$mpg, mtcars$wt)
summary(fit)
temp_csv <- tempfile(fileext = ".csv")
temp_parquet <- tempfile(fileext = ".parquet")
temp_json <- tempfile(fileext = ".json")
spark_write_csv(iris_tbl, temp_csv)
iris_csv_tbl <- spark_read_csv(sc, "iris_csv", temp_csv)
spark_write_parquet(iris_tbl, temp_parquet)
iris_parquet_tbl <- spark_read_parquet(sc, "iris_parquet", temp_parquet)
spark_write_json(iris_tbl, temp_json)
iris_json_tbl <- spark_read_json(sc, "iris_json", temp_json)
src_tbls(sc)
spark_write_parquet(path="file://xxxx",iris_tbl, temp_parquet)
spark_write_parquet(path="c:",mode='overwrite',iris_tbl, temp_parquet)
spark_write_parquet(iris_tbl, path="file://",mode='overwrite',temp_parquet)
tempfile <- tempfile(fileext = ".csv")
write.csv(nycflights13::flights, tempfile, row.names = FALSE, na = "")
count_lines <- function(sc, path) {
spark_context(sc) %>%
invoke("textFile", path, 1L) %>%
invoke("count")
}
count_lines(sc, tempfile)
spark_web(sc)
spark_disconnect(sc)
spark_disconnect(sc)
spark_disconnect(sc)
spark_disconnect(sc)
spark_disconnect(sc)
spark_disconnect(sc)
library(sparklyr)
sc <- spark_connect(master = "local")
savehistory("~/history.txt")
