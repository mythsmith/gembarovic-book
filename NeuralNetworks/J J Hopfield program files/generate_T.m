%generate_T
%generates the excitatory connection matrix on the basis of rules described
%in the paper.  

V=zeros(num_cat,in_cat);
N=num_cat*in_cat;
u=zeros(1,N);

%%%%%%% this chunk generates random memories with a sqrt(1/n) probability
%%%%%%% distribution
long_memory=zeros(n_mem,N);
ziff_p=1.0./sqrt([1:in_cat]);% now root zipf not 1/n zipf
ziff_p=ziff_p/sum(ziff_p);
cumulative=cumsum(ziff_p);
cumulative = [0 cumulative];
for j=1:n_mem
    for k=1:num_cat
        %n=1+fix(in_cat*rand(1));
        temp=rand(1);
        dudd=sign(temp-cumulative);
        n=find(diff(dudd)==-2);
        long_memory(j,n+in_cat*(k-1))=1;
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% the following lines make the name 'carlos' according to alpha_decode
% encoded in the last five categories of memory 10
long_memory(10,881:1000)=0;
long_memory(10,892)=1;
long_memory(10,903)=1;
long_memory(10,929)=1;
long_memory(10,951)=1;
long_memory(10,964)=1;
long_memory(10,987)=1;
% the following lines make 'jessica' in memory 5
long_memory(5,881:1000)=0;
long_memory(5,900)=1;
long_memory(5,901)=1;
long_memory(5,927)=1;
long_memory(5,947)=1;
long_memory(5,965)=1;
long_memory(5,981)=1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%% these lines calculate T from the given memories
T=zeros(N,N);

for k=1:N
   for kk=1:N
        for j=1:n_mem
            T(k,kk)=T(k,kk)+long_memory(j,k)*long_memory(j,kk);
        end
        if(k==kk);T(k,kk)=0;end
    end
end

T=sign(T);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

figure(20);imagesc(T);colormap gray;grid on  % displays T
xlabel('Tij  matrix')








        


