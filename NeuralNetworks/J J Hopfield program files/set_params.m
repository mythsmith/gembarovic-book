%set_params

rand('state',0) % sets start of random number generator
delt=0.0015;  % time step for Euler integrator
num_cat = 50; % the number of different categories
in_cat = 20;   % the number of properties in each category
n_mem = 225  % the number of memories stored