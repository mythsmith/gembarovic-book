%memory_convergence.m
% an euler intergration of the equations of motion for u from an initial
% condition defined by the number 'correct_guys' defining the number of
% (correct) properties in the initial state.  Other properties are given
% initial values of 0.
%interesting other parameters:  values of the two constants in the
%line defining 'inhibition', and a possible negative bias 'off' in the
%equation for u which will make the u(:)=0 state stable

kkk=0;
clear overlap;
clear target;
clear width;
clear n_guys;
figure(100);clf;pause(5)
    
for p=1:10  %loop over the first 10 nominal memories
    p
    clear u
    correct_guys=10 % the number of correct entries in the initial state
    u=long_memory(p,:);u(1+20*correct_guys:end)=0;% sets the initial state to agree with
    %memory p in the first correct_guys categories
    %u=0.5*randn(size(u));
    V=u;
    
%     overlap=long_memory*u';
%     figure(50);clf;plot(overlap(:,end),'r.-');grid on;ylim([0 6]);drawnow;
       
    for j=2:5000  %Euler integration loop
        inhibition =    -30 + 3.5*sum(V);% was 3.5
        if(inhibition<0);inhibition=0;end
        u = u +delt*(-u/100 + V*T - inhibition);% -3.5 cut out
        V=u;
        V(find(V<0))=0;
    end 
%       foverlap=long_memory*V';
%       figure(55);clf;plot(foverlap,'r.-');grid on;ylim([0 num_cat]);drawnow;
    VV=reshape(long_memory(p,:),in_cat,num_cat)';
    figure(p);clf;imagesc(-VV);colormap gray;pause(5)
    VVV=reshape(V,in_cat,num_cat)';
    over(p)=sum(long_memory(p,:).*V);
    if(p<11);
        figure(100);handl=subplot(2,5,p);ha=imagesc(-VVV,[-0.5 0]);colormap gray;
        set(handl,'XtickLabel','','YtickLabel','')
        if(p==6 | p==1);ylabel('category');end
        if(p<5);xlabel('property');end
        if(p==5|p==10);xlabel(alpha_decode(V));end
        if(p>5 & p < 10);xlabel('property');end
         %if(p==10);colorbar;end
    end   
    y=alpha_decode(V)
    drawnow
end  % end loop over memories

return






