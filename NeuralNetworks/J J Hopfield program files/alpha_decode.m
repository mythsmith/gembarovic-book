function word = alpha_decode(V);
% decodes 20 positions into letters, ordered approximately on the basis of 
% letter usage frequencies in English
    word = [];
    for j=1:6;
        y=V(20*(j-1)+880+1:20*j+880);
        [junk b]=max(y);
        b;
        if(junk>0.01);
            if(b)==1;letter =  'e';end
            if(b)==2;letter =  't';end
            if(b)==3;letter =  'a';end
            if(b)==4;letter =  'o';end
            if(b)==5;letter =  'i';end
            if(b)==6;letter =  'n';end
            if(b)==7;letter =  's';end
            if(b)==8;letter =  'h';end
            if(b)==9;letter =  'r';end
            if(b)==10;letter =  'd';end
            if(b)==11;letter =  'l';end
            if(b)==12;letter =  'c';end
            if(b)==13;letter =  'u';end
            if(b)==14;letter =  'm';end
            if(b)==15;letter =  'w';end
            if(b)==16;letter =  'f';end
            if(b)==17;letter =  'g';end
            if(b)==18;letter =  'y';end
            if(b)==19;letter =  'p';end
            if(b)==20;letter =  'j';end
        else
            letter='.';
        end
        word = [word letter];
    end