AAAreadme
The following are 7 Matlab  programs 
alpha_decode.m
generate_T.m
memory_convergence.m
script_memory.m
set_params.m
sudoku_paper.m
typical_fiendish.m

Put the files into a single folder

The file script_memory.m  generate typical memory figures in the paper.
script_memory.m takes about 3 minutes to run on a 2.4 gigahertz PC
Figure(100) shows the convergence results for retrieving 10 memories.
Figures(1-10) shows the 10 correct memories
Figure(20) is the Tij matrix used to store all 225 memories

sudoku_paper.m runs the dynamics of the Sudoku network on a 'fiendish' problem
taken from the Times book of puzzles.    The program displays the current 
activity state of the networkstate every ~4 seconds. 
There are two plots.  Figure(1) is the current activity state, and figure(2)
shows the sum of all the activites as a function of time.  This a number 
always increasing (except for the noise of the discrete time steps)
and bounded above by 81. The program terminates when this number is >89.999
Where the system is converging to is rather clear at ~1 hour.