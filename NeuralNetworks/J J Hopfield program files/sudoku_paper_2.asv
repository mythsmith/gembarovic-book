%sudoku_paper  % a single convergence of a puzzle read in by line 4
%clear all
format long e
close all
typical_fiendish_2 % loads the puzzle typical_fiendish_2 lahsia z appl. SUDOKU on my iPhone
starting_board=board;% sets the starting board for this puzzle
starting_V=V;

delt=0.00005 % this step size is not trivially small, and as a result there are 
    % often noisy slight decreases to sum(sum(V)) even though if the integration is
    % done entirely legitimately, this sum should always increase.
    % you can watch this in figure(2)
    % figure(2)has kinks in it at places where the integration step changes
    % size
    
    % reducing the step size to .000025 takes ~twice as long to converge,
    % but is somewhat nearer to the ideal 'never decrease'
    
shorter_delt=delt/4;
N_steps = 40000000; % the actual number of iterations necessary for this particular
        % puzzle is about 2960000.  The program terminates at this point as
        % the resulting total sum(sum(V)) is close enough to 81
bias = 1;% drives all neurons toward being equally on

for n=2:N_steps

        V = V + delt*bias;% euler integration step 
        V(V<0)=0; % just to be sure all V are positive
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % if the integration results in a constraint being violated,
        % then a step in the direction perpendicular to the constraint
        % plane is implemented in the following lines.  This is equivalent
        % to implementing an inhibitory circuit with very high gain
        % each constraint plane must be tested
    nall = 0;
    R=[];
    for k=1:9;
        for kk=1:9
            if sum(V(k,kk,:))>1;
                V(k,kk,:)=V(k,kk,:)-1.5*delt*bias;
                nall = nall+1;
                c = V(k,kk,:);
         
                b = [nall k kk V(k,kk,1) V(k,kk,2) V(k,kk,1) V(k,kk,2)];
                R = [R; b];
            end
            if sum(V(k,:,kk))>1;
                V(k,:,kk)=V(k,:,kk)-1.5*delt*bias;
                nall = nall+1;
                b=[nall k kk V(k,:,kk)];
                R=[R; b];
            end            
            if sum(V(:,k,kk))>1;
                V(:,k,kk)=V(:,k,kk)-1.5*delt*bias;
                nall = nall+1;
                b=[nall k kk  V(:,k,kk)];
                R=[R; b];
            end
            
        end
    end
    ff=1.0;
    for k=1:9
        if sum(sum(V(1:3,1:3,k)))>1
            V(1:3,1:3,k)=V(1:3,1:3,k)-ff*delt*bias;
        end
    end
     
    for k=1:9
        if sum(sum(V(1:3,4:6,k)))>1
            V(1:3,4:6,k)=V(1:3,4:6,k)-ff*delt*bias;
        end
    end
    
    for k=1:9
        if sum(sum(V(1:3,7:9,k)))>1
            V(1:3,7:9,k)=V(1:3,7:9,k)-ff*delt*bias;
        end
    end
    
    for k=1:9
        if sum(sum(V(4:6,1:3,k)))>1
            V(4:6,1:3,k)=V(4:6,1:3,k)-ff*delt*bias;
        end
    end
     
    for k=1:9
        if sum(sum(V(4:6,4:6,k)))>1
            V(4:6,4:6,k)=V(4:6,4:6,k)-ff*delt*bias;
        end
    end
    
    for k=1:9
        if sum(sum(V(1:3,7:9,k)))>1
            V(4:6,7:9,k)=V(4:6,7:9,k)-ff*delt*bias;
        end
    end
    
    for k=1:9
        if sum(sum(V(7:9,1:3,k)))>1
            V(7:9,1:3,k)=V(7:9,1:3,k)-ff*delt*bias;
        end
    end
     
    for k=1:9
        if sum(sum(V(7:9,4:6,k)))>1
            V(7:9,4:6,k)=V(7:9,4:6,k)-ff*delt*bias;
        end
    end
    
    for k=1:9
        if sum(sum(V(7:9,7:9,k)))>1
            V(7:9,7:9,k)=V(7:9,7:9,k)-ff*delt*bias;
        end
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    V(find(V<0))=0;%#ok<*FNDSB> % to make sure that inaccuracies do not result in negative values of V
    V(find(starting_V))=1;% to make sure that inaccuracies do not result in values of V>1

    if rem(n,2000)==0;% plots out interesting stuff occasionally
        n;
        figure(1);clf;
        for p=1:9;
            subplot(3,3,p);imagesc(V(:,:,p));colormap gray;axis square
        end
        %pause
        total_on = sum(V(:));
        figure(2);subplot(1,2,1);plot(n,total_on,'r.');hold on;grid on;ylim([80.9 81]);ylabel('sum V');xlabel('integration step')
        figure(2);subplot(1,2,2);plot(n,total_on,'r.');hold on;grid on;ylim([50 81]);ylabel('sum V');xlabel('integration step')
        if(total_on>80.98);delt=shorter_delt;end% shortens time step in late stages
        if(total_on>80.995);delt=shorter_delt/4;end% further shortens time step
        if(total_on>80.999);break;end%ends program when total_on is very near its liming value of 81.
        pause(1)
    end
end

board=zeros(9,9);% now develop the usual board display in terms of integers
[dummy, board]=max(V,[],3);%'board is now the actual solution found, except in the
                            % case of convergence to a non-solution
for i=1:9  % this loop does stuff for the case of a non-solution 
    for j=1:9
        if(dummy(i,j)<0.5);board(i,j)=0;end
    end
end
board  % displays the board, with some allowance made for what to do if
       % the solution is not a correct total convergence
load('solution','V')
